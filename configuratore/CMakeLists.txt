project(SputnikConf LANGUAGES C CXX)
cmake_minimum_required(VERSION 3.7)
set(CMAKE_CXX_STANDARD 11)
set(CXX_STANDARD_REQUIRED ON)

add_executable(SputnikConf SputnikConf.cpp)

set(LIBS ${LIBS} config++)
target_link_libraries(SputnikConf ${LIBS})
