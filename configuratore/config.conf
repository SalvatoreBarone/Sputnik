
cmake_file = "../CMakeLists.txt";

src: {
						
	core = (		"Core/BaseMM.cpp",
					"Core/SafeMemory.cpp",
					"Core/SHA256.cpp",
					"Core/SHA256Voter.cpp",
					"Core/SHA512.cpp",
					"Core/SHA512Voter.cpp",
					"Core/VoterThread.cpp");

	linux = (		"Linux/LinuxMM.cpp",
					"Linux/Cpp11VoterThread.cpp",
					"Linux/Logger.cpp",
					"Linux/TCPSockServer.cpp",
					"Linux/TCPSockClient.cpp",
					"Linux/TCPCommunicator.cpp",
					"Linux/UDPSockServer.cpp",
					"Linux/UDPSockClient.cpp",
					"Linux/UDPCommunicator.cpp");
					
	freertos = (	"FreeRTOS/FreeRTOSMM.cpp",
					"FreeRTOS/FreeRTOSConditionVariable.cpp",
					"FreeRTOS/FreeRTOSMutex.cpp",
					"FreeRTOS/FreeRTOSVoterThread.cpp");

	xenomai = (	"Xenomai/XenomaiMM.cpp",
				"Xenomai/XenomaiVoterThread.cpp",
				"Xenomai/XenomaiConditionVariable.cpp",
				"Xenomai/XenomaiMutex.cpp",
				"Xenomai/UdpRtCommunicator.cpp",
				"Xenomai/UdpRtSockClient.cpp",
				"Xenomai/UdpRtSockServer.cpp",
				"Xenomai/XenomaiLogger.cpp");
	
	xilz7k = (	"XilinxZynq7000/SPI.cpp",
				"XilinxZynq7000/DualSPI.cpp");
};

configurations: {

# 	configuration_name: {
#		description = "Descrizione della configurazione";
#		so=0;
#		target=0;
# 		c_compiler="/usr/bin/gcc";
# 		cxx_compiler="/usr/bin/g++";
# 		cxx_parameters="-O0 -Wall -Wextra -ggdb3 -D__DEBUG";
# 		additional_parameters=""
# 		include_dir = ("");
# 		additional_include = ("");
# 		source_needed = ( "core" );
# 		chunk_number=32;
# 		chunk_size=12;
# 		voter=0;
# 		voter_period=0;
# 		voter_priority=0;
#		voter_stack_size=4096;
# 		build_test = false;
# 		build_examples = false;
# 	};

	
	
	linux_async_pc: {
		description = "GNU/Linux su pc, con voter asincrono";
		so=0;
		target=0;
		c_compiler="/usr/bin/gcc";
		cxx_compiler="/usr/bin/g++";
		cxx_parameters="-O0 -Wall -Wextra -ggdb3";
		additional_parameters="-D__DEBUG";
		include_dir = ("Core", "Linux");
		additional_include = ( "" );
		source_needed = ( "core", "linux" );
		chunk_number=32;
		chunk_size=12;
		voter=0;
		voter_period=10;
		voter_priority=2;
		voter_stack_size=4096;
		build_test = true;
		build_examples = false;
	};
	
	linux_alap_pc: {
		description = "GNU/Linux su pc, con voter alap";
		so=0;
		target=0;
		c_compiler="/usr/bin/gcc";
		cxx_compiler="/usr/bin/g++";
		cxx_parameters="-O0 -Wall -Wextra -ggdb3";
		additional_parameters="-D__DEBUG";
		include_dir = ("Core", "Linux");
		additional_include = ("");
		source_needed = ("core", "linux");
		chunk_number=32;
		chunk_size=12;
		voter=1;
		voter_period=0;
		voter_priority=0;
		voter_stack_size=4096;
		build_test = true;
		build_examples = false;
	};
	
	linux_asap_pc: {
		description = "GNU/Linux su pc, con voter asap-aware";
		so=0;
		target=0;
		c_compiler="/usr/bin/gcc";
		cxx_compiler="/usr/bin/g++";
		cxx_parameters="-O0 -Wall -Wextra -ggdb3";
		additional_parameters="-D__DEBUG";
		include_dir = ("Core", "Linux");
		additional_include = ("");
		source_needed = ("core", "linux");
		chunk_number=32;
		chunk_size=12;
		voter=2;
		voter_period=0;
		voter_priority=0;
		voter_stack_size=4096;
		build_test = true;
		build_examples = false;
	};
	
	freertos_async_zynq: {
		description = "FreeRTOS su Xilinx Zynq 7000, con voter asincrono";
		so=1;
		target=1;
		c_compiler="/usr/bin/arm-none-eabi-gcc";
		cxx_compiler="/usr/bin/arm-none-eabi-g++";
		cxx_parameters="-O0 -Wall -Wextra -ggdb3 -fmessage-length=0 -mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard";
		additional_parameters="";
		include_dir = ("Core", "FreeRTOS", "XilinxZynq7000/BareMetal");
		additional_include = ("~/Vivado/Progetti/project_8/project_8.sdk/SputnikALAP_bsp/ps7_cortexa9_0/include/");
		source_needed = ( "core", "freertos", "xilz7k" );
		chunk_number=10;
		chunk_size=10;
		voter=0;
		voter_period=10;
		voter_priority=2;
		voter_stack_size=4096;
		build_test = false;
		build_examples = false;
	};
	
	freertos_alap_zynq: {
		description = "FreeRTOS su Xilinx Zynq 7000, con voter alap";
		so=1;
		target=1;
		c_compiler="/usr/bin/arm-none-eabi-gcc";
		cxx_compiler="/usr/bin/arm-none-eabi-g++";
		cxx_parameters="-O0 -Wall -Wextra -ggdb3 -fmessage-length=0 -mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard";
		additional_parameters="";
		include_dir = ("Core", "FreeRTOS", "XilinxZynq7000/BareMetal");
		additional_include = ("~/Vivado/Progetti/project_8/project_8.sdk/SputnikALAP_bsp/ps7_cortexa9_0/include/");
		source_needed = ( "core", "freertos", "xilz7k" );
		chunk_number=10;
		chunk_size=4;
		voter=1;
		voter_period=0;
		voter_priority=0;
		voter_stack_size=4096;
		build_test = false;
		build_examples = false;
	};
	
	freertos_asap_zynq: {
		description = "FreeRTOS su Xilinx Zynq 7000, con voter asap";
		so=1;
		target=1;
		c_compiler="/usr/bin/arm-none-eabi-gcc";
		cxx_compiler="/usr/bin/arm-none-eabi-g++";
		cxx_parameters="-O0 -Wall -Wextra -ggdb3 -fmessage-length=0 -mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard";
		additional_parameters="";
		include_dir = ("Core", "FreeRTOS", "XilinxZynq7000/BareMetal");
		additional_include = ("~/Vivado/Progetti/project_8/project_8.sdk/SputnikALAP_bsp/ps7_cortexa9_0/include/");
		source_needed = ( "core", "freertos", "xilz7k" );
		chunk_number=10;
		chunk_size=4;
		voter=2;
		voter_period=0;
		voter_priority=0;
		voter_stack_size=4096;
		build_test = false;
		build_examples = false;
	};
	
	xeno_async_zynq : {
		description = "Xenomai su Xilinx Zynq 7000, voter asincrono";
		so=3;
		target=1;
 		c_compiler="arm-linux-gnueabihf-gcc";
 		cxx_compiler="arm-linux-gnueabihf-g++";
 		cxx_parameters="-O0 -Wall -Wextra -ggdb3 -march=armv7-a -mfpu=vfp3 -mfloat-abi=hard -D_GNU_SOURCE -D_REENTRANT -fasynchronous-unwind-tables -D__COBALT__ -D__COBALT_WRAP__";
 		additional_parameters="-D__DEBUG"
 		include_dir = ("Core", "Xenomai", "XilinxZynq7000/LinuxBased");
 		additional_include = ("/usr/xenomai/include/cobalt", "/usr/xenomai/include");
 		source_needed = ( "core", "xenomai", "xilz7k" );
 		chunk_number=32;
 		chunk_size=12;
 		voter=0;
 		voter_period=50;
 		voter_priority=90;
		voter_stack_size=4096;
 		build_test = false;
 		build_examples = false;
 	};
	
	xeno_alap_zynq : {
		description = "Xenomai su Xilinx Zynq 7000, voter alap";
		so=3;
		target=1;
 		c_compiler="arm-linux-gnueabihf-gcc";
 		cxx_compiler="arm-linux-gnueabihf-g++";
 		cxx_parameters="-O0 -Wall -Wextra -ggdb3 -march=armv7-a -mfpu=vfp3 -mfloat-abi=hard -D_GNU_SOURCE -D_REENTRANT -fasynchronous-unwind-tables -D__COBALT__ -D__COBALT_WRAP__";
 		additional_parameters="-D__DEBUG"
 		include_dir = ("Core", "Xenomai", "XilinxZynq7000/LinuxBased");
 		additional_include = ("/usr/xenomai/include/cobalt", "/usr/xenomai/include");
 		source_needed = ( "core", "xenomai", "xilz7k" );
 		chunk_number=32;
 		chunk_size=12;
 		voter=1;
 		voter_period=0;
 		voter_priority=0;
		voter_stack_size=0;
 		build_test = false;
 		build_examples = false;
 	};

};

template = "project(Sputnik LANGUAGES C CXX)
cmake_minimum_required(VERSION 3.7)
enable_testing()

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/libs)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/libs)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

set(CMAKE_CXX_STANDARD 11)
set(CXX_STANDARD_REQUIRED ON)

set(CMAKE_C_COMPILER $c_compiler)
set(CMAKE_CXX_COMPILER $cxx_compiler)
set(CMAKE_CXX_FLAGS \"${CMAKE_CXX_FLAGS} $cargs $additional_cargs\")

include_directories($include_dir $additional_include)

set(SRC $source_needed)

add_library(Sputnik STATIC ${SRC})
$enable_testing
$build_examples
";
