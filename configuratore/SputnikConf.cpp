/**
 * @file SputnikConf.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include <libconfig.h++>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <unordered_map>

using namespace std;
using namespace libconfig;

Setting& chooseConfiguration(Setting&, Setting&);
void prepare (string&, const unordered_map<string, string>&) noexcept;
void buildCMakeFile(Setting&, Setting&, Setting&, string);

int main(int argc, char **argv) {
	if (argc != 2) {
		cout << "Specifica un file di configurazione" << endl;
		return -1;
	}
	
	Config cfg;
	
	try {
		cfg.readFile(argv[1]);
	} catch(const FileIOException &fioex) {
		cerr << "File I/O error" << endl;
		return(EXIT_FAILURE);
	}
	catch(const ParseException &pex) {
		cerr << "Parse error at " << pex.getFile() << ":" << pex.getLine() << " - " << pex.getError() << endl;
		return(EXIT_FAILURE);
	}

	try {
		Setting &root = cfg.getRoot();
		Setting &src = root["src"];
		Setting &configurations = root["configurations"];
		Setting &cmake_template = root["template"];
		Setting &cmake_file = root["cmake_file"];
		
		Setting& conf = chooseConfiguration(root, configurations);
		buildCMakeFile(src, conf, cmake_template, cmake_file.c_str());
		
	} catch (SettingTypeException& stex) {
		cerr << stex.getPath() << "\t" << stex.what() << endl;
		return(EXIT_FAILURE);
	} catch (SettingNotFoundException & snfex) {
		cerr << snfex.getPath() << "\t" << snfex.what() << endl;
		return(EXIT_FAILURE);
	}
	catch (SettingException& sex) {
		cerr << sex.what() << endl;
		return(EXIT_FAILURE);
	} 
	
	return 0;
}

Setting& chooseConfiguration(Setting& root, Setting& configurations) {
	vector<string> conf_list;
	cout << "Scegli una configurazione:" << endl;
	int n_conf = 0;
	for (SettingIterator it = configurations.begin(); it != configurations.end(); it++, n_conf++) {
		conf_list.push_back(it->getPath());
		cout <<n_conf << ".\t" << it->lookup("description").c_str() << endl;
	}
	int choosed = 0;
	do 
		cin >> choosed;
	while (choosed < 0 || choosed > n_conf);
	return root.lookup(conf_list[choosed].c_str());
}

void prepare (string& str, const unordered_map<string, string>& parameter) noexcept {
	for (unordered_map<string, string>::const_iterator it = parameter.begin(); it != parameter.end(); it++) {
		string::size_type pos;
		do {
			pos = str.find ( string ( "$" + it->first ));
			if (pos != string::npos) {
				str.erase ( pos, it -> first.size() + 1 );
				str.insert ( pos, it -> second );
			}
		} while (pos != string::npos );
	}
}

void buildCMakeFile(Setting& src, Setting& conf, Setting& cmake_template, string outfile) {
	string cmake_str = cmake_template.c_str();
	unsigned so = conf.lookup("so");
	unsigned target = conf.lookup("target");
	string c_compiler = conf.lookup("c_compiler");
	string cxx_compiler = conf.lookup("cxx_compiler");
	string cargs = conf.lookup("cxx_parameters");
	string additional_cargs = conf.lookup("additional_parameters");
	unsigned chunk_number = conf.lookup("chunk_number");
	unsigned chunk_size = conf.lookup("chunk_size");
	unsigned voter = conf.lookup("voter");
	unsigned voter_period = conf.lookup("voter_period");
	unsigned voter_priority = conf.lookup("voter_priority");
	unsigned stack_size = conf.lookup("voter_stack_size");
	bool enable_testing = conf.lookup("build_test");
	bool build_examples = conf.lookup("build_examples");
	
	stringstream include_dir;
	Setting& include_set = conf.lookup("include_dir");
	for (int i = 0; i < include_set.getLength(); i++)
		include_dir << endl << "${CMAKE_SOURCE_DIR}/src/" << include_set[i].c_str();
		
	stringstream add_include_dir;
	Setting& add_include_set = conf.lookup("additional_include");
	for (int i = 0; i < add_include_set.getLength(); i++)
		add_include_dir << endl << add_include_set[i].c_str();
	
	stringstream source_needed;
	Setting& sources = conf.lookup("source_needed");
	for (int i = 0; i < sources.getLength(); i++) {
		Setting& srcs = src.lookup(sources[i].c_str());
		for (int j = 0; j < srcs.getLength(); j++)
			source_needed << endl << "${CMAKE_SOURCE_DIR}/src/" << srcs[j].c_str();
	}
	
	additional_cargs += " -DCHUNK_NUMBER=" + to_string(chunk_number) + " -DCHUNK_SIZE_2POW=" + to_string(chunk_size);
	
	switch (so) {
		case 0: // GNU/Linux
			additional_cargs += " -DSPUTNIK_ON_LINUX ";
			break;
		case 1: // FreeRTOS
			additional_cargs += " -DSPUTNIK_ON_FREERTOS ";
			break;
		case 2: // RTAI
			additional_cargs += " -DSPUTNIK_ON_RTAI ";
			break;
		case 3: // Xenomai
			additional_cargs += " -DSPUTNIK_ON_XENOMAI ";
			break;
		default:
			return;
	}
	
	switch (target) {
		case 0:
			additional_cargs += "";
			break;
		case 1:
			additional_cargs += "-DTARGET_XILZ7000";
			break;
		default:
			return;
	}
	
	switch (voter) {
	case 0:
		additional_cargs += " -DASYNC_VOTER -DVOTER_THREAD_PERIOD=" + to_string(voter_period) + " -DVOTER_THREAD_PRIORITY=" + to_string(voter_priority) + " -DVOTER_THREAD_STACK_SIZE=" +  to_string(stack_size);
		break;
	case 1:
		additional_cargs += " -DALAP_VOTER";
		break;
	case 2:
		additional_cargs += " -DASAP_VOTER";
		break;
	default:
		return;
	}
	
	unordered_map<string, string> parameters = {
		{"c_compiler", c_compiler},
		{"cxx_compiler", cxx_compiler},
		{"cargs", cargs},
		{"additional_cargs", additional_cargs},
		{"include_dir", include_dir.str()},
		{"additional_include", add_include_dir.str()},
		{"source_needed", source_needed.str()},
		{"enable_testing", (enable_testing ? "add_subdirectory(tests)" : "")},
		{"build_examples", (build_examples ? "add_subdirectory(examples)" : "")}
	};
	
	prepare(cmake_str, parameters);
	
	ofstream stream(outfile, ios::out);
	stream << cmake_str << endl;
	stream.close();
}
