/**
 * @file Sputnik.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#ifndef __SPUTNIK_MAIN_HEADER__
#define __SPUTNIK_MAIN_HEADER__

#include "Core/BadAlloc.hpp"
#include "Core/AlreadyAssigned.hpp"
#include "Core/CommError.hpp"

#include "Core/SafeMemory.hpp"
#include "Core/BaseMM.hpp"
#include "Core/BasePointer.hpp"
#include "Core/Pointer.hpp"

#include "Core/Mutex.hpp"
#include "Core/ConditionVariable.hpp"
#include "Core/VoterThread.hpp"

#include "Core/Communicator.hpp"

#include "Core/Voter.hpp"
#include "Core/SHA.hpp"
#include "Core/SHA256.hpp"
#include "Core/SHA512.hpp"
#include "Core/SHA256Voter.hpp"
#include "Core/SHA512Voter.hpp"

#ifdef SPUTNIK_ON_FREERTOS
	#include "FreeRTOS/FreeRTOSMM.hpp"
	#include "FreeRTOS/FreeRTOSMutex.hpp"
	#include "FreeRTOS/FreeRTOSConditionVariable.hpp"
	#include "FreeRTOS/FreeRTOSVoterThread.hpp"
	#ifdef TARGET_XILZ7000
		#include "XilinxZynq7000/SPI.hpp"
		#include "XilinxZynq7000/DualSPI.hpp"
	#endif
	#define MemoryManager FreeRTOSMM
#endif

#ifdef SPUTNIK_ON_LINUX
	#include "Linux/LinuxMM.hpp"
	#include "Linux/Cpp11Mutex.hpp"
	#include "Linux/Cpp11ConditionVariable.hpp"
	#include "Linux/Cpp11VoterThread.hpp"
	#include "Linux/Logger.hpp"
	#include "Linux/TCPCommunicator.hpp"
	#include "Linux/UDPCommunicator.hpp"
	#define MemoryManager LinuxMM
#endif

#ifdef SPUTNIK_ON_XENOMAI
	#include "Xenomai/XenomaiMM.hpp"
	#include "Xenomai/XenomaiMutex.hpp"
	#include "Xenomai/XenomaiConditionVariable.hpp"
	#include "Xenomai/XenomaiVoterThread.hpp"
	#include "Xenomai/UdpRtCommunicator.hpp"
	#include "Xenomai/XenomaiLogger.hpp"
	#ifdef TARGET_XILZ7000
		#include "XilinxZynq7000/SPI.hpp"
		#include "XilinxZynq7000/DualSPI.hpp"
	#endif
	#define MemoryManager XenomaiMM
#endif


#endif
