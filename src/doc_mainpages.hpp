/**
@mainpage

@section intro Introduzione
Sputnik (compagno di viaggio, in Russo) è una libreria software che fornisce le funzionalità per
realizzare sistemi two-out-of-two. Un sistema two-out-of-two, 2oo2 da ora in avanti, è un sistema
costituito da due sottosistemi identici, che effettuano le stesse identiche operazioni, eseguendo lo
stesso identico software, sollecitati dallo stesso input, nello stesso istante (con ragionevole
approssimazione), con l'obbiettivo di rilevare possibili guasti.

Attraverso una classe che fa da memory manager, la libreria permette di definire un'area di memoria
per la quale tutte le variabili e gli oggetti definiti al suo interno subiranno operazioni di
confronto. In altre parole, le due repliche che costituiscono il sistema 2oo2 si scambiano
informazioni circa lo stato in cui si trovano le variabili e gli oggetti allocati nella porzione di
memoria "sicura", generando eccezioni nel caso in cui si evidenzino differenze. La libreria permette
di definire un handler che permetta di gestire tali eccezioni.

Allo stato attuale la libreria può essere usata su un comune PC su cui sia in esecuzione il sistema
operativo GNU/Linux. Sono disponibili diversi porting della libreria su altri sistemi operativo ed
altre architetture target: è disponibile un porting che usa il sistema operativo real-time FreeRTOS,
uno che usi il sistema operativo real-time Xenomai ed uno su mpsoc Xilinx Zynq 7000. 
Sono, comunque, in programma diversi altri porting, come, ad esempio, quello su sistema operativo
real-time RTAI. La libreria è scritta in modo da essere facilmente estendibile. 

@section brief Una veloce panoramica.
La libreria mette a disposizione diverse classi che fanno da memory manager, attraverso le quali è
possibile definire aree di memoria "sicure". Tutto ciò che viene allocato all'interno di quest'area
di memoria subisce il confronto con le omologhe variabili usate, però, dall'altra replica.
 
Il confronto può avvenire secondo tre diverse strategie, messe a disposizione dalla libreria:
- confronto asincrono: un task apposito si assume l'onere di effettuare il confronto su base
  periodica;
- as-late-as-possible (ALAP): il confronto viene ritardato il più possibile, fino al momento in cui
  non diventa assolutamente indispensabile; questo istante coincide, tipicamente, con l'istante in
  cui si effettua una lettura di un oggetto che sia stato precedentemente scritto;
- as-soon-as-possible-aware (ASAP-aware): il confronto deve essere scatenato manualmente da chi
  utilizzi la libreria, avviando il processo di voting.
  
La strategia di voting da usare può essere scelta a tempo di compilazione, e, tranne nel caso in cui
si scelga la strategia ASAP-aware, è totalmente trasparente all'utilizzatore della libreria.

@subsection memory-organizzation Organizzazione della memoria sicura.
La memoria sicura è organizzata in "chunk". Ogni chunk può essere visto come una "fetta" di memoria.
Non c'è nessuna correlazione tra dimensione delle pagine, dei segmenti, o altre astrazioni della
memoria, con il concetto di chunk usato dalla libreria. I chunk hanno tutti la stessa dimensione ed
il loro numero può variare. 
Il chunk costituisce l'unità base con cui viene effettuato il confronto dello stato delle repliche:
il confronto viene effettuato non sui singoli oggetti, ma sull'intero chunk che essi occupano, e,
per ragioni di efficienza, solo su quei chunk su cio sia avvenuta un'operazione di scrittura.

Esistono due sole limitazioni:
1. la dimensione di ogni chunk deve essere una potenza del 2 (ed 1024 byte, 2048, 4096, eccetera);
2. allo stato attuale è possibile usare al più 64 chunk; questa restrizione sarà rimossa in un
   futuro prossimo.
Giocando con questi due parametri è possibile coprire tutte le esigenze in termini di organizzazione
e quantitativo di memoria necessaria ad un'applicazione.


@subsection voting Il voter.
Oltre a poter decidere la strategia di confronto (da ora voting), è possibile stabilire anche la
metodologia di voting. Attualmente la libreria mette a disposizione due diverse metodologie:
comparazione degli hashsum SHA-256 oppure comparazione degli hashsum SHA-512.

In entrambi i casi, quando è necessario effettuare il voting di un chunk, viene calcolato l'hashsum
dello stesso, inviandolo all'altra replica. Nel caso gli hashsum differiscano, viene generata un'
eccezione. Le eccezioni sollevate durante le operazioni di voting vengono gestite da un henadler,
definito dal programmatore, richiamato automaticamente.


@subsection communicator Comunicazione tra le repliche.
Come è facile immaginare, è necessario "connettere" tra loro le repliche affinché possano effettuare
il voting. La libreria mette a disposizione un layer che implementa diversi meccanismi di
comunicazione. È pensato per essere facilmente esteso, aggiungendo supporto per bus e/o protocolli di
comunicazione in modo estremamente semplice.
Al momento la libreria offre la possibilità di mettere in comunicazione le repliche mediante:
- socket TCP o UDP su sistema GNU/Linux;
- socket UDP real-time su sistema GNU/Linux/Xenomai
- bus SPI su MPSOC Xilinx Zynq 7000, a prescindere dal particolare sistema operativo adottato.

L'uso del particolare meccanismo di comunicazione è totalmente trasparente a chi utilizza la
libreria: dopo aver stabilito quale meccanismo utilizzare, non sarà mai necessario richiamare le
funzioni di invio e ricezione di messaggi.
*/



/**
@page gettingStarted Getting started.
Il modo più semplice di spiegare come usare la libreria è usare un esempio. Si supponga di dover
garantire la coerenza, tra due repliche, di oggetti di classe "Classe", la cui definizione è
riportata di seguito.

@code
class Classe {
public:
	Classe(int m1 = 0, int m2 = 0) : membro1(m1), membro2(m2) {};
	void setMembro1(int m1)	 {membro1 = m1;}
	void setMembro2(int m2) {membro2 = m2;}
	int getMembro1() const {return membro1;}
	int getMembro2() const {return membro2;}
private:
	int membro1;
	int membro2;
};
@endcode

Un esempio di programma che ciascuna delle repliche potrebbe eseguire è il seguente:

@code
Sputnik::SafeMemory safeMemory;
...
...
...
Sputnik::UDPCommunicator communicator(localPort, remoteAddress, remotePort);
Sputnik::SHA256Voter voter(communicator);
Sputnik::MemoryManager manager(safeMemory, voter, voteMismatchHandler, handlerArgs);
Sputnik::Pointer<Classe> ptr = manager.reserve<Classe>();
ptr.write().setMembro1(3);
Classe oggetto = ptr.read();
@endcode

@note L'esempio si riferisce a Sputnik usata su un sistema GNU/Linux, e, anche se non è evidente,
la strategia di voting scelta è ALAP. Lo stesso identico esempio, ma con strategia asincrona, viene
riportato in fondo alla pagina.

Analizziamo il codice riga per riga:
1. Viene definito un oggetto Sputnik::SafeMemory, il quale costituisce la memoria "sicura" all'
   interno della quale allocare variabili ed oggetti da confrontare con la controparte.
2. Viene definito quale meccanismo di comunicazione usare. In questo esempio viene usato un oggetto 
   Sputnik::UDPCommunicator, che implementa l'interfaccia Sputnik::Communicator. Un oggetto 
   Sputnik::UDPCommunicator è diviso in due parti: una parte "server" che è in ascolto per ricevere
   messaggi provenienti dalla replica, ed una parte "client" per inviare messaggi alla replica. Si
   faccia riferimento alla documentazione della classe per ulteriori dettagli.
3. Viene definita la metodologia di voting. In questo caso viene usato un oggetto
   Sputnik::SHA256Voter, che usa l'hashsum SHA-256 per confrontare tra loro le due repliche. Il
   voter richiede un oggetto che implementi l'interfaccia Sputnik::Communicator, che gli offra le
   primitive di comunicazione.
4. Viene definito l'oggetto Sputnik::MemoryManager, per il quale è necessario indicare un oggetto
   che implementi l'interfaccia Sputnik::Voter e che gli offra le funzionalità di voting. 
   La classe MemoryManager, in realtà, non esiste: a seconda della particolare configurazione di
   sistema può essere, ad esempio, Sputnik::LinuxMM o Sputnik::FreeRTOSMM. Dopo aver configurato
   Sputnik affinché venga utilizzato per una particolare configurazione di sistema, la classe
   opportuna viene AUTOMATICAMENTE rinominata in MemoryManager, in modo da semplificare la vita a
   chi usi la libreria.
5. Attraverso l'uso della funzione MemoryManager::reserve() viene riservato spazio per un oggetto di
   tipo "Classe".
   La funzione restituisce un oggetto Sputnik::Pointer attraverso il quale è possibile interagire
   con l'oggetto allocato. Anche Sputnik::Pointer, in realtà, non esiste, e la sua reale identità
   dipende dalla strategia di voting che si è deciso di usare. Può trattarsi di un oggetto
   Sputnik::ALAPointer (voting alap), Sputnik::ASAPointer (voting asap) o Sputnik::ASYNCPointer
   (voting asincrono). L'uso dell'oggetto pointer è l'unico modo ammesso per interagire con l'
   oggetto allocato nella memoria sicura.
6. Attraverso l'uso dell'oggetto Sputnik::Pointer, viene effettuata una scrittura sull'oggetto,
   chiamando uno dei metodi della classe Classe. La scrittura può avvenire anche copiando interi
   oggetti all'interno della memoria sicura, sia usando la funzione Sputnik::Pointer::write(), sia
   usando l'operatore di dereferenziazione.
@code
ptr.write(Classe(3,5));
// oppure
ptr.write() = Classe(3,5);
// oppure
*ptr = Classe(3,5);
// oppure
(*ptr).setMembro1(3);
@endcode
7. Attraverso l'uso dell'oggetto Sputnik::Pointer, viene effettuata una lettura. È possibile usare i
   metodi della classe come mostrato nell'esempio che segue:
@code
int membro1 = ptr.read().getMembro1();
// oppure
int membro1 = (*ptr).getMembro1();
@endcode

@note
Mi sento di sconsigliare l'utilizzo dell'operatore di dereferenziazione. Ritengo meglio utilizzare i
metodi Sputnik::Pointer::read() e Sputnik::Pointer::write(), che rendono più evidenti quali
operazioni si stia effettuando su un oggetto allocato all'interno della memoria sicura.

@par Una nota sull'uso della libreria con classi ed oggetti
Anche se normalmente non sarebbe necessario, quando si intende usare Sputnik è bene definire
l'operatore di copia assegnazione (operator=) ed il costruttore di copia anche quando i membri di 
una classe sono esclusivamente statici. Solo definendo il costruttore di copia e l'operatore di
copia-assegnazione si è in grado di garantire che le operazioni sulla memoria siano prevedibili.

@par Una nota sull'uso della libreria in applicazioni multithread, senza RTOS.
Quando la libreria viene usata in applicazioni multithread, oppure quando viene scelta la strategia
di voting asincrono (che di fatti usa un approccio multithread), è bene osservare alcune regole che
permettono di usare la libreria in modo efficiente e sicuro. 
Come sempre, un esempio vale più di mille parole. Si supponga che si voglia usare la libreria in un'
applicazione che effettua calcoli matriciali usando un approccio multithread. Per effettuare un 
prodotto tra due matrici, ad esempio, viene eseguito un partizionamento delle stesse tra più thread,
per cercare di ridurre i tempi di calcolo. A causa della mancanza di sincronizzazione tra le
repliche potrebbero manifestarsi errori di voting dovuti al diverso interleaving dei thread
(purtroppo non è possibile stabilire quale debba essere l'interleaving dei thread, a meno che il
sistema non sia real-time). Per prevenire tale situazione è possibile scrivere i prodotti su una
matrice temporanea, non allocata in memoria sicura, per poi copiare il risultato nella memoria
sicura solo dopo aver completato il calcolo, come mostrato nel seguente esempio.
@code
// effettua un prodotto matriciale C=A*B
void executeProduct(Sputnik::MemoryManager& manager, const Matrix& A, const Matrix& B, Matrix& C) {
	Sputnik::Pointer<Matrix> A_ptr = manager.reserve<Matrix>();
	Sputnik::Pointer<Matrix> B_ptr = manager.reserve<Matrix>();
	Sputnik::Pointer<Matrix> C_ptr = manager.reserve<Matrix>();
	Matrix tmp; // tmp non si trova in memoria sicura
	// copia di A e B in memoria sicura
	A_ptr.write(A);
	B_ptr.write(B);
	// creazione dei thread che eseguiranno il calcolo
	// si noti che gli viene passato "tmp", non C_ptr
	std::thread comp1(comp_thread, std::cref(A_ptr), std::cref(B_ptr), std::ref(tmp), 0, N/5,);
	std::thread comp2(comp_thread, std::cref(A_ptr), std::cref(B_ptr), std::ref(tmp), N/5, 2*N/5,);
	std::thread comp3(comp_thread, std::cref(A_ptr), std::cref(B_ptr), std::ref(tmp), 2*N/5, 3*N/5,);
	std::thread comp4(comp_thread, std::cref(A_ptr), std::cref(B_ptr), std::ref(tmp), 3*N/5, 4*N/5,);
	std::thread comp5(comp_thread, std::cref(A_ptr), std::cref(B_ptr), std::ref(tmp), 4*N/5, N,);
	comp1.join();
	comp2.join();
	comp3.join();
	comp4.join();
	comp5.join();
	// dopo aver completato il calcolo, il risultato, contenuto in tmp, viene scritto in C_ptr
	C_ptr.write(tmp);
	// in questo modo il risultato viene copiato in C solo dopo che sia stato eseguito il voting
	C = C_ptr.read();
}
// la funzione che segue effettua materialmente i calcoli
// è la funzione eseguita dai thread creati in executeProduct
void comp_thread(	const Sputnik::Pointer<Matrix>& A,
				const Sputnik::Pointer<Matrix>& B,
				Matrix& C,
				int start,
				int end,
				std::string name) {
	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print(name);
	for (int i = start; i < end; i++)
		for (int j = 0; j < N; j++) {
			C.element[i][j] = 0;
			for (int k = 0; k < N; k++)
				C.element[i][j] += A.read().element[i][k]B.read().element[k][j];
	}
}
@endcode
Lo stesso approccio può essere usato per aumentare le performance: scrivendo "spesso" in memoria
sicura fa decadere le performance (in termini di velocità di calcolo) a causa dell'overhead
introdotto dalle operazioni di voting. Se, invece, si usa una memoria tampone, non allocata in
memoria sicura, per mantenere i risultati parziali di un calcolo, e si scrive in memoria sicura solo
dopo aver completato i calcoli, l'overhead dovuto al voting viene minimizzato. Questa astuzia, però,
può essere usata solo e soltanto se i risultati parziali del calcolo non rivestano importanza vitale.

@par Esempio con voting asincrono.
@code
Sputnik::SafeMemory safeMemory;
...
...
...
Sputnik::UDPCommunicator communicator(localPort, remoteAddress, remotePort);
Sputnik::SHA256Voter voter(communicator);
Sputnik::MemoryManager manager(safeMemory, voter, voteMismatchHandler, handlerArgs);
Sputnik::Pointer<Classe> ptr = manager.reserve<Classe>();
ptr.write().setMembro1(3);
Classe oggetto = ptr.read();
@endcode
Il costruttore della classe fittizia "MemoryManager" per voting asincrono differisce da quello visto
precedentemente per la necessità di specificare due ulteriori parametri: un handler per le eccezioni
di tipo Sputnik::VoteMismatch e gli argomenti da passare a suddetto handler.
L'handler deve essere definito da una funzione del tipo
@code
void function(void*)
@endcode
a cui è delegato il compito di gestire eventuali eccezioni generate durante le operazioni di voting.
Tale funzione viene automaticamente chiamata dalla libreria quando si manifesta un'eccezione di tipo
Sputnik::VoteMismatch.
*/


/**
@page compilation Come compilare e linkare la libreria.

Com'è facile immaginare, compilare Sputnik potrebbe essere un'operazione complessa, a causa dei
numerosi parametri che andrebbero specificati, quali:
- sistema operativo target;
- architettura target;
- compilatore da usare e relativi flag;
- organizzazione della memoria (numero di chunk e loro dimensione);
- strategia di voting (alap, asap o asincrona);
- se viene usato voting asincrono vanno specificati periodo e priorità del thread di voting (e, nel
  caso si usasse FreeRTOS, dimensione dello stack del task di voting).
  
Niente paura. Sputnik è corredato di un configuratore, il quale DEVE essere usato per creare il file
CMakeLists.txt da usare, poi, per la compilazione. Il configuratore va, ovviamente, compilato a 
parte per poter essere usato. Il codice sorgente dello stesso si trova all'interno della directory
"configuratore", nella radice del repository, assieme al file cmake che ne consente la compilazione.

Per compilarlo, da terminale, dopo essersi spostati nella directory "configuratore", si esegua
@code
cmake .
make
@endcode

Il configuratore, SputnikConf, legge un file di configurazione (fornito a corredo, con alcune
configurazioni) e permette di scegliere quale usare. Il file di configurazione da usare va passato
come parametro al programma configuratore.
@code
./SputnikConf config.conf
@endcode
Al termine, SputnikConf creerà un file CMakeLists.txt nella radice del repository, usando la
configurazione scelta ed i parametri ad essa associati. Per compilare Sputnik, sarà sufficiente
spostarsi nella directory radice del repository ed eseguire
@code
cmake .
make
@endcode
La libreria Sputnik.a, prodotta al termine della compilazione, verrà posta nella directory "libs",
all'interno della directory radice del repository. Se la configurazione prevede che vengano
compilati anche i test, i binari relativi saranno posti nella directory "bin", sempre all'interno
della directory radice del repository.

@section configurations Creazione e modifica di una configurazione.
Il file di configurazione "config.conf" a corredo del configuratore è diviso in due parti:
1. specifica dei sorgenti della libreria (srcs);
2. specifica delle configurazioni (configurations).

La specifica dei sorgenti non va assolutamente modificata, per nessuna ragione, a meno che non sia
necessario aggiungere file sorgenti per il porting della libreria su qualche altro sistema operativo
o architettura target.

Per specificare una nuova configurazione, all'interno della voce "condifuration", è disponibile un
template che è possibile utilizzare, il cui testo è riportato di seguito:

@code
configuration_name: {
	description = "Descrizione della configurazione";
	so=0;
	target=0;
	c_compiler="/usr/bin/gcc";
	cxx_compiler="/usr/bin/g++";
	cxx_parameters="-O0 -Wall -Wextra -ggdb3 -D__DEBUG";
	additional_parameters=""
	include_dir = ("");
	additional_include = ("");
	source_needed = ( "core" );
	chunk_number="32";
	chunk_size="12";
	voter=0;
	voter_period=0;
	voter_priority=0;
	voter_stack_size=4096;
	build_test = false;
	build_examples = false;
}
@endcode

Nel dettaglio:
- configuration_name: deve essere un nome univoco che identifichi la particolare configurazione;
- description: è una descrizione informale della configurazione. Tale descrizione sarà usata da
  SputnikConf per costruire il menù attraverso il quale viene permesso all'utente di scegliere la
  configurazione di sistema da adottare;
- so: sistema operativo su cui si intende usare Sputnik; può essere:
	- 0 : GNU/Linux
	- 1 : FreeRTOS
	- 2 : RTAI
	- 3 : Xenomai
- target : "architettura" target; può essere
	- 0 : normale PC Intel/AMD based
	- 1 : Xilinx Zynq 7000 MPSOC
- c_compiler: percorso assoluto del compilatore C da usare per la compilazione della libreria;
- cxx_compiler: percorso assoluto del compilatore C++ da usare per la compilazione della libreria;
- cxx_parameters: parametri "di base" da passare al compilatore;
- additional_parameters: ulteriori parametri (ad esempio quelli dipendenti da una determinata
  architettura target) da dover passare al compilatore;
- include_dir: sottodirectory dell'albero dei sorgenti che è necessario tenere in considerazione
  durante il processo di compilazione; Possono essere usate:
	- Core: va sempre inclusa, contiene tutto cià che è indipendente da sistema operativo o target;
	- Linux: per il porting su GNU/Linux
	- FreeRTOS: per il porting su FreeRTOS;
	- Xenomai: per il porting su Xenomai;
	- XilinxZynq7000: per il porting su Xilinx Zynq 7000;
- additional_include: percorso di eventuali altri header file da tenere in considerazione durante il
  processo di compilazione;
- source_needed: sottoparti della libreria Sputnik da compilare (una o più sottovoci della voce
  "srcs" presente in testa al file di configurazione);
- chunk_number: numero di chunk (vedi organizzazione della memoria)
- chunk_size: dimensione di un chunk, in potenza del due (12 => 2^12 = 4096 byte), vedi
  organizzazione della memoria);
- voter: strategia di voting sa utilizzare:
	- 0: voting asincrono;
	- 1: voting alap;
	- 2: voting asap;
- voter_period: periodo del task di voting (solo per voter asincrono), in ms;
- voter_priority: priorità del task di voting (solo per voter asincrono);
- voter_stack_size: dimensione dello stack per il thread di voting, in byte;
- build_test: se vero, verranno compilati e linkati i test;
- build_examples: se vero, verranno compilati e linkati gli esempi a corredo;

  
@warning N.B. Alcuni porting (RTAI, STM32) NON sono implementati in questo momento.
*/

/**
@page contribuire Come contribuire

*/

/**
@page freertos_zybo FreeRTOS su Zybo

Questa pagina riporta una guida passo passo per la creazione di un progetto hardware su Xilinx
Vivado, per mostrare come sia possibile far funzionare Sputnik con FreeRTOS, usando come target una
board Digilent Zybo, sulla quale è disponibile un MPSOC Xilinx Zynq 7010.

@section part1 Progetto hardware
1. Creare un nuovo progetto Vivado, scegliando Digilent Zybo come board target (se la board non è
   disponibile, allora è necessario installare i board-files dal sito Digilent Inc.);
2. Creare un nuovo "block design", aggiungendo l'ip-core "processing system 7" e tre device axi gpio
   da usare per interfacciarsi con led, button e switch presenti on-board; sebbene non strettamente
   necessari, possono tornare molto utili in fase di debug; 
3. eseguire "block automation" e "connection automation";
4. Aprire il pannello di configurazione dell'ip-core "processing system 7" (doppio click sul blocco)
	- alla voce "interrupt", si spunti "Fabric interrupt";
	- alla voce "mio configuration" è possibile abilitare le diverse periferiche di cui dispone lo
	  Zynq 7000 on-board; affinché sia possibile usare Sputnik è necessario abilitare entrambi i
	  controller SPI presenti on board (SPI0 ed SPI1), e configurarli affinché usino l'interfaccia
	  EMIO, ed almeno uno dei controller UART (Uart1 è abilitato di default);
	- alla voce "clock configuration" è possibile scegliere quale debba essere il segnale del clock
	  di riferimento per i controller SPI; di default si tratta di un segnale generato da uno del
	  PLL interni al chip, con frequenza pari a circa 166MHz; si tenga presente che, usando EMIO, la
	  massima frequenza possibile per i segnali SPI sarà pari a 25MHz, motivo per cui sarà
	  necessario usare i prescaler interni ai controller;
5. Terminate le modifiche, si clicki su "Ok". Sarà possibile vedere le interfacce dei controller SPI
   aggiunte al blocco "processing system 7";
6. Si aggiunga una IP-core "constant", di lunghezza e valore unitari, e la si connetta al pin SS_I
   di uno dei due controller SPI. Tale controller dovrà essere usato, nel seguito, esclusivamente
   come master.
7. Si rendano "esterne" le interfacce dei controller SPI (make external, da menù contestuale);
8. Il design dovrebbe somigliare a quello riportato nell'immagine seguente
@image html block_design.png "" width=100%
@image latex block_design.png "" width=100%
9. Si validi, si salvi e si crei un wrapper HDL per il design, procedendo, poi, con una prima sintesi
10. Al termine della sintesi, si apra il design sintetizzato e si crei un nuovo file per i contraint
    (Constraint wizard).
11. Da "I/O Planning" sarà possibile associare a ciascun segnale di uscita un pin sulla board. Per
    alcuni dei segnali l'associazione viene effettuata da Vivado in modo audomatico (DDR, gpio, ecc)
    per cui gli unici segnali che dovremmo associare saranno quelli in ingresso/uscita dai controller
    SPI; in questo caso verranno usate le seguenti associazioni, pensate per minimizzare il fenomeno
    del crosstalking:
	- SPI_0_0_io0_io (MISO) -> T20 (JB1)
	- SPI_0_0_io1_io (MOSI) -> V20 (JB3)
	- SPI_0_0_sck_io (SCKL) -> Y18 (JB7)
	- SPI_0_0_ss1_o (SS1n) -> W18 (JB9), in questa demo verrà usato come slave select per selezionare
	  il device slave della replica.
	- SPI_0_0_ss2_o (SS2n) -> U20 (JB2), non usato
	- SPI_0_0_ss_o (SSn) -> W20 (JB4), non usato
	- SPI_0_0_ss_t (SSnT) -> Y19 (JB8), non usato
	- SPI_1_0_io0_io (MISO) -> V15 (JC1)
	- SPI_1_0_io1_io (MOSI) -> T11 (JC3)
	- SPI_1_0_sck_io (SCKL) -> W14 (JC7)
	- SPI_1_0_ss1_o (SS1n) -> W15 (JC2), non usato
	- SPI_0_0_ss2_o (SS2n) -> T10 (JC4), non usato
	- SPI_0_0_ss_o (SSn) -> T12 (JC9)
12. Sui segnali di cui sopra vanno impostate le seguenti:
	- I/O std. LVCMOS33
	- pull type: PULL DOWN
	- slew type: FAST (non è obbligatorio)
	- drive strength: 16 (non è obbligatorio)
13. Completate le modifiche, si salvi il file dei constraint.
14. Si proceda al reset del design sintetizzato (Open block design -> Source tab -> design_1.bd ->
    reset output product) ed alla ri-generazione degli stessi (design_1.bd -> generate output
    product, facendo attenzione a spuntare la voce "global" sotto "synthesis option")
15. Si proceda con una nuova sintesi, implementazione e generazione del bitstream.
16. Al termine della generazione del bitstream si esporti l'hardware, includendo il bitstream, e si
    lanci SDK.

@section part2 Creazione del progetto SDK
Per poter compilare Sputnik è necessario disporre degli header di FreeRTOS per Zynq. Per questo
motivo, prima di configurare e compilare Sputnik, è necessario creare un progetto FreeRTOS in SDK.

1. Dal menù File, si scelga New, quindi Application Project;
2. Si assegni un nome al progetto;
3. Da "OS platform" si scelga "freertos";
4. Alla voce Target software, si scelga C++ come linguaggio;
5. Si scelga il core che si intende usare (FreeRTOS è concepito per essere usato su un solo core);
6. Si clicki su Next;
7. Si scelga il template "Empty Application".

@section part3 Configurazione e compilazione di Sputnik
Una volta predisposto il progetto FreeRTOS su SDK, si dispone di tutto il necessario per configurare
e compilare Sputnik.

È possibile creare una nuova configurazione all'interno del file di configurazione di SputnikConf
per la configurazione hardware che si intende usare, oppure modificare una delle configurazioni per
FreeRTOS già disponibili, specificando il percorso assoluto al quale si trovano gli header del
progetto SDK creato. Supponendo che il progetto hardware si chiami "nuovoProgetto" e che si trovi
al percorso
@code
~/Vivado/Progetti/nuovoProgetto/
@endcode
e supponendo che il progetto SDK sia chiamato "DemoSputnik", dunque i sorgenti necessari saranno al
percorso
@code
@endcode
motivo per cui sarà necessario modificare la voce "additional_include" come segue
@code
additional_include = ("~/Vivado/Progetti/nuovoProgetto/nuovoProgetto.sdk/DemoSputnik_bsp/ps7_cortexa9_0/include/");
@endcode
Terminate le modifiche, si salvi il file di configurazione e si usi SputnikConf per la generazione
del file CMakeLists.txt, procedendo, poi con la compilazione della libreria. Al termine della stessa,
il file Sputnik.a sarà posto nella directory "libs" all'interno della directory radice del repository
di Sputnik.

@section part4 Configurazione del progetto SDK
Dopo aver compilato la libreria, è possibile configurare il progetto SDK affinché si possa compilare
il progetto e linkare Sputnik.

Da "Project explorer", si scelga "Properties" dal menù contestuale che si apre cliccando sul progetto
SDK. Si scelga la voce "C/C++ Build", quindi "Setting"
1. Alla voce "Directories", sotto la voce "Arm v7 gcc compiler", si inserisca il percorso dove
   risiedono gli header della libreria Sputnik, ad esempio
@image html sdk_directories.png "" width=60%
@image latex sdk_directories.png "" width=60%
2. Alla voce "Simbols", sempre  sotto la voce "Arm v7 gcc compiler", si dovranno definire i
   parametri di sistema che caratterizzano la configurazione di Sputnik. È possibile copiarli dal
   file CMakeFileLists.txt generato con il configuratore. Un esempio, nel caso si usasse una
   configurazione che usi voting alap, è il seguente:
@image html sputnik_parameters.png "" width=60%
@image latex sputnik_parameters.png "" width=60%
   Il numero e l'entità dei parametri varia da configurazione a configurazione. Se, ad esempio,
   si usasse una configurazione che usi voting asincrono...
@image html sputnik_parameters_async.png "" width=60%
@image latex sputnik_parameters_async.png "" width=60%
3. Alla voce "Libraries" sotto la voce "Arm v7 gcc linker" va aggiunto il nome e il percorso di
   Sputnik, affinché possa essere linkata, così come mostrato di seguito
@image html linker.png "" width=60%
@image latex linker.png "" width=60%

@section part5 Esempio

Il seguente esempio mostra una applicazione "sense-and-compute" che usi FreeRTOS e Sputnik.

@code
#include "FreeRTOS.h"
#include "task.h"

#include "Sputnik.hpp"
#include "xparameters.h"

// Indirizzi base dei GPIO per led e btn
#define led_base 0x41200000
#define btn_base 0x41210000

// Safe memory non dovrebbe stare nello stack del main-thread.
// CHUNK_SIZE = 256 byte
// CHUNK_NUMBER = 32
// totale = 8192 byte
// C'è spazio per 2048 interi;
Sputnik::SafeMemory safeMemory;

#define N 6 	// 6 * 4 * 6 = 144 byte usati su 160 disponibili

typedef struct {
	int element[N];
} Vector;


static void compute(void*);
static void sense(void*);

void votingMismatchHandler(void*);

// Struttura dati usata per il passaggio dei parametri ai task di sense e computing
typedef struct {
	Sputnik::MemoryManager* manager;

	// dati allocati in memoria sicura
	Sputnik::Pointer<Vector>* Re_A;
	Sputnik::Pointer<Vector>* Re_B;
	Sputnik::Pointer<Vector>* Re_Res;
	Sputnik::Pointer<Vector>* Im_A;
	Sputnik::Pointer<Vector>* Im_B;
	Sputnik::Pointer<Vector>* Im_Res;

	// il mutex viene usato per impedire che "sense" modifichi i dati mentre "compute" sta eseguendo il calcolo.
	Sputnik::FreeRTOSMutex* mutex;
	// se non è stato eseguito il sensing, il thread "compute" va bloccato
	bool sensed;
	// la c.v. viene usata per bloccare "compute" fino a quando non viene effettuato il sensing
	Sputnik::FreeRTOSConditionVariable* cv;
} myTaskParameters_t;

int main() {
	Xil_Out32(led_base + 4, 0);
	
	try {
		Sputnik::Z7DualSpi communicator(
				Sputnik::CadenceSPI::SPI0,
				Sputnik::CadenceSPI::SPI1,
				Sputnik::CadenceSPI::slave1);

		Sputnik::SHA256Voter voter(communicator);
#ifdef ASYNC_VOTER
		Sputnik::MemoryManager manager(safeMemory, voter, votingMismatchHandler);
#else
		Sputnik::MemoryManager manager(safeMemory, voter);
#endif
		// Per come sono allocati i vettori, ogni scrittura su Re_Res sporca il chunk dove risiede anche Re_B,
		// bloccando il task in attesa del voting. La stessa situazione si ripresenta con Im_B e Im_Res.
		// N.B. È buona norma allocare i chunk su cui vengono effettuare operazioni di lettura in chunk
		// dove non ci siano dati da leggere.
		Sputnik::Pointer<Vector> Re_A = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> Re_B = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> Im_A = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> Im_B = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> Re_Res = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> Im_Res = manager.reserve<Vector>();

		// il mutex viene usato per impedire che "sense" modifichi i dati mentre "compute" sta eseguendo il calcolo.
		Sputnik::FreeRTOSMutex mutex;
		Sputnik::FreeRTOSConditionVariable cv;

		// inizializzazione dei parametri da passare ai task
		myTaskParameters_t par;

		par.manager = &manager;

		par.Re_A = &Re_A;
		par.Re_B = &Re_B;
		par.Re_Res = &Re_Res;
		par.Im_A = &Im_A;
		par.Im_B = &Im_B;
		par.Im_Res = &Im_Res;

		par.mutex = &mutex;
		par.sensed = false;
		par.cv = &cv;

		xTaskCreate(
			sense,
			"sense",
			(configMINIMAL_STACK_SIZE + 2048),
			(void*) &par,
			(tskIDLE_PRIORITY + 2),
			NULL);

		xTaskCreate(
			compute,
			"compute",
			(configMINIMAL_STACK_SIZE + 4096),
			(void*) &par,
			(tskIDLE_PRIORITY + 5),
			NULL);


		vTaskStartScheduler();

	} catch (Sputnik::CommError& e) {
		Xil_Out32(led_base, 0x1);
		exit(0);
	} catch (Sputnik::BadAlloc& e) {
		Xil_Out32(led_base, 0x3);
		exit(0);
	}

	for (;;);
	
	return 0;
}

void votingMismatchHandler(void*) {
	Xil_Out32(led_base, 0xf);
	while (Xil_In32(btn_base) != 0x1);
	Xil_Out32(led_base, 0x0);
}

// ***************************************************************************************************************************
// task di computing
// attende che avvenga il "sensing" da parte del thread sense
// effettua il prodotto tra due vettori di numeri complessi
// scrive il risultato
// resetta il flag sensed
static void compute(void* par) {
	TickType_t wakeUp;
	const TickType_t blockTime = pdMS_TO_TICKS(100);
	wakeUp = xTaskGetTickCount();

	myTaskParameters_t* myPar = (myTaskParameters_t*) par;

	for (;;) {
		vTaskDelayUntil(&wakeUp, blockTime);

		// attendo che avvenga il sensing
		myPar->mutex->lock();
		while (!myPar->sensed)
			myPar->cv->wait(*(myPar->mutex));

		// quando il sensing è avvenuto, ed il task compute risvegliato, la riacquisizione del mutex è già avvenuta
		// si può procedere al calcolo.
		// da qualche parte, il thread compute deve bloccarsi in attesa del voting!
		// il voting è asincrono e avviene con periodo 10 ms
		try {
			for (unsigned i = 0; i < N; i++) {
				myPar->Re_Res->write().element[i] 	= myPar->Re_A->read().element[i] * myPar->Re_B->read().element[i]
													- myPar->Im_A->read().element[i] * myPar->Im_B->read().element[i];

				myPar->Im_Res->write().element[i]	= myPar->Re_A->read().element[i] * myPar->Im_B->read().element[i]
													+ myPar->Im_A->read().element[i] * myPar->Im_B->read().element[i];
			}
		} catch (Sputnik::VoteMismatch& e) {
			Xil_Out32(led_base, 0xf);
			while (Xil_In32(btn_base) != 0x1);
			Xil_Out32(led_base, 0x0);
		}

		myPar->sensed = false;

		// unlock del mutex
		myPar->mutex->unlock();
	}
}

// ***************************************************************************************************************************
// task di sensing
// scrive sui dati di origine
// setta il flag sensed ed effettua una signal, in modo che il thread "compute" venga risvegliato ed esegua i calcoli
static void sense(void* par) {
	TickType_t wakeUp;
	const TickType_t blockTime = pdMS_TO_TICKS(100);
	wakeUp = xTaskGetTickCount();

	myTaskParameters_t* myPar = (myTaskParameters_t*) par;

	for (;;) {
		vTaskDelayUntil(&wakeUp, blockTime);

		// attendo che avvenga il sensing
		myPar->mutex->lock();

		for (unsigned i = 0; i < N; i++) {
			myPar->Re_A->write().element[i] = i*3;
			myPar->Re_B->write().element[i] = i*7;
			myPar->Im_A->write().element[i] = i*11;
			myPar->Im_A->write().element[i] = i*13;
		}
		
		myPar->sensed = true;
		myPar->cv->signal();
		// unlock del mutex
		myPar->mutex->unlock();
	}
}
@endcode
*/


/**
@page linux_zybo Compilazione GNU/Linux su Zynq, con patch RTOS

La compilazione del kernel GNU/Linux è il passo preliminare per l'installazione e configurazione di
un sistema operativo real-time quale RTAI o Xenomai. Entrambi sono patch del kernel GNU/Linux
mainline, allo scopo di rendere Linux un sistema hard real-time.

Come architettura target, nel seguito si farà riferimento alla board di sviluppo Digilent Zybo,
sulla quale è disponibile un MPSOC Zynq 7010, al cui interno sono presenti due core ARM Cortex A9.
Il presente how-to fa riferimento ad una distribuzione Debian 9.3. Sulla Zybo verrà approntato un
sistema Ubuntu-based.

A livello hardware si farà uso di un design minimale, creato con il block design in Vivado. 
@note Dal menù di configurazione del processing system, si abilitino le interruzioni per la PS.

Nel resto del tutoria si fa riferimento ad una working-directory, all'interno della quale verrà
posto tutto ciò che è necessario alla compilazione del kernel ed all'installazione del sistema su
Zybo. Mi sento di consigliare di usare la directory
@code
$HOME/linux_zybo
@endcode
All'interno della working directory, si crei una subdirectory “SDCard”, al cui interno verranno
posti tutti i file prodotti durante il processo di compilazione, man mano che essi verranno creati.
Tali file andranno copiati su SDCard a processo ultimato, affinché sia possibile effettuare il boot
del sistema da SDcard.

@section software_necessario Installazione del software necessario
Per procedere è necessario installare quanto segue:
- Xilinx Vivado (con SDK)
- u-boot-tools
- device tree compiler
- gparted
- git
- linaro
- libssl-dev
- screen
- dh-autoreconf

È possibile usare
@code
# apt-get install u-boot-tools libncurses-dev device-tree-compiler gparted git libssl-dev screen \
dh-autoreconf
@endcode

A partire dalla versione 2017.4 di Vivado, linaro è l'ambiente di cross-compilazione di default per
Linux, dunque dovrebbe già essere disponibile.

@section configurazione_terminale Configurazione del terminale
Affinché sia possibile cross-compilare il kernel per la Zybo su un normale pc è necessario
configurare l'ambiente di lavoro, in particolar modo il teminale che verrà usato durante il processo.
La configurazione prevede il setting di alcune variabili d'ambiente e vale solo per la finestra di
terminale nella quale essa viene effettuata, dunque va ripetuta qualora tale finestra venisse chiusa.
Si può creare un file di configurazione, come segue

@code
#!/bin/bash
VIVADO_PATH=$HOME/Vivado/Vivado
VIVADO_VERS=2017.4
SDK_PATH=$HOME/Vivado/SDK
SDK_VERS=2017.4

source $VIVADO_PATH/$VIVADO_VERS/settings64.sh
source $SDK_PATH/$SDK_VERS/settings64.sh
export ARCH=arm
export CROSS_COMPILE=arm-linux-gnueabihf-
export PATH=$HOME/linux_zybo/u-boot-xlnx/tools/:$PATH
@endcode

@section uboot Compilazione di uboot
È necessario scaricare il sorgente di U-Boot (bootloader) e compilarlo. Dalla working-directory
@code
git clone https://github.com/Xilinx/u-boot-xlnx.git
@endcode

Nell'installazione GNU/Linux non verrà usato il ramdisk, pertanto le opzioni che ad esso fanno
riferimento vanno modificate. È necessario modificare il file
@code
u-boot-xlnx/include/configs/zynq-common.h 
@endcode
facendo sì che la voce
@code
"sdboot=if mmcinfo; then " \
"run uenvboot; " \
"echo Copying Linux from SD to RAM... && " \
"load mmc 0 ${kernel_load_address} ${kernel_image} && " \
"load mmc 0 ${devicetree_load_address} ${devicetree_image} && " \
"load mmc 0 ${ramdisk_load_address} ${ramdisk_image} && " \
"bootm ${kernel_load_address} ${ramdisk_load_address} ${devicetree_load_address}; "
@endcode
diventi
@code
"sdboot=if mmcinfo; then " \
"run uenvboot; " \
"echo Copying Linux from SD to RAM... && " \
"load mmc 0 ${kernel_load_address} ${kernel_image} && " \
"load mmc 0 ${devicetree_load_address} ${devicetree_image} && " \
"bootm ${kernel_load_address} - ${devicetree_load_address}; "
@endcode
Dopo aver effettuato tale modifica, si può procedere con la compilazione di u-boot
@code
cd u-boot-xlnx
make CROSS-COMPILE=arm-linux-gnueabihf- zynq_zybo_config
make -j 2 CROSS-COMPILE=arm-linux-gnueabihf- u-boot.elf
cp u-boot.elf ../SDCard/
cd ..
@endcode

@section creazione_fsbl Creazione del First Stage Bootloader (FSBL)
Il first Stage Bootloader va creato usando SDK. Da "File", si scelga "New -> Application Progect",
si scelga il nome del progetto (es. fsbl).
@image html fsbl_project.png "" width=30%
@image latex fsbl_project.png "" width=30%
e, dopo aver cliccato su "Next", si scelga il template "Zynq FSBL"
@image html fsbl_template.png "" width=30%
@image latex fsbl_template.png "" width=30%

@section creazione_boot_image Creazione dell'immagine di boot
La creazione dell'immagine di boot va fatta da SDK. Dal pannello “Project Explorer” si selezioni il
progetto di FSBL, quindi, dalla barra dei menù, "Xilinx Tools" -> Create BOOT image".
Si imposti il percorso dove verranno posti i file prodotti dal progetto
@image html boot_image1.png "" width=70%
@image latex boot_image1.png "" width=70%
poi, cliccando su "Add", si aggiunga il file ELF ottenuto dopo aver compilato u-boot.
@image html boot_image2.png "" width=40%
@image latex boot_image2.png "" width=40%
ottenendo
@image html boot_image3.png "" width=70%
@image latex boot_image3.png "" width=70%
L'immagine di boot verrà generata cliccando su "Create Image".

@section creazione_dtb Creazione e compilazione del Device Tree Blob (DTB)
Il Device Tree Source descrive l'architettura hardware al sistema operativo, è scritto in json e deve
essere compilato per poter ottenete il Device Tree Blob (devicetree.dtb), il quale va posto nella
stessa partizione dove viene posto l'immagine del kernel compilata, affinché sia caricato al boot
in memoria per essere usato dal kernel.

Anche il DTB può essere generato da SDK. Dopo aver scaricato i DTS dal repository Xilinx.
@code
git clone https://github.com/Xilinx/device-tree-xlnx
@endcode
è necessario aggiungere tale repositry a quelli disponibili in SDK. Da "Xilinx Tool" si scelga la
voce "Repositories", e si aggiunga il repository, come indicato di seguito.
@image html dtb1.png "" width=40%
@image latex dtb1.png "" width=40%
Si crei, poi, un nuovo progetto, scegliendo, questa volta, "Board support package", anziché 
"Application project"
@image html dtb2.png "" width=40%
@image latex dtb2.png "" width=40%
Quindi si scelga la voce "device_tree" tra le opzioni disponibili in "Board Support Package OS",
cliccando, poi, "Finish", poi su "OK". Il device tree source è diviso su più file:
- system-top.dts: indica la configurazione di sistema ed i  parametri di boot per Linux
- pl.dtsi : descrizione della logica sintetizzata su PL;
- zynq-7000.dtsi: descrive i device disponibili attraverso PS

È necessario modificare i parametri di bool per il kernel, editando il file "system-top.dts", come
indicato di seguito.
@code
bootargs = "console=ttyPS0,115200 root=/dev/mmcblk0p2 rw earlyprintk rootfstype=ext4 rootwait devtmpfs.mount=1 earlycon";
@endcode
Il device-tree-source va compilato per ottenere il device-tree-blob: ci si sposti all'interno della
directory contenente il file system-top.dts e si impartisca
@code
dtc -I dts -O dtb -o devicetree.dtb system-top.dts
@endcode
Si copi devicetree.dtb nella directory “SDCard”.

@subsection working_usb Abilitare USB-host
Come descritto <a href="https://forums.xilinx.com/t5/Embedded-Linux/USB-Host-not-working-on-Digilent-Zybo/td-p/730993">
quì</a>, affinché sia possibile usare USB-Host è necessario apportare alcune modifiche al file dts.
In primis, nel file <i>zynq-7000.dtsi</i> è necessario aggiungere <i>dr_mode = "host";</i> alle
configurazioni per usb0.
@code
usb0: usb@e0002000 {
			dr_mode = "host";
			compatible = "xlnx,zynq-usb-2.20a", "chipidea,usb2";
			clocks = <&clkc 28>;
			interrupt-parent = <&intc>;
			interrupts = <0 21 4>;
			reg = <0xe0002000 0x1000>;
			phy_type = "ulpi";
		};
@endcode
Inoltre, nel file <i>system-top.dts</i>, è necessario aggiungere quanto segue
@code
...
/include/ "zynq-7000.dtsi"
/ {
	...
	usb_phy0: phy0 {
        compatible = "usb-nop-xceiv";
        #phy-cells = <0>;
    };
	
	...
	
};
&usb0 {
    usb-phy = <&usb_phy0>;
};
@endcode

@subsection working_spi Abilitare i controller SPI0 ed SPI1



@section scaricare_sorgenti Scaricare il sorgente del kernel.

@subsection xilinx_linux Sorgenti del kernel già patchati per Xilinx Zynq
È disponibile una versione del Kernel Linux Patchata per MPSOC Xilinx Zynq, i cui sorgenti sono
scaricabili con git.
@code
git clone https://github.com/Xilinx/linux-xlnx.git
@endcode
dopo essersi spostati nella directory contenente i sorgenti, si impartiscano i seguenti
@code
make xilinx_zynq_defconfig
make menuconfig
@endcode
Dopo aver terminato la configurazione con "menuconfig", si può procedere alla compilazione
@code
make
make UIMAGE_LOADADDR=0x8000 uImage
@endcode
Al termine del processo verrà creata l'immagine "arch/arm/boot/uImage", la quale va copiata all'
interno della directory SDCard.

@subsection xenomai_patch Applicare la patch Xenomai
Sebbene sia disponibile una versione del kernel già patchata per MPSOC Xilinx Zynq, è consigliato
usare la versione "vanilla" del kernel quando si ha intenzione di patchare lo stesso per usarlo con
Xenomai o RTAI.

@note Nel seguito si fa riferimento alla configurazione Xenomai Cobald (dual-kernel).

Per usare Xenomai è necessario scaricare una delle versioni del kernel per le quali esista una patch
I-Pipe disponibile. Le patch I-Pipe disponibili sono disponibili
<a href="https://xenomai.org/downloads/ipipe/v4.x/arm/">quì</a>.
In questo caso verrà usata la versione 4.9.51 del kernel GNU/Linux.
Tutto ciò che c'è da sapere sul processo di patching è reperibile
<a href="https://xenomai.org/installing-xenomai-3-x/#Preparing_the_Cobalt_kernel">quì</a> >

@note
Tutto ciò che segue, in questa sezione del tutorial, costituisce un passo alternativo a quello
descritto alla sezione @ref xilinx_linux. Tutto ciò che precede, e tutto ciò che segue, può
essere ripetuto nello stesso identico modo in cui precedentemente descritto.

Fortunatamente esiste già una versione del kernel patchata con I-Pipe, motivo per cui useremo quella
anziché effettuare la patch manualmente, il che consente di risparmiare diverso tempo.

@par Scaricare i sorgenti del kernel patchato e del kernel Xilinx
Si impartiscano, nella working-directory, i seguenti comandi.
@code
git clone git://git.xenomai.org/ipipe-arm.git
git clone https://github.com/Xilinx/linux-xlnx.git 
@endcode
@note
Scaricare l'intero repo del kernel Xilinx non è strettamente necessario: è necessario solo il file di
configurazione zynq_zybo_config che si trova al suo interno, alla directory arch/arm/configs.

@par Scaricare Xenomai 3.0
Si impartisca, nella working directory, il seguente comando
@code
git clone git://git.xenomai.org/xenomai-3.git
@endcode

@par Applicare la patch I-Pipe
Prima di applicare la patch, è necessario creare alcune directory, e, siccome sarà necessario
passare da una directory all'altra abbastanza agilmente, è bene esportare i percorsi dove sono
presenti i sorgenti del kernel, i sorgenti Xenomai e la directory dove verranno posti i file
patchati.
@code
mkdir -p xeno3_build
mkdir -p xeno3_zynq_stage
mkdir -p zynq_modules
mkdir -p SDCard

export UBOOT=$PWD/u-boot-xlnx
export XENO_HOME=$PWD/xenomai-3
export LINUX_HOME=$PWD/ipipe-arm
export XIL_LINUX=$PWD/linux-xlnx
export XENO_BUILD=$PWD/xeno3_build
export XENO_ZYNQ_STAGE=$PWD/xeno3_zynq_stage
export ZYNQ_MODULES=$PWD/zynq_modules
export SDCARD=$PWD/SDCard
@endcode

Per applicare la patch, dalla working-directory, si impartisca
@code
$XENO_HOME/scripts/prepare-kernel.sh --linux=$LINUX_HOME --arch=arm --verbose
@endcode

@par Configurare il kernel per MPSOC Zynq
Il file xilinx_zynq_defconfig non appartiene al kernel mainline. Per questo motivo, prima di poter
procedere alla configurazione è necessario scaricarlo e porlo all'interno della directory "config"
dei sorgenti del kernel. Il file è presente, però, all'interno del porting del kernel fornito da
Xilinx.
Dalla working-directory
@code
cp $XIL_LINUX/arch/arm/configs/xilinx_zynq_defconfig $LINUX_HOME/arch/arm/configs/
@endcode
Dopo aver eseguito lo script di preparazione ambiente, come descritto alla sezione
@ref configurazione_terminale, si può procedere con la configurazione e la compilazione.
@code
cd $LINUX_HOME
make xilinx_zynq_defconfig
make menuconfig
@endcode
Affinché sia possibile procedere alla compilazione senza problemi, è necessario operare alcune
modifiche:
- disabilitare il frequency scaling della cpu, da CPU Power Management -> CPU Frequency Scaling,
  premendo "n" l'opzione verrà disabilitata;
- disabilitare la Continuous Memory Allocation, da Kernel features, si prema "n" alla voce Continuous
  Memory Allocation
- disabilitare tutte le features per il kernek hacking, trannel le opzioni per il debugging (di
  default dovrebbero essere già tutte disabilitate).
.

@par Compilare il kernel ed i moduli
Dopo aver terminato le modifiche, si salvi la configurazione e si proceda con la compilazione.
@code
make
make UIMAGE_LOADADDR=0x8000 uImage modules
make modules_install INSTALL_MOD_PATH=$ZYNQ_MODULES
cp $LINUX_HOME/arch/arm/boot/uImage $SDCARD
@endcode
Al termine del processo verrà creata l'immagine "arch/arm/boot/uImage". Nella directory zynq_modules
verranno temporaneamente posti i moduli kernel che dovranno, poi, essere aggiunti al root-filesystem.

@par Compilazione di Xenomai
Dalla working directory ci si sposti alla directory dei sorgenti Xenomai, poi si esegua
@code
cd $XENO_HOME
git checkout tags/v3.0.6 -b xenomai_3.0.6
./scripts/bootstrap
@endcode
che prepara il sistema alla compilazione dei tool necessari alla compilazione di Xenomai.
È necessario creare una directory dove porre il risultato della compilazione. Nella working directory
si crei una cartella "xeno3_build", ci si sposti al suo interno e si esegua lo script di
configurazione di Xenomai.
<a href="https://xenomai.org/installing-xenomai-3-x/#Generic_configuration_options_both_cores">Quì</a> >
sono disponibili tutte le informazioni del caso.
@code
cd $XENO_BUILD
$XENO_HOME/configure CFLAGS="-march=armv7-a -mfpu=vfp3 -mfloat-abi=hard" LDFLAGS="-march=armv7-a" --build=x86_64-pc-linux-gnu --host=arm-none-linux-gnueabi --with-core=cobalt --enable-smp --enable-tls CC=arm-linux-gnueabihf-gcc LD=arm-linux-gnueabihf-ld
@endcode
A questo punto, nella directory xeno_build creata nella working directory, c'è tutto quello che serve
alla compilazione. Per procedere alla compilazione è possibile impartire il comando
@code
make DESTDIR=$XENO_ZYNQ_STAGE install
@endcode
In questo modo il prodotto della compilazione verrà posto nella directory xeno_zynq_stage all'
interno della working directory, in modo che, successivamente, aggiungere tali file al root
filesystem sarà più semplice.

@section sdcard Preparazione della SD-card per il boot
La SDCard utilizzata per il boot va formattata in modo preciso: sono necessarie tre partizioni:
- 4MB spazio non allocato
- 1GB FAT32
- restante spazio formattato in EXT4.
Le partizioni vengono etichettate, per ragioni di comodo come “boot” (FAT32) e “root” (EXT4).

@note Si noti la corrispondenza con i parametri di boot:
-root=/dev/mmcblk0p2 indica quale partizione contenga il root-filesystem
-rootfstype=ext4 indica la tipologia del filesystem

Sulla partizione “boot” (FAT32) vanno copiati
- BOOT.bin, generato al passo @ref creazione_boot_image
- devicetree.dts, generato al passo @ref creazione_dtb
- uImage, generato al passo @ref xilinx_linux o @ref xenomai_patch, presente nella cartella
  arch/arm/boot/ dei sorgenti del kernel.
.
Sulla partizione "root" (EXT4) va istanziato il filesystem di root. In questo caso viene usato 
Linaro, che può essere scaricato all'indirizzo 
@code
https://releases.linaro.org/debian/images/developer-armhf/latest/
@endcode

Dopo aver scompattato l'archivio tar, scaricato precedentemente, è possibile impartire
@code
#rsync -azv path_linaro/binary/ rootfs-mountpoin
@endcode
dove rootfs-mountpoin è il punto di mount della SDCard.

@subsection xenomai_sdcard Aggiunta della libreria Xenomai al root filesystem
Se si è scelto di patchare il kernel con con Xenomai, dopo aver completato gli step descritti
oprecedentemente, è necessario, inoltre, copiare la libreria Xenomai nel root filesystem
@code
sudo rsync -aAXv $LINARO_BINARY $ROOT_FS
sudo rsync -aAXv $XENO_ZYNQ_STAGE $ROOT_FS
sudo rsync -aAXv $ZYNQ_MODULES $ROOT_FS
@endcode
dove rootfs è il punto di mount della SDCard.
A questo punto si può procedere come descritto alla sezione @ref avvio.

@note
Potresti aver bisogno di scaricare e copiare, nella cartella rootfs/root/ i seguenti pacchetti, da
installare a sistema avviato:
- nano_2.9.5-1_armhf.deb, scaricabile a questo <a href="http://ftp.it.debian.org/debian/pool/main/n/nano/nano_2.9.5-1_armhf.deb">url</a>;
- libarchive13_3.2.2-2_armhf.deb, scaricabile a questo <a href="http://ftp.it.debian.org/debian/pool/main/liba/libarchive/libarchive13_3.2.2-2_armhf.deb">url</a>;
- libuv1_1.9.1-3_armhf.deb, scaricabile a questo <a href="http://ftp.lt.debian.org/debian/pool/main/libu/libuv1/libuv1_1.9.1-3_armhf.deb">url</a>;
- libjsoncpp1_1.7.4-3_armhf.deb, scaricabile a questo <a href="http://ftp.it.debian.org/debian/pool/main/libj/libjsoncpp/libjsoncpp1_1.7.4-3_armhf.deb">url</a>;
- pkg-config_0.29-4+b1_armhf.deb, scaricabile a questo <a href="http://ftp.it.debian.org/debian/pool/main/p/pkg-config/pkg-config_0.29-4+b1_armhf.deb">url</a>;
- cmake-data_3.7.2-1_all.deb, scaricabile a questo <a href="http://ftp.lt.debian.org/debian/pool/main/c/cmake/cmake-data_3.7.2-1_all.deb">url</a>;
- cmake_3.7.2-1_armhf.deb, scaricabile a questo <a href="http://ftp.it.debian.org/debian/pool/main/c/cmake/cmake_3.7.2-1_armhf.deb">url</a>;
- libconfig9_1.5-0.3_armhf.deb, scaricabile a questo <a href="http://ftp.it.debian.org/debian/pool/main/libc/libconfig/libconfig9_1.5-0.3_armhf.deb">url</a>;
- libconfig++9v5_1.5-0.3_armhf.deb, scaricabile a questo <a href="http://ftp.it.debian.org/debian/pool/main/libc/libconfig/libconfig++9v5_1.5-0.3_armhf.deb">url</a>;
- libconfig-dev_1.5-0.3_armhf.deb, scaricabile a questo <a href="http://ftp.it.debian.org/debian/pool/main/libc/libconfig/libconfig-dev_1.5-0.3_armhf.deb">url</a>;
- libconfig++-dev_1.5-0.3_armhf.deb, scaricabile a questo <a href="http://ftp.it.debian.org/debian/pool/main/libc/libconfig/libconfig++-dev_1.5-0.3_armhf.deb">url</a>;

@section avvio Avvio del sistema
È necessario spostare il jumper JP5 in posizione SD, come indicato nell'immagine seguente, per poter
eseguire il boot da microSD. Sarà possibile interagire con la board usando
@code
screen /dev/ttyUSB1 115200
@endcode
@image html zybo_sd_boot.png "" width=40%
@image latex zybo_sd_boot.png "" width=40%

@subsection calibration Calibrazione di Xenomai
Se si è scelto di installare il sistema Xenomai, esso va calibrato.

In Xenomai-3 è possibile usare l'utility "autotune" per calibrare il sistema. In poche parole, il
tool determina il "gravity value" corretto, ossia tempo minimo nel quale il sistema deve "consegnare"
un'interrupt ad un interrupt-handler Xenomai, ad un task kernel-space RTDM oppure ad un task Xenomai
user-space. La documentazione di "autotune" è disponibile <a href="">quì</a> >.

@warning La calibrazione va ripetuta ogni volta che si effettua il boot del sistema.

I gravity value che autotune calcola sono tre: uno per gli interrupt-handler Xenomai, uno per i task
kernel-space RTDM ed uno per i task Xenomai user-space. È possibile passare alcuni parametri al tool,
in modo che calcoli tutti i gravity value oppure solo alcuni di essi.
È necessario specificare un "sampling period": la documentazione suggerisce di partire con un valore
di 1000000, abbassandolo gradualmente (100000, 10000, ecc), e raccomanda di non usare valori troppo
piccoli, altrimenti si rischia il blocco del sistema. Il tool può essere lanciato come segue.
@code
/usr/xenomai/sbin/autotune --period=10000
@endcode
Il tool impiega circa 30 secondi per completare le operazioni.

@section scripts Script di automazione
Nella cartella Script, nella home del repository, è possibile reperire due script che automatizzano
il processo di compilazione del kernel Linux. È necessario impostare il valore delle variabili poste
in testa allo script correttamente, prima di poter procedere. Gli unici passi non automatizzati sono
quelli descritti alle sezioni @ref creazione_fsbl, @ref creazione_dtb e @ref creazione_boot_image.
Lo script usa la directory all'interno del quale viene lanciato come working directory.
*/


/**
@page xenomai_example Compilare la libreria su Xenomai
La compilazione di Sputnik su Xenomai necessita di diversi parametri e flag da passare al compilatore.
Tali parametri possono essere ottenuti eseguendo, sul sistema sul quale è stato cross-compilato
Xenomai
@code
working_directory/xeno3_zynq_stage/usr/xenomai/bin/xeno-config --posix --cc
working_directory/xeno3_zynq_stage/usr/xenomai/bin/xeno-config --posix --cflags
working_directory/xeno3_zynq_stage/usr/xenomai/bin/xeno-config --posix --ldflags --auto-init --mode-check --auto-init-solib
@endcode
oppure, sul sistema target
@code
/usr/xenomai/bin/xeno-config --posix --cc
/usr/xenomai/bin/xeno-config --posix --cflags
/usr/xenomai/bin/xeno-config --posix --ldflags --auto-init --mode-check --auto-init-solib
@endcode
per ottenere, rispettivamente, quale sia il compilatore da usare, quale siano i flag da passare al
compilatore e quali siano i flag da passare al linker. Se questi flag non vengono usati durante il
processo di compilazione e linking, anche se il processo andasse a buon fine, l'eseguibile prodotto
semplicemente non funzionerà come ci si aspetta.
Per ulteriori informazioni è possibile leggere
<a href="http://xenomai.org/building-applications-with-xenomai-3-x/">come compilare</a>
e la documentazione relativa allo script
<a href="http://www.xenomai.org/documentation/xenomai-3/html/man1/xeno-config/index.html">
xeno-config</a>.

Sputnik viene fornito con un configuratore, come discusso nella sezione @ref configurations, il
quele è già configurato affinché generi un file CMakeLists.txt per la compilazione di Sputnik su
Xenomai. L'uso dei comandi di cui sopra servirà comunque quando sarà necessario scrivere il file
CMakeLists.txt, od il Makefile, per compilare l'applicazione e linkarla alle librerie necessarie.

@section Compilare il programma di esempio.
Come per la compilazione della libreria, anche per la compilazione di un programma sono necessari
molti flag, da passare al compilatore ed al linker. Per questo motivo, assieme al programma di
esempio, viene fornito un file CMakeLists.txt che può essere riutilizzato per compilare altri
programmi con quasi nessuna modifica.

@note
Di seguito viene riportato un estratto del file CMakeLists.txt per la compilazione con voter alap.
@code
set(CMAKE_C_COMPILER arm-linux-gnueabihf-gcc)
set(CMAKE_CXX_COMPILER arm-linux-gnueabihf-g++)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -march=armv7-a -mfpu=vfp3 -mfloat-abi=hard -D_GNU_SOURCE -D_REENTRANT -fasynchronous-unwind-tables -D__COBALT__ -D__COBALT_WRAP__ -O0 -Wall -Wextra -ggdb3 -D__DEBUG -DCHUNK_NUMBER=32 -DCHUNK_SIZE_2POW=12 -DSPUTNIK_ON_XENOMAI -DTARGET_XILZ7000 -DALAP_VOTER")
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -Wl,--no-as-needed -Wl,@/usr/xenomai/lib/cobalt.wrappers -Wl,@/usr/xenomai/lib/modechk.wrappers  /usr/xenomai/lib/xenomai/bootstrap-pic.o  -L/usr/xenomai/lib -lcobalt -lmodechk -lpthread -lrt -march=armv7-a")

include_directories(
/root/Sputnik/src/
/usr/xenomai/include/cobalt
/usr/xenomai/include)

link_directories(/root/Sputnik/libs)
link_libraries(-lSputnik)
@endcode

@note
Di seguito viene riportato un estratto del file CMakeLists.txt per la compilazione con voter
asincrono.
@code
set(CMAKE_C_COMPILER arm-linux-gnueabihf-gcc)
set(CMAKE_CXX_COMPILER arm-linux-gnueabihf-g++)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -march=armv7-a -mfpu=vfp3 -mfloat-abi=hard -D_GNU_SOURCE -D_REENTRANT -fasynchronous-unwind-tables -D__COBALT__ -D__COBALT_WRAP__ -O0 -Wall -Wextra -ggdb3 -D__DEBUG -DCHUNK_NUMBER=32 -DCHUNK_SIZE_2POW=12 -DSPUTNIK_ON_XENOMAI -DTARGET_XILZ7000 -DASYNC_VOTER -DVOTER_THREAD_PERIOD=50 -DVOTER_THREAD_PRIORITY=80 -DVOTER_THREAD_STACK_SIZE=4096")
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -Wl,--no-as-needed -Wl,@/usr/xenomai/lib/cobalt.wrappers -Wl,@/usr/xenomai/lib/modechk.wrappers  /usr/xenomai/lib/xenomai/bootstrap-pic.o  -L/usr/xenomai/lib -lcobalt -lmodechk -lpthread -lrt -march=armv7-a")

include_directories(
/root/Sputnik/src/
/usr/xenomai/include/cobalt
/usr/xenomai/include)

link_directories(/root/Sputnik/libs)
link_libraries(-lSputnik)
@endcode


@warning Al lancio del programma potrebbe presentarsi un problema riguardo ad alcune shared-library
non trovate. Impartendo il comando seguente, dalla directory in cui si trova l'eseguibile, sarà
possibile individuare quali librerie manchino e creare un link simbolico in /usr/lib. Quasi
certamente, tutte le librerie mancanti si troveranno in /usr/xenomai/lib.
@code
ldd senseAndCompute
@endcode

*/

/**
@page name Perché questo nome.
Il nome Sputnik è un tributo a Sergej Pavlovic Korolev, un semisconosciuto ingegnere aerospaziale, padre
del programma spaziale Sovietico. Uno dei razzi da lui progettati, il razzo R-7 Semyorka, ha portato in 
orbita attorno alla Terra il primo satellite artificiale (lo Sputnik 1, il 4 ottobre 1957), il primo essere
vivente (la cagnetta Laika, il 3 novembre 1957), il primo uomo (Jurij Gagarin, 12 aprile 1961) e la prima
donna (Valentina Tereskova, il 16 giugno 1963).

Una delle evoluzioni del razzo R-7, il lanciatore Sojuz, è ancora in uso e, in questo momento, è l'unico
vettore certificato per il volo umano. Viene usato per spedire gli equipaggi sulla Stazione Spaziale
Internazionale e per il loro rientro sulla terra.

Il nome, la vita ed il lavoro di Korolev sono stati coperti da strettissimo segreto militare fino alla sua
morte, avvenuta prematuramente nel 1966. Dopo il lancio dello Sputnik 1, la commissione che assegna i premi
Nobel chiese alle autorità dell'Unione Sovietica chi fosse stata la mente dietro una simile impresa, senza
mai ricevere risposta.

Avrei potuto scegliere qualsiasi altro nome, come Mir (pace, in Russo). La scelta è caduta su Sputnik perché
credo sia divertente l'idea che in un sistema 2oo2 ciascuna delle repliche abbia un compagno di viaggio.

@image html sojuz.jpg "" width=90%
@image latex sojuz.jpg "" width=90%
*/
