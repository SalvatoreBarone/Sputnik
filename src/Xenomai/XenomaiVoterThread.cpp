/**
 * @file XenomaiVoterThread.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "XenomaiVoterThread.hpp"
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <sys/syscall.h>

#ifdef __DEBUG
#include "XenomaiLogger.hpp"
#include <string>
#include <iostream>
#include <iomanip>
#endif

void* voterThreadFunction(void* args) {
#ifdef __DEBUG
	Sputnik::XenomaiLogger& logger = Sputnik::XenomaiLogger::getLogger();
	logger.print("Voter Thread");
#endif
	
	// Configurazione delle modalita' con cui il task periodico verra' risvegliato
	// viene usato il segnale SIGALRM, spedito al thread a seconda del suo identificativo.
	struct sigevent sevent;
	sigset_t set;
	int signum = SIGALRM;
	sevent.sigev_notify = SIGEV_THREAD_ID;
	sevent.sigev_notify_thread_id = syscall(__NR_gettid);
	sevent.sigev_signo = signum;
	sigemptyset(&set);
	sigaddset(&set, signum);
	sigprocmask(SIG_BLOCK, &set, NULL);
	
	// Creazione del timer.
	// Viene creato un timer periodico, di periodo 1s e ritardo iniziale 1s per rendere periodico
	// il task.
	timer_t timer;	
	struct itimerspec new_value, old_value;
	// creazione del timer
	timer_create (CLOCK_MONOTONIC, &sevent, &timer);
	new_value.it_interval.tv_sec = 0;	// periodo (secondi)
	new_value.it_interval.tv_nsec = VOTER_THREAD_PERIOD * 1000000;	// periodo (nanosecondi)
	new_value.it_value.tv_sec = 1;		// ritardi di inizio conteggio (secondi)
	new_value.it_value.tv_nsec = 0;		// ritardo di inizio conteggio (nanosecondi)
	// set delle caratteristiche del timer
	timer_settime (timer, 0, &new_value, &old_value);
		
	Sputnik::XenomaiVoterThread::VoterThreadPar* voterThreadArgs = (Sputnik::XenomaiVoterThread::VoterThreadPar*) args;
	while (voterThreadArgs->execVoting) {
		// aspetto il prossimo periodo
		// viene effettuata una lettura sul timer periodico utilizzandone il descrittore
		// la lettura causa il blocco del thread chiamante fin quando il timer non genererà
		// l'interruzione, ad ogni periodo, capace di risvegliare il thread.		
		sigwait (&set, &signum);
#ifdef __DEBUG
		long unsigned ticks = timer_getoverrun (timer);
		if (ticks > 0)
			logger.print("voter deadline miss! Ticks=" + std::to_string(ticks));
#endif
		// esecuzione del voting
		voterThreadArgs->parentObject->vote();
		
		struct itimerspec expiration;
		timer_gettime(timer, &expiration);
#ifdef __DEBUG
		logger.print("expiration:" + std::to_string(expiration.it_value.tv_sec) +  " sec, " + std::to_string(expiration.it_value.tv_nsec) +  "nsec");
#endif		
	}
	
	return 0;
}


Sputnik::XenomaiVoterThread::XenomaiVoterThread(BaseMM& m, void(*h)(void*), void* args) : 
	Sputnik::VoterThread (m, h, args)
{
	voterThreadArgs.parentObject = (XenomaiVoterThread*) this;
	struct sched_param schedpar = {.sched_priority = VOTER_THREAD_PRIORITY};
	pthread_attr_init(&attr);
	pthread_attr_setstacksize(&attr, VOTER_THREAD_STACK_SIZE);
	pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
	pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	pthread_attr_setschedparam(&attr, &schedpar);
}

Sputnik::XenomaiVoterThread::~XenomaiVoterThread() {
	voterThreadArgs.execVoting = false;
	pthread_attr_destroy(&attr);
}

void Sputnik::XenomaiVoterThread::run() {
	// creazione del thread di voting
	voterThreadArgs.execVoting = true;
	pthread_create(&thread, &attr, voterThreadFunction, (void*)&voterThreadArgs);
	pthread_setname_np(thread, "Sputnik Voter Thread");
}

void Sputnik::XenomaiVoterThread::stop() {
	voterThreadArgs.execVoting = false;
}
