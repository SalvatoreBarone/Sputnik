/**
 * @file XenomaiLogger.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "XenomaiLogger.hpp"
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <sys/syscall.h>
#include <chrono>

#define TERM_COLOR_DEFAULT	"\e[0m"
#define TERM_COLOR_RED		"\e[1;31m"
#define TERM_COLOR_GREEN	"\e[1;32m"
#define TERM_COLOR_YELLOW	"\e[1;33m"
#define TERM_COLOR_BLUE		"\e[1;34m"
#define TERM_COLOR_MAGENTA	"\e[1;35m"
#define TERM_COLOR_CYAN		"\e[1;36m"

Sputnik::XenomaiLogger *Sputnik::XenomaiLogger::instance = 0;
Sputnik::XenomaiMutex Sputnik::XenomaiLogger::instanceMutex;

Sputnik::XenomaiLogger& Sputnik::XenomaiLogger::getLogger() {
	instanceMutex.lock();
	if (instance == 0)
		instance = new XenomaiLogger();
	instanceMutex.unlock();
	return *instance;
}

void Sputnik::XenomaiLogger::print(const std::string& str) {
	struct timespec timestamp;
	clock_gettime(CLOCK_REALTIME, &timestamp);
	
	mtx.lock();
	rt_printf(TERM_COLOR_MAGENTA);
	rt_printf("%lu+%lu", timestamp.tv_sec, timestamp.tv_nsec);
	rt_printf(TERM_COLOR_DEFAULT);
	rt_printf(",");
	rt_printf(TERM_COLOR_GREEN);
	rt_printf("%d", syscall(SYS_gettid));
	rt_printf(TERM_COLOR_DEFAULT);
	rt_printf(",");
	rt_printf("%s\n", str.c_str());
	mtx.unlock();
}

void Sputnik::XenomaiLogger::print(const std::string& thread_name, const std::string& str) {
	struct timespec timestamp;
	clock_gettime(CLOCK_REALTIME, &timestamp);
	
	mtx.lock();
	rt_printf(TERM_COLOR_MAGENTA);
	rt_printf("%lu+%lu", timestamp.tv_sec, timestamp.tv_nsec);
	rt_printf(TERM_COLOR_DEFAULT);
	rt_printf(",");
	rt_printf(TERM_COLOR_GREEN);
	rt_printf("%d", syscall(SYS_gettid));
	rt_printf(TERM_COLOR_DEFAULT);
	rt_printf(",");
	rt_printf(TERM_COLOR_BLUE);
	rt_printf("%s", thread_name.c_str());
	rt_printf(TERM_COLOR_DEFAULT);
	rt_printf(",");
	rt_printf("%s\n", str.c_str());
	mtx.unlock();
}
