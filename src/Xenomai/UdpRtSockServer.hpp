/**
 * @file UdpRtSockServer.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __SPUTNIK_UDP_RT_SOCKSERVER_INTERFACE__
#define __SPUTNIK_UDP_RT_SOCKSERVER_INTERFACE__

#include "../Core/Communicator.hpp"

struct sockaddr_in;

namespace Sputnik {
	
/**
 * @ingroup xenomai
 * @{
 * @defgroup xenomai_communicator Comunicazione
 * @{
 * @brief Primitive di comunicazione per GNU/Linux con patch real-time Xenomai
 * 
 * @details
 * L'implementazione delle classi di questo modulo fa uso dei servizi di networking offerti da RTnet.
 * 
 * RTnet è uno stack protocollare che si interpone tra il livello Ethernet ed il livello IP. Mira,
 * attraverso l'uso di "time-slots" a rendere deterministica la comunicazione, disabilitando il
 * meccanismo di collision detection CSMA/CD caratteristico di Ethernet ed evitando che i pacchetti
 * possano subire buffering attraversando la rete.
 * RTnet è sviluppato per essere eseguito su un sistema GNU/Linux con patch RTAI o Xenomai, facendo
 * uso di funzioni real-time, anziché quelle standard del kernel GNU/Linux, in modo da definire
 * limiti per le latenze di comunicazione, rendendo deterministico il tempo di comunicazione.
 * 
 * Indipendentemente da RTnet, le applicazioni Xenomai possono richiedere il servizio di networking
 * al kernel Linux, perdendo la caratteristica di essere real-time. La POSIX-skin di Xenomai
 * permette di usare le funzioni come si farebbe in una normale applicazione Linux, ma, nel caso in
 * cui il servizio richiesto sia fruibile attraverso RTnet (come nel caso di UDP), ed il modulo 
 * RTnet è caricato correttamente, le richieste verranno gestite da quest'ultimo, conservando la
 * caratteristica real-time. Se, invece, il servizio richiesto non è fruibile attraverso RTnet, sarà
 * richiesto al kernel Linux standard, perdendo le garanzie rispetto i vincoli temporali.
 */

/**
 * @brief Server UDP Real Time
 * 
 * @details 
 * Un oggetto UdpRtSockServer costituisce un server UDP che permette la comunicazione con un client.
 * UDP.
 * 
 * @note L'oggetto è pensato per connessioni "uno ad uno", nel senso che il server è in grado di
 * servire un solo client.
 */
class UdpRtSockServer {
public:
/**
 * @brief Codici di errore per le eccezioni.
 */
	enum {
		socket_error,	/**< socket() restituisce un descrittore errato (-1) */
		bind_error		/**< bind() non restituisce 0 */
	};
/**
 * @brief Costruttore
 *
 * @details
 * Crea il server e mette in ascolto il thread per connessioni provenienti da un client.
 * 
 * @note L'oggetto è pensato per connessioni "uno ad uno", nel senso che il server è in grado di
 * servire un solo client.
 * 
 * @param [in] port porta UDP sul quale il server sarà in ascolto.
 *
 * @exception CommError se:
 * - socket() restituisce un descrittore errato (-1);
 * - bind() non restituisce 0;
 */
	explicit UdpRtSockServer(int port) throw (CommError);
	
	virtual ~UdpRtSockServer();
/**
 * @brief Invia un messaggio al client
 *
 * @param [in]  sendbuff	array di byte, contenente i dati da inviare;
 * @param [in]  size		dimensione, in byte, dei dati da ricevere/inviare.
 */		
	int sendMsg(const uint8_t* const sendbuff, size_t size);
/**
 * @brief Attende la ricezione di un messaggio dal client
 *
 * @param [out] recvbuff	buffer di ricezione; deve essere preallocato e deve avere dimensione
 * 							opportuna.
 * @param [in]  size		dimensione, in byte, dei dati da ricevere/inviare.
 * 
 * @note La funzione è bloccante.
 */
	int recvMsg(uint8_t* const recvbuff, size_t size);
/**
 * @brief Invio/ricezione di un messaggio.
 * 
 * @param [in]  sendbuff	array di byte, contenente i dati da inviare;
 * @param [out] recvbuff	buffer di ricezione; deve essere preallocato e deve avere dimensione
 * 							opportuna.
 * @param [in]  size		dimensione, in byte, dei dati da ricevere/inviare.
 * 
 * @note La funzione è bloccante, in modo da risultar essere un punto di rendez-vouz.
 */
    virtual void sendRecv(const uint8_t* const sendbuff, uint8_t* const recvbuff, size_t size);
    
protected:
	int localSocketDescriptor;
	struct sockaddr_in *si_other;
	unsigned slen;
};

/**
 * @}
 * @}
 */

};

#endif
