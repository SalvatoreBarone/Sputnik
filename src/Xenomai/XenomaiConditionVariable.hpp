/**
 * @file XenomaiConditionVariable.hpp
 * @author Salvatore Barone salvator.barone@gmail.com
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __SPUTNIK_XENOMAI_CV_INTERFACE__
#define __SPUTNIK_XENOMAI_CV_INTERFACE__

#include "../Core/ConditionVariable.hpp"
#include <pthread.h>

namespace Sputnik {

/**
 * @ingroup xenomai_oslayer
 * @{
 */
    
/**
 * @brief Classe wrapper per condition-variable in Xenomai-GNU/Linux.
 * 
 * @details
 * La classe Cpp11ConditionVariable viene usata per il wrapping delle condition variable per s.o.
 * Xenomai-GNU/Linux.
 * Implementa l'interfaccia Sputnik::ConditionVariable.
 * 
 * Questa implementazione fa uso della POSIX-skin di Xenomai, la quale prevede l'uso di oggetti
 * POSIX, successivamente sostituiti, all'atto della compilazione e linking, con oggetti analoghi,
 * definiti dalla libreria Xenomai.
 */
class XenomaiConditionVariable : public ConditionVariable {
public:
/**
 * @brief Costruttore
 * 
 * @details
 * Crea una nuova condition variable.
 */
    XenomaiConditionVariable();
    virtual ~XenomaiConditionVariable();
/**
 * @brief Sospensione per condizione attesa.
 * 
 * @param [in,out] m mutex acquisito per valutare la condizione da attendere.
 * 
 * @details
 * La semantica delle condition varible prevede che venga rilasciato il mutex usato per accedere al
 * valore della condizione che si attende.
 */
    virtual void wait(Mutex& m) noexcept;
/**
 * @brief Risveglio di uno dei thread in atteda di una condizione.
 */
    virtual void signal() noexcept;
/**
 * @brief Risveglio di tutti i thread in atteda di una condizione.
 */ 
    virtual void broadcast() noexcept;
	
public:
	pthread_condattr_t attr;
	pthread_cond_t cond;
};
/**
 * @}
 */
}


#endif
 
