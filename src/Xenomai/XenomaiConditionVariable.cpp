/**
 * @file XenomaiConditionVariable.cpp
 * @author Salvatore Barone salvator.barone@gmail.com
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "XenomaiConditionVariable.hpp"
#include "XenomaiMutex.hpp"

Sputnik::XenomaiConditionVariable::XenomaiConditionVariable() {
	pthread_condattr_init(&attr);
	pthread_condattr_setpshared(&attr, PTHREAD_PROCESS_PRIVATE);
	pthread_cond_init(&cond, &attr);
}

Sputnik::XenomaiConditionVariable::~XenomaiConditionVariable() {
	pthread_condattr_destroy(&attr);
	pthread_cond_destroy(&cond);
}

void Sputnik::XenomaiConditionVariable::wait(Mutex &m) noexcept {
	XenomaiMutex &xenomutex = dynamic_cast<XenomaiMutex&>(m);
	pthread_cond_wait(&cond, xenomutex.getNativeObject());
}

void Sputnik::XenomaiConditionVariable::signal() noexcept {
	pthread_cond_signal(&cond);
}

void Sputnik::XenomaiConditionVariable::broadcast() noexcept {
	pthread_cond_broadcast(&cond);
}
