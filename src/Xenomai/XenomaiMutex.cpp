/**
 * @file XenomaiMutex.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "XenomaiMutex.hpp"

Sputnik::XenomaiMutex::XenomaiMutex() {
	pthread_mutexattr_init(&attr);
	pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_NORMAL);
	pthread_mutexattr_setprotocol(&attr, PTHREAD_PRIO_INHERIT);
	pthread_mutexattr_setpshared(&attr, PTHREAD_PROCESS_PRIVATE);
	pthread_mutex_init(&mtx, &attr);
}

Sputnik::XenomaiMutex::~XenomaiMutex() {
	pthread_mutexattr_destroy(&attr);
	pthread_mutex_destroy(&mtx);
}

void Sputnik::XenomaiMutex::lock() noexcept {
	pthread_mutex_lock(&mtx);
}

void Sputnik::XenomaiMutex::unlock() noexcept {
	pthread_mutex_unlock(&mtx);
}
