/**
 * @file XenomaiMutex.hpp
 * @author Salvatore Barone salvator.barone@gmail.com
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __SPUTNIK_XENOMAI_MUTEX_INTERFACE__
#define __SPUTNIK_XENOMAI_MUTEX_INTERFACE__

#include "../Core/Mutex.hpp"
#include <pthread.h>

namespace Sputnik {
    
/**
 * @ingroup xenomai
 * @{
 * @defgroup xenomai_oslayer OS-layer
 * @{
 * @brief Primitive di sincronizzazione e threading per Xenomai GNU/Linux
 */



/**
 * @brief Classe wrapper per mutex in Xenomai GNU/Linux
 * 
 * @details
 * Implementa l'interfaccia Sputnik::Mutex.
 * 
 * Questa implementazione fa uso della POSIX-skin di Xenomai, la quale prevede l'uso di oggetti
 * POSIX, successivamente sostituiti, all'atto della compilazione e linking, con oggetti analoghi,
 * definiti dalla libreria Xenomai.
 * 
 * @note Questa classe di Mutex usa il protocollo di priority-inheritance.
 */
class XenomaiMutex : public Mutex {
public:
/**
 * @brief Costruttore
 * 
 * @details
 * Crea un nuovo mutex.
 */  
    XenomaiMutex();
    virtual ~XenomaiMutex();
/**
 * @brief Acquisizione del mutex.
 */    
    virtual void lock() noexcept;
/**
 * @brief Rilascio del semaforo.
 */    
    virtual void unlock() noexcept;
	
	pthread_mutex_t* getNativeObject() {return &mtx;}
	
private:
	pthread_mutexattr_t attr;
	pthread_mutex_t mtx;
};
/**
 * @}
 * @}
 */  
}


#endif
 
