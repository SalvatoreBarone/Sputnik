/**
 * @file XenomaiLogger.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __SPUTNIK_XENOMAI_LOGGER__
#define __SPUTNIK_XENOMAI_LOGGER__

#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>
#include "XenomaiMutex.hpp"

namespace Sputnik {

/**
 * @ingroup xenomai
 * @{
 * @defgroup xenomai_logger Logging
 * @{
 * @brief Primitive di logging Xenomai GNU/Linux
 */

class XenomaiLogger {
public:
	XenomaiLogger(const XenomaiLogger&) = delete;
	const XenomaiLogger& operator=(const XenomaiLogger&) const = delete;
	const XenomaiLogger& operator=(const XenomaiLogger&) = delete;

	static XenomaiLogger& getLogger();

	void print(const std::string&);
	void print(const std::string&, const std::string&);

private:
	static XenomaiLogger* instance;
	static XenomaiMutex instanceMutex;
	XenomaiMutex mtx;
	explicit XenomaiLogger() {}
};

/**
 * @}
 * @}
 */

};

#endif
