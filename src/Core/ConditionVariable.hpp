/**
 * @file ConditionVariable.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#ifndef __SPUTNIK_CV_INTERFACE__
#define __SPUTNIK_CV_INTERFACE__

#include "Mutex.hpp"

namespace Sputnik {
/**
 * @ingroup core_oslayer
 * @{
 */


/**
 * @brief Interfaccia delle classi condition-variable
 * 
 * @details
 * L'interfaccia definisce quali funzioni debba fornire una classe che implementi i meccanismi tipici delle
 * condition variable.
 */
class ConditionVariable {
public:
/**
 * @brief Sospensione per condizione attesa.
 * 
 * @details
 * La semantica delle condition varible prevede che venga rilasciato il mutex usato per accedere al valore
 * della condizione che si attende.
 */
    virtual void wait(Mutex &) noexcept = 0;
/**
 * @brief Risveglio di uno dei thread in atteda di una condizione.
 */
    virtual void signal() noexcept = 0;
/**
 * @brief Risveglio di tutti i thread in atteda di una condizione.
 */
    virtual void broadcast() noexcept = 0;
};
/**
 * @}
 */
}

#endif
