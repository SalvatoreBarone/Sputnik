/**
 * @file ASAPointer.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#ifndef __SPUTNIK_ASAPOINTER__
#define __SPUTNIK_ASAPOINTER__

#ifdef ASAP_VOTER

#include "BasePointer.hpp"

namespace Sputnik {
/**
 * @ingroup core
 * @{
 * @addtogroup core_mm
 * @{
 */


/**
 * @brief Classe pointer per strategia di voting as-soon-as-possible.
 * 
 * @details
 * La classe ASAPointer esiste solo se viene usata la stratedia di voting asap. In tal caso viene "rinominata"
 * semplicemente Pointer.
 * La classe definisce le metodologie di lettura di oggetti dalla memoria qualora la configurazione di sistema
 * preveda l'uso di voting as-soon-as-possible.
 */
template <class T> 
class ASAPointer : public BasePointer<T> {
/**
 * La classe BaseMM inizializza direttamente i membri privati di un oggetto BasePointer.
 * Il costruttore della classe BasePointer, così come quelli di tutte le altri classi puntatore, è privato,
 * per non permettere all'utente della libreria di creare oggetti puntatore.
 * L'unico modo per ottenere oggetti puntatore è usare la funzione Sputnik::BaseMM::reserve.
 */
	friend class BaseMM;
	using BasePointer<T>::parent;
	using BasePointer<T>::t_ref;
	using BasePointer<T>::chunkMask;
public:

/**
 * @brief Lettura di un oggetto dalla memoria.
 * 
 * @details
 * La funzione read() è quella preposta alla lettura di un oggetto dalla memoria. A seconda della
 * particolare configurazione di Sputnik può scatenare i meccanismi di voting o fermare l'esecuzione del
 * thread chiamante fino a quando i chunk interessati dall'operazione di lettura non siano stati votati.
 * 
 * Se viene usata la strategia di voting asap la lettura non scatena il meccanismo di voting. Quando si usa
 * la strategia di voting asap È NECESSARIO SCATENARE IL VOTING ESPLICITAMENTE, chiamando la funzione 
 * BaseMM::vote().
 */
	virtual const T& read() const {
		return t_ref;
	}
	
private:
/**
 * @brief Costrutore
 * 
 * @details
 * Il costruttore della classe è reso privato, in modo che l'unico modo per ottenere un oggetto puntatore sia
 * ricorrere ad una chiamata alla funzione BaseMM::reserve() function.
 * 
 * @param [in] p_ref 	riferimento all'oggetto BaseMM che gestisce la memoria su cui è allocato l'oggetto a cui
 * 						il puntatore si riferisce.
 * 
 * @param [in] T_ref	riferimento all'oggetto di tipo generico T, a cui il puntatore si riferisce.
 * 
 * @param [in] cs 		maschera, in codifica one-hot, che rappresenta quali chunk siano occupati dall'oggetto.
 */
	ASAPointer(BaseMM& p_ref, T& T_ref, uint64_t cs) : 
		BasePointer<T>(p_ref, T_ref, cs)
		{}
};

/**
 * @}
 * @}
 */
}

#endif
#endif
