/**
 * @file SHA256Voter.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __SPUTNIK_SHA256VOTER__
#define __SPUTNIK_SHA256VOTER__

#include "Voter.hpp"
#include "SHA256.hpp"

namespace Sputnik {
/**
 * @ingroup core_voter
 * @{
 */

/**
 * @brief Implementa il voting usando SHA-256 per la comparazione delle aree di memoria.
 */
class SHA256Voter : public Voter {
public :
/**
 * @brief Costruttore
 *
 * @param [in] c riferimento ad una classe che implementi l'interfaccia Sputnik::Communicator.
 */
	explicit SHA256Voter(Communicator& c) : Voter(c) {}
/**
 * @brief Voting;
 *
 * @param [in] index	indice del chunk votato
 * @param [in] data		riferimento all'area di memoria occupata dal chunk votato
 *  
 * @details
 * La funzione calcola l'hashsum SHA-256 dell'area di memoria occupata dal chunk votato e la invia al sistema
 * replicato usando una classe che che implementi l'interfaccia Sputnik::Communicator. Il sistema gemello,
 * ovviamente, farà lo stesso. Dopo che l'hashsum è stata ricevuta, viene effettuata la comparazione.
 *
 * @exception VoteMismatch se il voting evidenzia differenze tra le aree di memoria comparate.
 */
	virtual void vote(uint8_t, const uint8_t (&data)[CHUNK_SIZE]) const throw(VoteMismatch&);
};


/**
 * @}
 */
};

#endif
