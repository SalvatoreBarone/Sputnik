/**
 * @file SafeMemory.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#ifndef __SPUTNIK_SAFEMEMORY_HEADER__
#define __SPUTNIK_SAFEMEMORY_HEADER__

#include <inttypes.h>

#define CHUNK_SIZE (1<<CHUNK_SIZE_2POW)

namespace Sputnik {
/**
 * @ingroup core
 * @{
 * @addtogroup core_mm
 * @{
 */

class BaseMM;

/**
 * @brief Porzione di memoria gestita da Sputnik.
 * 
 * @details
 * Ogni oggetto SafeMemory può essere associato ad un solo oggetto di classe BaseMM (o ad una classe derivata)
 * nel momento in cui l'oggetto BaseMM viene costruito.
 */
class SafeMemory {
	friend class BaseMM;
public:
	SafeMemory();
	
private:
/**
 * Puntatore al memory manager a cui la memoria è stata assegnata
 */
	BaseMM	*assignedMM;
/**
 * Porzione di memoria gestita da un memory manager, così come definita dai parametri di sistema
 * CHUNK_NUMBER e CHUNK_SIZE_2POW
 */
	uint8_t byteArray[CHUNK_NUMBER * CHUNK_SIZE];
};

/**
 * @}
 * @}
 */

};

#endif
