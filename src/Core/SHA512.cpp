/**
 * @file SHA512.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "SHA512.hpp"

bool Sputnik::SHA512::Hashsum::operator == (const Hashsum& hs) const noexcept {
	for (int i = 0; i < 8; i++)
		if (byte_array[i] != hs.byte_array[i])
			return false;
	return true;
}

void Sputnik::SHA512::Hashsum::marshall(uint8_t (&byteArray)[64]) const noexcept {
	uint64_t *ptr = (uint64_t*)&byteArray;
	for (int i = 0; i < 8; i++)
		ptr[i] = byte_array[i];
}

void Sputnik::SHA512::Hashsum::unmarshall(const uint8_t (&byteArray)[64]) noexcept {
	uint64_t *ptr = (uint64_t*)&byteArray;
	for (int i = 0; i < 8; i++)
		byte_array[i] = ptr[i];
}

/**
 * @brief The 64 bits of the fractional part of the cubic root of the first 80 prime numbers.
 */
const uint64_t Sputnik::SHA512::Kt[80] = {
	0x428a2f98d728ae22,	0x7137449123ef65cd,	0xb5c0fbcfec4d3b2f,	0xe9b5dba58189dbbc,
	0x3956c25bf348b538,	0x59f111f1b605d019,	0x923f82a4af194f9b,	0xab1c5ed5da6d8118,
	0xd807aa98a3030242,	0x12835b0145706fbe,	0x243185be4ee4b28c,	0x550c7dc3d5ffb4e2,
	0x72be5d74f27b896f,	0x80deb1fe3b1696b1,	0x9bdc06a725c71235,	0xc19bf174cf692694,
	0xe49b69c19ef14ad2,	0xefbe4786384f25e3,	0x0fc19dc68b8cd5b5,	0x240ca1cc77ac9c65,
	0x2de92c6f592b0275,	0x4a7484aa6ea6e483,	0x5cb0a9dcbd41fbd4,	0x76f988da831153b5,
	0x983e5152ee66dfab,	0xa831c66d2db43210,	0xb00327c898fb213f,	0xbf597fc7beef0ee4,
	0xc6e00bf33da88fc2,	0xd5a79147930aa725,	0x06ca6351e003826f,	0x142929670a0e6e70,
	0x27b70a8546d22ffc,	0x2e1b21385c26c926,	0x4d2c6dfc5ac42aed,	0x53380d139d95b3df,
	0x650a73548baf63de,	0x766a0abb3c77b2a8,	0x81c2c92e47edaee6,	0x92722c851482353b,
	0xa2bfe8a14cf10364,	0xa81a664bbc423001,	0xc24b8b70d0f89791,	0xc76c51a30654be30,
	0xd192e819d6ef5218,	0xd69906245565a910,	0xf40e35855771202a,	0x106aa07032bbd1b8,
	0x19a4c116b8d2d0c8,	0x1e376c085141ab53,	0x2748774cdf8eeb99,	0x34b0bcb5e19b48a8,
	0x391c0cb3c5c95a63,	0x4ed8aa4ae3418acb,	0x5b9cca4f7763e373,	0x682e6ff3d6b2b8a3,
	0x748f82ee5defb2fc,	0x78a5636f43172f60,	0x84c87814a1f0ab72,	0x8cc702081a6439ec,
	0x90befffa23631e28,	0xa4506cebde82bde9,	0xbef9a3f7b2c67915,	0xc67178f2e372532b,
	0xca273eceea26619c,	0xd186b8c721c0c207,	0xeada7dd6cde0eb1e,	0xf57d4f7fee6ed178,
	0x06f067aa72176fba,	0x0a637dc5a2c898a6,	0x113f9804bef90dae,	0x1b710b35131c471b,
	0x28db77f523047d84,	0x32caab7b40c72493,	0x3c9ebe0a15c9bebc,	0x431d67c49c100d4c,
	0x4cc5d4becb3e42b6,	0x597f299cfc657e2a,	0x5fcb6fab3ad6faec,	0x6c44198c4a475817
};

const uint64_t Sputnik::SHA512::IV[8] = {
	0x6A09E667F3BCC908,	0xBB67AE8584CAA73B,	0x3C6EF372FE94F82B,	0xA54FF53A5F1D36F1,
	0x510E527FADE682D1,	0x9B05688C2B3E6C1F,	0x1F83D9ABFB41BD6B,	0x5BE0CD19137E2179
};

inline uint64_t Sputnik::SHA512::circularRightShift(uint64_t word, uint64_t shamt) noexcept {
	return (((word)>>((shamt)&0x3F)) | ((word)<<(64-((shamt)&0x3F))));
}

inline uint64_t Sputnik::SHA512::circularLeftShift(uint64_t word, uint64_t shamt) noexcept {
	return (((word)<<((shamt)&0x3F)) | ((word)>>(64-((shamt)&0x3F))));
}

inline uint64_t Sputnik::SHA512::rotateWsA(uint64_t word) noexcept {
	return (circularRightShift(word, 1) ^ circularRightShift(word, 8) ^ (word>>7));
}

inline uint64_t Sputnik::SHA512::rotateWsB(uint64_t word) noexcept {
	return (circularRightShift(word, 19) ^ circularRightShift(word, 61) ^ (word>>6));
}

inline uint64_t Sputnik::SHA512::rotateRA(uint64_t word) noexcept {
	return (circularRightShift(word, 28) ^ circularRightShift(word, 34) ^ circularRightShift(word, 39));
}

inline uint64_t Sputnik::SHA512::rotateRB(uint64_t word) noexcept {
	return (circularRightShift(word, 14) ^ circularRightShift(word, 18) ^ circularRightShift(word, 41));
}

inline uint64_t Sputnik::SHA512::Ch(uint64_t a, uint64_t b, uint64_t c) noexcept {
	return ((a & b)^(~a & c));
}

inline uint64_t Sputnik::SHA512::Maj(uint64_t a, uint64_t b, uint64_t c) noexcept {
	return ((a & b) ^ (a & c) ^ (b & c));
}

uint64_t Sputnik::SHA512::computeTotalLength(uint64_t dataSize) noexcept {
	uint64_t byteOfPad;
	if (dataSize % 128 == 112)
		byteOfPad = 128;
	else
		byteOfPad = 112 - (dataSize % 128);
	return dataSize + byteOfPad + 16;
}

void Sputnik::SHA512::wordScheduling(const uint64_t (&inputBlock)[16], uint64_t (&Wt)[80]) noexcept {
	for (int i = 0; i < 16; i++)
		Wt[i] = inputBlock[i];
	for (int i=16; i<80; i++)
		Wt[i] = rotateWsB(Wt[i-2]) + Wt[i-7] + rotateWsA(Wt[i-15]) + Wt[i-16];
}

void Sputnik::SHA512::fbox(const uint64_t (&inputBlock)[16], const uint64_t (&chainingVariable)[8], uint64_t (&hashValue)[8]) noexcept {
	uint64_t W[80];
	wordScheduling(inputBlock, W);
	for (int i = 0; i < 8; i++)
		hashValue[i] = chainingVariable[i];
	for (int i = 0; i < 80; i++)
		round(hashValue, W[i], Kt[i]);
	for (int i = 0; i < 8; i++)
		hashValue[i] += chainingVariable[i];
}

void Sputnik::SHA512::round(uint64_t (&buffer)[8], uint64_t Wt, uint64_t Kt) noexcept {
	uint64_t T1 = buffer[7] + Ch(buffer[4], buffer[5], buffer[6]) + rotateRB(buffer[4]) + Wt + Kt;
	uint64_t T2 = rotateRA(buffer[0]) + Maj(buffer[0], buffer[1], buffer[2]);
	buffer[7] = buffer[6];
	buffer[6] = buffer[5];
	buffer[5] = buffer[4];
	buffer[4] = buffer[3] + T1;
	buffer[3] = buffer[2];
	buffer[2] = buffer[1];
	buffer[1] = buffer[0];
	buffer[0] = T1 + T2;
}

Sputnik::SHA512::Hashsum Sputnik::SHA512::sum(const uint8_t* const inputData, uint64_t dataSize) noexcept {
	SHA512::Hashsum hashValue;
	uint64_t chainingVariable[8];
	for (int i = 0; i < 8; i++)
		chainingVariable[i] = IV[i];
	uint64_t totalLength = computeTotalLength(dataSize);
	// buffer vector, progressively filled with input / padding data, used to calculate hash
	uint64_t data[16];
	// pointer used to scroll in the buffer vector one byte at a time
	uint8_t *ptr = (uint8_t*)data;
	// scrolls the input data vector of four in four bytes
	for (uint64_t i = 7; i < totalLength; i+=8) {
		// copy the bytes from the input vector in the buffer vector or padding on-the-fly
		for (int j = 0; j< 8; j++, ptr++)
			if ((i-j) < dataSize)
				*ptr = inputData[i-j];
			else if ((i-j) == dataSize)
				*ptr = 0x80;
			else
				*ptr = 0x00;
		// once the buffer vector has been filled, it can be used to calculate hash
		if ((i+1)%128==0) {
			// if you areprocessing the last partition, the original message size (without padding, in bit)
			// is inserted in the last two 64-bit words
			if (i == totalLength - 1) {
					data[14] = dataSize>>61;
					data[15] = dataSize<<3;
			} else ptr = (uint8_t*)data;
			// partial hashsum computation
			fbox(data, chainingVariable, hashValue.byte_array);
			// copying the partial hashsum in the chaining variables
			for (int j = 0; j < 8; j++)
				chainingVariable[j] = hashValue.byte_array[j];
		}
	}
	return hashValue;
}
