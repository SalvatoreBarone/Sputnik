/**
 * @file VoteMismatch.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __SPUTNIK_VOTEMISMATCH__
#define __SPUTNIK_VOTEMISMATCH__

#include <inttypes.h>

namespace Sputnik {
/**
 * @ingroup core_eccezioni
 * @{
 */

/**
 * @brief Eccezione generata dal processo di voting.
 * 
 * @details
 * Quando le operazioni di voting evidenziano una differenza tra le aree di memoria confrontate viene
 * lanciata un'eccezione VoteMismatch, in modo da rendere più semplice gestire questo tipo di eventi.
 */
class VoteMismatch {
public:
/**
 * @brief Costruttore
 * 
 * @param [in] index	indice del chunk per cui è stata generata eccezione
 * @param [in] address	indirizzo base della porzione di memoria su cui è stato effettuato il voting
 */
	explicit VoteMismatch(uint8_t index, const uint8_t* const address) : 
		chunkIndex(index),
		baseAddress((uint8_t*)address) {}
/**
 * @brief Restituisce l'indice del chunk che ha causato il manifestarsi dell'eccezione
 */
	uint8_t getChunkIndex() const noexcept {return chunkIndex;}
/**
 * @brief Restituisce l'indirizzo base del chunk che ha causato il manifestarsi dell'eccezione
 */
	const uint8_t* getBaseAddress() const {return baseAddress;}
	
private:
	uint8_t chunkIndex;
	uint8_t* baseAddress;
};

/**
 * @}
 */

}

#endif
