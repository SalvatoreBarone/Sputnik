/**
 * @file SHA512Voter.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "SHA512Voter.hpp"

void Sputnik::SHA512Voter::vote(uint8_t index, const uint8_t (&data)[CHUNK_SIZE]) const throw (VoteMismatch&) {
	SHA512::Hashsum computedHashsum = SHA512::sum((const uint8_t* const)&data, CHUNK_SIZE), receivedHashsum;
	uint8_t sendbuff[SHA512::Hashsum::size], recvbuff[SHA512::Hashsum::size];
	computedHashsum.marshall(sendbuff);
	communicator.sendRecv(sendbuff, recvbuff, SHA512::Hashsum::size);
	receivedHashsum.unmarshall(recvbuff);
	if (computedHashsum != receivedHashsum)
		throw VoteMismatch(index, (const uint8_t* const)&data);
}
