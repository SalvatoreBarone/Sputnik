/**
 * @file SHA256.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "SHA256.hpp"

bool Sputnik::SHA256::Hashsum::operator ==(const Hashsum& hs) const noexcept {
	for (int i = 0; i < 8; i++)
		if (byte_array[i] != hs.byte_array[i])
			return false;
	return true;
}

void Sputnik::SHA256::Hashsum::marshall(uint8_t (&byteArray)[32]) const noexcept {
	uint32_t *ptr = (uint32_t*)&byteArray;
	for (int i = 0; i < 8; i++)
		ptr[i] = byte_array[i];
}

void Sputnik::SHA256::Hashsum::unmarshall(const uint8_t (&byteArray)[32]) noexcept {
	uint32_t *ptr = (uint32_t*)&byteArray;
	for (int i = 0; i < 8; i++)
		byte_array[i] = ptr[i];
}

const uint32_t Sputnik::SHA256::Kt[64] = {
	0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
	0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
	0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
	0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
	0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
	0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
	0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
	0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
};


const uint32_t Sputnik::SHA256::IV[8] = {
	0x6A09E667,	0xBB67AE85,	0x3C6EF372,	0xA54FF53A,	0x510E527F,	0x9B05688C,	0x1F83D9AB,	0x5BE0CD19
};

inline uint32_t Sputnik::SHA256::circularRightShift(uint32_t word, uint32_t shamt) noexcept {
	return (((word)>>((shamt)&0x1F)) | ((word)<<(32-((shamt)&0x1F))));
}

inline uint32_t Sputnik::SHA256::circularLeftShift(uint32_t word, uint32_t shamt) noexcept {
	return (((word)<<((shamt)&0x1F)) | ((word)>>(32-((shamt)&0x1F))));
}

inline uint32_t Sputnik::SHA256::rotateWsA(uint32_t word) noexcept {
	return (circularRightShift(word, 7) ^ circularRightShift(word, 18) ^ (word>>3));
}

inline uint32_t Sputnik::SHA256::rotateWsB(uint32_t word) noexcept {
	return (circularRightShift(word, 17) ^ circularRightShift(word, 19) ^ (word>>10));
}

inline uint32_t Sputnik::SHA256::rotateRA(uint32_t word) noexcept {
	return (circularRightShift(word, 2) ^ circularRightShift(word, 13) ^ circularRightShift(word, 22));
}

inline uint32_t Sputnik::SHA256::rotateRB(uint32_t word) noexcept{
	return (circularRightShift(word, 6) ^ circularRightShift(word, 11) ^ circularRightShift(word, 25));
}

inline uint32_t Sputnik::SHA256::Ch(uint32_t a, uint32_t b, uint32_t c) noexcept {
	return((a & b)^(~a & c));
}

inline uint32_t Sputnik::SHA256::Maj(uint32_t a, uint32_t b, uint32_t c) noexcept {
	return ((a & b) ^ (a & c) ^ (b & c));
}

uint64_t Sputnik::SHA256::computeTotalLength(uint64_t dataSize) noexcept{
	uint64_t byteOfPadding;
	if (dataSize % 64 == 56)
		byteOfPadding = 64;
	else
		byteOfPadding = 56 - (dataSize % 64);
	return dataSize + byteOfPadding + 8;
}

void Sputnik::SHA256::wordScheduling(const uint32_t (&inputBlock)[16], uint32_t (&Wt)[64]) noexcept {
	for (int i = 0; i < 16; i++)
		Wt[i] = inputBlock[i];
	for (int i=16; i<64; i++)
		Wt[i] = rotateWsB(Wt[i-2]) + Wt[i-7] + rotateWsA(Wt[i-15]) + Wt[i-16];
}

void Sputnik::SHA256::fbox(const uint32_t (&inputBlock)[16], const uint32_t (&chainingVariable)[8], uint32_t (&hashValue)[8]) noexcept {
	uint32_t Wt[64];
	wordScheduling(inputBlock, Wt);
	for (int i = 0; i < 8; i++)
		hashValue[i] = chainingVariable[i];
	for (int i = 0; i < 64; i++)
		round(hashValue, Wt[i], Kt[i]);
	for (int i = 0; i < 8; i++)
		hashValue[i] += chainingVariable[i];
}

void Sputnik::SHA256::round(uint32_t (&buffer)[8], uint32_t Wt, uint32_t Kt) noexcept {
	uint32_t T1 = buffer[7] + Ch(buffer[4], buffer[5], buffer[6]) + rotateRB(buffer[4]) + Wt + Kt;
	uint32_t T2 = rotateRA(buffer[0]) + Maj(buffer[0], buffer[1], buffer[2]);
	buffer[7] = buffer[6];
	buffer[6] = buffer[5];
	buffer[5] = buffer[4];
	buffer[4] = buffer[3] + T1;
	buffer[3] = buffer[2];
	buffer[2] = buffer[1];
	buffer[1] = buffer[0];
	buffer[0] = T1 + T2;
}

Sputnik::SHA256::Hashsum Sputnik::SHA256::sum(const uint8_t* const inputData, uint64_t dataSize) noexcept {
	Hashsum hashValue;
	uint32_t chainingVariable[8];
	for (int i = 0; i < 8; i++)
		chainingVariable[i] = IV[i];
	uint64_t totalLength = computeTotalLength(dataSize);
	// buffer vector, progressively filled with input / padding data, used to calculate hash
	uint32_t data[16];
	// pointer used to scroll in the buffer vector one byte at a time
	uint8_t *ptr = (uint8_t*)data;
	// scrolls the input data vector of four in four bytes
	for (uint64_t i = 3; i < totalLength; i+=4) {
		// copy the bytes from the input vector in the buffer vector or padding on-the-fly
		for (int j = 0; j< 4; j++, ptr++)
			if ((i-j) < dataSize)
				*ptr = inputData[i-j];
			else if ((i-j) == dataSize)
				*ptr = 0x80;
			else
				*ptr = 0x00;
		// once the buffer vector has been filled, it can be used to calculate hash
		if ((i+1)%64==0) {
			// if you areprocessing the last partition, the original message size (without padding, in bit)
			// is inserted in the last two 32-bit words
			if (i == totalLength-1) {
					data[14] = (uint32_t)(dataSize>>29);
					data[15] = (uint32_t)(dataSize<<3);
			} else ptr = (uint8_t*)data;
			// partial hashsum computation
			fbox(data, chainingVariable, hashValue.byte_array);
			// copying the partial hashsum in the chaining variables
			for (int j = 0; j < 8; j++)
				chainingVariable[j] = hashValue.byte_array[j];
		}
	}
	return hashValue;
}
