 /**
 * @file Voter.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __SPUTNIK_VOTER_INTERFACE__
#define __SPUTNIK_VOTER_INTERFACE__

#include <inttypes.h>
#include <cstdlib>

#include "Communicator.hpp"
#include "SafeMemory.hpp"
#include "VoteMismatch.hpp"

namespace Sputnik {
/**
 * @ingroup core
 * @{
 * @defgroup core_voter Voter
 * @{
 * 
 * @brief Raccoglie le interfacce e le funzionalità che permettono di confrontare le repliche.
 */

/**
 * @brief Interfaccia per le classi di voting
 * 
 * @details
 * L'interfaccia definisce le funzionalità che le classi che offrono la funzionalità di voting devono necessariamente
 * implementare. In un sistema 2oo2, due sistemi esattamente identici effetuano le stesse operazioni e si scambiano i
 * risultati "votandosi" a vicenda. Le classi di voting devono fornire, dunque, funzionalità che permettono di
 * confrontare aree di memoria appartenenti a sistemi diversi ma identici dal punto di vista funzionale.
 * 
 * Affinché le repliche possano comunicare tra loro durante le operazioni di voting, viene usata una classe che
 * implementi l'interfaccia Sputnik::Communicator.
 */
class Voter {
public:

/**
 * @brief Costruttore
 *
 * @param [in] c riferimento ad una classe che implementi l'interfaccia Sputnik::Communicator.
 */
	explicit Voter(Communicator& c) : communicator(c) {}

/**
 * @brief Voting.
 * 
 * @details
 * Le classi che implementano l'interfaccia Sputnik::Voter dovranno implementare questa funzione.
 * 
 * @param [in] index	indice del chunk votato
 * @param [in] data		riferimento all'area di memoria occupata dal chunk votato
 * 
 * @exception VoteMismatch se il votinf dovesse rendere manifeste differenze tra le aree di memoria confrontate.
 */
	virtual void vote(uint8_t index, const uint8_t (&data)[CHUNK_SIZE]) const throw (VoteMismatch&) = 0;


protected :
	Communicator& communicator; /**< riferimento ad una classe che implementi l'interfaccia Sputnik::Communicator */
};

/**
 * @}
 * @}
 */
};

#endif
