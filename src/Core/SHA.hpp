/**
 * @file SHA.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

/**
 * @ingroup core
 * @{
 * @defgroup SHA SHA
 * @{
 * @brief Implementazione delle funzioni hash della famiglia SHA-2
 *
 * @details
 * @image html iterative_hash_function.jpg "Iterative hash function" width=40%
 * @image latex iterative_hash_function.jpg "Iterative hash function" width=40%
 *
 * Quasi tutte le funzioni hash, incluse quelle della famiglia SHA, possiedono la struttura mostrata nella figura
 * precedente. A questo tipo di funzioni hash ci si riferisce come "funzioni hash iterative".
 * La funzione prende in ingresso un messaggio di lunghezza arbitraria e lo partiziona in L blocchi di dimensione
 * fissa pari a b bit ciascuno. Se necessario, il blocco finale viene esteso fino a b bit. Il valore della lunghezza
 * totale del messaggio viene incluso nel blocco finale, in modo da rendere più difficoltoso il lavoro di un attaccante,
 * che, in questo modo, dovrà trovare due messaggi della stessa lunghezza che diano lo stesso valore hash, oppure due
 * messaggi di lunghezza diversa che insieme abbiano lunghezza pari a quella del messaggio considerato, ed abbiano lo
 * stesso valore hash.
 * 
 * L'algoritmo prevede l'utilizzo ripetuto di funzioni di compressione F, che prendono due valori di input, uno di
 * lunghezza pari ad n bit, prodotto dal passo precedente e chiamato chaining-variable, ed uno di b bit corrispondente
 * ad uno dei blocchi del messaggio originario. Al primo step, il chaining-variable viene inizializzato usando un
 * valore specifico, diverso da algoritmo ad algoritmo, mentre all'ultimo passo conterrà il valore hash. Ovviamente
 * b>n, da quì il termine funzione di compressione. La funzione hash può, quindi, essere così riassunta:
 * \f[CV_{0} = IV\f]
 * \f[CV_{i} = F(CV_{i-1}, Y_{i}), 1 \leq i \leq L\f]
 * \f[H(M) = CV_{L}\f]
 * con M input della funzione hash, IV initialization-vector, ed Y_{i} blocchi in cui tale messaggio viene partizionato.
 * La motivazione principale che sta dietro questa struttura sta nel fatto che se la funzione di compressione è
 * collision-resistant, anche la struttura risultante sarà collision-resistant. Questa struttura può essere usata per
 * produrre una funzione hash che operi su un messaggio di lunghezza qualsiasi.
 *
 * @section SHA SHA
 * @image html SHA-512_structure.jpg "SHA hash function block diagram" width=40%
 * @image latex SHA-512_structure.jpg "SHA hash function block diagram" width=40%
 * La figura precedente mostra la struttura dell'algoritmo SHA. Prende in ingresso un messaggio la cui dimensione massima
 * è @f$ 2 ^ 128 @f$ bit (@f$ 2 ^ 64 @f$ per SHA-256) e produce in output 512 bit (256 bit per SHA-256). L'input viene
 * processato in blocchi di 1024 bit (512 per SHA-256). L'elaborazione consiste nei seguenti passi:
 *
 * 1. Padding: 
 * il messaggio viene esteso con padding, in modo che la sua lunghezza sia congruente con 896 mod 1024 bit (448 mod 512
 * per SHA-256). Il padding viene effettuato sempre, anche se il messaggio è già della lunghezza opportuna, quindi il
 * numero di bit di padding è nell'intervallo [1, 1024] ([1, 512] per SHA-256). Il padding consiste in un singolo bit 1,
 * seguito dal numero opportuno di bit 0.
 *
 * 2. Append length: 
 * un blocco di 128 bit (64 bit nel caso di SHA-256) viene posto in append al messaggio. Tale blocco è trattato come
 * un intero senza segno di 128 bit, il cui primo byte è quello più significativo, e contiene la lunghezza in bit del
 * messaggio originale, prima del padding. Il risultato di questi primi due passi è un messaggio che, in lunghezza, è
 * multiplo intero di 1024 bit (512 bit per SHA-256).
 *
 * 3. Initialize Hash Buffer: 
 * viene usato un buffer di 512 bit (256 bit per SHA-256) per mantenere i risultati intermedi e finali delle funzioni
 * hash. Il buffer può essere rappresentato come otto word di 64 bit (32 bit per SHA-256) ciascuna, contenenti,
 * inizialmente, la parte frazionaria della radice quadrata dei primi otto numeri primi. Tali registri vengono
 * memorizzati in formato big-endian, il che vuol dire che la word all'indirizzo più basso contiene il byte più
 * significativo.
 *
 * 4. Process message: 
 * il cuore dell'algoritmo è costituito dal blocco F, il quale effettua trasformazioni dell'input in 80 round (64 per
 * SHA-256). La struttura nel dettaglio è riportata di seguito. Ogni round prende in input il buffer di 512 bit discusso
 * al punto precedente, ed aggiorna il suo contenuto. Ogni round fa uso di un valore a 64 bit (32 nel caso di SHA-256) 
 * @f$ W_{t} @f$, derivato dal blocco di 1024 bit del messaggio che si sta processando. Tali valori @f$ W_{t} @f$ vengono
 * derivati usando un algoritmo di schedulazione che sarà successivamente. Ogni round aggiunge una costante @f$ K_{t} @f$,
 * di 64 bit (32 nel caso di SHA-256), pari ai 64 bit della parte frazionaria della radice cubica dei primi ottanta numeri
 * primi (i primi 64 numeri primi, per SHA-256). L'uso delle costanti fornisce pattern casuali di 64 bit che eliminano ogni
 * regolarità presente nel blocco di input. L'output dell'ottantesimo round (sessantaquattresimo per SHA-256) viene
 * addizionato all'input del round zero. L'addizione viene effettuata indipendentemente per ognuna delle word del buffer, 
 * usando un addizione modulo @f$ 2^{64} @f$ (indovina il modulo per SHA-256 ... @f$ 2^{32} @f$).
 *
 * 5. Output: 
 * dopo che tutti gli n blocchi da 1024 bit (512 bit per SHA-256) siano stati processati, l'output dell' n-esimo stage
 * costituisce il digest di 512 bit del messaggio originale.
 * .
 * 
 * @image latex SHA-512_block_operation.jpg "SHA operatins on a single data block" width=40%
 * @image html SHA-512_block_operation.jpg "SHA operatins on a single data block" width=40%
 *
 * @section Round Round
 * 
 * @image html SHA-512_single_round.jpg "block diagram of a round" width=40%
 * @image latex SHA-512_single_round.jpg "block diagram of a round" width=40%
 *
 * Ogni roud effettua le seguenti operazioni:
 * @f[T_{1} = h + Ch(e, f , g ) + ROT_{e}(e) + W_{t} + K_{t}@f]
 * @f[T 2 = ROT_{a}(a) + Maj( a, b, c)@f]
 * @f[h = g@f]
 * @f[g = f@f]
 * @f[f = e@f]
 * @f[e = d + T_{1}@f]
 *
 * dove:
 * - t: indice del round;
 * - @f$Ch(e, f , g) = (e \:AND\: f) \:XOR\: (NOT e \:AND\: g)@f$;
 * - @f$Maj(a, b, c) = (a \:AND\: b) \:XOR\: (a \:AND\: c) \:XOR\: (b \:AND\: c)@f$
 * - @f$ROT_{a}(a) = ROTR_{28}(a) \:XOR\: ROTR_{34}(a) \:XOR\: ROTR_{39}(a)@f$ for SHA-512
 *   @f$ROT_{a}(a) = ROTR_{2}(a) \:XOR\: ROTR_{13}(a) \:XOR\: ROTR_{22}(a)@f$ for SHA-256;
 * - @f$ROT_{e}(e) = ROTR_{14}(e) \:XOR\: ROTR_{18}(e) \:XOR\: ROTR_{41}(e)@f$ for SHA-512,
 *   @f$ROT_{e}(e) = ROTR_{6}(e) \:XOR\: ROTR_{11}(e) \:XOR\: ROTR_{25}(e)@f$ for SHA-256;
 * - @f$ROTR_{n}(x)@f$: right circular shift di n-bit;
 * .
 *
 * @section Scheduling Scheduling
 * 
 * @image html SHA-512_word_scheduling.jpg "block diagram of a round" width=40%
 * @image latex SHA-512_word_scheduling.jpg "block diagram of a round" width=40%
 *
 * La figura precedente mostra come vengano schedulate le word @f$ W_{t} @f$. I primi sedici valori di @f$ W_{t} @f$
 * vengono presi direttamente dalle sedici word del blocco corrente (anche nel caso di SHA-256 le word sono sedici, 
 * ma anziché essere word di 64 bit, in SHA-256 le word sono da 32 bit). Le rimanenti vengono definite come segue:
 * @f[W_{t} = ROT_{b}(W_{t-2}) + W_{t-7} + ROT_{a}(W_{t-15}) + W_{t-16}@f]
 * where:
 * - @f$ROT_{a}(x) = ROTR_{1}(x) \:XOR\: ROTR_{8}(x) \:XOR\: SHR_{7}(x)@f$ per SHA-512,
 *   @f$ROT_{a}(x) = ROTR_{7}(x) \:XOR\: ROTR_{18}(x) \:XOR\: SHR_{3}(x)@f$ per SHA-256
 * - @f$ROT_{b}(x) = ROTR_{19}(x) \:XOR\: ROTR_{61}(x) \:XOR\: SHR_{6}(x)@f$ per SHA-512,
 *   @f$ROT_{b}(x) = ROTR_{19}(x) \:XOR\: ROTR_{19}(x) \:XOR\: SHR_{10}(x)@f$ per SHA-256
 * - @f$ROTR_{n}(x)@f$: right circular shift di x, di n-bit;
 * - @f$SHR_{n}(x)@f$: right shift di x, con zero-padding;
 * @}
 * @}
 */

#ifndef __SHA_HEADER_H__
#define __SHA_HEADER_H__

#include "SHA256.hpp"
#include "SHA512.hpp"

#endif
