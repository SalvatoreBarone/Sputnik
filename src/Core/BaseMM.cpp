/**
 * @file BaseMM.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#include "BaseMM.hpp"
#include "VoterThread.hpp"

Sputnik::BaseMM::BaseMM(SafeMemory& memory, Voter& v, void(*h)(void*), void* a) throw (AlreadyAssigned) :  
	voter(v),
	baseAddress((uint8_t*)memory.byteArray),
	head(baseAddress),
	chunkStatus(0),
	handler(h),
	handlerArgs(a)
{
	if (memory.assignedMM == nullptr)
		memory.assignedMM = (BaseMM*)this;
	else
		throw AlreadyAssigned(memory);
}

uint64_t Sputnik::BaseMM::getChunkStatus() noexcept {
    uint64_t status;
    chunkStatusMutex->lock();
	status = chunkStatus;
    chunkStatusMutex->unlock();
    return status;
}

void Sputnik::BaseMM::setDirtyChunk(uint64_t chunkMask) noexcept {
    chunkStatusMutex->lock();
	chunkStatus |= chunkMask;
    chunkStatusMutex->unlock();
}

void Sputnik::BaseMM::vote() {
	chunkStatusMutex->lock();
	for (uint8_t i = 0; i < CHUNK_NUMBER; i++)
		if (chunkStatus & (1<<i)) {
			try {
				voter.vote(i, (const uint8_t (&)[CHUNK_SIZE])*getChunkBaseAddress(i));
			} catch (VoteMismatch &e) {
				handler(handlerArgs);
			}
		}
	chunkStatus = 0;
	chunkStatusMutex->unlock();
#ifdef ASYNC_VOTER
	waitVoting->broadcast();
#endif
}

void Sputnik::BaseMM::suspendUntilVoted(uint64_t chunkMask) noexcept {
#ifdef ASYNC_VOTER
	chunkStatusMutex->lock();
	while(chunkStatus & chunkMask)
		waitVoting->wait(*chunkStatusMutex);
	chunkStatusMutex->unlock();
#else
	vote();
#endif
}

bool Sputnik::BaseMM::checkSpace(size_t size) const noexcept {
	if (head + size > (uint8_t*)baseAddress + CHUNK_NUMBER*CHUNK_SIZE)
		return false;
	return true;
}

uint64_t Sputnik::BaseMM::computeChunkMask(size_t size) const noexcept {
	uint8_t starting_cindex = (head - (uint8_t*)baseAddress) / CHUNK_SIZE;
	uint8_t final_cindex = (head + size - (uint8_t*)baseAddress) / CHUNK_SIZE;
	return ~((1<<starting_cindex)-1) & ((1<<(final_cindex+1))-1); // Grazie Mario!
}

