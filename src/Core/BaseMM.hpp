/**
 * @file BaseMM.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#ifndef __SPUTNIK_BASEMEMORY_MANAGER__
#define __SPUTNIK_BASEMEMORY_MANAGER__

#include <inttypes.h>
#include <memory>

#include "BadAlloc.hpp"
#include "AlreadyAssigned.hpp"
#include "Voter.hpp"
#include "Mutex.hpp"
#include "ConditionVariable.hpp"
#include "VoterThread.hpp"

namespace Sputnik {
/**
 * @defgroup core Core
 * @{
 * @defgroup core_mm Memory Management
 * @{
 * 
 * @brief Modulo per la gestione della memoria.
 * 
 * @details
 * Il modulo include tutte le classi preposte alla gestione della memoria, le quali definiscono le metodologie
 * di accesso e gestione delle operazioni di voting.
 */
class VoterThread;

#ifdef ALAP_VOTER
template<class T> class ALAPointer;
#define Pointer ALAPointer
#endif

#ifdef ASAP_VOTER
template<class T> class ASAPointer;
#define Pointer ASAPointer
#endif

#ifdef ASYNC_VOTER
template<class T>  class ASYNCPointer;
#define Pointer ASYNCPointer
#endif

/**
 * @brief Classe base per la gestione della memoria.
 *
 * @details
 * La classe BaseMM costituisce la classe base da cui derivano tutte le altre classi di gestione della memoria.
 * Definisce le metodologie per riservare aree di memoria protette e le metodologie di gestione delle stesse.
 * 
 * Si tratta di una classe astratta, che non è in grado, da sola, di assolvere alle sue funzioni, poiché ha
 * necessità di instanziare tutti i meccanismi di sincronizzazione e di comunicazione che dipendono dalla
 * particolare configurazione di sistema adottata, dal sistema operativo e dalla piattaforma hardware target.
 */
class BaseMM {
public:
	
/**
 * @brief Costrutture
 *
 * @details
 * Crea un nuovo oggetto per la gestione della memoria, assegnandogli una porzione di memoria ed un voter.
 * La porzione di memoria diviene ad uso esclusivo del manager che si sta istanziando.
 * L'oggetto voter con cui l'oggetto viene istanziato può essere condiviso tra memory manager diversi, anche se è
 * buona norma far si che ogni a new memory manager corrisponda un voter diverso.
 * 
 * @param [in] memory	riferimento ad un oggetto di tipo SafeMemory, al quale corrisponde la porzione di memoria
 * 						gestita dal manager instanziato. La porzione di memoria diviene ad uso esclusivo del
 * 						manager che si sta istanziando.
 * 
 * @param [in] voter	riferimento ad un oggetto di una classe che implementi l'interfaccia Sputnik::Voter.
 * 						L'oggetto voter con cui l'oggetto viene istanziato può essere condiviso tra memory manager
 * 						diversi, anche se è buona norma far si che ogni a new memory manager corrisponda un voter
 * 						diverso.
 * 
 * @param [in] handler	handler delle eccezioni di tipo Sputnik::VoteMismatch.
 * 						Deve essere un puntatore ad una funzione del tipo
 * 						@code
 * 							void function(void*)
 * 						@endcode
 * 						a cui è delegato il compito di gestire eventuali eccezioni generate durante le operazioni
 * 						di voting. Tale funzione viene automaticamente chiamata dalla libreria quando si manifesta
 * 						un'eccezione di tipo Sputnik::VoteMismatch.
 * 
 * @param [in] args		puntatore agli argomenti da passare all'handler delle eccezioni di tipo Sputnik::VoteMismatch.
 * 
 * @exception AlreadyAssigned si tenta di assegnare all'oggetto in costruzione un oggetto SafeMemory che sia già
 * stato assegnato ad un oggetto BaseMM differente.
 */
	BaseMM(SafeMemory& memory, Voter& voter, void(*handler)(void*), void* args = NULL) throw (AlreadyAssigned);
	
	virtual ~BaseMM() {}
	
/**
 * @brief Costruttore di copia (cancellato)
 *
 * @details
 * Non deve essere possibile effettuare la copia di questo tipo di oggetti. 
 */
	BaseMM(const BaseMM& m) = delete;
/**
 * @brief Operatore di assegnazione (cancellato)
 *
 * @details
 * Non deve essere possibile effettuare la copia di questo tipo di oggetti. 
 */
	const BaseMM& operator= (const BaseMM& m) const = delete;
/**
 * @brief Operatore di assegnazione (cancellato)
 *
 * @details
 * Non deve essere possibile effettuare la copia di questo tipo di oggetti. 
 */
	const BaseMM& operator= (const BaseMM& m) = delete;
/**
 * @brief Riserva spazio per un oggetto.
 *
 * @details
 * La funzione consente di riservare spazio per un oggetto di tipo generico T.
 * 
 * @warning La funzione non chiama il costruttore dell'oggetto per cui è stato riservato spazio.
 * Sarà necessario inizializzare l'oggetto manualmente.
 * 
 * @tparam T tipo dell'oggetto per cui riservare spazio.
 *
 * @exception BadAlloc se lo spazio di memoria non è sufficiente ad allocare l'oggetto richiesto.
 * 
 * @note La funzione è thread safe: se due thread chiamano la funzione nello stesso istante, la loro esecuzione
 * viene serializzata. 
 */
	template<class T>
	Pointer<T> reserve() throw (BadAlloc) {
		reserveMutex->lock();
		if (!checkSpace(sizeof(T)))
			throw BadAlloc(baseAddress + CHUNK_NUMBER * CHUNK_SIZE - head, sizeof(T));
		uint64_t cm = computeChunkMask(sizeof(T));
		T& T_ref = *((T*)head);
		head += sizeof(T) + (sizeof(T) % correctionFactor);
		reserveMutex->unlock();
		return Pointer<T>(*this, T_ref, cm);
	}
	
/**
 * @brief Restituisce lo stato dei chunk in codifica one-hot.
 * 
 * @return maschera di stato dei chunk. Se l'n-esimo bit della maschera è '1' il corrispondente chunk è dirty, il
 * che vuol dire che dovrà essere sottoposto a voting.
 * 
 * @note La funzione è thread safe.
 */
	uint64_t getChunkStatus() noexcept;
/**
 * @brief Contrassegna uno o più chunk come dirty.
 * 
 * @param [in] chunkMask maschera dei chunk da rendere dirty, in codifica one-hot. Se l'n-esimo bit della maschera è '1' il
 * corrispondente chunk sarà reso dirty, il che vuol dire che dovrà essere sottoposto a voting.
 * 
 * @note La funzione è thread safe.
 */
	void setDirtyChunk(uint64_t chunkMask) noexcept;
/**
 * @brief Restituisce un riferimento all'oggetto voter associato al gestore della memoria.
 * 
 * @return riferimento all'oggetto di classe Sputnik::Voter (o di classe da essa derivata).
 */
	Voter& getVoter() const {
		return voter;
	}
/**
 * @brief Effettua le operazioni di voting.
 * 
 * @details
 * Il voting viene effettuato di chunk in chunk, solo se i chunk si trovano nello stato dirty.
 * Se il voting non evidenzia differenze, lo stato dei chunk viene riportato a clean.
 * 
 * @exception Voter::VoteVoteMismatch se il confronto con il sistema gemello rende manifeste differenze.
 * 
 * @note La funzione è thread safe. Gli accessi allo stato dei chunk avvengono sempre in mutua esclusione,
 * serializzati.
 * Se un thread sta aggiornando lo stato dei chunk, la stessa operazione non può avvenire da parte di un altro
 * thread. Se è in corso il voting, nessuna operazione di lettura/scrittura può avvenire sui chunk fino al
 * completamento del voting.
 * 
 * @note La funzione garantisce che un solo thread per volta possa effettuare le operazioni di voting.
 */
	virtual void vote();
	
/**
 * @brief Sospende il thread che la chiama fino al completamento delle operazioni di voting.
 * 
 * @details
 * La funzione fa si che il thread chiamante venga sospeso se i chunk interessati da un'operazione
 * siano contrassegnati come dirty. Il thread chiamante viene automaticamente risvegliato nel momento
 * in cui le operazioni di voting vengono completate. Nel caso di gestori che usino
 * logica alap o asap viene chiamata la funzione di voting.
 * 
 * @param [in] chunkMask maschera, in codifica one-hot, dei chunk interessati da un'operazione.
 */
	virtual void suspendUntilVoted(uint64_t chunkMask = 0xffffffffffffffff) noexcept;

protected:
/*
 * @brief Riferimento all'oggetto Voter (o un tipo derivato) usato per eseguire la comparazione con la replica gemella.
 */
	Voter& voter;
/*
 * @brief Indirizzo base dell'area di memoria sicura
 */
	uint8_t* baseAddress;
/*
 * @brief Head-pointer.
 * 
 * @details
 * Questo puntatore viene progressivamente incrementato man mano che viene richiesto spazio per variabili ed oggetti.
 * Non è prevista la deallocazione di oggetti.
 */
	uint8_t* head;
/*
 * @brief Fattore di correzione, usato per allineare l'head-pointer agli indirizzi di memoria accessibili.
 */
	static const int correctionFactor = 4;
/*
 * @brief Chunk status.
 * 
 * @details
 * Lo status dei chunk viene memorizzato in una maschera, usando la codifica one-hot: se il bit n-esimo della maschera è
 * '1', il corrispondente chunk è dirty (deve essere effettuato il voting).
 * 
 * @note L'accesso allo stato dei chunk avviene in mutua escluzione, protetto dal mutex chunkStatusMutex.
 */
	uint64_t chunkStatus;
/*
 * @brief Mutex usato per serializzare l'esecuzione di reserve().
 */
	std::unique_ptr<Mutex> reserveMutex;
/*
 * @brief Mutex usato per l'accesso in mutua esclusione allo stato dei chunk.
 */
	std::unique_ptr<Mutex> chunkStatusMutex;
/*
 * @brief Verifica che ci sia spazio a sufficienza da riservare ad un nuovo oggetto.
 * 
 * @details
 * La funzione verifica che nella porzione di memoria gestita esista spazio a sufficienza da riservare
 * ad un oggetto di dimensione specifica.
 *
 * @param [in] size dimensione dell'oggetto per cui si intende riservare spazio.
 *
 * @retval false se non c'è abbastanza spazio.
 * @retval true se c'è spazio a sufficienza.
 */
	bool checkSpace(size_t size) const noexcept;
/*
 * @brief Calcolo della maschera dei chunk occupati da un oggetto.
 * 
 * @details
 * La funzione calcola la maschera di bit, in codifica one-hot, che rappresenta quali chunk siano
 * occupati da un oggetto di una certa dimensione.
 * 
 * @param [in] size dimensione, in byte, dell'oggetto.
 * @return chunk mask, in codifica one-hot. Se il bit n-esimo è '1', allora il corrispondente chunk contiene l'oggetto
 * in questione, o parte di esso.
 */
	uint64_t computeChunkMask(size_t size) const noexcept;
/*
 * @brief Calcola l'indirizzo base del chunk, dato il suo indice,
 * 
 * @param [in] index indice del chunk
 * 
 * @return indirizzo base del chunk
 */
	const uint8_t* getChunkBaseAddress(uint8_t index) const {
		return baseAddress+index*CHUNK_SIZE;
	}
	
	
/*
 * Handler delle eccezioni di tipo Sputnik::VoteMismatch.
 * Deve essere un puntatore ad una funzione del tipo
 * @code
 * 	void function(void*)
 * @endcode
 * a cui è delegato il compito di gestire eventuali eccezioni generate durante le operazioni di voting. Tale
 * funzione viene automaticamente chiamata dalla libreria quando si manifesta un'eccezione di tipo
 * Sputnik::VoteMismatch.
 */
	void(*handler)(void*);
/**
 * Puntatore agli argomenti da passare all'handler delle eccezioni di tipo Sputnik::VoteMismatch.
 */
	void* handlerArgs;

#ifdef ASYNC_VOTER
/*
 * @brief Condition variable usata per la sospensione dei thread.
 * 
 * @details
 * La variabile condition viene usata per sospendere i thread se viene tentata una lettura su un chunk dirty prima
 * che esso sia stato votato dal thread di voting.
 */
	std::unique_ptr<ConditionVariable> waitVoting;
/*
 * @brief Thread che effettua le operazioni di voting su base periodica.
 */
    std::unique_ptr<VoterThread> voterThread;
#endif

	
};

/**
 * @}
 * @}
 */
};
#endif
