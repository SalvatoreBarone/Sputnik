/**
 * @file SHA512.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#ifndef __SHA512_HEADER_H__
#define __SHA512_HEADER_H__

#include <inttypes.h>
#include <cstdlib>

namespace Sputnik {

/**
 * @ingroup SHA
 * @{
 */

/**
 * @brief Implementazione dell'algoritmo SHA-512
 */
class SHA512 {
public:
/**
 * @brief Astrae la rappresentazione dell'hashsum calcolato con SHA-512
 */
	class Hashsum {
		friend class SHA512;
	public:
		Hashsum() {}
/**
 * @brief Operatore di comparazione
 *
 * @param [in] hs riferimento ad un oggetto Sputnik::SHA512::Hashsum
 * 
 * @retval true se i due oggetti Sputnik::SHA512::Hashsum comparati sono uguali
 * @retval false se i due oggetti Sputnik::SHA512::Hashsum comparati non sono uguali
 */
		bool operator ==(const Hashsum&) const noexcept;
/**
 * @brief Operatore di comparazione (disuguaglianza)
 *
 * @param [in] hs riferimento ad un oggetto Sputnik::SHA512::Hashsum
 * 
 * @retval true se i due oggetti Sputnik::SHA512::Hashsum comparati non sono uguali
 * @retval false se i due oggetti Sputnik::SHA512::Hashsum comparati sono uguali
 */
		bool operator !=(const Hashsum& hs) const noexcept {return !operator==(hs);}
/**
 * @brief Marshaling
 * 
 * @details
 * Il marshalling è il processo di trasformazione di un oggetto Sputnik::SHA512::Hashsum sottoforma di stream di byte.
 * 
 * @param [out] byteArray riferimendo ad un array di 64 byte.
 */
		void marshall(uint8_t (&byteArray)[64]) const noexcept;
/**
 * @brief Unmarshalling
 * 
 * @details
 * L'unmarshalling è il processo di trasformazione che, partendo da un array di 64 byte, produce un oggetto
 * Sputnik::SHA512::Hashsum
 * 
 * @param [in] byteArray riferimendo ad un array di 64 byte.
 */
		void unmarshall(const uint8_t (&byteArray)[64]) noexcept;
/**
 * @brief Dimensione, in byte, di un oggetto Sputnik::SHA512::Hashsum
 */
		static const size_t size = 64;
	private:
		uint64_t byte_array[8];
	};
/**
 * @brief Calcolo dell'hashsum SHA-512
 *
 * @param [in] inputData	array di byte di cui calcolare l'hashsum
 * @param [in] dataSize		dimensione dell'array di byte
 * 
 * @return restituisce un oggetto Sputnik::SHA512::Hashsum, che contiene l'hashsum calcolato.
 */
	static Hashsum sum(const uint8_t* const inputData, uint64_t dataSize) noexcept;

private:
/**
 * @brief The 64 bits of the fractional part of the cubic root of the first 80 prime numbers.
 */
	static const uint64_t Kt[80];
/**
 * @brief 64 bits fractional part of the square root of the first 8 prime numbers.
 */
	static const uint64_t IV[8];
/**
 * @brief Right circular shift
 *
 * @param [in] word word to shift
 * @param [in] shamt shift amount
 * @return uint64_t shifted word
 */
	static inline uint64_t circularRightShift(uint64_t, uint64_t) noexcept;
/**
 * @brief Left circular shift
 *
 * @param [in] word word to shift
 * @param [in] shamt shift amount
 * @return uint64_t shifted word
 */
	static inline uint64_t circularLeftShift(uint64_t, uint64_t) noexcept;
/**
 * @brief Perform the @f$ROT_{a}(x)@f$ rotation used by the wordScheduling() function.
 *
 * @param [in] word word to be rotated
 * @return uint64_t word rotated
 */
	static inline uint64_t rotateWsA(uint64_t) noexcept;
/**
 * @brief Perform the @f$ROT_{b}(x)@f$ rotation used by the wordScheduling() function.
 *
 * @param [in] word word to be rotated
 * @return uint64_t word rotated
 */
	static inline uint64_t rotateWsB(uint64_t) noexcept;
/**
 * @brief Perform the @f$ROT_{a}(x)@f$ rotation used by the round() function.
 *
 * @param [in] word word to be rotated
 * @return uint64_t word rotated
 */
	static inline uint64_t rotateRA(uint64_t) noexcept;
/**
 * @brief Perform the @f$ROT_{b}(x)@f$ rotation used by the round() function.
 *
 * @param [in] word word to be rotated
 * @return uint64_t word rotated
 */
	static inline uint64_t rotateRB(uint64_t) noexcept;
/**
 * @brief @f$Ch(e, f , g)@f$ function used in the round() function.
 *
 * @param [in] a element at position 4 in the partial hash buffer
 * @param [in] b element at position 5 in the partial hash buffer
 * @param [in] c element at position 6 in the partial hash buffer
 * @return @f$Ch(e, f , g) = (e \:AND\: f) \:XOR\: (NOT e \:AND\: g)@f$;
 */
	static inline uint64_t Ch(uint64_t, uint64_t, uint64_t) noexcept;
/**
 * @brief @f$Maj(a, b, c)@f$ function used by the round() function.
 *
 * @param [in] a element at position 0 in the partial hash buffer
 * @param [in] b element at position 1 in the partial hash buffer
 * @param [in] c element at position 2 in the partial hash buffer
 * @return @f$Maj(a, b, c) = (a \:AND\: b) \:XOR\: (a \:AND\: c) \:XOR\: (b \:AND\: c)@f$
 */
	static inline uint64_t Maj(uint64_t, uint64_t, uint64_t) noexcept;
/**
 * @brief Computation of the total length of the data to be processed.
 *
 * @param [in] dataSize size of the data to be processed.
 * @return uint64_t total length
 */
	static uint64_t computeTotalLength(uint64_t) noexcept;
/**
 * @brief Word scheduling
 *
 * @details
 * Each round takes in input the 512 bit buffer, and updates its contents. Each round uses a 64 bit value
 * (32 in the case of SHA-256) Wt, derived from the 1024 bit block of the message being processed.
 * The first sixteen values of Wt are taken directly from the sixteen words of the current block (even in
 * the case of SHA-256 the words are sixteen, but instead of being 64-bit words, in SHA-256 the words are
 * 32-bit). The rest are defined as follows:
 * @f[W_{t} = ROT_{b}(W_{t-2}) + W_{t-7} + ROT_{a}(W_{t-15}) + W_{t-16}@f]
 * where:
 * - @f$ROT_{a}(x) = ROTR_{1}(x) \:XOR\: ROTR_{8}(x) \:XOR\: SHR_{7}(x)@f$ for SHA-512,
 *   @f$ROT_{a}(x) = ROTR_{7}(x) \:XOR\: ROTR_{18}(x) \:XOR\: SHR_{3}(x)@f$ for SHA-256
 * - @f$ROT_{b}(x) = ROTR_{19}(x) \:XOR\: ROTR_{61}(x) \:XOR\: SHR_{6}(x)@f$ for SHA-512,
 *   @f$ROT_{b}(x) = ROTR_{19}(x) \:XOR\: ROTR_{19}(x) \:XOR\: SHR_{10}(x)@f$for per SHA-256
 * - @f$ROTR_{n}(x)@f$: right circular shift of x, of n-bit;
 * - @f$SHR_{n}(x)@f$: right shift of x, with zero-padding;
 *
 * @param [in] inputBlock sixteen words of the current block being processed.
 * @param [out] Wt word scheduled.
 */
	static void wordScheduling(const uint64_t (&inputBlock)[16], uint64_t (&Wt)[80]) noexcept;
/**
 * @brief Implement all the transformation in a single round of the algorithm.
 *
 * @details
 * Each round can be expressed as follow:
 * @f[T_{1} = h + Ch(e, f , g ) + ROT_{e}(e) + W_{t} + K_{t}@f]
 * @f[T 2 = ROT_{a}(a) + Maj( a, b, c)@f]
 * @f[h = g@f]
 * @f[g = f@f]
 * @f[f = e@f]
 * @f[e = d + T_{1}@f]
 *
 * where:
 * - t: number of the round;
 * - @f$Ch(e, f , g) = (e \:AND\: f) \:XOR\: (NOT e \:AND\: g)@f$;
 * - @f$Maj(a, b, c) = (a \:AND\: b) \:XOR\: (a \:AND\: c) \:XOR\: (b \:AND\: c)@f$
 * - @f$ROT_{a}(a) = ROTR_{28}(a) \:XOR\: ROTR_{34}(a) \:XOR\: ROTR_{39}(a)@f$ for SHA-512
 *   @f$ROT_{a}(a) = ROTR_{2}(a) \:XOR\: ROTR_{13}(a) \:XOR\: ROTR_{22}(a)@f$ for SHA-256;
 * - @f$ROT_{e}(e) = ROTR_{14}(e) \:XOR\: ROTR_{18}(e) \:XOR\: ROTR_{41}(e)@f$ for SHA-512,
 *   @f$ROT_{e}(e) = ROTR_{6}(e) \:XOR\: ROTR_{11}(e) \:XOR\: ROTR_{25}(e)@f$ for SHA-256;
 * - @f$ROTR_{n}(x)@f$: right circular shift of n-bit;
 *
 * @param [in,out] buffer input/output hash buffer.
 * @param [in] Wt one of the key scheduled by the wordScheduling() function.
 * @param [in] Kt one of the cubic root of the first 64 prime numbers.
 */
	static void round(uint64_t (&buffer)[8], uint64_t, uint64_t) noexcept;
/**
 * @brief SHA-256 F-box function
 *
 * @details
 * The heart of the algorithm consists of block F of the block diagram, which performs input transformations in 80
 * rounds (64 for SHA-256). Each round takes in input the 512 (256 bits for SHA-256) bit buffer ans updates its
 * contents.
 * Each round uses a 64 bit value (32 in the case of SHA-256) Wt, derived from the 1024 bit block of the message
 * being processed. These Wt values ​​are derived using a scheduling algorithm which is implemented in the
 * wordScheduling() function.
 * Each round adds a constant Kt, of 64 bits (32 in the case of SHA-256), equal to the 64 bits of the fractional
 * part of the cubic root of the first eighty prime numbers (the first 64 prime numbers, for SHA-256). The use of
 * constants provides random 64-bit patterns that eliminate any regularity in the input block.
 *
 * @param [in] inputBlock 1024-bit (16x64-bit word) long input data block
 * @param [in] chainingVariable 512-bit (8x64-bit word) long chaining values, passed through each stage of the algorithm.
 * @param [out] hashValue 512-bit (8x64-bit word) long partial hashsum
 */
	static void fbox(const uint64_t (&inputBlock)[16], const uint64_t (&chainingVariable)[8], uint64_t (&hashValue)[8]) noexcept;
};

/**
 * @}
 */

};
#endif
