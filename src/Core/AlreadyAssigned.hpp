/**
 * @file AlreadyAssigned.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#ifndef __SPUTNIK_ALREADYASSIGNED_EXCEPTION__
#define __SPUTNIK_ALREADYASSIGNED_EXCEPTION__

#include "SafeMemory.hpp"

namespace Sputnik {
/**
 * @ingroup core
 * @{
 * @defgroup core_eccezioni Eccezioni
 * @{
 */
	
/**
 * @brief Eccezione lanciata quando si tenta di riassegnare oggetti SafeMemory.
 * 
 * Un'eccezione AlreadyAssigned viene lanciata quando si tenta di assegnare ad un oggetto di classe
 * BaseMM (o di classe derivata da BaseMM), un oggetto SafeMemory che sia già stato assegnato ad un
 * oggetto BaseMM differente.
 */
class AlreadyAssigned {
public:
	
/**
 * @brief Costruttore
 * 
 * @param [in] memory riferimento all'oggetto SafeMemory
 */
	AlreadyAssigned(SafeMemory& memory) :
		safeMemory(memory)
		{}
/**
 * @brief Restituisce il riferimento all'oggetto SafeMemory che ha causato il manifestarsi dell'eccezione.
 */
	SafeMemory& getSafeMemoryObject() const {return safeMemory;}
	
private:
/**
 * Riferimento all'oggetto SafeMemory che ha causato il manifestarsi dell'eccezione.
 */
	SafeMemory& safeMemory;
};

/**
 * @}
 * @}
 */

}

#endif
