/**
 * @file Communicator.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __SPUTNIK_COMMUNICATOR_INTERFACE__
#define __SPUTNIK_COMMUNICATOR_INTERFACE__

#include <inttypes.h>
#include <cstdlib>

#include "CommError.hpp"

namespace Sputnik {

/**
 * @ingroup core
 * @{
 * @defgroup communication Comunicazione
 * @{
 * 
 * @brief Raccoglie tutte le interfacce da implementare per la comunicazione tra le repliche.
 */

/**
 * @brief Interfaccia che tutte le classi del layer di comunicazione devono implementare.
 */
class Communicator {
public:

	virtual ~Communicator() {}
    
/**
 * @brief Invio/ricezione di un messaggio.
 * 
 * @param [in]  sendbuff	array di byte, contenente i dati da inviare;
 * @param [out] recvbuff	buffer di ricezione; deve essere preallocato e deve avere dimensione opportuna.
 * @param [in]  size		dimensione, in byte, dei dati da ricevere/inviare.
 * 
 * @warning La funzione deve risultare bloccante, in modo da risultare essere un punto di rendez-vous per
 * entrambi i sistemi.
 */
    virtual void sendRecv(const uint8_t* const sendbuff, uint8_t* const recvbuff, size_t size) = 0;
};
/**
 * @}
 * @}
 */
};

#endif
