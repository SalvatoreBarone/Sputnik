/**
 * @file Mutex.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#ifndef __SPUTNIK_MUTEX_INTERFACE__
#define __SPUTNIK_MUTEX_INTERFACE__


namespace Sputnik {
/**
 * @ingroup core
 * @{
 * @defgroup core_oslayer Os-layer
 * @{
 * 
 * @brief Interfacce delle classi dipendenti dal sistema operativo.
 * 
 * @details Il modulo raccoglie interfacce e classi la cui implementazione dipende dal particolare sistema
 * operativo utilizzato.
 * 
 * Per ognuno dei sistemi operativi è necessario fornire l'implementazione di:
 * - mutex
 * - condition-variable
 * - thread di voting
 * 
 * Se per alcuni sistemi operativi dovesse rivelarsi necessario qualche ulteriore meccanismo di sincronizzazione
 * o di multithreading, essi vanno forniti rispettando una delle tre interfacce definite da questo modulo.
 */

/**
 * @brief Interfaccia della classe Mutex.
 * 
 * @details
 * La classe definisce l'interfaccia che devono implementare le classi che forniscono le funzionalità
 * tipiche di un semaforo binario.
 */
class Mutex {
public:
/**
 * @brief Acquisizione del mutex.
 */
    virtual void lock() noexcept = 0;
/**
 * @brief Rilascio del semaforo.
 */
    virtual void unlock() noexcept = 0;
};

/**
 * @}
 * @}
 */
}


#endif
