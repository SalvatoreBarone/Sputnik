/**
 * @file VoterThread.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __SPUTNIK_ASYNCVOTERTHREAD_INTERFACE__
#define __SPUTNIK_ASYNCVOTERTHREAD_INTERFACE__

#include <stdlib.h>

namespace Sputnik {
/**
 * @ingroup core_oslayer
 * @{
 */
	
class BaseMM;

/**
 * @brief Interfaccia che tutte le classi che astraggono il task di voting devono implementare
 *
 * @details
 * Questa interfaccia viene implementata solo dai gestori della memoria che gestiscano il voting con strategia
 * asincrona (vale a dire che il voting viene effettuato su base periodica da un thread ad-hoc).
 * 
 * @note Alcuni dei parametri caratteristici del thread di voting vengono definiti usando direttive di precompilazione:
 * - il periodo del thread di voting viene definito attraverso la macro VOTER_THREAD_PERIOD
 * - la priodità del thread di voting viene definita attraverso la macro VOTER_THREAD_PRIORITY
 * - la dimensione dello stack del thread di voting viene definita attraverso la macro VOTER_THREAD_STACK_SIZE.
 * 
 * Non sempre definire il valore di suddette macro ha effetto: in Linux, ad esempio, sebbene sia possibile definire il
 * valore delle macro VOTER_THREAD_PRIORITY e VOTER_THREAD_STACK_SIZE, esse non hanno nessun effetto sulla configurazione
 * di sistema.
 */
class VoterThread {
public:
	
/**
 * @brief Costrutture
 *
 * @details
 * Il costruttore deve limitarsi alla creazione del thread. Esso deve essere lanciato usando la funzione run().
 * 
 * @param [in] manager	riferimento ad un oggetto di tipo AsyncMM, al quale corrisponde la porzione di memoria
 * 						gestita.
 * 
 * @param [in] handler	handler delle eccezioni di tipo Sputnik::VoteMismatch.
 * 						Deve essere un puntatore ad una funzione del tipo
 * 						@code
 * 							void function(void*)
 * 						@endcode
 * 						a cui è delegato il compito di gestire eventuali eccezioni generate durante le operazioni
 * 						di voting. Tale funzione viene automaticamente chiamata dalla libreria quando si manifesta
 * 						un'eccezione di tipo Sputnik::VoteMismatch. NON DEVE ESSERE NULL.
 * 
 * @param [in] args		puntatore agli argomenti da passare all'handler delle eccezioni di tipo Sputnik::VoteMismatch.
 */
    VoterThread(BaseMM& manager, void(*handler)(void*), void* args = NULL);
		
	VoterThread(const VoterThread&) = delete;
	const VoterThread& operator= (const VoterThread&) = delete;
/**
 * @brief Avvio del thread di voting.
 */
    virtual void run() = 0;
/**
 * @brief Arresto del thread di voting.
 */
	virtual void stop() = 0;
/**
 * @brief Funzione di voting
 * 
 * @details
 * La funzione non fa altro che chiamare AsyncMM::vote() e gestire l'eventuale l'eccezione Voter::VoteMismatch
 * chiamando l'handler, se impostato.
 */
	void vote() noexcept;
	
protected:
/**
 * Riferimento ad un oggetto di tipo AsyncMM, al quale corrisponde la porzione di memoria gestita.
 */
    BaseMM& asyncManager;
/**
 * Handler delle eccezioni di tipo Sputnik::VoteMismatch.
 * Deve essere un puntatore ad una funzione del tipo
 * @code
 * 	void function(void*)
 * @endcode
 * a cui è delegato il compito di gestire eventuali eccezioni generate durante le operazioni di voting. Tale
 * funzione viene automaticamente chiamata dalla libreria quando si manifesta un'eccezione di tipo
 * Sputnik::VoteMismatch.
 */
 void(*handler)(void*);
/**
 * Puntatore agli argomenti da passare all'handler delle eccezioni di tipo Sputnik::VoteMismatch.
 */
	void* handlerArgs;
};
/**
 * @}
 */
}

#endif

