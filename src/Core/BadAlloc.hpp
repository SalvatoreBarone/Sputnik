/**
 * @file BadAlloc.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */


#ifndef __SPUTNIK_BAD_ALLOCK_EXCEPTION__
#define __SPUTNIK_BAD_ALLOCK_EXCEPTION__

#include "SafeMemory.hpp"

namespace Sputnik {

/**
 * @ingroup core_eccezioni
 * @{
 */
	
/**
 * @brief Eccezione lanciata per mancanza di spazio in memoria.
 * 
 * @details
 * Viene usata per lanciare eccezioni quando non c'è spazio a sufficienza per allocare oggetti nella memoria gestita
 * da un memory manager.
 * Contiene alcune informazioni utili per il debug quali:
 *  - memoria totale disponibile;
 *  - memoria disponibile restante;
 *  - memoria richiesta;
 */
	class BadAlloc {
	public:
/**
 * @brief Costruttore della classe BaseMM::BadAlloc
 * 
 * @param [in] available memoria disponibile
 * @param [in] required memoria richiesta
 */
		BadAlloc(uint64_t available, uint64_t required) :
			totalMemory(CHUNK_SIZE * CHUNK_NUMBER),
			availableMemory(available),
			requiredMemory(required) {}
/**
 * @brief Restituisce la memoria totale disponibile.
 */
		uint64_t getTotalMemory() const {return totalMemory;}
/**
 * @brief Restituisce la memoria disponibile restante.
 */
		uint64_t getAvailableMemory() const {return availableMemory;}
/**
 * @brief Restituisce l'ammontare di memoria richiesta nel momento in cui si è generata eccezione.
 */
		uint64_t getRequiredMemory() const {return requiredMemory;}

	private:
		uint64_t totalMemory;		/**< Total available memory */
		uint64_t availableMemory;	/**< Remaining available memory */
		uint64_t requiredMemory;	/**< Required memory */
	};
	
/**
 * @}
 */
};

#endif
