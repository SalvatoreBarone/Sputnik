/**
 * @file CommError.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __COMMUNICATOR_ERROR__
#define __COMMUNICATOR_ERROR__

#include <string>

namespace Sputnik {

/**
 * @ingroup core_eccezioni
 * @{
 */

/**
 * @brief Eccezione generata durante la comunicazione tra le repliche.
 * 
 * @details
 * La classe viene usata per segnalare eventuali errori che si generano durante il processo di comunicazione.
 * È quanto più generica possibile, per potersi adattare facilmente alle diverse tecniche di comunicazione.
 */
class CommError {
public:
	
/**
 * @brief Costruttore
 * 
 * @param [in] code	codice di errore;
 * 
 * @param [in] msg messaggio descrittivo dell'errore riscontrato.
 */
	explicit CommError(int code, const std::string msg) :
		errorCode(code),
		errorMessage(msg)
		{}
/**
 * @brief Restituisce il codice di errore
 */	
	int getErrorCode() const
		{return errorCode;}
/**
 * @brief Restituisce un messaggio esplicativo per l'errore che si è manifestato
 */	
	const char* what() const
		{return errorMessage.c_str();}
		
private:
/**
 * Codice di errore
 */
	int errorCode;
/**
 * Messaggio di errore.
 */
	std::string errorMessage;
};

/**
 * @}
 */

};

#endif
