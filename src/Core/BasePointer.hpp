/**
 * @file BasePointer.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#ifndef __SPUTNIK_BASEPOINTER__
#define __SPUTNIK_BASEPOINTER__

#include "BaseMM.hpp"

namespace Sputnik {
/**
 * @ingroup core
 * @{
 * @addtogroup core_mm
 * @{
 */


/**
 * @brief Classe puntatore, governa i meccanismi di accesso alla memoria.
 *
 * @details
 * BasePointer è una classe template astratta, che definisce le metodologie di accesso alla memoria comuni alle
 * diverse classi "puntatore".
 */
template<class T>
class BasePointer {

/**
 * La classe BaseMM inizializza direttamente i membri privati di un oggetto BasePointer.
 * Il costruttore della classe BasePointer, così come quelli di tutte le altri classi puntatore, è privato,
 * per non permettere all'utente della libreria di creare oggetti puntatore.
 * L'unico modo per ottenere oggetti puntatore è usare la funzione Sputnik::BaseMM::reserve.
 */
	friend class BaseMM;
public:
	
/**
 * @brief Lettura di un oggetto dalla memoria.
 * 
 * @details
 * La funzione astratta read() è quella preposta alla lettura di un oggetto dalla memoria. A seconda della
 * particolare configurazione di Sputnik può scatenare i meccanismi di voting o fermare l'esecuzione del
 * thread chiamante fino a quando i chunk interessati dall'operazione di lettura non siano stati votati.
 * Si veda Sputnik::ALAPointer::read, Sputnik::ASAPointer::read o Sputnk::ASYNCPointer::read per informazioni
 * aggiuntive.
 * 
 * @returns riferimento (costante) all'oggetto letto.
 *
 * @exception Voter::VoteVoteMismatch se viene usata logica di voting ALAP e le operazioni di voting evidenziano
 * differenze.
 * 
 */
	virtual const T& read() const = 0;
/**
 * @brief Scrittura di un oggetto in memoria.
 * 
 * Returns a constant reference to the object managed by the Pointer object. 
 * 
 * @details
 * La funzione informa l'oggetto BaseMM "padre" che è avvenuta una scrittura sui chunk occupati dall'oggetto
 * in questione.
 * 
 * Se il voting avviene con strategia asincrona (le operazioni di voting vengono effettuate periodicamente, in
 * modo automatico, da Sputnik), prima di sporcare i chunk sarà necessario attendere il completamento di
 * eventuali operazioni di voting in corso.
 * 
 * Se il voting avviene con strategia alap, le operazioni di voting vengono scatenate quando si tenterà la lettura
 * su chunk dirty. Anche in questo caso è necessario attendere il completamento di eventuali operazioni di voting
 * in corso prima di sporcare i chunk.
 * 
 * Se il voting viene effettuato con strategia asap-aware È NECESSARIO CHIAMARE ESPLICITAMENTE la funzione
 * BaseMM::vote(). Anche in questo caso è necessario attendere il completamento di eventuali operazioni di voting
 * in corso prima di sporcare i chunk.
 * 
 * @returns riferimento all'oggetto, attraverso il quale sarà possibile effettuare le operazioni di scrittura
 * in memoria.
 * 
 * @note Quando questa funzione ritorna la scrittura non è ancora avvenuta realmente. L'esempio tipico è
 * l'uso di questa funzione per ottenere il riferimento ad un oggetto, per poi chiamare uno dei suoi membri.
 * Se si esegue
 * @code
 * puntatore.write().metodo();
 * @endcode
 * nell'istante in cui write() ritorna, l'esecuzione di metodo() non è ancora avvenuta. Lo si tenga sempre a mente
 * e si presti attenzioni alle ripercussioni che possa avere sulla particolare applicazione che si stà realizzando.
 */
	virtual T& write() {
		parent.setDirtyChunk(chunkMask);
		return t_ref;
	}
/**
 * @brief Scrttura di un oggetto in memoria.
 * 
 * @details
 * La funzione copia nella memoria l'oggetto e sporca i chunk che esso occupa. A differenza della funzione write(),
 * in questo caso la scrittura in memoria è effettuata prima che i chunk siano contrassegnati come sporchi.
 * 
 * @param [in] object oggetto da copiare in memoria.
 * 
 * @note La copia avviene in modo "rozzo", ossia l'oggetto viene copiato in memoria byte a byte.
 */
	virtual void write(const T& object) {
		copy(object);
		parent.setDirtyChunk(chunkMask);
	}

/**
 * @brief Restituisce una maschera che rappresenta quali chunk siano occupati dall'oggetto.
 * 
 * @returns maschera, in codifica one-hot, che rappresenta quali chunk siano occupati dall'oggetto:
 * se il bit n-esimo della maschera è '1', allora il chunk corrispondente è occupato dall'oggetto in
 * questione, o parte di esso.
 */
	uint64_t getChunkMask() const {return chunkMask;}

protected:
/**
 * @brief Riferimento all'oggetto BaseMM che gestisce la memoria su cui è allocato l'oggetto a cui il puntatore si
 * riferisce.
 */
	BaseMM& parent;
/**
 * @brief Riferimento all'oggetto di tipo generico T, a cui il puntatore si riferisce.
 * 
 */
	T& t_ref;
/**
 * @brief Chunk mask.
 * Maschera, in codifica one-hot, che rappresenta quali chunk siano occupati dall'oggetto:
 * se il bit n-esimo della maschera è '1', allora il chunk corrispondente è occupato dall'oggetto in
 * questione, o parte di esso.
 */
	uint64_t chunkMask;
/**
 * @brief Costrutore
 * 
 * @details
 * Il costruttore della classe è reso privato, in modo che l'unico modo per ottenere un oggetto puntatore sia
 * ricorrere ad una chiamata alla funzione BaseMM::reserve() function.
 * 
 * @param [in] p_ref 	riferimento all'oggetto BaseMM che gestisce la memoria su cui è allocato l'oggetto a cui
 * 						il puntatore si riferisce.
 * 
 * @param [in] T_ref	riferimento all'oggetto di tipo generico T, a cui il puntatore si riferisce.
 * 
 * @param [in] cs 		maschera, in codifica one-hot, che rappresenta quali chunk siano occupati dall'oggetto.
 */
	BasePointer(BaseMM& p_ref, T& T_ref, uint64_t cs) :
		parent(p_ref), t_ref(T_ref), chunkMask(cs) {}
/**
 * @brief Copia "rozza" di un oggetto.
 * 
 * @details
 * Effettua la copia byte a byte di un oggetto.
 * 
 * @param [in] object object to be copied.
 */
	void copy(const T& object) {
		uint8_t *src = (uint8_t*)&object, *dst = (uint8_t*)&t_ref;
		for (unsigned i = 0; i < sizeof(T); i++)
			dst[i] = src[i];
	}
};

/**
 * @}
 * @}
 */
};
#endif
