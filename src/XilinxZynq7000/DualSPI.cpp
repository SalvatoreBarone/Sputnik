/**
 * @file DualSPI.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "DualSPI.hpp"
#include <unistd.h>
#include <time.h>
#include <assert.h>

#ifdef __DEBUG
#include "XenomaiLogger.hpp"
#include <sstream>
#include <iomanip>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#endif

Sputnik::DualSPI::DualSPI (
			SPI::Controller masterController,
			SPI::Controller slaveController,
			SPI::SlaveSelect slaveSelect,
			SPI::ClockPolarity polarity,
			SPI::ClockPhase phase,
			SPI::Prescaler prescaler,
			SPI::SelectMode modeSelect,
			SPI::StartMode startMode) throw (CommError) :
	slave(	slaveController, 
			SPI::deviceIsSlave, 
			SPI::none, 
			polarity, 
			phase),
	master(	masterController, 
			SPI::deviceIsMaster, 
			slaveSelect, 
			polarity,
			phase,
			prescaler,
			modeSelect,
			startMode)
{
	txTime = (6 * 8 * 2<<((prescaler>>3) + 1)) + 120000;
	
	slave.enable();
// da TRM:
// Lo slave deve essere sempre abilitato prima del master.
// Devono passare almeno dieci cicli del clock di riferimento prima di abilitare.
#if defined(SPUTNIK_ON_XENOMAI) || defined(SPUTNIK_ON_LINUX)
	struct timespec sleep_time = {.tv_sec = 0, .tv_nsec = 500000000};
	nanosleep(&sleep_time, NULL);
#else
	usleep(500000);
#endif
	master.enable();
	bonjour();
		
}

	
void Sputnik::DualSPI::sendRecv(const uint8_t* const sendbuff, uint8_t* const recvbuff, size_t size) {
#ifdef __DEBUG
	Sputnik::XenomaiLogger &logger = Sputnik::XenomaiLogger::getLogger();
#endif
	
#ifdef __DEBUG	
	logger.print("sendRecv", "Setting soglie slave");
#endif	
	slave.setRxFifoThreshold(size);		// setting della soglia di ricezione per lo slave
#ifdef __DEBUG	
	logger.print("sendRecv", "Setting soglie master");
#endif
	master.setTxFifoThreshold(size);	// setting della soglia di trasmissione per il master
	master.setRxFifoThreshold(size);	// setting della soglia di ricezione per il master
	
	// se viene usata la modalità manual chip select per il master, i segnali slave select vengono
	// asseriti
	if (master.isManualChipSelectEnabled())
		master.assertSlaveSelect();
#ifdef __DEBUG	
	logger.print("sendRecv", "Riempimento coda TX master");
#endif
	master.fillTxFifo(sendbuff, size);	// riempimento della tx-fifo del master con i dati
	
	// se viene usata la modalità manual start per il master, la trasmissione viene avviata
	if (master.isManualStartEnabled())
		master.startTransmission();
#ifdef __DEBUG
	logger.print("sendRecv", "Start transmission");
#endif
	// attesa del completamento della trasmissione
	// per ogni byte trasmesso vi è un byte ricevuto, dunque l'attesa la si può implementare
	// usando il flag "rx-fifo not empty"
	while (!master.isRXFifoNotEmpty()); 
#ifdef __DEBUG
	logger.print("sendRecv", "Lettura coda RX master");
#endif
	// se viene usata la modalità manual chip select per il master, i segnali slave select vengono
	// deasseriti
	if (master.isManualChipSelectEnabled())
		master.deassertSlaveSelect();	
	master.readRxFifo(NULL, size);	// flush RX-fifo
	
#ifdef __DEBUG
	int descriptor = open ("/dev/mem", O_RDWR);
	
	uint32_t page_size = sysconf(_SC_PAGESIZE);
	uint32_t page_mask = ~(page_size-1);
	uint32_t spi0_addr = 0xe0006000 & page_mask;
	uint32_t spi1_addr = 0xe0007000 & page_mask;
	
	volatile uint32_t* spi0_vaddr = (volatile uint32_t*) 
					mmap(NULL, page_size, PROT_READ | PROT_WRITE, MAP_SHARED, descriptor, spi0_addr);
					
	volatile uint32_t* spi1_vaddr = (volatile uint32_t*) 
					mmap(NULL, page_size, PROT_READ | PROT_WRITE, MAP_SHARED, descriptor, spi1_addr);
	close(descriptor);

	std::stringstream ss;
	ss << std::endl << "spi0\t\t spi1" << std::endl;
	for (int i = 0; i < 2; i++) {
		ss 	<< i << "\t"
			<< std::hex << std::setw(8) << std::setfill('0') << (int) spi0_vaddr[i] << "\t"
			<< std::hex << std::setw(8) << std::setfill('0') << (int) spi1_vaddr[i]
			<<std::endl;
	}
	logger.print(ss.str());
	munmap((void*) spi0_vaddr, page_size);
	munmap((void*) spi1_vaddr, page_size);	
	logger.print("sendRecv", "Attendo riempimento coda RX slave");
#endif

	// attesa del completamento della ricezione sullo slave
	// per ogni byte trasmesso vi è un byte ricevuto, dunque l'attesa la si può implementare
	// usando il flag "rx-fifo not empty"
	// per mantenere sincronizzazione tra le repliche ed evitare che la replica "lenta" diventi
	// quella più valoce, alla ricezione, se la ricezione era già avvenuta, si attende un tempo
	// pari al tempo necessario a ricevere l'array di byte
	uint32_t counter = 0;
	while (!slave.isRXFifoNotEmpty()) {
		assert(counter < (1<<19) && "Counter overflow");
		assert(!slave.isModeFail() && "Mode fail!");
		counter++;
	}
	if (counter != 0) {
		struct timespec sleep_time;
		sleep_time.tv_sec = 0;
		// sleep_time.tv_nsec = txTime * size - counter / (txTime * size);
		sleep_time.tv_nsec = 362000; // questo tempo di attesa va bene solo per prescaler a 32
		nanosleep(&sleep_time, NULL);
	}
		
	
	// lettura dei dati ricevuti dal device master della replica
#ifdef __DEBUG
	logger.print("sendRecv", "Lettura coda RX slave");
#endif
	slave.readRxFifo(recvbuff, size);
	
}


void Sputnik::DualSPI::bonjour() {
	master.setTxFifoThreshold(bonjourSize);
	master.setRxFifoThreshold(bonjourSize);
	slave.setTxFifoThreshold(bonjourSize);
	slave.setRxFifoThreshold(bonjourSize);

	uint8_t tmp;
	do {
		send(bonjourA);
		tmp = recv();
	} while (tmp == noMessage);
	if (tmp == bonjourB) {
		recv();
		send(bonjourB);
	} else {
		send(bonjourB);
		do {
			tmp = recv();
			if (tmp == noMessage)
				send(bonjourA);
		} while (tmp != bonjourB);
	}
//  	send(bonjourC);
//   	do
//   		tmp = recv();
//   	while (tmp != bonjourC);
	tmp = bonjourC;
	sendRecv(&tmp, NULL, bonjourSize);
}

void Sputnik::DualSPI::send(uint8_t byte) const {
#ifdef __DEBUG
	Sputnik::XenomaiLogger &logger = Sputnik::XenomaiLogger::getLogger();

	int descriptor = open ("/dev/mem", O_RDWR);
	
	uint32_t page_size = sysconf(_SC_PAGESIZE);
	uint32_t page_mask = ~(page_size-1);
	uint32_t spi0_addr = 0xe0006000 & page_mask;
	uint32_t spi1_addr = 0xe0007000 & page_mask;
	
	volatile uint32_t* spi0_vaddr = (volatile uint32_t*) 
					mmap(NULL, page_size, PROT_READ | PROT_WRITE, MAP_SHARED, descriptor, spi0_addr);
					
	volatile uint32_t* spi1_vaddr = (volatile uint32_t*) 
					mmap(NULL, page_size, PROT_READ | PROT_WRITE, MAP_SHARED, descriptor, spi1_addr);
	close(descriptor);	
#endif

	if (master.isManualChipSelectEnabled())
		master.assertSlaveSelect();
	master.fillTxFifo(&byte, bonjourSize);
	if (master.isManualStartEnabled())
		master.startTransmission();
	while (!master.isRXFifoNotEmpty());
	if (master.isManualChipSelectEnabled())
		master.deassertSlaveSelect();	
	master.readRxFifo(NULL, bonjourSize); //flush RX fifo

#ifdef __DEBUG	
	std::stringstream ss;
	ss << "send 0x" << std::hex << std::setw(8) << std::setfill('0') << (int) byte;
	logger.print(ss.str());
	
	ss.str(std::string());
	ss << std::endl << "\tspi0\t\t spi1" << std::endl;
	for (int i = 0; i < 2; i++) {
		ss 	<< i << "\t"
			<< std::hex << std::setw(8) << std::setfill('0') << (int) spi0_vaddr[i] << "\t"
			<< std::hex << std::setw(8) << std::setfill('0') << (int) spi1_vaddr[i]
			<<std::endl;
	}
	logger.print(ss.str());
#endif
}

uint8_t Sputnik::DualSPI::recv() const {
#ifdef __DEBUG
	Sputnik::XenomaiLogger &logger = Sputnik::XenomaiLogger::getLogger();
	int descriptor = open ("/dev/mem", O_RDWR);
	
	uint32_t page_size = sysconf(_SC_PAGESIZE);
	uint32_t page_mask = ~(page_size-1);
	uint32_t spi0_addr = 0xe0006000 & page_mask;
	uint32_t spi1_addr = 0xe0007000 & page_mask;
	
	volatile uint32_t* spi0_vaddr = (volatile uint32_t*) 
					mmap(NULL, page_size, PROT_READ | PROT_WRITE, MAP_SHARED, descriptor, spi0_addr);
					
	volatile uint32_t* spi1_vaddr = (volatile uint32_t*) 
					mmap(NULL, page_size, PROT_READ | PROT_WRITE, MAP_SHARED, descriptor, spi1_addr);
	close(descriptor);	
#endif

	uint8_t tmp = noMessage;
#if defined(SPUTNIK_ON_XENOMAI) || defined(SPUTNIK_ON_LINUX)
	struct timespec sleep_time = {.tv_sec = 0, .tv_nsec = 500000000};
	nanosleep(&sleep_time, NULL);
#else
	usleep(500000);
#endif
	// controllo se ho ricevuto qualcosa
	if (slave.isRXFifoNotEmpty()) {
		slave.readRxFifo((uint8_t*)&tmp, bonjourSize);
#ifdef __DEBUG
		std::stringstream ss;
		ss 	<< "Ricevuto " << std::hex << std::setw(8) << std::setfill('0') << (int) tmp << std::endl;
		logger.print(ss.str());
#endif
	}
#ifdef __DEBUG
	else
		logger.print("Non è stato ricevuto nulla");
	std::stringstream ss;
	
	ss.str(std::string());
	ss << std::endl << "\tspi0\t\t spi1" << std::endl;
	for (int i = 0; i < 2; i++) {
		ss 	<< i << "\t"
			<< std::hex << std::setw(8) << std::setfill('0') << (int) spi0_vaddr[i] << "\t"
			<< std::hex << std::setw(8) << std::setfill('0') << (int) spi1_vaddr[i]
			<<std::endl;
	}
	logger.print(ss.str());
	
	munmap((void*) spi0_vaddr, page_size);
	munmap((void*) spi1_vaddr, page_size);	
#endif
	
	return tmp;
}


