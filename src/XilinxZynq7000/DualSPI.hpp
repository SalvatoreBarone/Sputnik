/**
 * @file DualSPI.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __XILZ7K_DUAL_XSPIPS_WRAPPER__
#define __XILZ7K_DUAL_XSPIPS_WRAPPER__

#include "../Core/Communicator.hpp"
#include "SPI.hpp"

namespace Sputnik {

/**
 * @ingroup xilz7
 * @{
 */
	
/**
 * @brief Implementazione dell'interfaccia communicator con uso di SPI
 * 
 * @details
 * DualSPI usa due device SPI (uno configurato come master, l'altro come slave), per ciascuna
 * replica, nell'ottica di rendere le repliche esattamente identiche.
 * Il device master viene usato esclusivamente per la trasmissione verso l'altra replica, mentre il
 * device slave viene usato esclusivamente per la ricezione di dati provenienti dalla replica. I
 * device master e slave sono connessi "in croce", vale a dire che il master della replica A è
 * connesso al device slave della replica B e viceversa.
 * 
 * Per poter usare il driver è necessario includere, nel progetto hardware, entrambi i device SPI
 * (SPI0 ed SPI1) di cui Zynq 7000 dispone.
 * 
 * @note
 * I device SPI integrati nel mpsoc Zynq sono "ispirate" alle Cadence Serial Peripheral Interface.
 * Tali device hanno la particolarità di poter essere usati su bus multimaster. Ciascuno dei device
 * possiede un segnale nSS (slave select, attivo basso) attraverso il quale il device può essere
 * "scelto" dal master "corrente" per la comunicazione, anche se il device "scelto" sia a sua volta
 * configurato come master.
 * 
 * @warning
 * Per poter usare i due device SPI "in croce" è necessario scegliere fin dalla fase di progetto
 * hardware quale dei due device usare come master e quale come slave. Come è possibile leggere
 * all'url
 * https://www.xilinx.com/support/answers/47511.html
 * quando il controller SPI è configurato come master, il segnale SS0 è un segnale di outpus. Il
 * segnale MIO/EMIO nSS di input non usato non deve essere asserito. Per questo motivo il segnale
 * SS0 di output non può essere usato ed il segnale nSS non usato va connesso ad una costante '1'.
 * 
 * Si tenga presente, inoltre, che sui segnali in uscita dai controller SPI vanno impostati i
 * pulldown
 * 	- master:
 * 		- MISO: pulldown;
 * 		- MOSI: pulldown;
 * 		- SCK: a seconda della polatità, se polarityLow allora pullup, se polarityHigh allora 
 * 		  pulldown;;
 * 		- SS1: pulldown;
 * 		- SS2 (se usato): pulldown;
 * 	- slave:
 * 		- MISO: pulldown;
 * 		- MOSI: pulldown;
 * 		- SCK: a seconda della polatità, se polarityLow allora pullup, se polarityHigh allora pulldown;
 * 		- SS: pulldown;
 * 
 * Per raggiungere determinati valori di throughput potrebbero essere necessarie ulteriori
 * impostazioni.
 */
class DualSPI : public Communicator {
public:

/**
 * @brief Costruttore
 * 
 * @details
 * Inizializza e configura il device, effettuando un self-test per verificare la bontà della
 * configurazione.
 * 
 * @param [in]	masterController istanza che verrà usata come master
 * 
 * @param [in]	slaveController	istanza che verrà usata come slave
 * 
 * @param [in]	slave		segnale slave select con cui sarà possibile selezionare il device slave;
 * 
 * @param [in]	polarity	polarità del segnale SCLK. Di default la polarità del clock è bassa, vale
 * 							a dire che il livello logico per clock idle è 0, mentre il livello
 * 							logico per il clock attivo è 1 (SCKLPolarityHigh).
 * 
 * @param [in]	phase		fase di campionamento delle linee dati. Di default il campionamento
 * 							avviene sul primo fronte del segnale SCKL (sampleInFirstEdge).
 * 
 * @param [in]	prescaler	impostazione per il prescaler. Questo parametro viene preso in 
 * 							considerazione se e solo se il controller è configurato come master.
 * 							Di default viene usato un valore di prescaler pari a 8.
 * 
 * @param [in]	modeSelect	asserzione/deasserzione chip select automatico/manuale. Di default il
 * 							device viene configurato in modo che venga usata la modalità con
 * 							manual chip select.
 * 
 * @param [in]	startMode	start della trasmissione automatico/manuale. Di default il device viene
 * 							configurato in modo che venga usata la modalità con manual start.
 * 
 * @exception CommError se:
 * - se l'istanza scelta è già usata (codice 0);
 * - dopo il reset del device, il registro di configurazione presenta un valore inatteso (codice 1);
 * - dopo il reset del device, il registro di stato presenta un valore inatteso (codice 2);
 * - non è possibile leggere/scrivere alcuni dei registri del device (codice 3).
 */
	DualSPI (	SPI::Controller masterController,
				SPI::Controller slaveController,
				SPI::SlaveSelect slave = SPI::none,
				SPI::ClockPolarity polarity = SPI::SCKLPolarityHigh,
				SPI::ClockPhase phase = SPI::sampleOnFirstEdge,
				SPI::Prescaler prescaler  = SPI::prescaler32,
				SPI::SelectMode modeSelect = SPI::manualChipSelectEnable,
				SPI::StartMode startMode = SPI::manualStartEnable) throw (CommError);

	virtual ~DualSPI() {}

/**
 * @brief Invio/ricezione di un messaggio.
 * 
 * @param [in]  sendbuff	array di byte, contenente i dati da inviare;
 * @param [out] recvbuff	buffer di ricezione; deve essere preallocato e deve avere dimensione
 * 							opportuna.
 * @param [in]  size		dimensione, in byte, dei dati da ricevere/inviare.
 * 
 * @note La funzione è bloccante, in modo da risultar essere un punto di rendez-vouz.
 */
    virtual void sendRecv(const uint8_t* const sendbuff, uint8_t* const recvbuff, size_t size);

private:
	SPI slave;
	SPI master;
	
	static const uint8_t bonjourA = 0x4A;
	static const uint8_t bonjourB = 0x6B;
	static const uint8_t bonjourC = 0x8C;
	static const uint8_t noMessage = 0xAA;
	static const size_t bonjourSize = 1;
	
	void bonjour();
	void send(uint8_t byte) const;
	uint8_t recv() const;
	
	uint32_t txTime;
};

/**
 * @}
 */
    
}

#endif
 

