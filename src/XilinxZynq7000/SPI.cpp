/**
 * @file SPI.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "SPI.hpp"


#if defined(SPUTNIK_ON_XENOMAI) || defined(SPUTNIK_ON_LINUX)
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include <string>
#include <sstream>
#include <iomanip>

#endif

/* *************************************************************************************************
 * Workaround per abilitare clock AMBA AXI e clock di riferimento per i device SPI0 ed SPI1
 */
#define SLCR_ADDR			0xf8000000U	// indirizzo base dei registri SLCR
#define AMBA_CLK_CRTL		0x0000012cU // offset del registro AMBA Peripheral Clock Control
#define SPI_CLK_CTRL		0x00000158U // offset del registro SPI Ref Clock Control
#define SPI_RST_CTRL		0x0000021cU // offset del registro SPI Software Reset Control
#define AMBA_SPI1_CLK_EN	0x00008000U // maschera di abilitazione per AMBA clock SPI1
#define AMBA_SPI0_CLK_EN	0x00004000U // maschera di abilitazione per AMBA clock SPI0
#define SPI0_CLK_EN			0x00000001U // maschera di abilitazione per ref. clock SPI0
#define SPI1_CLK_EN			0x00000002U // maschera di abilitazione per ref. clock SPI1
#define SPI0_RST_EN			0x00000005U // maschera di reset per SPI0
#define SPI1_RST_EN			0x0000000aU // maschera di reset per SPI1
/* ************************************************************************************************/

bool Sputnik::SPI::deviceUsed[2] = {false, false};

const uint32_t Sputnik::SPI::resetValues[] = {
	0x00020000U,	// config register reset value
	0x00000004U,	// status register reset value
	0x00000000U,	// int. en. register reset value
	0x00000000U,	// int. dis. register reset value
	0x00000000U,	// int. mask register reset value
	0x00000000U,	// enable register reset value
	0x00000000U,	// delay register reset value
	0x00000000U,	// TX-Fifo write register reset value
	0x00000000U,	// TX-fifo read register reset value
	0x000000ffU,	// Slave idle count register reset value
	0x00000001U,	// TX threashold register reset value
	0x00000001U		// RX threashold register reset value
};

Sputnik::SPI::SPI (	Controller controller,
					Mode mode,
					SlaveSelect slave,
					ClockPolarity polarity,
					ClockPhase phase,
					Prescaler prescaler,
					SelectMode modeSelect,
					StartMode startMode) throw (CommError)
{

	if (deviceUsed[(controller == SPI0 ? 0 : 1)] == true)
		throw CommError(0, "Device gia' in uso");

#if defined(SPUTNIK_ON_XENOMAI) || defined(SPUTNIK_ON_LINUX)
	int descriptor = open ("/dev/mem", O_RDWR);
	if (descriptor < 1)
		throw CommError(1, "impossibile aprire /dev/mem");
	
	uint32_t page_size = sysconf(_SC_PAGESIZE);		// dimensione della pagina
	uint32_t page_mask = ~(page_size-1);			// maschera di conversione indirizzo -> indirizzo pagina
	uint32_t page_addr = controller & page_mask;	// indirizzo della "pagina fisica" a cui è mappato il device
	
/* *************************************************************************************************
 * Workaround per abilitare clock AMBA AXI e clock di riferimento per i device SPI0 ed SPI1
 */
	uint32_t* slcr_vaddr = (uint32_t*) mmap(NULL, page_size, PROT_READ | PROT_WRITE, MAP_SHARED, descriptor, SLCR_ADDR);
	uint32_t* amba_clk_ctrl = (uint32_t*) slcr_vaddr + (AMBA_CLK_CRTL >> 2);
	uint32_t* spi_clk_ctrl = (uint32_t*) slcr_vaddr + (SPI_CLK_CTRL >> 2);
	uint32_t* spi_rst_ctrl = (uint32_t*) slcr_vaddr + (SPI_RST_CTRL >> 2);
	if (controller == SPI0) {
		*spi_rst_ctrl |= SPI0_RST_EN;		// mantiene il controller in reset durante l'attivazione
		*amba_clk_ctrl |= AMBA_SPI0_CLK_EN;	// abilita il clock amba
		*spi_clk_ctrl |= SPI0_CLK_EN;		// abilita il ref. clock
		*spi_rst_ctrl &= ~SPI0_RST_EN;		// deasserisce il reset
		
	} else {
		*spi_rst_ctrl |= SPI1_RST_EN;		// mantiene il controller in reset durante l'attivazione
		*amba_clk_ctrl |= AMBA_SPI1_CLK_EN;	// abilita il clock amba
		*spi_clk_ctrl |= SPI1_CLK_EN;		// abilita il ref. clock
		*spi_rst_ctrl &= ~SPI1_RST_EN;		// deasserisce il reset
	}
	munmap((void*) slcr_vaddr, page_size);
/* ************************************************************************************************/
	
	baseAddress = (volatile uint32_t*) mmap(NULL, page_size, PROT_READ | PROT_WRITE, MAP_SHARED, descriptor, page_addr);
	if (baseAddress == MAP_FAILED)
		throw CommError(2, "mmap fallita");
	
	// dopo aver effettuato mmap non è necessario tenere il file aperto.
	close(descriptor);
#else
	baseAddress = (uint32_t*) controller;
#endif
	
	slaveSelected = none;
	reset();
	selftest();
	reset();
	
	baseAddress[CR] = modeFailEnable| mode | polarity | phase;
	if (mode == deviceIsMaster) {
		setSlaveSelected(slave);
		baseAddress[CR] |= none | prescaler | modeSelect | startMode;
	}
	
	deviceUsed[(controller == SPI0 ? 0 : 1)] = true;
}

Sputnik::SPI::~SPI() {
#if defined(SPUTNIK_ON_XENOMAI) || defined(SPUTNIK_ON_LINUX)
	uint32_t page_size = sysconf(_SC_PAGESIZE);
	munmap((void*) baseAddress, page_size);
#endif
	deviceUsed[(baseAddress == (uint32_t*)SPI0 ? 0 : 1)] = false;
}

void Sputnik::SPI::reset() const {
	abort();
	baseAddress[CR] = resetValues[CR];
}

void Sputnik::SPI::abort() const {
	// disabilita il device
	disable();
	
	// leggo tutta la RX fifo, scartando i dati che essa contiene
	for (uint8_t i = 0; i < fifoDepth; i++) {
		uint8_t byte = baseAddress[RX];
		byte = 0;	// evita che il compilatore mostri un worning per variabile non usata.
	}
	
	// resetto la mode-fault condition
	baseAddress[SR] |= modeFail;
}

void Sputnik::SPI::setSlaveSelected(SlaveSelect slave) {
	if (isMaster()) {
		slaveSelected = slave;
		if (!isManualChipSelectEnabled())
			baseAddress[CR] = (baseAddress[CR] & ~none) | slaveSelected;
	}
}

void Sputnik::SPI::assertSlaveSelect() const {
	if (isMaster() && isManualChipSelectEnabled())
		baseAddress[CR] = (baseAddress[CR] & ~none) | slaveSelected;
}

void Sputnik::SPI::deassertSlaveSelect() const {
	if (isMaster() && isManualChipSelectEnabled())
		baseAddress[CR] |= none;
}

void Sputnik::SPI::fillTxFifo(const uint8_t* const sendbuff, size_t size) const {
	size_t nbyte = size < fifoDepth ? size : fifoDepth;
	for (size_t i = 0; i < nbyte; i++)
		baseAddress[TX] = sendbuff[i];
}

void Sputnik::SPI::readRxFifo(uint8_t* const recvbuff, size_t size) const {
	size_t nbyte = size < fifoDepth ? size : fifoDepth;
	for (size_t i = 0; i < nbyte; i++) {
		uint8_t tmp  = (uint8_t) baseAddress[RX];
		if (recvbuff != NULL)
			recvbuff[i] = tmp;
	}
}

void Sputnik::SPI::startTransmission() const {
	if (isMaster() && isManualStartEnabled())
		baseAddress[CR] |= manualStartCommandMask;
}

void Sputnik::SPI::setTxFifoThreshold(uint32_t threshold) const {
	baseAddress[TXWR] = (threshold & 0xff);
}

void Sputnik::SPI::setRxFifoThreshold(uint32_t threshold) const {
	baseAddress[RXWR] = (threshold & 0xff);
}

void Sputnik::SPI::selftest() const throw (CommError) {
	// scrivo nel registro "delays" solo per verificare che l'hardware sia effettivamente
	// presente
	baseAddress[D] = 0xA55AA55AU;

	if (baseAddress[CR] != resetValues[CR])
		throw CommError(1, "Il registro CR presenta un valore inatteso dopo il reset");
	
	if (baseAddress[SR] != resetValues[SR])
		throw CommError(2, "Il registro SR presenta un valore inatteso dopo il reset");
	
	if (baseAddress[D] != 0xA55AA55AU)
		throw CommError(3, "Errore durante la lettura/scrittura del registro delay");
	
	// Riporto D al valore di default
	baseAddress[D] = 0x00000000U;
}

