/**
 * @file SPI.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#ifndef __XIL7L_SPI_CONTROLLER_DRIVER__
#define __XIL7L_SPI_CONTROLLER_DRIVER__

#include <inttypes.h>
#include <stddef.h>

#include "../Core/CommError.hpp"

namespace Sputnik {
	
/**
 * @ingroup porting
 * @{
 * @defgroup xilz7 Xilinx Zynq 7000
 * @{
 * @brief Driver per Xilinx Zynq 7000
 */


/**
 * @brief Driver per i controller SPI su Xilinx Zynq7000
 * 
 * @details
 * Il driver è scritto con l'ottica di effettuare trasmissioni e ricezioni bloccanti, poiché l'uso
 * che se ne deve fare è consentire alle due repliche di comunicare tra loro e stabilire un punto
 * di rendez-vous ad ogni transazione di ricezione-trasmissione.
 * 
 * Il driver è concepito per essere usato sia bare-metal (senza sistema operativo, accedendo
 * direttamente agli indirizzi fisici dei device), sia su sistema operativo GNU/Linux (sia "vanilla"
 * sia con patch real-time).
 * 
 * Il driver offre solo le funzionalità strettamente necessarie ad essere usato per lo scopo per cui
 * è pensato: potrebbero essere necessarie estese modifiche affinché esso possa essere usato per
 * altri scopi.
 * 
 * @section linux_example Esempio d'uso.
 * Si supponga di disporre di un oggetto master, creato come segue
 * @code
 * Sputnik::SPI master(Sputnik::SPI::SPI0, Sputnik::SPI::deviceIsMaster);
 * @endcode
 * e di un oggetto slave, creato come segue
 * @code
 * Sputnik::SPI slave(Sputnik::SPI::SPI1, Sputnik::SPI::deviceIsSlave);
 * @endcode
 * Sia il master che lo slave così inizializzati useranno polarità del clock alta (il livello logico
 * per clock idle è 0, mentre il livello logico per il clock attivo è 1) e campionamento sul primo
 * fronte del clock.
 * Il master, inoltre, viene configurato in modalità manual start e manual slave select, il che vuol
 * sire che sarà il software a farsi carico di selezionare lo slave, avviare la trasmissione e 
 * deasserire i segnali slave select al termine.
 * 
 * Si supponga che si voglia trasferire un array di byte dal master, che inizierà la trasmissione,
 * allo slave. Per far si che lo slave sia in grado di ricevere ciò che il master gli invierà, è
 * necessario predisporlo correttamente, così come indicato di seguito:
 * @code
 * slave.enable();						// abilitazione del device
 * slave.setTxFifoThreshold(size);		// setting della soglia di trasmissione
 * slave.setRxFifoThreshold(size);		// setting della soglia di ricezione
 * slave.fillTxFifo(sendbuf, size);	// riempimendo della coda di trasmissione
 * 
 * while (!slave.isRXFifoNotEmpty());	// attesa del termine della ricezione dei dati dal master
 *										// l'attesa avviene con polling sul flag "rx-fifo not empty".
 * slave.readRxFifo(recvbuff, size);		// lettura dei dati ricevuti dal master
 * slave.disable();					// disabilitazione del device
 * @endcode
 * dove sendbuff e size, rispettivamente, rappresentano i dati da inviare e la dimensione, in byte,
 * degli stessi, mentre recvbuff è un buffer (che può essere null nel caso non si sia interessati
 * a ricevere nulla) dove verrò copiato cià che il master ha trasmesso allo slave.
 * 
 * Il master, invece, dovrà effettuare le seguenti operazioni:
 * @code
 * master.setTxFifoThreshold(size);							// impostazione soglia trasmissione
 * master.setRxFifoThreshold(size);							// impostazione soglia ricezione
 * master.setSlaveSelected(Sputnik::SPI::slave1);	// selezione dello slave
 * 
 * master.enable();						// abilitazione del device
 * if (master.isManualChipSelectEnabled())	// se si usa "manual chip select"
 * 	master.assertSlaveSelect();			// il segnale slave select viene asserito
 * master.fillTxFifo(sendbuff, size);		// viene riempita la coda di trasmissione
 * if (master.isManualStartEnabled())		// se si usa "manual start"
 * 	master.startTransmission();			// viene avviato il trasferimento
 * 
 * while (!master.isRXFifoNotEmpty());		// si attente il termine del trasferimento
 *											// l'attesa avviene in polling sulla condizione rx fifo
 *											// not empty
 * master.readRxFifo(recvbuff, size);		// viene effettuata la lettura della rx-fifo
 * if (master.isManualChipSelectEnabled())	// se si usa "manual chip select"
 * master.deassertSlaveSelect();			// il segnale slave select viene deasserito
 * master.disable();						// il device viene disabilitato
 * @endcode
 */
class SPI {

public:
/**
 * @brief Istanze di controller SPI presenti su Zynq 7000
 */
	typedef enum {
		SPI0 = 0xe0006000U,		/**< Controller SPI 0, indirizzo base */
		SPI1 = 0xe0007000U		/**< Controller SPI 1, indirizzo base */
	} Controller;

/**
 * @brief Capacità, in byte, delle TX ed RX fifo
 */
	static const size_t fifoDepth = 128;

/**
 * @brief Modalità di funzionamento
 */
	typedef enum {
		deviceIsSlave	= 0x00000000,	/**< Il device viene configurato come slave */
		deviceIsMaster	= 0x00000001	/**< Il device viene configurato come master */
	} Mode;

/**
 * @brief Polarità del segnale SCLK generato dal device master.
 * 
 * @note Questa configurazione ha senso se e solo se il controller è configurato come master.
 */
	typedef enum {
		SCKLPolarityLow = 0x00000002,	/**< SCKL è attivo-basso, idle quando '1' */
		SCKLPolarityHigh = 0x00000000	/**< SCKL è attivo-alto, idle quando '0' */
	} ClockPolarity;

/**
 * @brief Fase di campionamento dei dati.
 * 
 * @note Questa configurazione ha senso se e solo se il controller è configurato come master.
 */	
	typedef enum {
		sampleOnFirstEdge	= 0x00000000,	/**< il campionamentoavviene al primo fronte di SCKL */
		sampleOnSecondEdge	= 0x00000004	/**< il campionamentoavviene al secondo fronte di SCKL */
	} ClockPhase;

/**
 * @brief Impostazione per il prescaler.
 * 
 * @details
 * La frequenza del segnale SCLK generato dal master dipende da due parametri:
 *  - la sorgente di clock che alimenta il device;
 *  - il valore di prescaler.
 * 
 * La sorgente di clock che alimenta il device deve essere impostata nel progetto hardware (usando, 
 * in Vivado, il tool di configurazione della IP Zynq processing system). Tipicamente si tratta di
 * un segnale a 166Mhz derivado da uno dei PLL di cui Zynq dispone.
 * 
 * @note Questa configurazione ha senso se e solo se il controller è configurato come master.
 * 
 * @note Si tenga presente che la frequenza massima del segnale di clock è limitata dal MIO/EMIO
 * attraverso la quale il segnale arriva all'esterno. Usando i MIO, la frequenza può essere al
 * massimo 50MHz, mentre, usando EMIO, la frequenza massima scende a 25MHz.
 */
	typedef enum {
		prescaler4 = 	0x00000008, /**< SCKL ha frequenza pari ad 1/4 del clock di riferimento */
		prescaler8 = 	0x00000010, /**< SCKL ha frequenza pari ad 1/8 del clock di riferimento */
		prescaler16 = 	0x00000018, /**< SCKL ha frequenza pari ad 1/16 del clock di riferimento */
		prescaler32 = 	0x00000020, /**< SCKL ha frequenza pari ad 1/32 del clock di riferimento */
		prescaler64 = 	0x00000028, /**< SCKL ha frequenza pari ad 1/64 del clock di riferimento */
		prescaler128 =	0x00000030, /**< SCKL ha frequenza pari ad 1/128 del clock di riferimento */
		prescaler256 =	0x00000038  /**< SCKL ha frequenza pari ad 1/256 del clock di riferimento */
	} Prescaler;

/**
 * @brief Slave select.
 * 
 * @note Questa configurazione ha senso se e solo se il controller è configurato come master.
 */
	typedef enum {
		slave0	= 0x00000000,	/**< asserisce la linea SS0 */
		slave1	= 0x00000400,	/**< asserisce la linea SS1 */
		slave2	= 0x00000C00,	/**< asserisce la linea SS2 */
		none	= 0x00003C00	/**< deasserisce tutte le linee SS */
	} SlaveSelect;
	
/**
 * @brief Chip select automatico/manuale.
 * 
 * @details
 * Quando viene scelta la modalità di funzionamento con manual chip select, è il software ad
 * asserire il segnale SS opportuno, appena prima di avviare il trasferimento, e a deasserire lo
 * stesso non appena il trasferimento viene completato.
 * 
 * Con al modalità automatica, invece, il controller asserisce il segnale ss opportuno prima del
 * trasferimento e lo deasserisce automaticamente al termine.
 * 
 * @note Questa configurazione ha senso se e solo se il controller è configurato come master.
 */
	typedef enum {
		manualChipSelectEnable	= 0x00004000,	/**< Manual chip select */
		autoChipSelectEnable	= 0x00000000	/**< Automatic chip select */
	} SelectMode;

/**
 * @brief Start della trasmissione automatico/manuale.
 * 
 * @details
 * Quando viene scelta la modalità di funzionamento con manual start, è il software che avvia la 
 * trasmissione, scrivendo nel registro di configurazione. Il controller comincia a trasmettere non
 * appena sono disponibili dati nella TX-FIFO.
 * 
 * Con la modalità di funzionamento con start automatico, invece, il controller avvia la
 * trasmissione automaticamente non appena ci sono dati disponibili nella TX-FIFO.
 * 
 * @note Questa configurazione ha senso se e solo se il controller è configurato come master.
 */
	typedef enum {
		manualStartEnable	= 0x00008000,	/**< Manual start */
		autoStartEnable		= 0x00000000	/**< Automatic start */
	} StartMode;

/**
 * @brief Costruttore
 * 
 * @details
 * Inizializza e configura il device, effettuando un self-test per verificare la bontà della
 * configurazione.
 * 
 * @param [in]	controller	una delle due istanze di controller SPI disponibili su chip Zynq
 * 
 * @param [in]	mode		Modalità di funzionamento del device (master/slave)
 * 
 * @param [in]	slave		Slave select. Questo parametro viene preso in considerazione se e solo 
 * 							se il controller è configurato come master.
 * 
 * @param [in]	polarity	polarità del segnale SCLK. Di default la polarità del clock è bassa, vale
 * 							a dire che il livello logico per clock idle è 0, mentre il livello
 * 							logico per il clock attivo è 1 (SCKLPolarityHigh).
 * 
 * @param [in]	phase		fase di campionamento delle linee dati. Di default il campionamento
 * 							avviene sul primo fronte del segnale SCKL (sampleOnFirstEdge).
 * 
 * @param [in]	prescaler	impostazione per il prescaler. Questo parametro viene preso in 
 * 							considerazione se e solo se il controller è configurato come master.
 * 							Di default viene usato un valore di prescaler pari a 8.
 * 
 * @param [in]	modeSelect	asserzione/deasserzione chip select automatico/manuale. Di default il
 * 							device viene configurato in modo che venga usata la modalità con
 * 							manual chip select. Questo parametro viene preso in considerazione solo
 * 							se il device è configurato come master.
 * 
 * @param [in]	startMode	start della trasmissione automatico/manuale. Di default il device viene
 * 							configurato in modo che venga usata la modalità con manual start. Questo
 * 							parametro viene preso in considerazione solo se il device è configurato
 * 							come master.
 * 
 * @warning Il costruttore abilita forzatamente il clock AMBA ed il clock di riferimento del device
 * scrivendo i registri AMBA_CLK_CRTL (0xf800012c), SPI_CLK_CTRL (0xf8000158) e SPI_RST_CTRL
 * (0xf800021c)
 * 
 * @exception CommError se:
 * - se l'istanza scelta è già usata (codice 0);
 * - dopo il reset del device, il registro di configurazione presenta un valore inatteso (codice 1);
 * - dopo il reset del device, il registro di stato presenta un valore inatteso (codice 2);
 * - non è possibile leggere/scrivere alcuni dei registri del device (codice 3).
 */
	SPI (	Controller controller,
			Mode mode,
			SlaveSelect slave = none,
			ClockPolarity polarity = SCKLPolarityHigh,
			ClockPhase phase = sampleOnFirstEdge,
			Prescaler prescaler  = prescaler8,
			SelectMode modeSelect = manualChipSelectEnable,
			StartMode startMode = manualStartEnable) throw (CommError);
	
	~SPI();
	
/**
 * @brief Verifica che il device sia configurato come master.
 * 
 * @retval true se il device è configurato come master.
 * @retval false se il device non è configurato come master.
 */
	bool isMaster() const {
		return (baseAddress[CR] & deviceIsMaster) != 0;
	}
/**
 * @brief Verifica che sia abilitata la modalità manual chip select.
 * 
 * @retval true se la modalità di funzionamento manual chip select è abilitata;
 * @retval false se la modalità di funzionamento manual chip select non è abilitata;
 */
	bool isManualChipSelectEnabled() const {
		return (baseAddress[CR] & manualChipSelectEnable) != 0;
	}
/**
 * @brief Verifica che sia abilitata la modalità manual start.
 * 
 * @retval true se la modalità di funzionamento manual start è abilitata;
 * @retval false se la modalità di funzionamento manual start non è abilitata;
 */
	bool isManualStartEnabled() const {
		return (baseAddress[CR] & manualStartEnable) != 0;
	}

/**
 * @brief Verifica se si sia manifestato underflow sulla TX-FIFO
 * 
 * @retval true		se si è verificato un underflow sulla TX-FIFO
 * @retval false	se non si è verificato un underflow sulla TX-FIFO
 */
	bool isTxFifoUnderflow() const {
		return (baseAddress[SR] & txFifoUnderflow) != 0;
	}
/**
 * @brief Verifica se la TX-FIFO sia piena.
 * 
 * @details
 * Il flag "TX-FIFO full" viene asserito quando il valore di riempimento della TX-fifo raggiunge
 * il valore massimo, cioè 128 byte. Viene automaticamente deasserito dal device non appena il
 * livello di riempimento scende al di sotto di tale quantità.
 * 
 * @retval true		se la coda è piena;
 * @retval false	se la coda non è piena.
 */
	bool isTxFifoFull() const {
		return (baseAddress[SR] & txFifoFull) != 0;
	}
/**
 * @brief Verifica lo stato di riempimento della TX-FIFO.
 * 
 * @details
 * Finché il valore di riempimento della TX-fifo resta al si sotto della soglia, il flag TX-fifo not
 * full resta asserito. Nel momento in cui il valore di riempimento supera la soglia, il flag viene
 * deasserito.
 * 
 * @retval true se il valore di riempimento della fifo è sotto la soglia;
 * @retval false se il valore di riempimento della fito è sopra la soglia.
 */
	bool isTXFifoNotFull() const {
		return (baseAddress[SR] & txFifoNotFull) != 0;
	}
/**
 * @brief Verifica se la RX-FIFO sia piena.
 * 
 * @retval true		se la coda è piena;
 * @retval false	se la coda non è piena.
 */
	bool isRxFifoFull() const {
		return (baseAddress[SR] & rxFifoFull) != 0;
	}
/**
 * @brief Verifica se la RX-FIFO non sia piena.
 * 
 * @details
 * Verifica che la RX-FIFO non sia piena, ossia che il valore di riempimento sia sotto la soglia
 * impostata.
 * 
 * @retval true		se la coda non è piena;
 * @retval false	se la coda è piena.
 */	
	bool isRXFifoNotEmpty() const {
		return (baseAddress[SR] & rxFifoNotEmpty) != 0;
	}
/**
 * @brief Verifica se si sia manifestato overflow sulla RX-FIFO
 * 
 * @retval true		se si è verificato un overflow sulla RX-FIFO
 * @retval false	se non si è verificato un overflow sulla RX-FIFO
 */
	bool isRxFifoOverflow() const {
		return (baseAddress[SR] & rxFifoOverflow) != 0;
	}
/**
 * @brief Verifica se si siano manifestati errori.
 * 
 * @retval true se
 * 	- il device è configurato come master, ma il segnale SSn è asserito;
 * 	- se il device è configurato come slave, ma il segnale SSn è stato deasserito durante un
 *    trasferimento.
 * @retval false se non si sono manifestati errori.
 */
	bool isModeFail() const {
		return (baseAddress[SR] & modeFail) != 0;
	}
/**
 * @brief Reset del flag Mode-fail
 */	
	void clearModeFail() const {
		baseAddress[SR] |= modeFail;
	}
/**
 * @brief Reset del flag TX-FIFO underflow
 */
	void clearTxFifoUnderflow() const {
		baseAddress[SR] |= txFifoUnderflow;
	}
/**
 * @brief Reset del flag RX-FIFO overflow
 */
	void clearRxFifoOverflow() const {
		baseAddress[SR] |= rxFifoOverflow;
	}
/**
 * @brief Abilita il device.
 */
	void enable() const {
		baseAddress[E] |= deviceEnable;
	}
/**
 * @brief Disabilita il device.
 */
	void disable() const {
		baseAddress[E] &= ~deviceEnable;
	}
/**
 * @brief Reset del controller.
 * 
 * @details Riporta il controller al suo stato iniziale.
 */
	void reset() const;
/**
 * @brief Forza il termine di qualsiasi trasferimento in corso.
 * 
 * @details
 * Termina forzatamente il trasferimento, disabilita il device e svuota la RX fifo.
 */
	void abort() const;
/**
 * @brief Verifica che il device sia abilitato
 * 
 * @retval true se il device è abilitato;
 * @retval false se il device non è abilitato
 */
	bool isEnabled() const {
		return (baseAddress[E] & deviceEnable) != 0 ? true : false;
	}

/**
 * @brief Imposta e seleziona lo slave interlocutore per la prossima comunicazione.
 * 
 * @details
 * La funzione tiene solo traccia di quale slave dovrà essere usato, senza asserire i segnali.
 * Qualora sia il device sia usato con la modalità di selezione dello slave automatico, lo slave
 * scelto viene scritto nel registro di configurazione.
 * Quando viene scelta la modalità di funzionamento con manual chip select, è il software ad
 * asserire il segnale SS opportuno, appena prima di avviare il trasferimento, e a deasserire lo
 * stesso non appena il trasferimento viene completato, chiamando assertSlaveSelect e
 * deassertSlaveSelect, rispettivamente.
 * 
 * @note
 * Ha effetto solo se il device è configurato come master.
 * 
 * @param [in] slave slave selezionato.
 */
	void setSlaveSelected(SlaveSelect slave);
/**
 * @brief Asserisce i segnali slave select.
 * 
 * @details
 * Asserisce i segnali slave select, a seconda di quale slave sia stato precedentemente selezionato.
 * Quando viene scelta la modalità di funzionamento con manual chip select, è il software ad
 * asserire il segnale SS opportuno, appena prima di avviare il trasferimento, e a deasserire lo
 * stesso non appena il trasferimento viene completato.
 */
	void assertSlaveSelect() const;
/**
 * @brief Deasserisce i segnali slave select.
 * 
 * @details
 * Quando viene scelta la modalità di funzionamento con manual chip select, è il software ad
 * asserire il segnale SS opportuno, appena prima di avviare il trasferimento, e a deasserire lo
 * stesso non appena il trasferimento viene completato.
 */
	void deassertSlaveSelect() const;
/**
 * @brief Scrive un array di byte nella coda di trasmissione.
 * 
 * @details
 * A seconda di come sia configurato il device, questo potrebbe dar luogo all'inizio della 
 * trasmissione.
 * 
 * @param [in] sendbuff	array di byte da scrivere nella coda di trasmissione.
 * @param [in] size		numero di byte da scrivere nella coda di trasmissione.
 * 
 * @note La funzione effettua semplicemente la scrittura nella coda di trasmissione. È compito di
 * chi la usi controllare le condizioni del caso.
 * 
 * @warning se il valore di size supera 128 byte (la dimensione della fifo) solo i primi 128 byte
 * di sendbuff verranno effettivamente scritti nella fifo.
 */
	void fillTxFifo(const uint8_t* const sendbuff, size_t size) const;
	
/**
 * @brief Legge byte dalla coda di ricezione
 * 
 * @param [in,out]	recvbuff	array dove copiare i byte ricevuti.
 * @param [in]		size		numero di byte da copiare.
 * 
 * @note La funzione effettua semplicemente la lettura, senza badare a nessuna condizione. È compito
 * di chi usa la funzione assicurarsi che la ricezione sia effettivamente avvenuta.
 * 
 * @warning possono essere letti al più 128 byte, cioè un numero pari alla dimensione della coda di
 * ricezione.
 */
	void readRxFifo(uint8_t* const recvbuff, size_t size) const;
/**
 * @brief Avvia la trasmissione.
 * 
 * @details
 * La funzione ha effetto solo se il device è configurato come master ed esso è configurato affinché
 * venga usata la modalità di funzionamento con start manuale.
 */
	void startTransmission() const;
/**
 * @brief Setta il valore di soglia per la coda di trasmissione.
 * 
 * @param [in] threshold valore di soglia
 */
	void setTxFifoThreshold(uint32_t threshold) const;
/**
 * @brief Setta il valore di soglia per la coda di ricezione.
 * 
 * @param [in] threshold valore di soglia
 */
	void setRxFifoThreshold(uint32_t threshold) const;
	
	
/**
 * @brief Setta il valore del registro Slave Idle Count
 * 
 * @details
 * Il valore del registro SIC determina il ritardo, in termini di periodi del clock di riferimento
 * (quello con cui il controller è cloccato) dopo il quale, quando il controller funziona come slave,
 * il segnale SCKL può essere considerato stabile.
 * 
 * @param [in] count 	numero di periodi del clock di riferimento dopo il quale il segnale SCKL è
 * 					 	considerato stabile.
 */
	void setSlaveIdleCount(uint32_t count) const {
		baseAddress[SIC] = count & 0xf;
	}
/**
 * @brief Setta il tempo per il quale il segnale SS viene deasserito.
 * 
 * @details
 * Setta il tempo, in temini di periodi del clock di riferimento, per il quale il segnale SS viene
 * deasserito tra termine del trasferimento di una word e l'inizio del trasferimento della word
 * successiva, se viene usata fase di campionamento sampleOnFirsEdge.
 * 
 * @param [in] count	periodi del clock di riferimento per il quale il segnale SS viene deasserito
 */
	void setBackToBackSSDeassertion(uint8_t count) const {
		uint32_t Dreg = baseAddress[D] & 0x00ffffff;
		Dreg |= ((uint32_t)count) << 24;
		baseAddress[D] = Dreg;
	}
/**
 * @brief Setta il ritardo che intercorre tra deasserzione ed asserzione di due slave-select
 * 
 * @details
 * Setta il tempo, in termini di periodi del clock di riferimento, che intercorre tra la deasserzione
 * di uno dei segnali slave select e l'asserzione di un segnale slave select differente
 * 
 * @param [in] count	numero di periodi di clock tra deasserzione ed asserzione di slave-select
 */
	void setSSAssertionDelay(uint8_t count) const {
		uint32_t Dreg = baseAddress[D] & 0xff00ffff;
		Dreg |= ((uint32_t)count) << 16;
		baseAddress[D] = Dreg;
	}
/**
 * @brief Setta il tempo che intercorre tra l'ultimo bit di una word ed il primo bit della successiva
 * 
 * @details
 * Setta il tempo, in termini di periodi del clock di riferimento, che intercorre tra la trasmissione
 * dell'ultimo bit di una word e l'istante in cui viene trasmesso il primo bit della word successiva.
 * 
 * @param [in] count	numero di periodi di clock
 */
	void setWordDelay(uint8_t count) const {
		uint32_t Dreg = baseAddress[D] & 0xffff00ff;
		Dreg |= ((uint32_t)count) << 8;
		baseAddress[D] = Dreg;
	}
/**
 * @brief Setta il tempo che intercorre tra l'asserzione del segnale slave select e l'inizio della trasmissione
 * 
 * @details
 * Setta il tempo, in termini di periodi del clock di riferimento, che intercorre tra l'asserzione
 * del segnale slave select e l'inizio della trasmissione di una word.
 * 
 * @param [in] count	periodi di clock
 */
	void setStartingDelay(uint8_t count) const {
		uint32_t Dreg = baseAddress[D] & 0xffffff00;
		Dreg |= ((uint32_t)count);
		baseAddress[D] = Dreg;
	}
	
	
private:
	// device in uso
	static bool deviceUsed[2];
	
	// Indirizzo base del device controllato dall'oggetto corrente
	volatile uint32_t* baseAddress;
	
	// maschera di selezione del device slave con cui avviare la prossima comunicazione
	SlaveSelect slaveSelected;
	
	//**********************************************************************************************
	// Offset ed indici dei registri dei controller
	
	typedef enum {
		CR		= 0,	// config register
		SR		= 1,	// status register
		IE		= 2,	// interrupt enable register
		ID		= 3,	// interrupt disable
		IM		= 4,	// interrupt mask register
		E  		= 5,	// enable register
		D  		= 6,	// delay register
		TX 		= 7,	// TX-FIFO write register
		RX 		= 8,	// RX-FIFO read register
		SIC 	= 9,	// slave idle count register
		TXWR 	= 10,	// TX-FIFO threshold
		RXWR	= 11,	// RX-FIFO threshold
		DEVID	= 63	// device ID
	} RegisterIndex;
	
	// valori di default dei registri della periferica, dopo il reset 
	static const uint32_t resetValues[];
	
	//**********************************************************************************************
	// Maschere
	
	enum {
		// maschera di abilitazione del mode-fail status bit
		modeFailEnable = 0x00020000,
		
		// maschera per avviare la trasmissione quando è abilitato il manual-start
		manualStartCommandMask = 0x00010000,
	
		// Tx Fifo underflow. Quando si tenta di iniziare una trasmissione ma la TX-FIFO è vuota,
		// questo flag viene settato dal device. Per resettarlo, occorre scrivere questa maschera
		// sul registro di stato.
		txFifoUnderflow	= 0x00000040U,
		
		// Quando la RX-FIFO si riempie (sono stati ricevuti 128 byte dall'ultima volta che è stata
		// effettuata una lettura dalla fifo) questo flag viene settato.
		rxFifoFull		= 0x00000020U,
		
		// Quando il livello di riempimento della RX-FIFO supera quello del valore di soglia
		// impostato, questo bit viene asserito. Viene deasserito automaticamente dopo che il valore
		// di riempimento della coda scende sotto il valore di soglia a seguito di letture.
		rxFifoNotEmpty	= 0x00000010U,
		
		// Quando la TX-FIFO si riempie (sono stati scritti 128 byte) questo bit viene asserito.
		// Se venissero scritti altri dati nella cosa, essi verrebbero persi. Il flag viene
		// automaticamente deasserito quando il livello di riempimento della coda scende sotto un
		// valore di soglia preimpostato.
		txFifoFull		= 0x00000008U,
		
		// Questo flag viene asserito quando la TX-FIFO contiene meno byte di quanti la soglia
		// indichi. Viene deasserito quando la soglia di riempimento viene superata.
		txFifoNotFull	= 0x00000004U,
		
		// Questo flag viene asserito quando:
		//  - il voltaggio su SSn è inconsistente rispetto alla modalità di funzionamento del
		//    controller, ad esempio quando SSn è basso ed il controller è configurato come master.
		//  - se il segnale SSn torna alto, durante un trasferimento, ed il device è configurato
		//    come slave.
		// Quando questo flag viene asserito, il bit di abilitazione del device viene resettato.
		// Può essere resettato solo attraverso il reset dell'intero sistema.
		modeFail		= 0x00000002U,
		
		// Questo flag viene asserito quando la coda di ricezione è piena e viene ricevuto 
		// un ulteriore byte. Per resettare il flag occorre scrivere la maschera.
		rxFifoOverflow	= 0x00000001U,

		// abilita il device
		deviceEnable = 0x00000001U,
		
		// disabilita il device
		deviceDisable = 0x00000000U
	};
	
/*
 * @brief Chiamata subito dpo il reset, verifica che il device sia in uno stato consistente
 * 
 * @exception CommError se:
 * - dopo il reset del device, il registro di configurazione presenta un valore inatteso (codice 1);
 * - dopo il reset del device, il registro di stato presenta un valore inatteso (codice 2);
 * - non è possibile leggere/scrivere alcuni dei registri del device (codice 3).
 */
	void selftest() const throw (CommError);

};
	
/**
 * @}
 * @}
 */

}

#endif
