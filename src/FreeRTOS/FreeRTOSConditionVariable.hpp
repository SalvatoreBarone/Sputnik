/**
 * @file FreeRTOSConditionVariable.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#ifndef __SPUTNIK_FREERTOSCV_INTERFACE__
#define __SPUTNIK_FREERTOSCV_INTERFACE__

#include "../Core/ConditionVariable.hpp"
#include "FreeRTOS.h"
#include "event_groups.h"

namespace Sputnik {
	
/**
 * @ingroup freertos
 * @{
 * @defgroup freertos_oslayer OS-Layer
 * @{
 * @brief Primitive di sincronizzazione e threading per FreeRTOS
 */

/**
 * @brief Classe wrapper per condition-variable in FreeRTOS
 * 
 * @details
 * La classe FreeRTOSConditionVariable viene usata per fornire le funzionalità tipiche delle condition variable
 * per s.o. FreeRTOS.
 * Implementa l'interfaccia Sputnik::ConditionVariable.
 * 
 * FreeRTOS, attualmente, non dispone di un'implementazione delle c.v., motivo per cui la classe fornisce solamente
 * un'implementazione funzionalmente equivalente ad esse (sospensione e risveglio di thread). Tale implementazione
 * fa uso degli event-group.
 */
class FreeRTOSConditionVariable : public ConditionVariable {
public:
/**
 * @brief Costruttore
 * 
 * @details
 * Crea una nuova condition variable.
 */
    FreeRTOSConditionVariable();
	
    virtual ~FreeRTOSConditionVariable();
/**
 * @brief Sospensione per condizione attesa.
 * 
 * @param [in,out] mtx mutex acquisito per valutare la condizione da attendere.
 * 
 * @details
 * La semantica delle condition varible prevede che venga rilasciato il mutex usato per accedere al valore
 * della condizione che si attende.
 */	
    virtual void wait(Mutex & mtx) noexcept;
/**
 * @brief Risveglio di uno dei thread in attedsa di una condizione.
 * 
 * @warning Questa implementazione di signal devia da quella classica. Questa implementazione risveglia TUTTI
 * i task in attesa di una condizione.
 */
    virtual void signal() noexcept;
/**
 * @brief Risveglio di tutti i thread in attesa di una condizione.
 */   
    virtual void broadcast() noexcept;
	
private:
/**
 * Handle FreeRTOS di un event-group
 */
	EventGroupHandle_t handle;
};
      
/**
 * @}
 * @}
 */
}

#endif
 
