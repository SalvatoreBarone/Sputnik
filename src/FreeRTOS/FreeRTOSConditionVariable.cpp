/**
 * @file FreeRTOSConditionVariable.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
 
#include "FreeRTOSConditionVariable.hpp"

Sputnik::FreeRTOSConditionVariable::FreeRTOSConditionVariable() {
	handle = xEventGroupCreate();
}

Sputnik::FreeRTOSConditionVariable::~FreeRTOSConditionVariable() {
	vEventGroupDelete(handle);
}

void Sputnik::FreeRTOSConditionVariable::wait(Mutex& m) noexcept {
	m.unlock();
	xEventGroupWaitBits(
		(const EventGroupHandle_t) handle,
		0x1,
		pdTRUE,
		pdFALSE,
		portMAX_DELAY);
	m.lock();
}

void Sputnik::FreeRTOSConditionVariable::signal() noexcept {
	xEventGroupSetBits(handle, 0x01);
}

void Sputnik::FreeRTOSConditionVariable::broadcast() noexcept {
	xEventGroupSetBits(handle, 0x01);
}
