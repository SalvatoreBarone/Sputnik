/**
 * @file FreeRTOSVoterThread.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "FreeRTOSVoterThread.hpp"
#include "FreeRTOS.h"
#include "task.h"

static void voter_function(void* args) {
	Sputnik::FreeRTOSVoterThread* vt = (Sputnik::FreeRTOSVoterThread*) args;
	TickType_t nextWakeTime;
	const TickType_t blockTime = pdMS_TO_TICKS(VOTER_THREAD_PERIOD);
	nextWakeTime = xTaskGetTickCount();
	for (;;) {
		vTaskDelayUntil(&nextWakeTime, blockTime);
		vt->vote();
	}
}

Sputnik::FreeRTOSVoterThread::~FreeRTOSVoterThread() {}

void Sputnik::FreeRTOSVoterThread::run() {
	xTaskCreate(	voter_function,
					"Voter",
					(configMINIMAL_STACK_SIZE + VOTER_THREAD_STACK_SIZE),
					(void*)this,
					(tskIDLE_PRIORITY+VOTER_THREAD_PRIORITY),
					NULL);
}

void Sputnik::FreeRTOSVoterThread::stop() {}


