/**
 * @file FreeRTOSMutex.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#ifndef __SPUTNIK_FREERTOSMUTEX_INTERFACE__
#define __SPUTNIK_FREERTOSMUTEX_INTERFACE__

#include "../Core/Mutex.hpp"
#include "FreeRTOS.h"
#include "semphr.h"

namespace Sputnik {
	
/**
 * @ingroup freertos_oslayer
 * @{
 */

/**
 * @brief Classe wrapper per mutex in FreeRTOS.
 * 
 * @details
 * La classe FreeRTOSMutex viene usata per il wrapping dei mutex per s.o. FreeRTOS/Linux.
 * Implementa l'interfaccia Sputnik::Mutex.
 * 
 * @note I mutex usati nell'implementazione utilizzano il protocollo di priority-inheritance.
 */
class FreeRTOSMutex : public Mutex {
public:
/**
 * @brief Costruttore
 * 
 * @details
 * Crea un nuovo mutex.
 */  	
    FreeRTOSMutex();
	
    virtual ~FreeRTOSMutex();
/**
 * @brief Acquisizione del mutex.
 */ 
	virtual void lock() noexcept;
/**
 * @brief Rilascio del semaforo.
 */  
	virtual void unlock() noexcept;
    
private:
/**
 * Astrazione nativa di Mutex in FreeRTOS
 */
	SemaphoreHandle_t xSemaphore;
};
	
/**
 * @}
 */
}


#endif
 
