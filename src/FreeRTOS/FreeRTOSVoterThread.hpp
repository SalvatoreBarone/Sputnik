/**
 * @file FreeRTOSVoterThread.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#ifndef __SPUTNIK_FRTOSVOTERTHREAD_INTERFACE__
#define __SPUTNIK_FRTOSVOTERTHREAD_INTERFACE__

#include "../Core/VoterThread.hpp"

namespace Sputnik {
/**
 * @ingroup freertos_oslayer
 * @{
 */

/**
 * @brief Thread di voting per s.o. FreeRTOS
 *
 * @details
 * Questa classe viene fornita solo qualora la configurazione di sistema preveda il voting con strategia asincrona
 * (vale a dire che il voting viene effettuato su base periodica da un thread ad-hoc).
 * 
 * @note Alcuni dei parametri caratteristici del thread di voting vengono definiti usando direttive di precompilazione:
 * - il periodo del thread di voting viene definito attraverso la macro VOTER_THREAD_PERIOD
 * - la priodità del thread di voting viene definita attraverso la macro VOTER_THREAD_PRIORITY
 * - la dimensione dello stack del thread di voting viene definita attraverso la macro VOTER_THREAD_STACK_SIZE.
 */
class FreeRTOSVoterThread : public VoterThread {
public:
/**
 * @brief Costrutture
 * 
 * @details
 * Il costruttore si limita alla configurazione del thread. L'esecuzione dello stesso comincia
 * usando la funzione run().
 * 
 * @param [in] manager	riferimento ad un oggetto di tipo AsyncMM, al quale corrisponde la porzione di memoria
 * 						gestita.
 * 
 * @param [in] handler	handler delle eccezioni di tipo Sputnik::VoteMismatch.
 * 						Deve essere un puntatore ad una funzione del tipo
 * 						@code
 * 							void function(void*)
 * 						@endcode
 * 						a cui è delegato il compito di gestire eventuali eccezioni generate durante le operazioni
 * 						di voting. Tale funzione viene automaticamente chiamata dalla libreria quando si manifesta
 * 						un'eccezione di tipo Sputnik::VoteMismatch. NON DEVE ESSERE NULL.
 * 
 * @param [in] args		puntatore agli argomenti da passare all'handler delle eccezioni di tipo Sputnik::VoteMismatch.
 */
    FreeRTOSVoterThread(BaseMM& manager, void(*handler)(void*), void* args = nullptr) :
		Sputnik::VoterThread(manager, handler, args)
		{}
		
    virtual ~FreeRTOSVoterThread();
/**
 * @brief Avvio del thread di voting.
 * 
 * @warning La funzione crea solo il task. Esso verrà avviato quando verrà chiamata vStartScheduling().
 */
    virtual void run();
/**
 * @brief Arresto del thread di voting.
 * 
 * @warning L'implementazione è vuota. Non è previsto l'arresto del thread di voting.
 */
    virtual void stop();
};
	
/**
 * @}
 */
}


#endif



