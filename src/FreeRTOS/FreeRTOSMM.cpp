/**
 * @file FreeRTOSMM.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "FreeRTOSMM.hpp"
#include "FreeRTOSMutex.hpp"
#include "FreeRTOSVoterThread.hpp"
#include "FreeRTOSConditionVariable.hpp"

Sputnik::FreeRTOSMM::FreeRTOSMM(SafeMemory& memory, Voter& voter, void(*handler)(void*), void* args) :
	BaseMM(memory, voter, handler, args)
{
	reserveMutex = std::unique_ptr<Mutex>(new FreeRTOSMutex);
	chunkStatusMutex = std::unique_ptr<Mutex>(new FreeRTOSMutex);
#ifdef ASYNC_VOTER
	waitVoting = std::unique_ptr<ConditionVariable>(new FreeRTOSConditionVariable);
	voterThread = std::unique_ptr<VoterThread>(new FreeRTOSVoterThread(*this, handler, args));
	voterThread->run();
#endif
}
