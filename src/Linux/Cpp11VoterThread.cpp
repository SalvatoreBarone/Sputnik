/**
 * @file Cpp11VoterThread.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "Cpp11VoterThread.hpp"
#include "Voter.hpp"

#ifdef __DEBUG
#include "Logger.hpp"
#endif

const std::chrono::duration<int64_t, std::milli> Sputnik::Cpp11VoterThread::period = std::chrono::milliseconds(VOTER_THREAD_PERIOD);

Sputnik::Cpp11VoterThread::~Cpp11VoterThread() {
    execVoting = false;
	voterThread.join();
}
    
void Sputnik::Cpp11VoterThread::run() {
    voterThread = std::thread(&Cpp11VoterThread::voterFunction, this);
}

void Sputnik::Cpp11VoterThread::stop() {
    execVoting = false;
	voterThread.join();
}

void Sputnik::Cpp11VoterThread::voterFunction() {
#ifdef __DEBUG
	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print("I'm the voter thread!\n");
#endif
    while(execVoting) {
		std::this_thread::sleep_for(period);
#ifdef __DEBUG
		logger.print("Voting!\n");
#endif
		VoterThread::vote();
	}
}
