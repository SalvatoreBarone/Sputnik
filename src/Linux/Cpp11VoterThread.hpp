/**
 * @file Cpp11VoterThread.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */ 

#ifndef __SPUTNIK_Cpp11VOTERTHREAD_INTERFACE__
#define __SPUTNIK_Cpp11VOTERTHREAD_INTERFACE__

#include "../Core/VoterThread.hpp"
#include <chrono>
#include <thread>

namespace Sputnik {
	
/**
 * @ingroup linux_oslayer
 * @{
 */
    
/**
 * @brief Thread di voting per s.o. Linux
 *
 * @details
 * Questa classe viene fornita solo qualora la configurazione di sistema preveda il voting con strategia asincrona
 * (vale a dire che il voting viene effettuato su base periodica da un thread ad-hoc).
 * 
 * @note Alcuni dei parametri caratteristici del thread di voting vengono definiti usando direttive di precompilazione:
 * - il periodo del thread di voting viene definito attraverso la macro VOTER_THREAD_PERIOD
 * - la priodità del thread di voting viene definita attraverso la macro VOTER_THREAD_PRIORITY
 * - la dimensione dello stack del thread di voting viene definita attraverso la macro VOTER_THREAD_STACK_SIZE.
 * 
 * Non sempre definire il valore di suddette macro ha effetto: in Linux, ad esempio, sebbene sia possibile definire il
 * valore delle macro VOTER_THREAD_PRIORITY e VOTER_THREAD_STACK_SIZE, esse non hanno nessun effetto sulla configurazione
 * di sistema.
 */
class Cpp11VoterThread : public VoterThread {
public:
/**
 * @brief Costrutture
 *
 * @details
 * Il costruttore si limita alla configurazione del thread. L'esecuzione dello stesso comincia
 * usando la funzione run().
 * 
 * @param [in] manager	riferimento ad un oggetto di tipo AsyncMM, al quale corrisponde la porzione di memoria
 * 						gestita.
 * 
 * @param [in] handler	handler delle eccezioni di tipo Sputnik::VoteMismatch.
 * 						Deve essere un puntatore ad una funzione del tipo
 * 						@code
 * 							void function(void*)
 * 						@endcode
 * 						a cui è delegato il compito di gestire eventuali eccezioni generate durante le operazioni
 * 						di voting. Tale funzione viene automaticamente chiamata dalla libreria quando si manifesta
 * 						un'eccezione di tipo Sputnik::VoteMismatch. NON DEVE ESSERE NULL.
 * 
 * @param [in] args		puntatore agli argomenti da passare all'handler delle eccezioni di tipo Sputnik::VoteMismatch.
 */
    Cpp11VoterThread(BaseMM& manager, void(*handler)(void*), void* args = NULL) :
		VoterThread(manager, handler, args),
		execVoting(true)
		{}
		
    virtual ~Cpp11VoterThread();
/**
 * @brief Avvio del thread di voting.
 */
    virtual void run();
/**
 * @brief Arresto del thread di voting.
 */
    virtual void stop();
    
private:
/**
 * @brief Periodo del thread di voting
 */
	static const std::chrono::duration<int64_t, std::milli> period;
/**
 * @brief Flag usato per "attivare" o "disattivare" il thread di voting.
 * 
 * @details
 * Il thread di voting non viene mai fermato. Fin quando il flag è "true" le operazione di voting vengono
 * effettuate normalmente, altrimenti si passa di periodo in periodo senza che esse vengano veramente effettuate.
 */
	bool execVoting;
/**
 * @brief Thread che esegue la funzione VoterThread::vote().
 * 
 * @details
 * Il thread di voting non viene mai fermato. Fin quando il flag execVoting è "true" le operazione di voting vengono
 * effettuate normalmente, altrimenti si passa di periodo in periodo senza che esse vengano veramente effettuate.
 */
	std::thread voterThread;

/**
 * @brief Funzione che effettua le operazioni di voting.
 * 
 * @details
 * La funzione chiama VoterThread::vote() su base periodica se il flag execVoting è "true".
 */
	void voterFunction();
};

/**
 * @}
 */
        
}


#endif


