/**
 * @file Logger.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __SPUTNIK_LOGGER__
#define __SPUTNIK_LOGGER__

#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>
#include "Cpp11Mutex.hpp"

namespace Sputnik {

/**
 * @ingroup linux
 * @{
 * @defgroup linux_logging Logging
 * @{
 * @brief Funzionalità di logging su GNU/Linux
 */

class Logger {
public:
	Logger(const Logger&) = delete;
	const Logger& operator=(const Logger&) const = delete;
	const Logger& operator=(const Logger&) = delete;

	static Logger& getLogger(std::ostream&);
	static Logger& getLogger();

	void print(const std::string&);
	void print(const std::string&, const std::string&);

private:
	static Logger* instance;
	static Cpp11Mutex instanceMutex;
	Cpp11Mutex mtx;
	std::ostream& stream;
	explicit Logger(std::ostream& s) : stream(s) {}
};

/**
 * @}
 * @}
 */

};

#endif
