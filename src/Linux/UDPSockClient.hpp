/**
 * @file UDPSockClient.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __SPUTNIK_UDPSOCKETCLIENT_INTERFACE__
#define __SPUTNIK_UDPSOCKETCLIENT_INTERFACE__

#include "../Core/Communicator.hpp"


struct sockaddr_in;

namespace Sputnik {

/**
 * @ingroup linux_communicator
 * @{
 */

/**
 * @brief Client UDP
 * 
 * @details 
 * Un oggetto UDPSockClient costituisce un client UDP che permette la comunicazione con un server UDP.
 * 
 * @note L'oggetto è pensato per connessioni "uno ad uno".
 * 
 */
class UDPSockClient {
public:
/**
 * @brief Codici di errore per le eccezioni.
 */
	enum {
		socket_error,	/**< socket() ha restituito -1 */
		inet_error		/**< inet_aton() ha restituito 0 */
	};

/**
 * @brief Costruttore
 * 
 * @details
 * Costruisce un nuovo client.
 * 
 * @param [in] ipAddress	indirizzo ip del server al quale ci si vuole connettere.
 * @param [in] port			porta UDP sul quale il server remoto è in ascolto.
 * 
 * @exception CommError se:
 * - socket() restituisce un descrittore non utilizzabile (-1);
 * - inet_aton() ha restituito 0.
 */
	explicit UDPSockClient(const char* const ipAddress, int port) throw (CommError);
	virtual ~UDPSockClient();
/**
 * @brief Invia un messaggio al server
 *
 * @param [in]  sendbuff	array di byte, contenente i dati da inviare;
 * @param [in]  size		dimensione, in byte, dei dati da ricevere/inviare.
 */		
	int sendMsg(const uint8_t* const sendbuff, size_t size);
/**
 * @brief Attende la ricezione di un messaggio dal server
 *
 * @param [out] recvbuff	buffer di ricezione; deve essere preallocato e deve avere dimensione opportuna.
 * @param [in]  size		dimensione, in byte, dei dati da ricevere/inviare.
 * 
 * @note La funzione è bloccante.
 */
	int recvMsg(uint8_t* const recvbuff, size_t size);
/**
 * @brief Invio/ricezione di un messaggio.
 * 
 * @param [in]  sendbuff	array di byte, contenente i dati da inviare;
 * @param [out] recvbuff	buffer di ricezione; deve essere preallocato e deve avere dimensione opportuna.
 * @param [in]  size		dimensione, in byte, dei dati da ricevere/inviare.
 * 
 * @note La funzione è bloccante, in modo da risultar essere un punto di rendez-vouz.
 */
    virtual void sendRecv(const uint8_t* const sendbuff, uint8_t* const recvbuff, size_t size);
    
protected:
	int localSocketDescriptor;
	struct sockaddr_in *si_other;
	unsigned slen;
};

/**
 * @}
 */
};

#endif
