/**
 * @file TCPSockClient.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __SPUTNIK_TCPSOCKETCLIENT_INTERFACE__
#define __SPUTNIK_TCPSOCKETCLIENT_INTERFACE__


#include "../Core/Communicator.hpp"


namespace Sputnik {
	
/**
 * @ingroup linux_communicator
 * @{
 */

/**
 * @brief Client TCP
 * 
 * @details 
 * Un oggetto TCPSockClient costituisce un client TCP che permette la comunicazione con un server TCP.
 * 
 * @note L'oggetto è pensato per connessioni "uno ad uno".
 */
class TCPSockClient {
public:
/**
 * @brief Codici di errore per le eccezioni.
 */
	enum {
		socket_error,	/**< socket() restituisce un descrittore non utilizzabile (-1) */
		connect_error	/**< connect() non restituisce 0 */
	};
	
/**
 * @brief Costruttore
 * 
 * @details
 * Costruisce un nuovo client ed effettua una connessione al server.
 * 
 * @param [in] idAddress	indirizzo ip del server al quale ci si vuole connettere.
 * @param [in] port			porta TCP sul quale il server remoto è in ascolto.
 * 
 * @exception CommError se:
 * - socket() restituisce un descrittore non utilizzabile (-1);
 * - connect() non restituisce 0.
 */
	TCPSockClient(const char* const idAddress, int port) throw (CommError);
	
	virtual ~TCPSockClient();
/**
 * @brief Invia un messaggio al server
 *
 * @param [in]  sendbuff	array di byte, contenente i dati da inviare;
 * @param [in]  size		dimensione, in byte, dei dati da ricevere/inviare.
 */	
	int sendMsg(const uint8_t* const sendbuff, size_t size);
/**
 * @brief Attende la ricezione di un messaggio dal server
 *
 * @param [out] recvbuff	buffer di ricezione; deve essere preallocato e deve avere dimensione opportuna.
 * @param [in]  size		dimensione, in byte, dei dati da ricevere/inviare.
 * 
 * @note La funzione è bloccante.
 */
	int recvMsg(uint8_t* const recvbuff, size_t size);
/**
 * @brief Invio/ricezione di un messaggio.
 * 
 * @param [in]  sendbuff	array di byte, contenente i dati da inviare;
 * @param [out] recvbuff	buffer di ricezione; deve essere preallocato e deve avere dimensione opportuna.
 * @param [in]  size		dimensione, in byte, dei dati da ricevere/inviare.
 * 
 * @note La funzione è bloccante, in modo da risultar essere un punto di rendez-vouz.
 */
    virtual void sendRecv(const uint8_t* const sendbuff, uint8_t* const recvbuff, size_t size);
	
protected:
/**
 * @brief Descrittore del socket locale usato per connettersi al socket TCP remoto.
 */
	int remoteSocketDescriptor;
};

/**
 * @}
 */
}

#endif
