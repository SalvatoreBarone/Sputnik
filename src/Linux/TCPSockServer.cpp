/**
 * @file TCPSockServer.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "TCPSockServer.hpp"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

Sputnik::TCPSockServer::TCPSockServer(int port) throw (CommError) : 
		localSocketDescriptor(0),
		remoteSocketDescriptor(0) {

	localSocketDescriptor = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (localSocketDescriptor == -1)
		throw CommError(socket_error, "socket() returns a bad socket descriptor, server-side");

	// Once you bind a socket, the sockaddr struct you pass in doesn't need to be retained in any way.
	struct sockaddr_in server_addr;
	memset((char*)&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = INADDR_ANY;
	server_addr.sin_port = htons(port);

	if (bind(localSocketDescriptor, (struct sockaddr *)&server_addr, sizeof(server_addr)) != 0)
		throw CommError(bind_error, "bind() doesn't return 0, server-side");

	if (listen(localSocketDescriptor, 10) < 0)
		throw CommError(listen_error, "listen() returns a negative value, server-side");

	if ((remoteSocketDescriptor = accept(localSocketDescriptor, NULL, NULL)) < 0)
		throw CommError(accept_error, "accept() returns a negative valie, server side");
}

Sputnik::TCPSockServer::~TCPSockServer() {
	close(remoteSocketDescriptor);
	close(localSocketDescriptor);
}

int Sputnik::TCPSockServer::sendMsg(const uint8_t* const data, size_t size) {
	return send(remoteSocketDescriptor, data, size, 0);
}

int Sputnik::TCPSockServer::recvMsg(uint8_t* const data, size_t size) {
	return recv(remoteSocketDescriptor, data, size, 0);
}

void Sputnik::TCPSockServer::sendRecv(const uint8_t* const sendbuff, uint8_t* const recvbuff, size_t size) {
    recv(remoteSocketDescriptor, recvbuff, size, 0);   
    send(remoteSocketDescriptor, sendbuff, size, 0);
}
