/**
 * @file Cpp11ConditionVariable.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#ifndef __SPUTNIK_CPP11CV_INTERFACE__
#define __SPUTNIK_CPP11CV_INTERFACE__

#include "../Core/ConditionVariable.hpp"
#include "Cpp11Mutex.hpp"
#include <condition_variable>

namespace Sputnik {
	
/**
 * @ingroup linux_oslayer
 * @{
 */
    
/**
 * @brief Classe wrapper per condition-variable in Linux
 * 
 * @details
 * La classe Cpp11ConditionVariable viene usata per il wrapping delle condition variable per s.o. GNU/Linux.
 * Implementa l'interfaccia Sputnik::ConditionVariable.
 */
class Cpp11ConditionVariable : public ConditionVariable {
public:
/**
 * @brief Costruttore
 * 
 * @details
 * Crea una nuova condition variable.
 */
    Cpp11ConditionVariable() {}
    
    virtual ~Cpp11ConditionVariable() {}

/**
 * @brief Sospensione per condizione attesa.
 * 
 * @param [in,out] m mutex acquisito per valutare la condizione da attendere.
 * 
 * @details
 * La semantica delle condition varible prevede che venga rilasciato il mutex usato per accedere al valore
 * della condizione che si attende.
 */
    virtual void wait(Mutex &m) noexcept {
            Cpp11Mutex& cpp11m = dynamic_cast<Cpp11Mutex&>(m);
            cv.wait(cpp11m.getNativeObject());
    }
/**
 * @brief Risveglio di uno dei thread in atteda di una condizione.
 */
    virtual void signal() noexcept {
        cv.notify_one();
    }
 /**
 * @brief Risveglio di tutti i thread in atteda di una condizione.
 */   
    virtual void broadcast() noexcept {
        cv.notify_all();
    }
    
private:
    std::condition_variable_any cv; /**< c.v. wrappata */
};
        
/**
 * @}
 */

}


#endif
 
