/**
 * @file LinuxMM.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#ifndef __SPUTNIK_LINUX_MEMORY_MANAGER__
#define __SPUTNIK_LINUX_MEMORY_MANAGER__

#include "../Core/BaseMM.hpp"

namespace Sputnik {
	
/**
 * @defgroup porting Porting
 * @{
 * @brief Contiene tutti i sottomoduli per il porting su sistemi operativi ed architetture target
 * @defgroup linux Linux
 * @{
 * @brief Porting su GNU/Linux
 * @defgroup linux_mm Memory Management
 * @{
 * @brief Gestione della memoria su GNU/Linux
 */

/**
 * @brief Classe per la gestione della memoria in Linux.
 *
 * @details
 * Definisce le metodologie per riservare aree di memoria protette e le metodologie di gestione delle stesse
 * per il sistema operativo Linux quando la configurazione di sistema prevede l'uso di strategie di voting
 * alap o asap.
 * 
 * Si tratta di una classe concreta, che instanzia tutti i meccanismi di sincronizzazione e di comunicazione 
 * che dipendono dalla particolare configurazione di sistema adottata e dal sistema operativo. La classe viene
 * compilata e fornita SOLO E SOLTANTO SE la configurazione di sistema prevede l'uso di strategie di voting
 * alap o asap.
 * 
 * @warning Qualora la configurazione di sistema preveda l'uso di strategie di voting alap o asap, la classe
 * viene rinominata in MemoryManager.
 */
class LinuxMM : public BaseMM {
public:
/**
 * @brief Costrutture
 *
 * @details
 * Crea un nuovo oggetto per la gestione della memoria, assegnandogli una porzione di memoria ed un voter.
 * La porzione di memoria diviene ad uso esclusivo del manager che si sta istanziando.
 * L'oggetto voter con cui l'oggetto viene istanziato può essere condiviso tra memory manager diversi, anche se è
 * buona norma far si che ogni a new memory manager corrisponda un voter diverso.
 * 
 * @param [in] memory	riferimento ad un oggetto di tipo SafeMemory, al quale corrisponde la porzione di memoria
 * 						gestita dal manager instanziato. La porzione di memoria diviene ad uso esclusivo del
 * 						manager che si sta istanziando.
 * 
 * @param [in] voter	riferimento ad un oggetto di una classe che implementi l'interfaccia Sputnik::Voter.
 * 						L'oggetto voter con cui l'oggetto viene istanziato può essere condiviso tra memory manager
 * 						diversi, anche se è buona norma far si che ogni a new memory manager corrisponda un voter
 * 						diverso.
 * 
 * @param [in] handler	handler delle eccezioni di tipo Sputnik::VoteMismatch.
 * 						Deve essere un puntatore ad una funzione del tipo
 * 						@code
 * 							void function(void*)
 * 						@endcode
 * 						a cui è delegato il compito di gestire eventuali eccezioni generate durante le operazioni
 * 						di voting. Tale funzione viene automaticamente chiamata dalla libreria quando si manifesta
 * 						un'eccezione di tipo Sputnik::VoteMismatch.
 * 
 * @param [in] args		puntatore agli argomenti da passare all'handler delle eccezioni di tipo Sputnik::VoteMismatch.
 * 
 * @exception AlreadyAssigned si tenta di assegnare all'oggetto in costruzione un oggetto SafeMemory che sia già
 * stato assegnato ad un oggetto BaseMM differente.
 */
	LinuxMM(SafeMemory& memory, Voter& voter, void(*handler)(void*), void* args = NULL) throw (AlreadyAssigned);
	
	virtual ~LinuxMM() {}
};

/**
 * @}
 * @}
 * @}
 */

}

#endif
