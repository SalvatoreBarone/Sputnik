/**
 * @file UDPCommunicator.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __SPUTNIK_UDPCOMMUNICATOR_INTERFACE__
#define __SPUTNIK_UDPCOMMUNICATOR_INTERFACE__

#include "UDPSockServer.hpp"
#include "UDPSockClient.hpp"

struct sockaddr_in;

namespace Sputnik {
	
/**
 * @ingroup linux_communicator
 * @{
 */

/**
 * @brief Coppia client UDP e server UDP.
 * 
 * 
 * @details
 * La classe UDPCommunicator permette di rendere le due repliche assolutamente identiche dotando
 * ciascuna di esse di un server UDP ed un client UDP.
 * Consideriamo due repliche, A e B. Usando TCPCommunicator è possibile far si che
 * - il server A attende che il client B gli invii messaggi (e viceversa);
 * - il client A invia messaggi al server B (e viceversa).
 * 
 * Il server viene utilizzato esclusivamente per la ricezione di messaggi.
 * Il client esclusivamente per l'invio di messaggi.
 * 
 * @note La costruzione dell'oggetto UDPCommunicator costituisce il primo punto di rendez-vous tra
 * le repliche, le quali si attenderanno l'un l'altra prima di procedere nella sequenza di operazioni
 * da loro ottemperate.
 */
class UDPCommunicator : public Communicator {
public:
/**
 * @brief Costruttore
 * 
 * @details
 * Costruisce una coppia client-server UDP.
 * 
 * @param [in] localPort	porta UDP sul quale il "server locale" sarà in ascolto
 * @param [in] remoteIP		indirizzo ip sul quale si trova il "server remoto"
 * @param [in] remotePort	porta UDP sul quale il "server remoto" sarà in ascolto
 * 
 * @exception CommError se:
 * - socket() restituisce un descrittore non utilizzabile (-1), lato client;
 * - inet_aton() ha restituito 0, lato client.
 * - socket() restituisce un descrittore errato (-1), lato server;
 * - bind() non restituisce 0, lato server;
 */
	UDPCommunicator(	int localPort,
						const char* const remoteIP,
						int remotePort) throw (CommError);
								
	virtual ~UDPCommunicator();
/**
 * @brief Bonjour! Sincronizzazione iniziale tra le repliche.
 * 
 * @details
 * Lo scopo di questa funzione è realizzare meccanismi di sincronizzazione iniziale tra le repliche,
 * in modo che si attendano a vicenda fino a quando entrambe non siano pronte per inziare la loro
 * sequenza di operazioni.
 */
	
/**
 * @brief Invio/ricezione di un messaggio.
 * 
 * @param [in]  sendbuff	array di byte, contenente i dati da inviare;
 * @param [out] recvbuff	buffer di ricezione; deve essere preallocato e deve avere dimensione
 * 							opportuna.
 * @param [in]  size		dimensione, in byte, dei dati da ricevere/inviare.
 * 
 * @note La funzione è bloccante, in modo da risultar essere un punto di rendez-vouz.
 */
    virtual void sendRecv(const uint8_t* const sendbuff, uint8_t* const recvbuff, size_t size);
	
private:
	UDPSockServer server;
	UDPSockClient client;
};


/**
 * @}
 */
};

#endif
