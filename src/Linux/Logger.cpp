/**
 * @file Logger.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "Logger.hpp"
#include <iostream>
#include <unistd.h>
#include <sys/syscall.h>
#include <chrono>

#define TERM_COLOR_DEFAULT	"\e[0m"
#define TERM_COLOR_RED		"\e[1;31m"
#define TERM_COLOR_GREEN	"\e[1;32m"
#define TERM_COLOR_YELLOW	"\e[1;33m"
#define TERM_COLOR_BLUE		"\e[1;34m"
#define TERM_COLOR_MAGENTA	"\e[1;35m"
#define TERM_COLOR_CYAN		"\e[1;36m"

Sputnik::Logger *Sputnik::Logger::instance = 0;
Sputnik::Cpp11Mutex Sputnik::Logger::instanceMutex;

Sputnik::Logger& Sputnik::Logger::getLogger(std::ostream& stream) {
	instanceMutex.lock();
	if (instance == 0) 
		instance = new Logger(stream);
	instanceMutex.unlock();
	return *instance;
}

Sputnik::Logger& Sputnik::Logger::getLogger() {
	instanceMutex.lock();
	if (instance == 0)
		instance = new Logger(std::cout);
	instanceMutex.unlock();
	return *instance;
}

void Sputnik::Logger::print(const std::string& str) {
	mtx.lock();
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	stream	<< TERM_COLOR_MAGENTA << t1.time_since_epoch().count() << TERM_COLOR_DEFAULT << ", "
			<< TERM_COLOR_MAGENTA << syscall(SYS_gettid) << TERM_COLOR_DEFAULT << ", "  << str;
	mtx.unlock();
}

void Sputnik::Logger::print(const std::string& thread_name, const std::string& str) {
	mtx.lock();
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	stream << TERM_COLOR_MAGENTA << t1.time_since_epoch().count() << TERM_COLOR_DEFAULT << ", "
	<< TERM_COLOR_MAGENTA << syscall(SYS_gettid) << TERM_COLOR_DEFAULT << ", " 
	<< TERM_COLOR_BLUE << thread_name << TERM_COLOR_DEFAULT << ", "  << str << std::endl;
	mtx.unlock();
}
