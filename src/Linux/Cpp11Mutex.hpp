/**
 * @file Cpp11Mutex.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#ifndef __SPUTNIK_CPP11MUTEX_INTERFACE__
#define __SPUTNIK_CPP11MUTEX_INTERFACE__

#include "../Core/Mutex.hpp"
#include <mutex>

namespace Sputnik {
	
/**
 * @ingroup linux
 * @{
 * @defgroup linux_oslayer OS-layer
 * @{
 * @brief Primitive di sincronizzazione e threading per GNU/Linux
 */
    

/**
 * @brief Classe wrapper per mutex in Linux.
 * 
 * @details
 * La classe Cpp11Mutex viene usata per il wrapping dei mutex per s.o. GNU/Linux.
 * Implementa l'interfaccia Sputnik::Mutex.
 */
class Cpp11Mutex : public Mutex {
public:
/**
 * @brief Costruttore
 * 
 * @details
 * Crea un nuovo mutex.
 */  
    Cpp11Mutex() {}
    
    virtual ~Cpp11Mutex() {}
/**
 * @brief Acquisizione del mutex.
 */    
    virtual void lock() noexcept {
        mtx.lock();
    }
/**
 * @brief Rilascio del semaforo.
 */    
    virtual void unlock() noexcept {
        mtx.unlock();
    }
    
    std::mutex& getNativeObject() {return mtx;}
    
private:
    std::mutex mtx;
};
        
/**
 * @}
 * @}
 */

}


#endif
 
