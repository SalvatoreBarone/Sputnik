/**
 * @file UDPSockServer.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "UDPSockServer.hpp"
#include <arpa/inet.h>
#include <sys/socket.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

Sputnik::UDPSockServer::UDPSockServer(int port) throw (CommError) {
	struct sockaddr_in si_me;
	si_other = new struct sockaddr_in;
	slen = sizeof(struct sockaddr_in);
	
	//create a UDP socket
	localSocketDescriptor = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (localSocketDescriptor == -1)
		throw CommError(socket_error, "socket() ha restituito -1");

	memset((char *) &si_me, 0, sizeof(si_me));
	si_me.sin_family = AF_INET;
	si_me.sin_port = htons(port);
	si_me.sin_addr.s_addr = htonl(INADDR_ANY);
		
	//bind socket to port
	if (bind(localSocketDescriptor, (struct sockaddr*)&si_me, sizeof(si_me)) == -1)
		throw CommError(bind_error, "bind() ha restituito -1");
}

Sputnik::UDPSockServer::~UDPSockServer() {
	close(localSocketDescriptor);
	delete si_other;
}

int Sputnik::UDPSockServer::sendMsg(const uint8_t* const data, size_t size) {
	return sendto(localSocketDescriptor, data, size, 0, (struct sockaddr*)si_other, slen);
}

int Sputnik::UDPSockServer::recvMsg(uint8_t* const data, size_t size) {
	return recvfrom(localSocketDescriptor, data, size, 0, (struct sockaddr*)si_other, &slen);
}

void Sputnik::UDPSockServer::sendRecv(const uint8_t* const sendbuff, uint8_t* const recvbuff, size_t size) {
	recvMsg(recvbuff, size);
	sendMsg(sendbuff, size);
}
