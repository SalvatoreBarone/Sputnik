/**
 * @file UDPSockServer.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __SPUTNIK_UDPSOCKSERVER_INTERFACE__
#define __SPUTNIK_UDPSOCKSERVER_INTERFACE__

#include "../Core/Communicator.hpp"

struct sockaddr_in;

namespace Sputnik {
	
/**
 * @ingroup linux_communicator
 * @{
 */

/**
 * @brief Server UDP
 * 
 * @details 
 * Un oggetto UDPSockServer costituisce un server UDP che permette la comunicazione con un client.
 * 
 * @note L'oggetto è pensato per connessioni "uno ad uno", nel senso che il server è in grado di
 * servire un solo client.
 */
class UDPSockServer {
public:
/**
 * @brief Codici di errore per le eccezioni.
 */
	enum {
		socket_error,	/**< socket() restituisce un descrittore errato (-1) */
		bind_error		/**< bind() non restituisce 0 */
	};
/**
 * @brief Costruttore
 *
 * @details
 * Crea il server e mette in ascolto il thread per connessioni provenienti da un client.
 * 
 * @note L'oggetto è pensato per connessioni "uno ad uno", nel senso che il server è in grado di
 * servire un solo client.
 * 
 * @param [in] port porta UDP sul quale il server sarà in ascolto.
 *
 * @exception CommError se:
 * - socket() restituisce un descrittore errato (-1);
 * - bind() non restituisce 0;
 */
	explicit UDPSockServer(int port) throw (CommError);
	
	virtual ~UDPSockServer();
/**
 * @brief Invia un messaggio al client
 *
 * @param [in]  sendbuff	array di byte, contenente i dati da inviare;
 * @param [in]  size		dimensione, in byte, dei dati da ricevere/inviare.
 */		
	int sendMsg(const uint8_t* const sendbuff, size_t size);
/**
 * @brief Attende la ricezione di un messaggio dal client
 *
 * @param [out] recvbuff	buffer di ricezione; deve essere preallocato e deve avere dimensione opportuna.
 * @param [in]  size		dimensione, in byte, dei dati da ricevere/inviare.
 * 
 * @note La funzione è bloccante.
 */
	int recvMsg(uint8_t* const recvbuff, size_t size);
/**
 * @brief Invio/ricezione di un messaggio.
 * 
 * @param [in]  sendbuff	array di byte, contenente i dati da inviare;
 * @param [out] recvbuff	buffer di ricezione; deve essere preallocato e deve avere dimensione opportuna.
 * @param [in]  size		dimensione, in byte, dei dati da ricevere/inviare.
 * 
 * @note La funzione è bloccante, in modo da risultar essere un punto di rendez-vouz.
 */
    virtual void sendRecv(const uint8_t* const sendbuff, uint8_t* const recvbuff, size_t size);
    
protected:
	int localSocketDescriptor;
	struct sockaddr_in *si_other;
	unsigned slen;
};

/**
 * @}
 */

};

#endif
