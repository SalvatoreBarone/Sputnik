/**
 * @file TCPSockClient.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "TCPSockClient.hpp"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


Sputnik::TCPSockClient::TCPSockClient(const char* const host, int port) throw (CommError) {
	remoteSocketDescriptor = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (remoteSocketDescriptor == -1)
		throw CommError(socket_error, "socket() returns a bad socket descriptor");

	struct sockaddr_in server_address;
	memset((char*)&server_address, 0, sizeof(server_address));
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(port);
	server_address.sin_addr.s_addr = inet_addr(host);

	if (connect(remoteSocketDescriptor, (struct sockaddr *)&server_address, sizeof(server_address)) != 0)
		throw CommError(connect_error, "connect() doesn't return 0");
}

Sputnik::TCPSockClient::~TCPSockClient() {
	close(remoteSocketDescriptor);
}

int Sputnik::TCPSockClient::sendMsg(const uint8_t* const data, size_t size) {
	return send(remoteSocketDescriptor, data, size, 0);
}

int Sputnik::TCPSockClient::recvMsg(uint8_t* const data, size_t size) {
	return recv(remoteSocketDescriptor, data, size, 0);
}

void Sputnik::TCPSockClient::sendRecv(const uint8_t* const sendbuff, uint8_t* const recvbuff, size_t size) {
    send(remoteSocketDescriptor, sendbuff, size, 0);
	recv(remoteSocketDescriptor, recvbuff, size, 0);   
}
