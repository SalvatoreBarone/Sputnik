/**
 * @file TCPCommunicator.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "TCPCommunicator.hpp"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

Sputnik::TCPCommunicator::TCPCommunicator(	int localPort,
											const char* const remoteIPAddress,
											int remotePort) throw (CommError) :
	serverLocalDescriptor(0),
	serverRemoteDescriptor(0),
	clientDescriptor(0)
{
	// configurazione della socket server locale
	serverLocalDescriptor = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (serverLocalDescriptor == -1)
		throw CommError(socket_error, "socket() returns a bad socket descriptor, server-side");

	struct sockaddr_in server_addr;
	memset((char*)&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = INADDR_ANY;
	server_addr.sin_port = htons(localPort);

	if (bind(serverLocalDescriptor, (struct sockaddr *)&server_addr, sizeof(server_addr)) != 0)
		throw CommError(bind_error, "bind() doesn't return 0, server-side");
	if (listen(serverLocalDescriptor, 10) < 0)
		throw CommError(listen_error, "listen() returns a negative value, server-side");
	
	
	// configurazione della socket client locale
	clientDescriptor = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (clientDescriptor == -1)
		throw CommError(socket_client, "socket() returns a bad socket descriptor");

	struct sockaddr_in server_address;
	memset((char*)&server_address, 0, sizeof(server_address));
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(remotePort);
	server_address.sin_addr.s_addr = inet_addr(remoteIPAddress);

	if (connect(clientDescriptor, (struct sockaddr *)&server_address, sizeof(server_address)) != 0)
		throw CommError(connect_error, "connect() doesn't return 0");
	
	// attesa di connessioni client
	if ((serverRemoteDescriptor = accept(serverLocalDescriptor, NULL, NULL)) < 0)
		throw CommError(accept_error, "accept() returns a negative valie, server side");
}

Sputnik::TCPCommunicator::~TCPCommunicator() {
	close(serverRemoteDescriptor);
	close(serverLocalDescriptor);
	close(clientDescriptor);
}

void Sputnik::TCPCommunicator::sendRecv(const uint8_t* const sendbuff, uint8_t* const recvbuff, size_t size) {
	send(clientDescriptor, sendbuff, size, 0);
	recv(serverRemoteDescriptor, recvbuff, size, 0);
}

