/**
 * @file TCPCommunicator.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#ifndef __SPUTNIK_TCPCOMMUNICATOR_INTERFACE__
#define __SPUTNIK_TCPCOMMUNICATOR_INTERFACE__

#include "TCPSockServer.hpp"
#include "TCPSockClient.hpp"

namespace Sputnik {
	
/**
 * @ingroup linux
 * @{
 * @defgroup linux_communicator Comunicazione
 * @{
 * @brief Primitive di comunicazione per GNU/Linux
 */

/**
 * @brief TCPCommunicator
 * 
 * @details
 * Usare le classi Sputnik::TCPSockServer e Sputnik::TCPSockClient per far comunicare le due repliche del sistema
 * è formalmente non corretto, in quanto le due repliche non sarebbero perfettamente identiche. La classe TCPCommunicator
 * permette di rendere le due repliche assolutamente identiche dotando ciascuna di esse di un server TCP ed un client TCP.
 * Consideriamo due repliche, A e B. Usando TCPCommunicator è possibile far si che
 * - il server A attende che il client B instanzi una connessione verso di esso (e viceversa);
 * - il client A instaura una connessione con il server B (e viceversa).
 * 
 * Il server viene utilizzato esclusivamente per la ricezione di messaggi. Il client esclusivamente per l'invio di messaggi.
 */
class TCPCommunicator : public Communicator {
public:
	
/**
 * @brief Codici di errore per le eccezioni.
 */
	enum {
		socket_error,	/**< socket() restituisce un descrittore errato (-1), server side */
		bind_error,		/**< bind() non restituisce 0, server side */
		listen_error,	/**< listen() restituisce un valore negativo, server side */
		accept_error,	/**< accept() restituisce un valore negativo, server side */
		
		socket_client,	/**< socket() restituisce un descrittore non utilizzabile (-1), client side */
		connect_error	/**< connect() non restituisce 0, client side */
	};
	
/**
 * @brief Costruttore
 * 
 * @details
 * Costruisce una coppia client-server TCP.
 * 
 * @param [in] localPort	porta TCP sul quale il "server locale" sarà in ascolto
 * @param [in] remoteIP		indirizzo ip sul quale si trova il "server remoto"
 * @param [in] remotePort	porta TCP sul quale il "server remoto" sarà in ascolto
 * 
 * @exception CommError se:
 * - socket() restituisce un descrittore errato (-1), lato server;
 * - bind() non restituisce 0, lato server;
 * - listen() restituisce un valore negativo, lato server;
 * - accept() restituisce un valore negativo, lato server;
 * - socket() restituisce un descrittore non utilizzabile (-1), lato client;
 * - connect() non restituisce 0, lato client.
 */
	TCPCommunicator(	int localPort,
						const char* const remoteIP,
						int remotePort) throw (CommError);
						
	virtual ~TCPCommunicator();
	
/**
 * @brief Invio/ricezione di un messaggio.
 * 
 * @param [in]  sendbuff	array di byte, contenente i dati da inviare;
 * @param [out] recvbuff	buffer di ricezione; deve essere preallocato e deve avere dimensione opportuna.
 * @param [in]  size		dimensione, in byte, dei dati da ricevere/inviare.
 * 
 * @note La funzione è bloccante, in modo da risultar essere un punto di rendez-vouz.
 */
    virtual void sendRecv(const uint8_t* const sendbuff, uint8_t* const recvbuff, size_t size);
	
private:
/**
 * Descrittore della socket su cui il server locale è in ascolto
 */
	int serverLocalDescriptor;
/**
 * Descrittore della socket del client connesso al server locale
 */
	int serverRemoteDescriptor;
/**
 * Descrittore della socket del client locale
 */
	int clientDescriptor;
	
};



/**
 * @}
 * @}
 */
};

#endif
