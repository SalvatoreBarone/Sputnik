/**
 * @file LinuxMM.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */
#include "LinuxMM.hpp"
#include "Cpp11Mutex.hpp"
#include "Cpp11VoterThread.hpp"
#include "Cpp11ConditionVariable.hpp"

Sputnik::LinuxMM::LinuxMM(SafeMemory& memory, Voter& voter, void(*handler)(void*), void* args) throw (AlreadyAssigned) : 
	BaseMM(memory, voter, handler, args)
{
	reserveMutex = std::unique_ptr<Mutex>(new Cpp11Mutex);
	chunkStatusMutex = std::unique_ptr<Mutex>(new Cpp11Mutex);
	
#ifdef ASYNC_VOTER
	waitVoting = std::unique_ptr<ConditionVariable>(new Cpp11ConditionVariable);
	voterThread = std::unique_ptr<VoterThread>(new Cpp11VoterThread(*this, handler, args));
	voterThread->run();
#endif
}
	
