#include <sys/mman.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>

#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>
using namespace std;


int main (int argc, char** argv) {

	if (argc != 2 && argc != 3) {
		cout	<< "Uso: " << endl
				<< "\t- lettura:\tmemrw indirizzo" << endl
				<< "\t- scrittura:\tmemrw indirizzo valore" << endl;
		return -1;
	}
	
	uint32_t addr = 0;
	uint32_t value = 0;
	
	int descr = 0;									// descrittore del file /dev/mem
	uint32_t page_size = sysconf(_SC_PAGESIZE);		// dimensione della pagina
	uint32_t page_mask = ~(page_size-1);			// maschera di conversione indirizzo -> indirizzo pagina
	uint32_t page_addr = 0;							// indirizzo della "pagina fisica" a cui è mappato il device
	uint32_t offset = 0;							// offset del device rispetto all'indirizzo della pagina
	
	void* vrt_page_addr = 0;
	uint32_t* vrt_addr = 0;
	
	descr = open ("/dev/mem", O_RDWR);
	if (descr < 1) {
		cout << "Impossibile aprire /dev/mem" << endl;
		return -1;
	}

	addr = strtoul(argv[1], NULL, 0);
	page_addr = addr & page_mask;
	offset = addr - page_addr;
	
	vrt_page_addr = mmap(NULL, page_size, PROT_READ | PROT_WRITE, MAP_SHARED, descr, page_addr);
	if (vrt_page_addr == MAP_FAILED) {
		cout << "Mapping indirizzo fisico - indirizzo virtuale FALLITO!" << endl;
		close(descr);
		return -1;
	}
	vrt_addr = (uint32_t*) vrt_page_addr + (offset >> 2);
	
#ifdef __DEBUG	
	cout	<< "page size: " << page_size << std::endl
			<< "page_mask: " << std::hex << std::setw(8) << std::setfill('0') << (int) page_mask << std::endl
			<< "page_addr: " << std::hex << std::setw(8) << std::setfill('0') << (int) page_addr << std::endl
			<< "offset:    " << std::hex << std::setw(8) << std::setfill('0') << (int) offset << std::endl
			<< "virt_page: " << std::hex << std::setw(8) << std::setfill('0') << (int) vrt_page_addr << std::endl
			<< "virt_addr: " << std::hex << std::setw(8) << std::setfill('0') << (int) vrt_addr << std::endl;
#endif	
	
	if (argc == 3)
		*vrt_addr =  strtoul(argv[2], NULL, 0);
	
	cout 	<< std::hex << std::setw(8) << std::setfill('0') << vrt_addr << " = "
			<< std::hex << std::setw(8) << std::setfill('0') << (int) *vrt_addr << std::endl;
	
	close(descr);
	munmap(vrt_page_addr, page_size);
	return 0;
	
	
}
