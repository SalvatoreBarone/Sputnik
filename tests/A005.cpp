/**
 * @file A005.cpp
 * 
 * @test A005
 * The hashsum of Giacomo Leopardi's poem "L'infinito" is calculated. The calculated hashsum is compared to that
 * obtained from the GNU implementation of SHA256 (sha256sum). The intent is to calculate the hashsum of a string
 * of considerable size.
 */
#include "Sputnik.hpp"
#include "test.hpp"

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	const uint32_t expectedHashsumArray[8] = {
		0x251f34f7, 0x73895b95, 0x7b998ad0, 0x3c719190, 0xab1013a5, 0x8cc98d6e, 0xb5dca712, 0x1216164e
	};
	Sputnik::SHA256::Hashsum expectedHashsum;
	expectedHashsum.unmarshall((const uint8_t(&)[32])expectedHashsumArray);

	std::string data = "Sempre caro mi fu quest'ermo colle, E questa siepe, che da tanta parte Dell'ultimo orizzonte il guardo esclude. Ma sedendo e mirando, interminati Spazi di là da quella, e sovrumani Silenzi, e profondissima quiete Io nel pensier mi fingo; ove per poco Il cor non si spaura. E come il vento Odo stormir tra queste piante, io quello Infinito silenzio a questa voce Vo comparando: e mi sovvien l'eterno, E le morte stagioni, e la presente E viva, e il suon di lei. Così tra questa Immensità s'annega il pensier mio: E il naufragar m'è dolce in questo mare.";
	Sputnik::SHA256::Hashsum computedHashsum = Sputnik::SHA256::sum((const uint8_t*)data.c_str(), data.size());

	assert(computedHashsum == expectedHashsum);

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
