/**
 * @file C003.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 */
#include "Sputnik.hpp"
#include "test.hpp"

#include <thread> 

void client();
void server();

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	thread server_thr(server);
	thread client_thr(client);

	server_thr.join();
	client_thr.join();

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}

void client() {
	try {
		Sputnik::TCPCommunicator communicator(1334, "127.0.0.1", 1335);
		const size_t size = 4;
		const int num = 3;
		uint8_t recvBuffer[size];
		for (unsigned i = 0; i < size; i++)
			recvBuffer[i] = 0;
		communicator.sendRecv((const uint8_t*)&num, recvBuffer, size);
		
		assert (*((int*)recvBuffer) == num);
	} catch (Sputnik::CommError& e) {
		std::cout << "Bad Connection" << std::endl;
		std::cout << e.what() << std::endl;
		assert(false && "No BadConnection exception must be thrown!");
	}
}

void server() {
	try {
		Sputnik::TCPCommunicator communicator(1335, "127.0.0.1", 1334);
		const size_t size = 4;
		uint8_t recvBuffer[size];
		for (unsigned i = 0; i < size; i++)
			recvBuffer[i] = 0;
		const int num = 3;
		communicator.sendRecv((const uint8_t*)&num, recvBuffer, size);
		assert (*((int*)recvBuffer) == num);
	} catch (Sputnik::CommError& e) {
		std::cout << "Bad Connection" << std::endl;
		std::cout << e.what() << std::endl;
		assert(false && "No BadConnection exception must be thrown!");
	}
}
