/**
 * @file A002.cpp
 * 
 * @test A002
 * Use the SHA256::Hashsum::unmarshall() function with two different preset vectors to set the value of two
 * different SHA256::Hashsum objects. Then use the comparison operator, checking that it returns the value "false".
 */
#include "Sputnik.hpp"
#include "test.hpp"

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	const uint8_t array1[Sputnik::SHA256::Hashsum::size] = {
		 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15,
		16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31
	};
	const uint8_t array2[Sputnik::SHA256::Hashsum::size] = {
		 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15,
		16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 30
	};


	Sputnik::SHA256::Hashsum hashsum1, hashsum2;
	hashsum1.unmarshall(array1);
	hashsum2.unmarshall(array2);

	assert(!(hashsum1 == hashsum2) && "The compare operator must returns false");

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
