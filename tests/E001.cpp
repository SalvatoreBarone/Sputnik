/**
 * @file E001.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * @test E001
 * The test consists of reserving space for an object, then calling Pointer::write() on the object,
 * then an object method that changes its state. It is verified that:
 * - immediately before the change the chunk that hosts the object is clean and immediately becomes dirty;
 * - the change has occurred as expected (a reading is performed, triggering the voting)
 * - after viting the status of the chunk hosting the object becomes clean;
 * - a VoteMismatch exception is not thrown;
 * - a BadAlloc exception is not thrown;
 * - a BadConnection exception is not thrown;
 * 
 * @note The SHA256Voter class is used for voting.
 */
#include "Sputnik.hpp"
#include "test.hpp"

#include <thread> 

class Classe {
public:
	Classe(int m1 = 0, int m2 = 0) : membro1(m1), membro2(m2) {};
	void setMembro1(int m1)	 {membro1 = m1;}
	void setMembro2(int m2) {membro2 = m2;}
	int getMembro1() const {return membro1;}
	int getMembro2() const {return membro2;}
private:
	int membro1;
	int membro2;
};

void client();
void server();
Sputnik::SafeMemory clientMemory;
Sputnik::SafeMemory serverMemory;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);
	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print("I'm the main Thread\n");

	thread server_thr(server);
	thread client_thr(client);

	server_thr.join();
	client_thr.join();

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}

void client() {
	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print("I'm the client Thread\n");
	try {
		int localPort = 3001;
		const char* remoteAddress = "127.0.0.1";
		int remotePort = 3002;
		Sputnik::UDPCommunicator communicator(localPort, remoteAddress, remotePort);
		Sputnik::SHA256Voter voter(communicator);
		Sputnik::MemoryManager manager(clientMemory, voter, voteMismatchHandler, NULL);

		logger.print("Reserving memory...\n");
		Sputnik::Pointer<Classe> ptr = manager.reserve<Classe>();
		logger.print("Reserving memory completed\n");

		stringstream ss;
		ss << "Chunk status " << std::hex << std::setfill('0') << std::setw(16) << manager.getChunkStatus() << std::endl;
		logger.print(ss.str());
		assert(manager.getChunkStatus() == 0 && "The chunk must be clean");


		logger.print("Writing...\n");
		ptr.write().setMembro1(3);
		logger.print("Writing done\n");
		ss.str(std::string());
		ss << "Chunk status after write " << std::hex << std::setfill('0') << std::setw(16) << manager.getChunkStatus() << std::endl;
		logger.print(ss.str());
		assert(manager.getChunkStatus() == 1 && "The chunk must be dirty");

		#ifdef ASAP_VOTER
		manager.vote();
		#endif
		
		logger.print("Reading...\n");
		assert(ptr.read().getMembro1() == 3);
		ss.str(std::string());
		ss << "Chunk status after read " << std::hex << std::setfill('0') << std::setw(16) << manager.getChunkStatus() << std::endl;
		logger.print(ss.str());
		assert(manager.getChunkStatus() == 0 && "The chunk must be clean");

	} catch (Sputnik::BadAlloc& e) {
		std::stringstream ss;
		ss << "Bad alloc!" << std::endl;
		ss << "Total memory (byte): " << e.getTotalMemory() << std::endl;
		ss << "Available memory (byte): " << e.getAvailableMemory() << std::endl;
		ss << "Required memory (byte): " << e.getRequiredMemory() << std::endl;
		logger.print(ss.str());
		assert(false && "No exception must be thrown");
	} catch (Sputnik::CommError& e) {
		std::stringstream ss;
		ss << "Bad Connection" << std::endl;
		ss << e.what() << std::endl;
		logger.print(ss.str());
		assert(false && "No BadConnection exception must be thrown!");
	} catch (Sputnik::VoteMismatch& e) {
		std::stringstream ss;
		ss << "Bad vote!" << std::endl;
		
		logger.print(ss.str());
		assert(false && "No VoteMismatch exception must be thrown!");
	}
}

void server() {
	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print("I'm the server Thread\n");
	try {
		int localPort = 3002;
		const char* remoteAddress = "127.0.0.1";
		int remotePort = 3001;
		Sputnik::UDPCommunicator communicator(localPort, remoteAddress, remotePort);
		Sputnik::SHA256Voter voter(communicator);
		#ifdef ASYNC_VOTER
			Sputnik::MemoryManager manager(serverMemory, voter, voteMismatchHandler, NULL);
		#else
			Sputnik::MemoryManager manager(serverMemory, voter);
		#endif

		logger.print("Reserving memory...\n");
		Sputnik::Pointer<Classe> ptr = manager.reserve<Classe>();
		logger.print("Reserving memory completed\n");

		stringstream ss;
		ss << "Chunk status " << std::hex << std::setfill('0') << std::setw(16) << manager.getChunkStatus() << std::endl;
		logger.print(ss.str());
		assert(manager.getChunkStatus() == 0 && "The chunk must be clean");

		logger.print("Writing...\n");
		ptr.write().setMembro1(3);
		logger.print("Writing done\n");
		ss.str(std::string());
		ss << "Chunk status after write " << std::hex << std::setfill('0') << std::setw(16) << manager.getChunkStatus() << std::endl;
		logger.print(ss.str());
		assert(manager.getChunkStatus() == 1 && "The chunk must be dirty");

		#ifdef ASAP_VOTER
		manager.vote();
		#endif
		
		logger.print("Reading...\n");
		assert(ptr.read().getMembro1() == 3);
		ss.str(std::string());
		ss << "Chunk status after read " << std::hex << std::setfill('0') << std::setw(16) << manager.getChunkStatus() << std::endl;
		logger.print(ss.str());
		assert(manager.getChunkStatus() == 0 && "The chunk must be clean");

	} catch (Sputnik::BadAlloc& e) {
		std::stringstream ss;
		ss << "Bad alloc!" << std::endl;
		ss << "Total memory (byte): " << e.getTotalMemory() << std::endl;
		ss << "Available memory (byte): " << e.getAvailableMemory() << std::endl;
		ss << "Required memory (byte): " << e.getRequiredMemory() << std::endl;
		logger.print(ss.str());
		assert(false && "No exception must be thrown");
	} catch (Sputnik::CommError& e) {
		std::stringstream ss;
		ss << "Bad Connection" << std::endl;
		ss << e.what() << std::endl;
		logger.print(ss.str());
		assert(false && "No BadConnection exception must be thrown!");
	} catch (Sputnik::VoteMismatch& e) {
		std::stringstream ss;
		ss << "Bad vote!" << std::endl;
		
		logger.print(ss.str());
		assert(false && "No VoteMismatch exception must be thrown!");
	}
}
