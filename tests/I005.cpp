/**
 * @file I005.cpp
 * 
 * @test I005
 * Testing the entire library with multithreaded programs.
 * The I005 test consists of executing the product between two matrixes of integers, using a multithread
 * approach. After creating two threads representing the two replicas of the 2oo2 system, each replica creates two
 * threads, dividing the number of products to be made equally between them. The goal is to verify that if the
 * programming model proposed by the library is respected, no bugs can arise, the manifestation of which depends
 * on the interleaving of the threads.
 */
#include "Sputnik.hpp"
#include "test.hpp"
#include <thread> 
#include <assert.h>
#include <time.h>
#include <stdlib.h>


using namespace std;

#define N 100

typedef struct {
	int element[N][N]; 
} Matrix;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

void client(const Matrix&, const Matrix&);
void server(const Matrix&, const Matrix&, Matrix&);
Sputnik::SafeMemory clientMemory;
Sputnik::SafeMemory serverMemory;

void comp_thread(	const Sputnik::Pointer<Matrix>&,
					const Sputnik::Pointer<Matrix>&,
					Matrix&,
					int,
					int,
					std::string);

int main() {
	signal(SIGABRT, abortHandler);

	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print("This is the main thread\n");

	srand(time(NULL));   // should only be called once
	
	logger.print("Initializing matrixes\n");
	Matrix A, B, C;
	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++) {
			A.element[i][j] = rand();
			B.element[i][j] = rand();
		}	
	logger.print("Done\nCreating threads\n");

	std::thread server_thr(server, std::cref(A), std::cref(B), std::ref(C));
	std::thread client_thr(client, std::cref(A), std::cref(B));

	server_thr.join();
	client_thr.join();
	

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}

void client(const Matrix& A, const Matrix& B) {
	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print("This is the client thread\n");
	try {
		int localPort = 3001;
		const char* remoteAddress = "127.0.0.1";
		int remotePort = 3002;
		Sputnik::UDPCommunicator communicator(localPort, remoteAddress, remotePort);
		Sputnik::SHA256Voter voter(communicator);
		Sputnik::MemoryManager manager(clientMemory, voter, voteMismatchHandler, NULL);

		logger.print("Reserving memory\n");
		Sputnik::Pointer<Matrix> A_ptr = manager.reserve<Matrix>();
		Sputnik::Pointer<Matrix> B_ptr = manager.reserve<Matrix>();
		Sputnik::Pointer<Matrix> C_ptr = manager.reserve<Matrix>();
		Matrix tmp;

		stringstream ss;
		ss 	<< "A chunk mask: " <<std::hex << std::setw(16) << std::setfill('0') << A_ptr.getChunkMask() << endl
			<< "B chunk mask: " <<std::hex << std::setw(16) << std::setfill('0') << B_ptr.getChunkMask() << endl
			<< "C chunk mask: " <<std::hex << std::setw(16) << std::setfill('0') << C_ptr.getChunkMask() << endl;
		logger.print(ss.str());

		logger.print("Copying object\n");
		A_ptr.write(A);
		B_ptr.write(B);
		
		ss.str(string());
		ss << "chunk mask: " <<std::hex << std::setw(16) << std::setfill('0') << manager.getChunkStatus() << endl;
		logger.print(ss.str());

		assert(manager.getChunkStatus() != 0 && "Chunks must be dirty");

		#ifdef ASAP_VOTER
		manager.vote();
		assert(manager.getChunkStatus() == 0 && "Chunks must be clean");
		#endif

		logger.print("Creating threads\n");
		std::thread comp1(comp_thread, std::cref(A_ptr), std::cref(B_ptr), std::ref(tmp), 0, N/2, "client comp thread 1\n");
		std::thread comp2(comp_thread, std::cref(A_ptr), std::cref(B_ptr), std::ref(tmp), N/2, N, "client comp thread 2\n");
		logger.print("Waiting threads\n");
		comp1.join();
		comp2.join();
		logger.print("Computation completed\n");
		
		C_ptr.write(tmp);

		#ifdef ASAP_VOTER
		manager.vote();
		assert(manager.getChunkStatus() == 0 && "Chunks must be clean");
		#endif

		C_ptr.read();

	} catch (Sputnik::BadAlloc& e) {
		stringstream ss;
		ss	<< "Bad alloc!" << std::endl
			<< "Total memory (byte): " << e.getTotalMemory() << std::endl
			<< "Available memory (byte): " << e.getAvailableMemory() << std::endl
			<< "Required memory (byte): " << e.getRequiredMemory() << std::endl;
		logger.print(ss.str());
		assert(false && "No exception must be thrown");
	} catch (Sputnik::CommError& e) {
		stringstream ss;
		ss	<< "Bad Connection" << std::endl
			<< e.what() << std::endl;
		logger.print(ss.str());
		assert(false && "No BadConnection exception must be thrown!");
	} catch (Sputnik::VoteMismatch& e) {
		stringstream ss;
		ss 	<< " Bad vote!" << std::endl
			<< "Chunk index: " << (int)e.getChunkIndex() << std::endl;
		
		logger.print(ss.str());
		assert(false && "No VoteMismatch exception must be thrown!");
	}
}

void server(const Matrix& A, const Matrix& B, Matrix& C) {
	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print("This is the server thread\n");
	try {
		int localPort = 3002;
		const char* remoteAddress = "127.0.0.1";
		int remotePort = 3001;
		Sputnik::UDPCommunicator communicator(localPort, remoteAddress, remotePort);
		Sputnik::SHA256Voter voter(communicator);
		#ifdef ASYNC_VOTER
			Sputnik::MemoryManager manager(serverMemory, voter, voteMismatchHandler, NULL);
		#else
			Sputnik::MemoryManager manager(serverMemory, voter);
		#endif

		logger.print("Reserving memory\n");
		Sputnik::Pointer<Matrix> A_ptr = manager.reserve<Matrix>();
		Sputnik::Pointer<Matrix> B_ptr = manager.reserve<Matrix>();
		Sputnik::Pointer<Matrix> C_ptr = manager.reserve<Matrix>();
		Matrix tmp;

		stringstream ss;
		ss 	<< "A chunk mask: " <<std::hex << std::setw(16) << std::setfill('0') << A_ptr.getChunkMask() << endl
			<< "B chunk mask: " <<std::hex << std::setw(16) << std::setfill('0') << B_ptr.getChunkMask() << endl
			<< "C chunk mask: " <<std::hex << std::setw(16) << std::setfill('0') << C_ptr.getChunkMask() << endl;
		logger.print(ss.str());

		logger.print("Copying object\n");
		A_ptr.write(A);
		B_ptr.write(B);
		
		ss.str(string());
		ss << "chunk mask: " <<std::hex << std::setw(16) << std::setfill('0') << manager.getChunkStatus() << endl;
		logger.print(ss.str());

		assert(manager.getChunkStatus() != 0 && "Chunks must be dirty");

		#ifdef ASAP_VOTER
		manager.vote();
		assert(manager.getChunkStatus() == 0 && "Chunks must be clean");
		#endif

		logger.print("Creating threads\n");
		std::thread comp1(comp_thread, std::cref(A_ptr), std::cref(B_ptr), std::ref(tmp), 0, N/2, "server comp thread 1\n");
		std::thread comp2(comp_thread, std::cref(A_ptr), std::cref(B_ptr), std::ref(tmp), N/2, N, "server comp thread 2\n");
		logger.print("Waiting threads\n");
		comp1.join();
		comp2.join();
		logger.print("Computation completed\n");
		
		C_ptr.write(tmp);

		#ifdef ASAP_VOTER
		manager.vote();
		assert(manager.getChunkStatus() == 0 && "Chunks must be clean");
		#endif

		C = C_ptr.read();

	} catch (Sputnik::BadAlloc& e) {
		stringstream ss;
		ss	<< "Bad alloc!" << std::endl
			<< "Total memory (byte): " << e.getTotalMemory() << std::endl
			<< "Available memory (byte): " << e.getAvailableMemory() << std::endl
			<< "Required memory (byte): " << e.getRequiredMemory() << std::endl;
		logger.print(ss.str());
		assert(false && "No exception must be thrown");
	} catch (Sputnik::CommError& e) {
		stringstream ss;
		ss	<< "Bad Connection" << std::endl
			<< e.what() << std::endl;
		logger.print(ss.str());
		assert(false && "No BadConnection exception must be thrown!");
	} catch (Sputnik::VoteMismatch& e) {
		stringstream ss;
		ss 	<< " Bad vote!" << std::endl
			<< "Chunk index: " << (int)e.getChunkIndex() << std::endl;
		
		logger.print(ss.str());
		assert(false && "No VoteMismatch exception must be thrown!");
	}
}


void comp_thread(	const Sputnik::Pointer<Matrix>& A,
					const Sputnik::Pointer<Matrix>& B,
					Matrix& C,
					int start,
					int end,
					std::string name) {

	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print(name);

	for (int i = start; i < end; i++)
		for (int j = 0; j < N; j++) {
			C.element[i][j] = 0;
			for (int k = 0; k < N; k++)
				C.element[i][j] += A.read().element[i][k] * B.read().element[k][j];

		}
}
