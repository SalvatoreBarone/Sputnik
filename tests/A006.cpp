/**
 * @file A006.cpp
 * 
 * @test A006
 * The hashsum of a portion of memory equal in size to those of a typical chunk, or a little more, is computed.
 * Specifically, the portion of memory consists of 67457 bytes 'a'. The calculated hashsum is compared to that
 * obtained from the GNU implementation of SHA256 (sha256sum).
 */
#include "Sputnik.hpp"
#include "test.hpp"

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	const uint32_t expectedHashsumArray[8] = {
		0x7c3788b4, 0x766e4b2f, 0x192f04cb, 0x89481503, 0x65083441, 0xc0d157c5, 0xa57fab78, 0xc4afba92
	};
	Sputnik::SHA256::Hashsum expectedHashsum;
	expectedHashsum.unmarshall((const uint8_t(&)[32])expectedHashsumArray);

	uint8_t data[67457];
	for (int i = 0; i < 67457; i++) data[i] = 'a';
	Sputnik::SHA256::Hashsum computedHashsum = Sputnik::SHA256::sum(data, 67457);

	assert(computedHashsum == expectedHashsum);

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
