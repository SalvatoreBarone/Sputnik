/**
 * @file C002.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 */
#include "Sputnik.hpp"
#include "test.hpp"

#include <thread> 

void client();
void server();

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	thread server_thr(server);
	thread client_thr(client);

	server_thr.join();
	client_thr.join();

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}

void client() {
	try {
		Sputnik::UDPSockClient client("127.0.0.1", 1234);
		const size_t size = 4;
		const int num = 3;
		uint8_t recvBuffer[size];
		for (unsigned i = 0; i < size; i++)
			recvBuffer[i] = 0;
		client.sendMsg((const uint8_t*)&num, size);
		client.recvMsg(recvBuffer, size);
		assert (*((int*)recvBuffer) == num);
	} catch (Sputnik::CommError& e) {
		std::cout << "Bad Connection" << std::endl;
		std::cout << e.what() << std::endl;
		assert(false && "No BadConnection exception must be thrown!");
	}
}

void server() {
	try {
		Sputnik::UDPSockServer server(1234);
		const size_t size = 4;
		uint8_t recvBuffer[size];
		for (unsigned i = 0; i < size; i++)
			recvBuffer[i] = 0;
		server.recvMsg(recvBuffer, size);
		const int num = 3;
		assert (*((int*)recvBuffer) == num);
		server.sendMsg((const uint8_t*)&num, size);
	} catch (Sputnik::CommError& e) {
		std::cout << "Bad Connection" << std::endl;
		std::cout << e.what() << std::endl;
		assert(false && "No BadConnection exception must be thrown!");
	}
}
