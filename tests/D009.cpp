/**
 * @file D008.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * @test D008
 */
#include "Sputnik.hpp"
#include "test.hpp"

#include <thread>

class Classe {
public:
	Classe(char m1 = 0, int m2 = 0) : membro1(m1), membro2(m2) {};

	Classe (const Classe& c) : membro1(c.membro1), membro2(c.membro2) {};

	const Classe& operator=(const Classe& c) {
		if (&c != this) {
			membro1 = c.membro1;
			membro2 = c.membro2;
		}
		return *this;
	}

	void setMembro1(char m1)	{membro1 = m1;}
	void setMembro2(int m2)		{membro2 = m2;}
	char getMembro1() const		{return membro1;}
	int getMembro2() const 		{return membro2;}
private:
	char membro1;
	int membro2;
};

void client();
void server();
Sputnik::SafeMemory clientMemory;
Sputnik::SafeMemory serverMemory;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print("I'm the main Thread\n");

	thread server_thr(server);
	thread client_thr(client);

	server_thr.join();
	client_thr.join();

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}

void client() {
	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print("I'm the client Thread\n");
	try {
		int localPort = 3001;
		const char* remoteAddress = "127.0.0.1";
		int remotePort = 3002;
		Sputnik::UDPCommunicator communicator(localPort, remoteAddress, remotePort);
		Sputnik::SHA256Voter voter(communicator);
		Sputnik::MemoryManager manager(clientMemory, voter, voteMismatchHandler, NULL);

		Sputnik::Pointer<Classe> ptr1 = manager.reserve<Classe>();
		Sputnik::Pointer<Classe> ptr2 = manager.reserve<Classe>();
		Sputnik::Pointer<Classe> ptr3 = manager.reserve<Classe>();

		std::stringstream ss;
		ss 	<< "Address of:" << std::endl
			<< "\tptr1 : " << std::hex << std::setw(16) << std::setfill('0') << &ptr1.read() << std::endl
			<< "\tptr2 : " << std::hex << std::setw(16) << std::setfill('0') << &ptr2.read() << std::endl
			<< "\tptr3 : " << std::hex << std::setw(16) << std::setfill('0') << &ptr3.read() << std::endl;
		logger.print(ss.str());

		ptr1.write() = Classe('1', 1);
		ptr2.write() = Classe('2', 2);
		ptr3.write() = Classe('3', 3);
		
		assert(ptr1.read().getMembro1() == '1');
		assert(ptr2.read().getMembro1() == '2');
		assert(ptr3.read().getMembro1() == '3');

		assert(ptr1.read().getMembro2() == 1);
		assert(ptr2.read().getMembro2() == 2);
		assert(ptr3.read().getMembro2() == 3);

	} catch (Sputnik::BadAlloc& e) {
		std::stringstream ss;
		ss << "Bad alloc!" << std::endl;
		ss << "Total memory (byte): " << e.getTotalMemory() << std::endl;
		ss << "Available memory (byte): " << e.getAvailableMemory() << std::endl;
		ss << "Required memory (byte): " << e.getRequiredMemory() << std::endl;
		logger.print(ss.str());
		assert(false && "No BadAlloc exception must be thrown!");
	} catch (Sputnik::CommError& e) {
		std::stringstream ss;
		ss << "Bad Connection" << std::endl;
		ss << e.what() << std::endl;
		logger.print(ss.str());
		assert(false && "No BadConnection exception must be thrown!");
	} catch (Sputnik::VoteMismatch& e) {
		std::stringstream ss;
		ss << "Bad vote!" << std::endl;
		
		logger.print(ss.str());
		assert(false && "No VoteMismatch exception must be thrown!");
	}
}

void server() {
	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print("I'm the server Thread\n");
	try {
		int localPort = 3002;
		const char* remoteAddress = "127.0.0.1";
		int remotePort = 3001;
		Sputnik::UDPCommunicator communicator(localPort, remoteAddress, remotePort);
		Sputnik::SHA256Voter voter(communicator);
		#ifdef ASYNC_VOTER
			Sputnik::MemoryManager manager(serverMemory, voter, voteMismatchHandler, NULL);
		#else
			Sputnik::MemoryManager manager(serverMemory, voter);
		#endif

		Sputnik::Pointer<Classe> ptr1 = manager.reserve<Classe>();
		Sputnik::Pointer<Classe> ptr2 = manager.reserve<Classe>();
		Sputnik::Pointer<Classe> ptr3 = manager.reserve<Classe>();

		std::stringstream ss;
		ss 	<< "Address of:" << std::endl
			<< "\tptr1 : " << std::hex << std::setw(16) << std::setfill('0') << &ptr1.read() << std::endl
			<< "\tptr2 : " << std::hex << std::setw(16) << std::setfill('0') << &ptr2.read() << std::endl
			<< "\tptr3 : " << std::hex << std::setw(16) << std::setfill('0') << &ptr3.read() << std::endl;
		logger.print(ss.str());

		ptr1.write() = Classe('1', 1);
		ptr2.write() = Classe('2', 2);
		ptr3.write() = Classe('3', 3);
		
		assert(ptr1.read().getMembro1() == '1');
		assert(ptr2.read().getMembro1() == '2');
		assert(ptr3.read().getMembro1() == '3');

		assert(ptr1.read().getMembro2() == 1);
		assert(ptr2.read().getMembro2() == 2);
		assert(ptr3.read().getMembro2() == 3);

	} catch (Sputnik::BadAlloc& e) {
		std::stringstream ss;
		ss << "Bad alloc!" << std::endl;
		ss << "Total memory (byte): " << e.getTotalMemory() << std::endl;
		ss << "Available memory (byte): " << e.getAvailableMemory() << std::endl;
		ss << "Required memory (byte): " << e.getRequiredMemory() << std::endl;
		logger.print(ss.str());
		assert(false && "No BadAlloc exception must be thrown!");
	} catch (Sputnik::CommError& e) {
		std::stringstream ss;
		ss << "Bad Connection" << std::endl;
		ss << e.what() << std::endl;
		logger.print(ss.str());
		assert(false && "No BadConnection exception must be thrown!");
	} catch (Sputnik::VoteMismatch& e) {
		std::stringstream ss;
		ss << "Bad vote!" << std::endl;
		
		logger.print(ss.str());
		assert(false && "No VoteMismatch exception must be thrown!");
	}

}
