/**
 * @file I001.cpp
 * 
 * @test I001
 * Testing the entire library with multithreaded programs.
 * The I001 test consists of executing the scalar product between two complex number vectors, using a multithread
 * approach. After creating two threads representing the two replicas of the 2oo2 system, each replica creates two
 * threads, dividing the number of products to be made equally between them. The goal is to verify that if the
 * programming model proposed by the library is respected, no bugs can arise, the manifestation of which depends
 * on the interleaving of the threads.
 */
#include "Sputnik.hpp"

#include "test.hpp"
#include <thread> 
#include <assert.h>
using namespace std;

#define N 256

int n = N;

const int Re_A[N] = {
 	 64,		90,		89,		87,		83,		80,		75,		70,		64,		57,		50,		43,		36,		25,		18,		9 ,
 	 64,		87,		75,		57,		36,		9,		-18,	-43,	-64,	-80,	-89,	-90,	-83,	-70,	-50,	-25 ,
 	 64,		80,		50,		9,		-36,	-70,	-89,	-87,	-64,	-25,	18,		57,		83,		90,		75,		43 ,
 	 64,		70,		18,		-43,	-83,	-87,	-50,	9,		64,		90,		75,		25,		-36,	-80,	-89,	-57 ,
 	 64,		57,		-18,	-80,	-83,	-25,	50,		90,		64,		-9,		-75,	-87,	-36,	43,		89,		70 ,
 	 64,		43,		-50,	-90,	-36,	57,		89,		25,		-64,	-87,	-18,	70,		83,		9,		-75,	-80 ,
 	 64,		25,		-75,	-70,	36,		90,		18,		-80,	-64,	43,		89,		9,		-83,	-57,	50,		87 ,
 	 64,		9,		-89,	-25,	83,		43,		-75,	-57,	64,		70,		-50,	-80,	36,		87,		-18,	-90 ,
 	 64,		-9,		-89,	25,		83,		-43,	-75,	57,		64,		-70,	-50,	80,		36,		-87,	-18,	90 ,
 	 64,		-25,	-75,	70,		36,		-90,	18,		80,		-64,	-43,	89,		-9,		-83,	57,		50,		-87 ,
 	 64,		-43,	-50,	90,		-36,	-57,	89,		-25,	-64,	87,		-18,	-70,	83,		-9,		-75,	80 ,
 	 64,		-57,	-18,	80,		-83,	25,		50,		-90,	64,		9,		-75,	87,		-36,	-43,	89,		-70 ,
 	 64,		-70,	18,		43,		-83,	87,		-50,	-9,		64,		-90,	75,		-25,	-36,	80,		-89,	57 ,
 	 64,		-80,	50,		-9,		-36,	70,		-89,	87,		-64,	25,		18,		-57,	83,		-90,	75,		-43 ,
 	 64,		-87,	75,		-57,	36,		-9,		-18,	43,		-64,	80,		-89,	90,		-83,	70,		-50,	25 ,
 	 64,		-90,	89,		-87,	83,		-80,	75,		-70,	64,		-57,	50,		-43,	36,		-25,	18,		-9
 };

 const int Re_B[N] = {
 	64,	64,		64,		64,		64,		64,		64,		64,		64,		64,		64,		64,		64,		64,		64,		64,
 	90,	87,		80,		70,		57,		43,		25,		9,		-9,		-25,	-43,	-57,	-70,	-80,	-87,	-90,
 	89,	75,		50,		18,		-18,	-50,	-75,	-89,	-89,	-75,	-50,	-18,	18,		50,		75,		89,
 	87,	57,		9,		-43,	-80,	-90,	-70,	-25,	25,		70,		90,		80,		43,		-9,		-57,	-87,
 	83,	36,		-36,	-83,	-83,	-36,	36,		83,		83,		36,		-36,	-83,	-83,	-36,	36,		83,
 	80,	9,		-70,	-87,	-25,	57,		90,		43,		-43,	-90,	-57,	25,		87,		70,		-9,		-80,
 	75,	-18,	-89,	-50,	50,		89,		18,		-75,	-75,	18,		89,		50,		-50,	-89,	-18,	75,
 	70,	-43,	-87,	9,		90,		25,		-80,	-57,	57,		80,		-25,	-90,	-9,		87,		43,		-70,
 	64,	-64,	-64,	64,		64,		-64,	-64,	64,		64,		-64,	-64,	64,		64,		-64,	-64,	64,
 	57,	-80,	-25,	90,		-9,		-87,	43,		70,		-70,	-43,	87,		9,		-90,	25,		80,		-57,
 	50,	-89,	18,		75,		-75,	-18,	89,		-50,	-50,	89,		-18,	-75,	75,		18,		-89,	50,
 	43,	-90,	57,		25,		-87,	70,		9,		-80,	80,		-9,		-70,	87,		-25,	-57,	90,		-43,
 	36,	-83,	83,		-36,	-36,	83,		-83,	36,		36,		-83,	83,		-36,	-36,	83,		-83,	36,
 	25,	-70,	90,		-80,	43,		9,		-57,	87,		-87,	57,		-9,		-43,	80,		-90,	70,		-25,
 	18,	-50,	75,		-89,	89,		-75,	50,		-18,	-18,	50,		-75,	89,		-89,	75,		-50,	18,
 	9,	-25,	43,		-57,	70,		-80,	87,		-90,	90,		-87,	80,		-70,	57,		-43,	25,		-9
 };

 const int Im_A[N] = {
 	64,	64,		64,		64,		64,		64,		64,		64,		64,		64,		64,		64,		64,		64,		64,		64,
 	90,	87,		80,		70,		57,		43,		25,		9,		-9,		-25,	-43,	-57,	-70,	-80,	-87,	-90,
 	89,	75,		50,		18,		-18,	-50,	-75,	-89,	-89,	-75,	-50,	-18,	18,		50,		75,		89,
 	87,	57,		9,		-43,	-80,	-90,	-70,	-25,	25,		70,		90,		80,		43,		-9,		-57,	-87,
 	83,	36,		-36,	-83,	-83,	-36,	36,		83,		83,		36,		-36,	-83,	-83,	-36,	36,		83,
 	80,	9,		-70,	-87,	-25,	57,		90,		43,		-43,	-90,	-57,	25,		87,		70,		-9,		-80,
 	75,	-18,	-89,	-50,	50,		89,		18,		-75,	-75,	18,		89,		50,		-50,	-89,	-18,	75,
 	70,	-43,	-87,	9,		90,		25,		-80,	-57,	57,		80,		-25,	-90,	-9,		87,		43,		-70,
 	64,	-64,	-64,	64,		64,		-64,	-64,	64,		64,		-64,	-64,	64,		64,		-64,	-64,	64,
 	57,	-80,	-25,	90,		-9,		-87,	43,		70,		-70,	-43,	87,		9,		-90,	25,		80,		-57,
 	50,	-89,	18,		75,		-75,	-18,	89,		-50,	-50,	89,		-18,	-75,	75,		18,		-89,	50,
 	43,	-90,	57,		25,		-87,	70,		9,		-80,	80,		-9,		-70,	87,		-25,	-57,	90,		-43,
 	36,	-83,	83,		-36,	-36,	83,		-83,	36,		36,		-83,	83,		-36,	-36,	83,		-83,	36,
 	25,	-70,	90,		-80,	43,		9,		-57,	87,		-87,	57,		-9,		-43,	80,		-90,	70,		-25,
 	18,	-50,	75,		-89,	89,		-75,	50,		-18,	-18,	50,		-75,	89,		-89,	75,		-50,	18,
 	9,	-25,	43,		-57,	70,		-80,	87,		-90,	90,		-87,	80,		-70,	57,		-43,	25,		-9
 };

 const int Im_B[N] = {
 	64,		90,		89,		87,		83,		80,		75,		70,		64,		57,		50,		43,		36,		25,		18,		9,
 	64,		87,		75,		57,		36,		9,		-18,	-43,	-64,	-80,	-89,	-90,	-83,	-70,	-50,	-25,
 	64,		80,		50,		9,		-36,	-70,	-89,	-87,	-64,	-25,	18,		57,		83,		90,		75,		43,
 	64,		70,		18,		-43,	-83,	-87,	-50,	9,		64,		90,		75,		25,		-36,	-80,	-89,	-57,
 	64,		57,		-18,	-80,	-83,	-25,	50,		90,		64,		-9,		-75,	-87,	-36,	43,		89,		70,
 	64,		43,		-50,	-90,	-36,	57,		89,		25,		-64,	-87,	-18,	70,		83,		9,		-75,	-80,
 	64,		25,		-75,	-70,	36,		90,		18,		-80,	-64,	43,		89,		9,		-83,	-57,	50,		87,
 	64,		9,		-89,	-25,	83,		43,		-75,	-57,	64,		70,		-50,	-80,	36,		87,		-18,	-90,
 	64,		-9,		-89,	25,		83,		-43,	-75,	57,		64,		-70,	-50,	80,		36,		-87,	-18,	90,
 	64,		-25,	-75,	70,		36,		-90,	18,		80,		-64,	-43,	89,		-9,		-83,	57,		50,		-87,
 	64,		-43,	-50,	90,		-36,	-57,	89,		-25,	-64,	87,		-18,	-70,	83,		-9,		-75,	80,
 	64,		-57,	-18,	80,		-83,	25,		50,		-90,	64,		9,		-75,	87,		-36,	-43,	89,		-70,
 	64,		-70,	18,		43,		-83,	87,		-50,	-9,		64,		-90,	75,		-25,	-36,	80,		-89,	57,
 	64,		-80,	50,		-9,		-36,	70,		-89,	87,		-64,	25,		18,		-57,	83,		-90,	75,		-43,
 	64,		-87,	75,		-57,	36,		-9,		-18,	43,		-64,	80,		-89,	90,		-83,	70,		-50,	25,
 	64,		-90,	89,		-87,	83,		-80,	75,		-70,	64,		-57,	50,		-43,	36,		-25,	18,		-9
 };

void client();
void server();
Sputnik::SafeMemory clientMemory;
Sputnik::SafeMemory serverMemory;


typedef struct {
	int element[N]; // N is defined in the data.hpp file
} Vector;

void init_thread(	Sputnik::Pointer<Vector>&,
					const int (&)[N], 
					std::string);

void comp_thread(	Sputnik::Pointer<Vector>&,	// RE_A
					Sputnik::Pointer<Vector>&,	// RE_B
					Sputnik::Pointer<Vector>&,	// IM_A
					Sputnik::Pointer<Vector>&,	// IM_B
					unsigned,					// start index
					unsigned,					// end index
					Vector&,					// result (RE)
					Vector&, 					// result (IM)
					std::string);				// name

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}


int main() {
	signal(SIGABRT, abortHandler);

	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print("This is the main thread\n");

	thread server_thr(server);
	thread client_thr(client);

	server_thr.join();
	client_thr.join();

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}

void client() {
	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print("I'm the client Thread\n");
	try {
		int localPort = 3001;
		const char* remoteAddress = "127.0.0.1";
		int remotePort = 3002;
		Sputnik::UDPCommunicator communicator(localPort, remoteAddress, remotePort);
		Sputnik::SHA256Voter voter(communicator);
		Sputnik::MemoryManager manager(clientMemory, voter, voteMismatchHandler, NULL);

		Sputnik::Pointer<Vector> RE_A = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> RE_B = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> IM_A = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> IM_B = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> RE_RES = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> IM_RES = manager.reserve<Vector>();
		Vector Re_res;
		Vector Im_res;

		RE_A.write((const Vector&)Re_A);
		RE_B.write((const Vector&)Re_B);
		IM_A.write((const Vector&)Im_A);
		IM_B.write((const Vector&)Im_B);
	
		std::thread comp_1(comp_thread, std::ref(RE_A), std::ref(RE_B), std::ref(IM_A), std::ref(IM_B), 0, N/2, std::ref(Re_res), std::ref(Im_res), std::string("client comp_1 thread\n"));
		std::thread comp_2(comp_thread, std::ref(RE_A), std::ref(RE_B), std::ref(IM_A), std::ref(IM_B), N/2, N, std::ref(Re_res), std::ref(Im_res), std::string("client comp_2 thread\n"));
		comp_1.join();
		comp_2.join();

		RE_RES.write(Re_res);
		IM_RES.write(Im_res);

		#ifdef ASAP_VOTER
		manager.vote();
		#endif

		RE_RES.read();
		IM_RES.read();

	} catch (Sputnik::BadAlloc& e) {
		stringstream ss;
		ss	<< "Bad alloc!" << std::endl
			<< "Total memory (byte): " << e.getTotalMemory() << std::endl
			<< "Available memory (byte): " << e.getAvailableMemory() << std::endl
			<< "Required memory (byte): " << e.getRequiredMemory() << std::endl;
		logger.print(ss.str());
		assert(false && "No exception must be thrown");
	} catch (Sputnik::CommError& e) {
		stringstream ss;
		ss	<< "Bad Connection" << std::endl
			<< e.what() << std::endl;
		logger.print(ss.str());
		assert(false && "No BadConnection exception must be thrown!");
	} catch (Sputnik::VoteMismatch& e) {
		stringstream ss;
		ss 	<< " Bad vote!" << std::endl
			<< "Chunk index: " << (int)e.getChunkIndex() << std::endl;
		
		logger.print(ss.str());
		assert(false && "No VoteMismatch exception must be thrown!");
	}
}

void server() {
	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print("I'm the server Thread\n");
	try {
		int localPort = 3002;
		const char* remoteAddress = "127.0.0.1";
		int remotePort = 3001;
		Sputnik::UDPCommunicator communicator(localPort, remoteAddress, remotePort);
		Sputnik::SHA256Voter voter(communicator);
		#ifdef ASYNC_VOTER
			Sputnik::MemoryManager manager(serverMemory, voter, voteMismatchHandler, NULL);
		#else
			Sputnik::MemoryManager manager(serverMemory, voter);
		#endif

		Sputnik::Pointer<Vector> RE_A = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> RE_B = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> IM_A = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> IM_B = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> RE_RES = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> IM_RES = manager.reserve<Vector>();
		Vector Re_res;
		Vector Im_res;

		RE_A.write((const Vector&)Re_A);
		RE_B.write((const Vector&)Re_B);
		IM_A.write((const Vector&)Im_A);
		IM_B.write((const Vector&)Im_B);

		std::thread comp_1(comp_thread, std::ref(RE_A), std::ref(RE_B), std::ref(IM_A), std::ref(IM_B), 0, N/2, std::ref(Re_res), std::ref(Im_res), std::string("server comp_1 thread\n"));
		std::thread comp_2(comp_thread, std::ref(RE_A), std::ref(RE_B), std::ref(IM_A), std::ref(IM_B), N/2, N, std::ref(Re_res), std::ref(Im_res), std::string("server comp_2 thread\n"));
		comp_1.join();
		comp_2.join();

		RE_RES.write(Re_res);
		IM_RES.write(Im_res);

		#ifdef ASAP_VOTER
		manager.vote();
		#endif

		RE_RES.read();
		IM_RES.read();

	} catch (Sputnik::BadAlloc& e) {
		stringstream ss;
		ss	<< "Bad alloc!" << std::endl
			<< "Total memory (byte): " << e.getTotalMemory() << std::endl
			<< "Available memory (byte): " << e.getAvailableMemory() << std::endl
			<< "Required memory (byte): " << e.getRequiredMemory() << std::endl;
		logger.print(ss.str());
		assert(false && "No exception must be thrown");
	} catch (Sputnik::CommError& e) {
		stringstream ss;
		ss	<< "Bad Connection" << std::endl
			<< e.what() << std::endl;
		logger.print(ss.str());
		assert(false && "No BadConnection exception must be thrown!");
	} catch (Sputnik::VoteMismatch& e) {
		stringstream ss;
		ss 	<< " Bad vote!" << std::endl
			<< "Chunk index: " << (int)e.getChunkIndex() << std::endl;
		
		logger.print(ss.str());
		assert(false && "No VoteMismatch exception must be thrown!");
	}
}

void init_thread(	Sputnik::Pointer<Vector>& pointer,
					const int (&data)[N],
					std::string name) {
	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print(name);
	for (unsigned i= 0; i < N; i++)
		pointer.write().element[i] = data[i];
}

void comp_thread(	Sputnik::Pointer<Vector>& RE_A,
					Sputnik::Pointer<Vector>& RE_B,
					Sputnik::Pointer<Vector>& IM_A,
					Sputnik::Pointer<Vector>& IM_B,
					unsigned start,
					unsigned end,
					Vector& RE_res,
					Vector& IM_res,
					std::string name) {
	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	try {
		for (unsigned i = start; i < end; i++) {
			RE_res.element[i] 	= RE_A.read().element[i] * RE_B.read().element[i]
								- IM_A.read().element[i] * IM_B.read().element[i];
			IM_res.element[i]	= RE_A.read().element[i] * IM_B.read().element[i]
								+ IM_A.read().element[i] * IM_B.read().element[i];
		}
	} catch (Sputnik::VoteMismatch& e) {
		std::stringstream ss;
		ss 	<< name << " Bad vote!" << std::endl
			<< "Chunk index: " << (int)e.getChunkIndex() << std::endl;
		
		logger.print(ss.str());
		assert(false && "No VoteMismatch exception must be thrown!");
	}
}
