/**
 * @file B001.cpp
 * 
 * @test B001
 * Use the SHA512::Hashsum: unmarshall() function with a prefixed vector to set the value of the
 * SHA512::Hashsum object. Then use the function SHA512::Hashsum::marshall(), to obtain the value of the
 * hashsum, verifying that the vector produced by the latter function corresponds to the previous vector.
 */
#include "Sputnik.hpp"
#include "test.hpp"


void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	const uint8_t array1[Sputnik::SHA512::Hashsum::size] = {
		 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15,
		16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31,
		32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 
		48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63
	};
	uint8_t array2[Sputnik::SHA512::Hashsum::size];

	Sputnik::SHA512::Hashsum hashsum;
	hashsum.unmarshall(array1);
	hashsum.marshall(array2);
	for (unsigned i = 0; i < Sputnik::SHA512::Hashsum::size; i++)
		assert(array1[i] == array2[i] && "The two vector must equals");

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
