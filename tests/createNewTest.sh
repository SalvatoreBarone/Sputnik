#!/bin/bash

if [ $# -ne 3 ]
then
    echo "Numero di parametri errato";
    echo "createNewTest.sh <Letter> <first> <last>";
    exit 0;
fi

extension=.cpp;


read -p "Sicuro di voler creare il file? (s/n) " risposta
if [  "$risposta" = "s" ] || [ "$risposta" = "S" ]; then
	for ((i=$2; i<=$3; i++)); do
		source=$(printf "%s%03d.cpp" "$1" "$i");

		printf "/**
 * @file %s
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * @test %s%03d
 */
#include \"Sputnik.hpp\"
#include \"test.hpp\"

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}" "$source" "$1" "$i" > $source;
	done;
fi;

read -p "Aggiungi i file al CMakeLists.txt? (s/n) " risposta
if [  "$risposta" = "s" ] || [ "$risposta" = "S" ]; then
	for ((i=$2; i<=$3; i++)); do
		source=$(printf "%s%03d.cpp" "$1" "$i");
		printf "add_executable(%s%03d %s)\n" "$1" "$i" "$source" >> CMakeLists.txt;
		printf "add_test(NAME %s%03d COMMAND %s%03d)\n" "$1" "$i" "$1" "$i">> CMakeLists.txt;
		printf "SET_TESTS_PROPERTIES(%s%03d PROPERTIES FAIL_REGULAR_EXPRESSION \"FAILED\")\n" "$1" "$i" >> CMakeLists.txt;
		printf "SET_TESTS_PROPERTIES(%s%03d PROPERTIES PASS_REGULAR_EXPRESSION \"PASSED\")\n\n" "$1" "$i" >> CMakeLists.txt;
	done;
fi;
