/**
 * @file D001.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * @test D001
 * The test consists in allocation of objects that, in terms of memory requirements, do not exceed the available 
 * memory, and verifying that no exceptions are issued.
 */
#include "Sputnik.hpp"
#include "test.hpp"

#include <thread>
#include <chrono>

class Classe {
public:
	Classe(int m1 = 0, int m2 = 0) : membro1(m1), membro2(m2) {};
	void setMembro1(int m1)	 {membro1 = m1;}
	void setMembro2(int m2) {membro2 = m2;}
	int getMembro1() const {return membro1;}
	int getMembro2() const {return membro2;}
private:
	int membro1;
	int membro2;
};

void client();
void server();
Sputnik::SafeMemory clientMemory;
Sputnik::SafeMemory serverMemory;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print("I'm the main Thread\n");

	thread server_thr(server);
	thread client_thr(client);

	server_thr.join();
	client_thr.join();

	cout << __FILE__ << TEST_PASSED << endl;
	return 1;
}

void client() {
	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print("I'm the client Thread\n");
	try {
		int localPort = 3001;
		const char* remoteAddress = "127.0.0.1";
		int remotePort = 3002;
		Sputnik::UDPCommunicator communicator(localPort, remoteAddress, remotePort);
		Sputnik::SHA256Voter voter(communicator);
		Sputnik::MemoryManager manager(clientMemory, voter, voteMismatchHandler, NULL);
		logger.print("Trying to reserve memory...\n");
		Sputnik::Pointer<Classe> ptr = manager.reserve<Classe>();
		logger.print("Done\n");
		ptr.read();
	} catch (Sputnik::BadAlloc& e) {
		std::stringstream ss;
		ss << "Bad alloc!" << std::endl;
		ss << "Total memory (byte): " << e.getTotalMemory() << std::endl;
		ss << "Available memory (byte): " << e.getAvailableMemory() << std::endl;
		ss << "Required memory (byte): " << e.getRequiredMemory() << std::endl;
		logger.print(ss.str());
		assert(false && "No exception must be thrown");
	} catch (Sputnik::CommError& e) {
		std::stringstream ss;
		ss << "Bad Connection" << std::endl;
		ss << e.what() << std::endl;
		logger.print(ss.str());
		assert(false && "No BadConnection exception must be thrown!");
	}
}

void server() {
	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print("I'm the server Thread\n");
	try {
		int localPort = 3002;
		const char* remoteAddress = "127.0.0.1";
		int remotePort = 3001;
		Sputnik::UDPCommunicator communicator(localPort, remoteAddress, remotePort);
		Sputnik::SHA256Voter voter(communicator);
		#ifdef ASYNC_VOTER
			Sputnik::MemoryManager manager(serverMemory, voter, voteMismatchHandler, NULL);
		#else
			Sputnik::MemoryManager manager(serverMemory, voter);
		#endif
		logger.print("Trying to reserve memory...\n");
		Sputnik::Pointer<Classe> ptr = manager.reserve<Classe>();
		logger.print("Done\n");
		ptr.read();
	} catch (Sputnik::BadAlloc& e) {
		std::stringstream ss;
		ss << "Bad alloc!" << std::endl;
		ss << "Total memory (byte): " << e.getTotalMemory() << std::endl;
		ss << "Available memory (byte): " << e.getAvailableMemory() << std::endl;
		ss << "Required memory (byte): " << e.getRequiredMemory() << std::endl;
		logger.print(ss.str());
		assert(false && "No exception must be thrown");
	} catch (Sputnik::CommError& e) {
		std::stringstream ss;
		ss << "Bad Connection" << std::endl;
		ss << e.what() << std::endl;
		logger.print(ss.str());
		assert(false && "No BadConnection exception must be thrown!");
	}
}
