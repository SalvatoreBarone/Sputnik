/**
 * @file B005.cpp
 * 
 * @test B005
 * The hashsum of Giacomo Leopardi's poem "L'infinito" is calculated. The calculated hashsum is compared to that
 * obtained from the GNU implementation of SHA512 (SHA512sum). The intent is to calculate the hashsum of a string
 * of considerable size.
 */
#include "Sputnik.hpp"
#include "test.hpp"

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	const uint64_t expectedHashsumArray[8] = {
		0xf1923c7e153bff27, 0x267d8cf059f26207, 0x5bcf5e6dacaaf39e, 0xdd506c823dd9b2bf,
		0xdfbd8d91b6481796, 0x72e1d9196b16f1e2, 0xf3d02e6745f7c2a2, 0x82fd1f645189d4df
	};
	Sputnik::SHA512::Hashsum expectedHashsum;
	expectedHashsum.unmarshall((const uint8_t(&)[64])expectedHashsumArray);

	std::string data = "Sempre caro mi fu quest'ermo colle, E questa siepe, che da tanta parte Dell'ultimo orizzonte il guardo esclude. Ma sedendo e mirando, interminati Spazi di là da quella, e sovrumani Silenzi, e profondissima quiete Io nel pensier mi fingo; ove per poco Il cor non si spaura. E come il vento Odo stormir tra queste piante, io quello Infinito silenzio a questa voce Vo comparando: e mi sovvien l'eterno, E le morte stagioni, e la presente E viva, e il suon di lei. Così tra questa Immensità s'annega il pensier mio: E il naufragar m'è dolce in questo mare.";
	Sputnik::SHA512::Hashsum computedHashsum = Sputnik::SHA512::sum((const uint8_t*)data.c_str(), data.size());

	assert(computedHashsum == expectedHashsum);

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
