/**
 * @file A000.cpp
 * 
 * @test A000
 * Tests the singleton logic for Sputnik::Logger class.
 */
#include "Sputnik.hpp"
#include "test.hpp"

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	Sputnik::Logger& logger1 = Sputnik::Logger::getLogger();
	Sputnik::Logger& logger2 = Sputnik::Logger::getLogger();

	assert(&logger1 == &logger2);

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
