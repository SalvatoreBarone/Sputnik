/**
 * @file B006.cpp
 * 
 * @test B006
 * The hashsum of a portion of memory equal in size to those of a typical chunk, or a little more, is computed.
 * Specifically, the portion of memory consists of 67457 bytes 'a'. The calculated hashsum is compared to that
 * obtained from the GNU implementation of SHA512 (SHA512sum).
 */
#include "Sputnik.hpp"
#include "test.hpp"

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	const uint64_t expectedHashsumArray[8] = {
		0xe711ab943825048b, 0xdd3a1e59288e19be, 0xe9a719a018843819, 0x35cb8cc6f87aa13e,
		0x67c8fcbbff9e768e,	0xdae3fbfea9934999,	0x055c502cd007e181,	0x36be8b322cac481e
	};
	Sputnik::SHA512::Hashsum expectedHashsum;
	expectedHashsum.unmarshall((const uint8_t(&)[64])expectedHashsumArray);

	uint8_t data[67457];
	for (int i = 0; i < 67457; i++) data[i] = 'a';
	Sputnik::SHA512::Hashsum computedHashsum = Sputnik::SHA512::sum(data, 67457);

	assert(computedHashsum == expectedHashsum);

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
