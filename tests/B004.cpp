/**
 * @file B004.cpp
 * 
 * @test B004
 * The hashsum of the vector {'a', 'b', 'c'} is calculated. The calculated hashsum is compared to that obtained
 * from the GNU implementation of SHA512 (SHA512sum)
 */
#include "Sputnik.hpp"
#include "test.hpp"

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	const uint64_t expectedHashsumArray[8] = {
		0xddaf35a193617aba, 0xcc417349ae204131, 0x12e6fa4e89a97ea2, 0x0a9eeee64b55d39a,
		0x2192992a274fc1a8, 0x36ba3c23a3feebbd, 0x454d4423643ce80e, 0x2a9ac94fa54ca49f
	};
	Sputnik::SHA512::Hashsum expectedHashsum;
	expectedHashsum.unmarshall((const uint8_t(&)[64]) expectedHashsumArray);

	uint8_t dataToBeHashed[3] = {'a','b', 'c'};
	Sputnik::SHA512::Hashsum computedHashsum = Sputnik::SHA512::sum(dataToBeHashed, 3);
	
	assert(computedHashsum == expectedHashsum);

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
