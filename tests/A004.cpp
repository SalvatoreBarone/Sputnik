/**
 * @file A004.cpp
 * 
 * @test A004
 * The hashsum of the vector {'a', 'b', 'c'} is calculated. The calculated hashsum is compared to that obtained
 * from the GNU implementation of SHA256 (sha256sum)
 */
#include "Sputnik.hpp"
#include "test.hpp"

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	const uint32_t expectedHashsumArray[8] = {
		0xba7816bf, 0x8f01cfea, 0x414140de, 0x5dae2223, 0xb00361a3, 0x96177a9c, 0xb410ff61, 0xf20015ad
	};
	Sputnik::SHA256::Hashsum expectedHashsum;
	expectedHashsum.unmarshall((const uint8_t(&)[32]) expectedHashsumArray);

	uint8_t dataToBeHashed[3] = {'a','b', 'c'};
	Sputnik::SHA256::Hashsum computedHashsum = Sputnik::SHA256::sum(dataToBeHashed, 3);
	
	assert(computedHashsum == expectedHashsum);

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
