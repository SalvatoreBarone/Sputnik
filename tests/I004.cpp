/**
 * @file I004.cpp
 * 
 * @test I004
 * Testing the entire library with multithreaded programs.
 * The I004 test consists of executing the product between a matrix and a vector of integers, using a multithread
 * approach. After creating two threads representing the two replicas of the 2oo2 system, each replica creates four
 * threads, dividing the number of products to be made equally between them. The goal is to verify that if the
 * programming model proposed by the library is respected, no bugs can arise, the manifestation of which depends
 * on the interleaving of the threads.
 */
#include "Sputnik.hpp"
#include "test.hpp"
#include <thread> 
#include <assert.h>
#include <time.h>
#include <stdlib.h>


using namespace std;

#define N 100

typedef struct {
	int element[N][N]; 
} Matrix;

typedef struct {
	int element[N];
} Vector;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

void client(const Matrix&, const Vector&);
void server(const Matrix&, const Vector&, Vector&);
Sputnik::SafeMemory clientMemory;
Sputnik::SafeMemory serverMemory;

void comp_thread(	const Sputnik::Pointer<Matrix>&,
					const Sputnik::Pointer<Vector>&,
					Vector&,
					int,
					int,
					std::string);

int main() {
	signal(SIGABRT, abortHandler);

	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print("This is the main thread\n");

	srand(time(NULL));   // should only be called once
	
	logger.print("Initializing matrix and vector\n");
	Matrix mat;
	Vector vet, res;
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++)
			mat.element[i][j] = rand();
		vet.element[i] = rand();
	}
	logger.print("Done\nCreating threads\n");

	std::thread server_thr(server, std::cref(mat), std::cref(vet), std::ref(res));
	std::thread client_thr(client, std::cref(mat), std::cref(vet));

	server_thr.join();
	client_thr.join();
	

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}

void client(const Matrix& A, const Vector& B) {
	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print("This is the client thread\n");
	try {
		int localPort = 3001;
		const char* remoteAddress = "127.0.0.1";
		int remotePort = 3002;
		Sputnik::UDPCommunicator communicator(localPort, remoteAddress, remotePort);
		Sputnik::SHA256Voter voter(communicator);
		Sputnik::MemoryManager manager(clientMemory, voter, voteMismatchHandler, NULL);

		logger.print("Reserving memory\n");
		Sputnik::Pointer<Matrix> mat = manager.reserve<Matrix>();
		Sputnik::Pointer<Vector> vet = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> res = manager.reserve<Vector>();
		Vector tmp;

		stringstream ss;
		ss 	<< "mat chunk mask: " <<std::hex << std::setw(16) << std::setfill('0') << mat.getChunkMask() << endl
			<< "vet chunk mask: " <<std::hex << std::setw(16) << std::setfill('0') << vet.getChunkMask() << endl
			<< "res chunk mask: " <<std::hex << std::setw(16) << std::setfill('0') << res.getChunkMask() << endl;
		logger.print(ss.str());

		logger.print("Copying object\n");
		mat.write(A);
		vet.write(B);
		
		ss.str(string());
		ss << "chunk mask: " <<std::hex << std::setw(16) << std::setfill('0') << manager.getChunkStatus() << endl;
		logger.print(ss.str());

		assert(manager.getChunkStatus() != 0 && "Chunks must be dirty");

		#ifdef ASAP_VOTER
		manager.vote();
		assert(manager.getChunkStatus() == 0 && "Chunks must be clean");
		#endif

		logger.print("Creating threads\n");
		std::thread comp1(comp_thread, std::cref(mat), std::cref(vet), std::ref(tmp), 0, N/4, "client comp thread 1\n");
		std::thread comp2(comp_thread, std::cref(mat), std::cref(vet), std::ref(tmp), N/4, N/2, "client comp thread 2\n");
		std::thread comp3(comp_thread, std::cref(mat), std::cref(vet), std::ref(tmp), N/2, 3*N/4, "client comp thread 3\n");
		std::thread comp4(comp_thread, std::cref(mat), std::cref(vet), std::ref(tmp), 3*N/4, N, "client comp thread 4\n");
		logger.print("Waiting threads\n");
		comp1.join();
		comp2.join();
		comp3.join();
		comp4.join();
		logger.print("Computation completed\n");
		
		res.write(tmp);

		#ifdef ASAP_VOTER
		manager.vote();
		assert(manager.getChunkStatus() == 0 && "Chunks must be clean");
		#endif

		#ifdef ASYNC_VOTER
		manager.suspendUntilVoted();
		#endif

		#ifdef ALAP_VOTER
		res.read();
		#endif

	} catch (Sputnik::BadAlloc& e) {
		stringstream ss;
		ss	<< "Bad alloc!" << std::endl
			<< "Total memory (byte): " << e.getTotalMemory() << std::endl
			<< "Available memory (byte): " << e.getAvailableMemory() << std::endl
			<< "Required memory (byte): " << e.getRequiredMemory() << std::endl;
		logger.print(ss.str());
		assert(false && "No exception must be thrown");
	} catch (Sputnik::CommError& e) {
		stringstream ss;
		ss	<< "Bad Connection" << std::endl
			<< e.what() << std::endl;
		logger.print(ss.str());
		assert(false && "No BadConnection exception must be thrown!");
	} catch (Sputnik::VoteMismatch& e) {
		stringstream ss;
		ss 	<< " Bad vote!" << std::endl
			<< "Chunk index: " << (int)e.getChunkIndex() << std::endl;
		
		logger.print(ss.str());
		assert(false && "No VoteMismatch exception must be thrown!");
	}
}

void server(const Matrix& A, const Vector& B, Vector& C) {
	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print("This is the server thread\n");
	try {
		int localPort = 3002;
		const char* remoteAddress = "127.0.0.1";
		int remotePort = 3001;
		Sputnik::UDPCommunicator communicator(localPort, remoteAddress, remotePort);
		Sputnik::SHA256Voter voter(communicator);
		#ifdef ASYNC_VOTER
			Sputnik::MemoryManager manager(serverMemory, voter, voteMismatchHandler, NULL);
		#else
			Sputnik::MemoryManager manager(serverMemory, voter);
		#endif

		logger.print("Reserving memory\n");
		Sputnik::Pointer<Matrix> mat = manager.reserve<Matrix>();
		Sputnik::Pointer<Vector> vet = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> res = manager.reserve<Vector>();
		Vector tmp;

		stringstream ss;
		ss 	<< "mat chunk mask: " <<std::hex << std::setw(16) << std::setfill('0') << mat.getChunkMask() << endl
			<< "vet chunk mask: " <<std::hex << std::setw(16) << std::setfill('0') << vet.getChunkMask() << endl
			<< "res chunk mask: " <<std::hex << std::setw(16) << std::setfill('0') << res.getChunkMask() << endl;
		logger.print(ss.str());
		logger.print("Copying object\n");
		mat.write(A);
		vet.write(B);

		ss.str(string());
		ss << "chunk mask: " <<std::hex << std::setw(16) << std::setfill('0') << manager.getChunkStatus() << endl;
		logger.print(ss.str());

		assert(manager.getChunkStatus() != 0 && "Chunks must be dirty");

		#ifdef ASAP_VOTER
		manager.vote();
		assert(manager.getChunkStatus() == 0 && "Chunks must be clean");
		#endif

		logger.print("Creating threads\n");
		std::thread comp1(comp_thread, std::cref(mat), std::cref(vet), std::ref(tmp), 0, N/4, "server comp thread 1\n");
		std::thread comp2(comp_thread, std::cref(mat), std::cref(vet), std::ref(tmp), N/4, N/2, "server comp thread 2\n");
		std::thread comp3(comp_thread, std::cref(mat), std::cref(vet), std::ref(tmp), N/2, 3*N/4, "server comp thread 3\n");
		std::thread comp4(comp_thread, std::cref(mat), std::cref(vet), std::ref(tmp), 3*N/4, N, "server comp thread 4\n");
		logger.print("Waiting threads\n");
		comp1.join();
		comp2.join();
		comp3.join();
		comp4.join();
		logger.print("Computation completed\n");

		res.write(tmp);

		#ifdef ASAP_VOTER
		manager.vote();
		assert(manager.getChunkStatus() == 0 && "Chunks must be clean");
		#endif

		#ifdef ASYNC_VOTER
		manager.suspendUntilVoted();
		#endif

		C = res.read();

	} catch (Sputnik::BadAlloc& e) {
		stringstream ss;
		ss	<< "Bad alloc!" << std::endl
			<< "Total memory (byte): " << e.getTotalMemory() << std::endl
			<< "Available memory (byte): " << e.getAvailableMemory() << std::endl
			<< "Required memory (byte): " << e.getRequiredMemory() << std::endl;
		logger.print(ss.str());
		assert(false && "No exception must be thrown");
	} catch (Sputnik::CommError& e) {
		stringstream ss;
		ss	<< "Bad Connection" << std::endl
			<< e.what() << std::endl;
		logger.print(ss.str());
		assert(false && "No BadConnection exception must be thrown!");
	} catch (Sputnik::VoteMismatch& e) {
		stringstream ss;
		ss 	<< " Bad vote!" << std::endl
			<< "Chunk index: " << (int)e.getChunkIndex() << std::endl;
		
		logger.print(ss.str());
		assert(false && "No VoteMismatch exception must be thrown!");
	}
}


void comp_thread(	const Sputnik::Pointer<Matrix>& mat,
					const Sputnik::Pointer<Vector>& vet,
					Vector& res,
					int start,
					int end,
					std::string name) {

	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print(name);

	for (int i = start; i < end; i++) {
		res.element[i] = 0;
		for (int j = 0; j < N; j++)
			res.element[i] += mat.read().element[i][j] * vet.read().element[j];
	}

}
