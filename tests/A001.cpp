/**
 * @file A001.cpp
 * 
 * @test A001
 * Use the SHA256::Hashsum: unmarshall() function with a prefixed vector to set the value of the
 * SHA256::Hashsum object. Then use the function SHA256::Hashsum::marshall(), to obtain the value of the
 * hashsum, verifying that the vector produced by the latter function corresponds to the previous vector.
 */
#include "Sputnik.hpp"
#include "test.hpp"


void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	const uint8_t array1[Sputnik::SHA256::Hashsum::size] = {
		 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15,
		16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31
	};
	uint8_t array2[Sputnik::SHA256::Hashsum::size];

	Sputnik::SHA256::Hashsum hashsum;
	hashsum.unmarshall(array1);
	hashsum.marshall(array2);
	for (unsigned i = 0; i < Sputnik::SHA256::Hashsum::size; i++)
		assert(array1[i] == array2[i] && "The two vector must equals");

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}
