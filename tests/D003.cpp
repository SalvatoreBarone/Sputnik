/**
 * @file D003.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * @test D003
 * The test consists in allocation of objects that, in terms of memory requirements, exceed the available 
 * memory, and verifying that an exceptions is thrown.
 */
#include "Sputnik.hpp"
#include "test.hpp"

#include <thread> 

class Classe {
public:
	Classe() {};
private:
	uint8_t member[CHUNK_NUMBER][CHUNK_SIZE];
	int anotherMember;
};

void client();
void server();
Sputnik::SafeMemory clientMemory;
Sputnik::SafeMemory serverMemory;

void abortHandler(int) {
	cout << __FILE__ << TEST_FAILED << endl;
}

int main() {
	signal(SIGABRT, abortHandler);

	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print("I'm the main Thread\n");

	thread server_thr(server);
	thread client_thr(client);

	server_thr.join();
	client_thr.join();

	cout << __FILE__ << TEST_PASSED << endl;
	return 0;
}

void client() {
	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print("I'm the client Thread\n");
	try {
		int localPort = 3001;
		const char* remoteAddress = "127.0.0.1";
		int remotePort = 3002;
		Sputnik::UDPCommunicator communicator(localPort, remoteAddress, remotePort);
		Sputnik::SHA256Voter voter(communicator);
		Sputnik::MemoryManager manager(clientMemory, voter, voteMismatchHandler, NULL);
		logger.print("Trying to reserve memory...\n");
		Sputnik::Pointer<Classe> ptr = manager.reserve<Classe>();
		ptr.read();
		assert (false && "An exception must be thrown");
	} catch (Sputnik::BadAlloc& e) {
		std::stringstream ss;
		ss << "Bad alloc!" << std::endl;
		ss << "Total memory (byte): " << e.getTotalMemory() << std::endl;
		ss << "Available memory (byte): " << e.getAvailableMemory() << std::endl;
		ss << "Required memory (byte): " << e.getRequiredMemory() << std::endl;
		logger.print(ss.str());
	} catch (Sputnik::CommError& e) {
		std::stringstream ss;
		ss << "Bad Connection" << std::endl;
		ss << e.what() << std::endl;
		assert (false && "No BadConnection exception must be thrown!\n");
	}
}

void server() {
	Sputnik::Logger& logger = Sputnik::Logger::getLogger();
	logger.print("I'm the server Thread\n");
	try {
		int localPort = 3002;
		const char* remoteAddress = "127.0.0.1";
		int remotePort = 3001;
		Sputnik::UDPCommunicator communicator(localPort, remoteAddress, remotePort);
		Sputnik::SHA256Voter voter(communicator);
		#ifdef ASYNC_VOTER
			Sputnik::MemoryManager manager(serverMemory, voter, voteMismatchHandler, NULL);
		#else
			Sputnik::MemoryManager manager(serverMemory, voter);
		#endif
		logger.print("Trying to reserve memory...\n");
		Sputnik::Pointer<Classe> ptr = manager.reserve<Classe>();
		ptr.read();
		assert (false && "An exception must be thrown");
	} catch (Sputnik::BadAlloc& e) {
		std::stringstream ss;
		ss << "Bad alloc!" << std::endl;
		ss << "Total memory (byte): " << e.getTotalMemory() << std::endl;
		ss << "Available memory (byte): " << e.getAvailableMemory() << std::endl;
		ss << "Required memory (byte): " << e.getRequiredMemory() << std::endl;
		logger.print(ss.str());
	} catch (Sputnik::CommError& e) {
		std::stringstream ss;
		ss << "Bad Connection" << std::endl;
		ss << e.what() << std::endl;
		assert (false && "No BadConnection exception must be thrown!\n");
	}
}
