/**
 * @file test.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 */

#ifndef __TEST_HEADER_FILE__
#define __TEST_HEADER_FILE__

#include <cassert>
#include <csignal>
#include <iostream>
using namespace std;

#define TERM_COLOR_DEFAULT	"\e[0m"
#define TERM_COLOR_RED		"\e[1;31m"
#define TERM_COLOR_GREEN	"\e[1;32m"
#define TERM_COLOR_BLUE		"\e[1;34m"
#define TERM_COLOR_YELLOW	"\e[1;33m"
#define TERM_COLOR_MAGENTA	"\e[1;35m"
#define TERM_COLOR_CYAN		"\e[1;36m"

#define TEST_PASSED "\e[1;32m [PASSED] \e[0m"
#define TEST_FAILED "\e[1;31m [FAILED] \e[0m"

void voteMismatchHandler(void*) {
	cout << " Bad vote!" << std::endl;
	assert(false && "No VoteMismatch exception must be thrown!");
}

#endif
