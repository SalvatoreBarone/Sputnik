/**
 * @file FreeRTOS_senseAndCompute.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 * 
 * @example FreeRTOS_senseAndCompute.cpp
 * Semplice esempio di creazione thread periodici con FreeRTOS.
 * 
 * L'esempio mostra tutto ciò che è necessario alla creazione di task periodici, alla definizione
 * del loro periodo, alla definizione delle loro priorità ed alla definizione delle politiche di
 * scheduling.
 * 
 * Vengono creati due thread, uno di sensing e l'altro di computing, periodici e sincronizzati tra
 * loro. Il task di computing attende che sia avvenuto il sensing, mentre il task di sensing attende
 * che siano stati "consumati" i valori sensiti, da parte del task di computing.
 * Entrambi i thread eseguono un numero di cicli di sensing e comptuting pari alla costante
 * CICLI_SENSING, poi terminano ed il controllo ritorna al main, che, nel frattempo, viene messo in
 * attesa del completamento delle operazioni di sensing e computing.
 */
#include "FreeRTOS.h"
#include "task.h"

#include "Sputnik.hpp"
#include "xparameters.h"

// Indirizzi base dei GPIO per led e btn
#define led_base 0x41200000
#define btn_base 0x41210000

// Safe memory non dovrebbe stare nello stack del main-thread.
// CHUNK_SIZE = 256 byte
// CHUNK_NUMBER = 32
// totale = 8192 byte
// C'è spazio per 2048 interi;
Sputnik::SafeMemory safeMemory;

#define N 6 	// 6 * 4 * 6 = 144 byte usati su 160 disponibili

typedef struct {
	int element[N];
} Vector;


static void compute(void*);
static void sense(void*);

void votingMismatchHandler(void*);

// Struttura dati usata per il passaggio dei parametri ai task di sense e computing
typedef struct {
	Sputnik::MemoryManager* manager;

	// dati allocati in memoria sicura
	Sputnik::Pointer<Vector>* Re_A;
	Sputnik::Pointer<Vector>* Re_B;
	Sputnik::Pointer<Vector>* Re_Res;
	Sputnik::Pointer<Vector>* Im_A;
	Sputnik::Pointer<Vector>* Im_B;
	Sputnik::Pointer<Vector>* Im_Res;

	// il mutex viene usato per impedire che "sense" modifichi i dati mentre "compute" sta eseguendo il calcolo.
	Sputnik::FreeRTOSMutex* mutex;
	// se non è stato eseguito il sensing, il thread "compute" va bloccato
	bool sensed;
	// la c.v. viene usata per bloccare "compute" fino a quando non viene effettuato il sensing
	Sputnik::FreeRTOSConditionVariable* cv;
} myTaskParameters_t;

int main() {
	
	Xil_Out32(led_base + 4, 0);
	
	try {
		Sputnik::DualSPI communicator(
				Sputnik::SPI::SPI0,
				Sputnik::SPI::SPI1,
				Sputnik::SPI::slave1,
				Sputnik::SPI::SCKLPolarityLow,
				Sputnik::SPI::sampleOnFirstEdge,
				Sputnik::SPI::prescaler4,
				Sputnik::SPI::manualChipSelectEnable,
				Sputnik::SPI::manualStartEnable);

		Sputnik::SHA256Voter voter(communicator);
		Sputnik::MemoryManager manager(safeMemory, voter, votingMismatchHandler);
		// Per come sono allocati i vettori, ogni scrittura su Re_Res sporca il chunk dove risiede anche Re_B,
		// bloccando il task in attesa del voting. La stessa situazione si ripresenta con Im_B e Im_Res.
		// N.B. È buona norma allocare i chunk su cui vengono effettuare operazioni di lettura in chunk
		// dove non ci siano dati da leggere.
		Sputnik::Pointer<Vector> Re_A = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> Re_B = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> Im_A = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> Im_B = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> Re_Res = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> Im_Res = manager.reserve<Vector>();

		// il mutex viene usato per impedire che "sense" modifichi i dati mentre "compute" sta eseguendo il calcolo.
		Sputnik::FreeRTOSMutex mutex;
		Sputnik::FreeRTOSConditionVariable cv;

		// inizializzazione dei parametri da passare ai task
		myTaskParameters_t par;

		par.manager = &manager;

		par.Re_A = &Re_A;
		par.Re_B = &Re_B;
		par.Re_Res = &Re_Res;
		par.Im_A = &Im_A;
		par.Im_B = &Im_B;
		par.Im_Res = &Im_Res;

		par.mutex = &mutex;
		par.sensed = false;
		par.cv = &cv;

		xTaskCreate(
			sense,
			"sense",
			(configMINIMAL_STACK_SIZE + 2048),
			(void*) &par,
			(tskIDLE_PRIORITY + 2),
			NULL);

		xTaskCreate(
			compute,
			"compute",
			(configMINIMAL_STACK_SIZE + 4096),
			(void*) &par,
			(tskIDLE_PRIORITY + 5),
			NULL);


		vTaskStartScheduler();

	} catch (Sputnik::CommError& e) {
		Xil_Out32(led_base, 0x1);
		exit(0);
	} catch (Sputnik::BadAlloc& e) {
		Xil_Out32(led_base, 0x3);
		exit(0);
	}

	for (;;);
	
	return 0;
}

void votingMismatchHandler(void*) {
	Xil_Out32(led_base, 0xf);
	while (Xil_In32(btn_base) != 0x1);
	Xil_Out32(led_base, 0x0);
}

// ***************************************************************************************************************************
// task di computing
// attende che avvenga il "sensing" da parte del thread sense
// effettua il prodotto tra due vettori di numeri complessi
// scrive il risultato
// resetta il flag sensed
static void compute(void* par) {
	TickType_t wakeUp;
	const TickType_t blockTime = pdMS_TO_TICKS(100);
	wakeUp = xTaskGetTickCount();

	myTaskParameters_t* myPar = (myTaskParameters_t*) par;

	for (;;) {
		vTaskDelayUntil(&wakeUp, blockTime);

		// attendo che avvenga il sensing
		myPar->mutex->lock();
		while (!myPar->sensed)
			myPar->cv->wait(*(myPar->mutex));

		// quando il sensing è avvenuto, ed il task compute risvegliato, la riacquisizione del mutex è già avvenuta
		// si può procedere al calcolo.
		// da qualche parte, il thread compute deve bloccarsi in attesa del voting!
		// il voting è asincrono e avviene con periodo 10 ms
		try {
			for (unsigned i = 0; i < N; i++) {
				myPar->Re_Res->write().element[i] 	= myPar->Re_A->read().element[i] * myPar->Re_B->read().element[i]
													- myPar->Im_A->read().element[i] * myPar->Im_B->read().element[i];

				myPar->Im_Res->write().element[i]	= myPar->Re_A->read().element[i] * myPar->Im_B->read().element[i]
													+ myPar->Im_A->read().element[i] * myPar->Im_B->read().element[i];
			}
		} catch (Sputnik::VoteMismatch& e) {
			Xil_Out32(led_base, 0xf);
			while (Xil_In32(btn_base) != 0x1);
			Xil_Out32(led_base, 0x0);
		}

		myPar->sensed = false;

		// unlock del mutex
		myPar->mutex->unlock();
	}
}

// ***************************************************************************************************************************
// task di sensing
// scrive sui dati di origine
// setta il flag sensed ed effettua una signal, in modo che il thread "compute" venga risvegliato ed esegua i calcoli
static void sense(void* par) {
	TickType_t wakeUp;
	const TickType_t blockTime = pdMS_TO_TICKS(100);
	wakeUp = xTaskGetTickCount();

	myTaskParameters_t* myPar = (myTaskParameters_t*) par;

	for (;;) {
		vTaskDelayUntil(&wakeUp, blockTime);

		// attendo che avvenga il sensing
		myPar->mutex->lock();

		for (unsigned i = 0; i < N; i++) {
			myPar->Re_A->write().element[i] = i*3;
			myPar->Re_B->write().element[i] = i*7;
			myPar->Im_A->write().element[i] = i*11;
			myPar->Im_A->write().element[i] = i*13;
		}
		
		myPar->sensed = true;
		myPar->cv->signal();
		// unlock del mutex
		myPar->mutex->unlock();
	}
}
