/**
 * @file Xenomai_simple.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 * 
 * @example Xenomai_simple.cpp
 * Semplice esempio di creazione thread periodici con Xenomai.
 * 
 * L'esempio mostra tutto ciò che è necessario alla creazione di task periodici, alla definizione
 * del loro periodo, alla definizione delle loro priorità ed alla definizione delle politiche di
 * scheduling.
 * 
 * Vengono creati due thread, uno di sensing e l'altro di computing, periodici e sincronizzati tra
 * loro. Il task di computing attende che sia avvenuto il sensing, mentre il task di sensing attende
 * che siano stati "consumati" i valori sensiti, da parte del task di computing.
 * Entrambi i thread eseguono un numero di cicli di sensing e comptuting pari alla costante
 * CICLI_SENSING, poi terminano ed il controllo ritorna al main, che, nel frattempo, viene messo in
 * attesa del completamento delle operazioni di sensing e computing.
 */
#include "Sputnik.hpp"
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <pthread.h>
#include <sys/syscall.h>

// Dimensione dello stack dei thread
#define VOTER_THREAD_STACK_SIZE 4096
// Numero di cicli
#define CICLI_SENSING 10
// Dimensioni dei vettori
#define N 6 // 6 * 4 * 6 = 144 byte usati
typedef struct {
	int element[N];
} Vector;

void* sense(void*);
void* compute(void*);

// Struttura dati usata per il passaggio delle informazioni per la creazione di un UdpRtCommunicator
// alle due repliche.
typedef struct {
	const char* name; // nome della replica
} SystemPat_t;


// Struttura dati usata per il passaggio dei parametri ai task di sense e computing
typedef struct {
	// nome della replica a cui il thread appartiene
	const char* name;
	// il mutex viene usato per impedire che "sense" modifichi i dati mentre "compute" sta eseguendo
	// il calcolo.
	Sputnik::XenomaiMutex* mutex;
	// se non è stato eseguito il sensing, il thread "compute" va bloccato
	bool sensed;
	// la c.v. viene usata per bloccare "compute" fino a quando non viene effettuato il sensing
	Sputnik::XenomaiConditionVariable* cv;
} senseAndComputeTaskPar_t;


// *************************************************************************************************
// Main
int main() {
	Sputnik::XenomaiLogger& logger = Sputnik::XenomaiLogger::getLogger();

	logger.print("main", "avvio del thread");

	// il mutex viene usato per impedire che "sense" modifichi i dati mentre "compute" sta
	// eseguendo il calcolo.
	Sputnik::XenomaiMutex mutex;
	Sputnik::XenomaiConditionVariable cv;
	logger.print("main", "creazione mutex e c.v. per sense e compute completata");
	
	// inizializzazione dei parametri da passare ai task di sense & compute
	senseAndComputeTaskPar_t parameters;

	parameters.name = "main";
	parameters.sensed = false;
	parameters.mutex = &mutex;
	parameters.cv = &cv;

	logger.print("main", "impostazione parametri sense e compute completata");
	
	// creazione dei thread di sense and compute
	pthread_attr_t sense_attr, compute_attr;
	pthread_t senseThread, computeThread;
	
	// setting attributi e scheduling
	pthread_attr_init(&sense_attr);
	pthread_attr_setstacksize(&sense_attr, VOTER_THREAD_STACK_SIZE);
	pthread_attr_setinheritsched(&sense_attr, PTHREAD_EXPLICIT_SCHED);
	pthread_attr_setschedpolicy(&sense_attr, SCHED_FIFO);
	pthread_attr_setdetachstate(&sense_attr, PTHREAD_CREATE_JOINABLE);
	struct sched_param sense_schedpar = {.sched_priority = 80};
	pthread_attr_setschedparam(&sense_attr, &sense_schedpar);

	pthread_attr_init(&compute_attr);
	pthread_attr_setstacksize(&compute_attr, VOTER_THREAD_STACK_SIZE);
	pthread_attr_setinheritsched(&compute_attr, PTHREAD_EXPLICIT_SCHED);
	pthread_attr_setschedpolicy(&compute_attr, SCHED_FIFO);
	pthread_attr_setdetachstate(&compute_attr, PTHREAD_CREATE_JOINABLE);
	struct sched_param compute_schedpar = {.sched_priority = 80};
	pthread_attr_setschedparam(&compute_attr, &compute_schedpar);
	
	logger.print("main", "creazione attributi thread di sense and compute completata");
	
	pthread_create(&computeThread, &compute_attr, compute, (void*)&parameters);
	pthread_create(&senseThread, &sense_attr, sense, (void*)&parameters);
	pthread_setname_np(senseThread, "Sense Thread");
	pthread_setname_np(computeThread, "Compute Thread");
	
	logger.print("main", "creazione thread di sense e compute completata");
	
	pthread_join(senseThread, NULL);
	pthread_join(computeThread, NULL);

	return 0;
}

// *************************************************************************************************
// task di computing
// attende che avvenga il "sensing" da parte del thread sense
// effettua il prodotto tra due vettori di numeri complessi
// scrive il risultato
// resetta il flag sensed
void* compute(void* par) {
	
	// Configurazione delle modalita' con cui il task periodico verra' risvegliato
	// viene usato il segnale SIGALRM, spedito al thread a seconda del suo identificativo.
	struct sigevent sevent;
	sigset_t set;
	int signum = SIGALRM;
	sevent.sigev_notify = SIGEV_THREAD_ID;
	sevent.sigev_notify_thread_id = syscall(__NR_gettid);
	sevent.sigev_signo = signum;
	sigemptyset(&set);
	sigaddset(&set, signum);
	sigprocmask(SIG_BLOCK, &set, NULL);
	
	// Creazione del timer.
	// Viene creato un timer periodico, di periodo 1s e ritardo iniziale 1s per rendere periodico
	// il task.
	timer_t timer;	
	struct itimerspec new_value, old_value;
	// creazione del timer
	timer_create (CLOCK_MONOTONIC, &sevent, &timer);
	new_value.it_interval.tv_sec = 1;	// periodo (secondi)
	new_value.it_interval.tv_nsec = 0;	// periodo (nanosecondi)
	new_value.it_value.tv_sec = 1;		// ritardi di inizio conteggio (secondi)
	new_value.it_value.tv_nsec = 0;		// ritardo di inizio conteggio (nanosecondi)
	// set delle caratteristiche del timer
	timer_settime (timer, 0, &new_value, &old_value);
	
	senseAndComputeTaskPar_t* myPar = (senseAndComputeTaskPar_t*) par;
	Sputnik::XenomaiLogger& logger = Sputnik::XenomaiLogger::getLogger();
	std::string thread_name = std::string(myPar->name) + " compute";
	std::string ciclo = "ciclo ";
	
	for (int i = 0; i < CICLI_SENSING; i++) {
		logger.print(thread_name, ciclo + std::to_string(i));
		// aspetto il prossimo periodo
		// viene effettuata una lettura sul timer periodico utilizzandone il descrittore
		// la lettura causa il blocco del thread chiamante fin quando il timer non genererà
		// l'interruzione, ad ogni periodo, capace di risvegliare il thread.
		long unsigned ticks;
		
		if (sigwait (&set, &signum) == -1)
			perror ("sigwait");
		logger.print(thread_name, "compute risvegliato dal timer");
		ticks = timer_getoverrun (timer);
		if (ticks > 0)
			logger.print(thread_name, "deadline miss! Ticks=" + std::to_string(ticks));

		// attendo che avvenga il sensing
		myPar->mutex->lock();
		while (!myPar->sensed) {
			logger.print(thread_name, "sensed e' false");
			logger.print(thread_name, "attendo che avvenga il sensing");
			myPar->cv->wait(*(myPar->mutex));
		}
		logger.print(thread_name, "sensed e' true; procedo con computing");

		// computing
		
		myPar->sensed = false;

		logger.print(thread_name, "termine computing, signaling");
		// signal su c.v. ed unlock del mutex
		myPar->cv->broadcast();
		myPar->mutex->unlock();
	}
	
	return 0;
}

// *************************************************************************************************
// task di sensing
// scrive sui dati di origine
// setta il flag sensed ed effettua una signal, in modo che il thread "compute" venga risvegliato ed
// esegua i calcoli
void* sense(void* par) {
	// Configurazione delle modalita' con cui il task periodico verra' risvegliato
	// viene usato il segnale SIGALRM, spedito al thread a seconda del suo identificativo.
	struct sigevent sevent;
	sigset_t set;
	int signum = SIGALRM;
	sevent.sigev_notify = SIGEV_THREAD_ID;
	sevent.sigev_notify_thread_id = syscall(__NR_gettid);
	sevent.sigev_signo = signum;
	sigemptyset(&set);
	sigaddset(&set, signum);
	sigprocmask(SIG_BLOCK, &set, NULL);
	
	// Creazione del timer.
	// Viene creato un timer periodico, di periodo 1s e ritardo iniziale 1s per rendere periodico
	// il task.
	timer_t timer;	
	struct itimerspec new_value, old_value;
	// creazione del timer
	timer_create (CLOCK_MONOTONIC, &sevent, &timer);
	new_value.it_interval.tv_sec = 1;	// periodo (secondi)
	new_value.it_interval.tv_nsec = 0;	// periodo (nanosecondi)
	new_value.it_value.tv_sec = 1;		// ritardi di inizio conteggio (secondi)
	new_value.it_value.tv_nsec = 0;		// ritardo di inizio conteggio (nanosecondi)
	// set delle caratteristiche del timer
	timer_settime (timer, 0, &new_value, &old_value);
	
	senseAndComputeTaskPar_t* myPar = (senseAndComputeTaskPar_t*) par;
	Sputnik::XenomaiLogger& logger = Sputnik::XenomaiLogger::getLogger();
	std::string thread_name = std::string(myPar->name) + " sense";
	std::string ciclo = "ciclo ";
	for (int i = 0; i < CICLI_SENSING; i++) {
		logger.print(thread_name, ciclo + std::to_string(i));
		// aspetto il prossimo periodo
		// viene effettuata una lettura sul timer periodico utilizzandone il descrittore
		// la lettura causa il blocco del thread chiamante fin quando il timer non genererà
		// l'interruzione, ad ogni periodo, capace di risvegliare il thread.
		long unsigned ticks;
		
		if (sigwait (&set, &signum) == -1)
			perror ("sigwait");
		logger.print(thread_name, "sense risvegliato dal timer");
		ticks = timer_getoverrun (timer);
		if (ticks > 0)
			logger.print(thread_name, "deadline miss! Ticks=" + std::to_string(ticks));
		
		// attendo che sia avvenuto il computing
		myPar->mutex->lock();
		while (myPar->sensed) {
			logger.print(thread_name, "sensed e' true; attendo che avvenga il computing");
			myPar->cv->wait(*(myPar->mutex));
		}
		logger.print(thread_name, "sensed e' false; procedo con il sensing");

		// sensing

		logger.print(thread_name, "sensing completato, signaling");
		
		myPar->sensed = true;
		// signal su c.v. ed unlock del mutex
		myPar->cv->broadcast();
		myPar->mutex->unlock();
	}
	
	return 0;
}

