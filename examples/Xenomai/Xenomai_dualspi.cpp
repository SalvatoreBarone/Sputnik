/**
 * @file Xenomai_dualspi.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 * 
 * @example Xenomai_dualspi.cpp
 * Semplice esempio d'uso del driver Dual SPI per Xenomai.
 */
#include "Sputnik.hpp"
#include <string.h>
#include <assert.h>
#include <sys/mman.h>

#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

void* thr(void*);

int main() {
	
	mlockall(MCL_CURRENT | MCL_FUTURE);

	// creazione del thread "replica"
	pthread_attr_t replica_attr;
	pthread_t replicaThread;
	
	// setting attributi e scheduling
	pthread_attr_init(&replica_attr);
	pthread_attr_setstacksize(&replica_attr, 1<<24); // stack size  64K
	pthread_attr_setinheritsched(&replica_attr, PTHREAD_EXPLICIT_SCHED);
	pthread_attr_setschedpolicy(&replica_attr, SCHED_FIFO);
	pthread_attr_setdetachstate(&replica_attr, PTHREAD_CREATE_JOINABLE);
	struct sched_param replica_schedpar = {.sched_priority = 99};
	pthread_attr_setschedparam(&replica_attr, &replica_schedpar);
	
	pthread_create(&replicaThread, &replica_attr, thr, NULL);
	pthread_join(replicaThread, NULL);
	
	return 0;
}


void* thr(void*) {
	Sputnik::XenomaiLogger &logger = Sputnik::XenomaiLogger::getLogger();
	std::stringstream ss;
	
	size_t size = 5;
	uint8_t ciao[128] = "ciao";
	
	const size_t sha512size = 64;
	const uint8_t sha512sum[] = {
		0xf1, 0x92, 0x3c, 0x7e, 0x15, 0x3b, 0xff, 0x27,
		0x26, 0x7d, 0x8c, 0xf0, 0x59, 0xf2, 0x62, 0x07,
		0x5b, 0xcf, 0x5e, 0x6d, 0xac, 0xaa, 0xf3, 0x9e,
		0xdd, 0x50, 0x6c, 0x82, 0x3d, 0xd9, 0xb2, 0xbf,
		0xdf, 0xbd, 0x8d, 0x91, 0xb6, 0x48, 0x17, 0x96,
		0x72, 0xe1, 0xd9, 0x19, 0x6b, 0x16, 0xf1, 0xe2,
		0xf3, 0xd0, 0x2e, 0x67, 0x45, 0xf7, 0xc2, 0xa2,
		0x82, 0xfd, 0x1f, 0x64, 0x51, 0x89, 0xd4, 0xdf
	};

	const size_t sha256size = 32;
	const uint8_t sha256sum[] = {
			0x25, 0x1f, 0x34, 0xf7, 0x73, 0x89, 0x5b, 0x95,
			0x7b, 0x99, 0x8a, 0xd0, 0x3c, 0x71, 0x91, 0x90,
			0xab, 0x10, 0x13, 0xa5, 0x8c, 0xc9, 0x8d, 0x6e,
			0xb5, 0xdc, 0xa7, 0x12, 0x12, 0x16, 0x16, 0x4e
	};

	uint8_t recvbuff[128] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	
	try {
		Sputnik::DualSPI communicator(
					Sputnik::SPI::SPI0,
					Sputnik::SPI::SPI1,
					Sputnik::SPI::slave1,
					Sputnik::SPI::SCKLPolarityHigh,
					Sputnik::SPI::sampleOnFirstEdge,
					Sputnik::SPI::prescaler256,
					Sputnik::SPI::manualChipSelectEnable,
					Sputnik::SPI::manualStartEnable);
		
		communicator.sendRecv(ciao, recvbuff, size);
		
		ss.str(std::string());
		ss << "recvbuff:";
		for (size_t i = 0; i < size; i++)
				ss << " 0x" << std::hex << std::setw(2) << std::setfill('0') << (int) recvbuff[i];
		logger.print(ss.str());
		
		communicator.sendRecv(sha256sum, recvbuff, sha256size);
		ss.str(std::string());
		ss << "recvbuff:";
		for (size_t i = 0; i < sha256size; i++)
				ss << " 0x" << std::hex << std::setw(2) << std::setfill('0') << (int) recvbuff[i];
		logger.print(ss.str());
		
		communicator.sendRecv(sha512sum, recvbuff, sha512size);
		ss.str(std::string());
		ss << "recvbuff:";
		for (size_t i = 0; i < sha512size; i++)
				ss << " 0x" << std::hex << std::setw(2) << std::setfill('0') << (int) recvbuff[i];
		logger.print(ss.str());
		
	}
	catch (Sputnik::CommError& e) {
		cout << e.what() << endl;
	}
	return 0;
}
