project(SenseAndCompute LANGUAGES C CXX)
cmake_minimum_required(VERSION 3.7)
enable_testing()

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/libs)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/libs)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

set(CMAKE_CXX_STANDARD 11)
set(CXX_STANDARD_REQUIRED ON)

set(CMAKE_C_COMPILER arm-linux-gnueabihf-gcc)
set(CMAKE_CXX_COMPILER arm-linux-gnueabihf-g++)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -march=armv7-a -mfpu=vfp3 -mfloat-abi=hard -D_GNU_SOURCE -D_REENTRANT -fasynchronous-unwind-tables -D__COBALT__ -D__COBALT_WRAP__ -O0 -Wall -Wextra -ggdb3 -D__DEBUG -DCHUNK_NUMBER=32 -DCHUNK_SIZE_2POW=12 -DSPUTNIK_ON_XENOMAI -DTARGET_XILZ7000 -DASYNC_VOTER -DVOTER_THREAD_PERIOD=50 -DVOTER_THREAD_PRIORITY=90 -DVOTER_THREAD_STACK_SIZE=4096")
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -Wl,--no-as-needed -Wl,@/usr/xenomai/lib/cobalt.wrappers -Wl,@/usr/xenomai/lib/modechk.wrappers  /usr/xenomai/lib/xenomai/bootstrap-pic.o  -L/usr/xenomai/lib -lcobalt -lmodechk -lpthread -lrt -march=armv7-a")

include_directories(
/root/Sputnik/src/
/usr/xenomai/include/cobalt
/usr/xenomai/include)

link_directories(/root/Sputnik/libs)
link_libraries(-lSputnik)

add_executable(dualspi Xenomai_dualspi.cpp)
add_executable(spi Xenomai_spi.cpp)
add_executable(simple Xenomai_simple.cpp)
add_executable(senseAndCompute Xenomai_senseAndCompute.cpp)
add_executable(sim Xenomai_simulation.cpp)
