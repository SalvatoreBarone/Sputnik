/**
 * @file Xenomai_spi.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 * 
 * @example Xenomai_spi.cpp
 * Semplice esempio d'uso del driver SPI per Xenomai.
 */
#include "Sputnik.hpp"
#include <string.h>
#include <assert.h>
#include <sys/mman.h>

#include <iostream>
#include <iomanip>
using namespace std;

int main() {
	
	size_t size = 5;
	uint8_t sendbuff[128] = "ciao";
	uint8_t recvbuff[128] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, };
	uint8_t feedback[128] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, };
	uint8_t slave_fb [128] = {5, 4, 3, 2, 1, 0};
	
	mlockall(MCL_CURRENT | MCL_FUTURE);


	try {
		Sputnik::SPI master(
			Sputnik::SPI::SPI0,
			Sputnik::SPI::deviceIsMaster,
			Sputnik::SPI::slave1,
			Sputnik::SPI::SCKLPolarityHigh,
			Sputnik::SPI::sampleOnFirstEdge,
			Sputnik::SPI::prescaler256,
			Sputnik::SPI::manualChipSelectEnable,
			Sputnik::SPI::manualStartEnable);
		
		Sputnik::SPI slave(
			Sputnik::SPI::SPI1,
			Sputnik::SPI::deviceIsSlave,
			Sputnik::SPI::none,
			Sputnik::SPI::SCKLPolarityHigh,
			Sputnik::SPI::sampleOnFirstEdge);
		
		cout << "Device inizializzati" << endl;
	
		slave.enable();						// abilitazione del device
		slave.setTxFifoThreshold(size);		// setting della soglia di trasmissione
		slave.setRxFifoThreshold(size);		// setting della soglia di ricezione
		slave.fillTxFifo(slave_fb, size);	// riempimendo della coda di trasmissione
		
		cout << "Slave configurato" << endl;
			
		master.setTxFifoThreshold(size);					// impostazione soglia trasmissione
		master.setRxFifoThreshold(size);					// impostazione soglia ricezione
		master.setSlaveSelected(Sputnik::SPI::slave1);	// selezione dello slave
		
		cout << "Master configurato" << endl;

		master.enable();							// abilitazione del device
		if (master.isManualChipSelectEnabled())		// se si usa "manual chip select"
			master.assertSlaveSelect();				// il segnale slave select viene asserito
		master.fillTxFifo(sendbuff, size);			// viene riempita la coda di trasmissione
		if (master.isManualStartEnabled())			// se si usa "manual start"
			master.startTransmission();				// viene avviato il trasferimento
		
		cout << "sendbuff:";
		for (size_t i = 0; i < size; i++)
				cout << " 0x" << std::hex << std::setw(2) << std::setfill('0') << (int) sendbuff[i];
		cout << endl;
			
		// si attente il termine del trasferimento
		// l'attesa avviene in polling sulla condizione rx fifo not empty
		cout << "Attendo il termine del trasferimento (master)" << endl;
		while (!master.isRXFifoNotEmpty());
		
		master.readRxFifo(feedback, size);		// viene effettuata la lettura della rx-fifo
		if (master.isManualChipSelectEnabled())	// se si usa "manual chip select"
			master.deassertSlaveSelect();		// il segnale slave select viene deasserito
		master.disable();						// il device viene disabilitato
		
		cout << "feedback:";
		for (size_t i = 0; i < size; i++)
				cout << " 0x" << std::hex << std::setw(2) << std::setfill('0') << (int) feedback[i];
		cout << endl;
			
		// attesa del termine della ricezione dei dati dal master
		// l'attesa avviene con polling sul flag "rx-fifo not empty".
		cout << "Attendo il termine del trasferimento (slave)" << endl;
		while (!slave.isRXFifoNotEmpty());
		slave.readRxFifo(recvbuff, size);	// lettura dei dati ricevuti dal master
		slave.disable();					// disabilitazione del device
		
		cout << "recvbuff:";
		for (size_t i = 0; i < size; i++)
				cout << " 0x" << std::hex << std::setw(2) << std::setfill('0') << (int) recvbuff[i];
		cout << endl;
		
		// feedback deve essere uguale a slave_fb
		assert(memcmp(feedback, slave_fb, size) == 0 && "feedback != slave_fb");
		// recvbuff deve essere uguale a sendbuff
		assert(memcmp(sendbuff, recvbuff, size) == 0 && "sendbuff != recvbuff");
	}
	catch (Sputnik::CommError& e) {
		cout << e.what() << endl;
	}
	
	
	return 0;
}
