/**
 * @file Xenomai_simulation.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 * 
 * @example Xenomai_simulation.cpp
 * Simulazione di una situazione d'uso reale.
 * 
 * In questo esempio il main-thread crea due thread che eseguono la stessa funzione, come in un caso
 * di uso reale, creando due repliche identiche che si voteranno a vicenda il risultato del calcolo
 * di un prodotto vettoriale.
 * 
 * L'esempio mostra tutto ciò che è necessario alla creazione di task periodici, alla definizione
 * del loro periodo, alla definizione delle loro priorità ed alla definizione delle politiche di
 * scheduling, e, ovviamente, come queste cose vanno integrate con gli oggetti e le funzionalità
 * offerte da Sputnik.
 * 
 * Ogni replica esegue crea due thread, uno di sensing e l'altro di computing, periodici e
 * sincronizzati tra loro. Ciascuno dei task di computing attende che sia avvenuto il sensing da
 * parte dei thread di sensing appartenente alla sua stessa "replica", mentre il task di sensing
 * attende che siano stati "consumati" i valori sensiti, da parte del task di computing.
 * 
 * Entrambi i thread eseguono un numero di cicli di sensing e comptuting pari alla costante
 * CICLI_SENSING, poi terminano ed il controllo ritorna al main, che, nel frattempo, viene messo in
 * attesa del completamento delle operazioni di sensing e computing.
 */
#include "Sputnik.hpp"
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <pthread.h>
#include <sys/syscall.h>
#include <iostream>


// Dimensione dello stack dei thread
#define VOTER_THREAD_STACK_SIZE 4096
// Numero di cicli
#define CICLI_SENSING 10
// Dimensioni dei vettori
#define N 6 // 6 * 4 * 6 = 144 byte usati
typedef struct {
	int element[N];
} Vector;

// Funzione eseguita dal thread che simula una delle due repliche
void* replica(void*);
// Ogni replica crea due thread periodici:
// - task di sensing: simula il "sensing", creando dati di origine, poi setta il flag sensed ed
//   effettua una signal, in modo che il thread "compute" venga risvegliato ed esegua i calcoli
// - task di computing, che attende che avvenga il "sensing" da parte del thread sense, effettua il
//   prodotto tra due vettori di numeri complessi, scrive il risultato e resetta il flag "sensed"
void* sense(void*);
void* compute(void*);

#ifdef ASYNC_VOTER
void votingMismatchHandler(void*) {
	Sputnik::XenomaiLogger& logger = Sputnik::XenomaiLogger::getLogger();
	logger.print("Vote mismatch");
}
#endif

// Struttura dati usata per il passaggio delle informazioni per la creazione di un UdpRtCommunicator
// alle due repliche.
typedef struct {
	int localPort;	// porta UDP su cui il communicator "locale" alla replica sarà in ascolto
	int remotePort; // porta UDP su cui il communicator "remoto" sarà in ascolto
	const char* name; // nome della replica
} SystemPat_t;


// Struttura dati usata per il passaggio dei parametri ai task di sense e computing
typedef struct {
	// puntatore al manager che gestisce la memoria della replica
	Sputnik::MemoryManager* manager;
	// puntatore ai dati allocati in memoria sicura
	Sputnik::Pointer<Vector>* Re_A;
	Sputnik::Pointer<Vector>* Re_B;
	Sputnik::Pointer<Vector>* Re_Res;
	Sputnik::Pointer<Vector>* Im_A;
	Sputnik::Pointer<Vector>* Im_B;
	Sputnik::Pointer<Vector>* Im_Res;
	
	// nome della replica a cui il thread appartiene
	const char* name;
	// il mutex viene usato per impedire che "sense" modifichi i dati mentre "compute" sta eseguendo
	// il calcolo.
	Sputnik::XenomaiMutex* mutex;
	// se non è stato eseguito il sensing, il thread "compute" va bloccato
	bool sensed;
	// la c.v. viene usata per bloccare "compute" fino a quando non viene effettuato il sensing
	Sputnik::XenomaiConditionVariable* cv;
} senseAndComputeTaskPar_t;


// *************************************************************************************************
// Main
int main() {
	Sputnik::XenomaiLogger& logger = Sputnik::XenomaiLogger::getLogger();
	
	SystemPat_t replicaAPar = {.localPort = 3001, .remotePort = 3002, .name = "replica A"};
	SystemPat_t replicaBPar = {.localPort = 3002, .remotePort = 3001, .name = "replica B"};
	pthread_t replicaA, replicaB;
	
	pthread_attr_t replica_attr;
	pthread_attr_init(&replica_attr);
	pthread_attr_setstacksize(&replica_attr, 4096000);
	pthread_attr_setdetachstate(&replica_attr, PTHREAD_CREATE_JOINABLE);

	pthread_create(&replicaA, &replica_attr, replica, (void*) &replicaAPar);
	pthread_create(&replicaB, &replica_attr, replica, (void*) &replicaBPar);
	logger.print("main", "repliche create, in attesa del completamento...");
	
	pthread_join(replicaA, NULL);
	pthread_join(replicaB, NULL);

	return 0;
}

// *************************************************************************************************
// Thread che simula una delle due repliche
// 
void* replica(void* par) {
	Sputnik::XenomaiLogger& logger = Sputnik::XenomaiLogger::getLogger();
	
	SystemPat_t* replicaPar = (SystemPat_t*) par;
	logger.print(replicaPar->name, "avvio del thread");
	try {
				// definizione della safe-memory
		Sputnik::SafeMemory safeMemory;
		// creazione del communicator real-time
		Sputnik::UdpRtCommunicator communicator(replicaPar->localPort, "127.0.0.1", replicaPar->remotePort);
		// creazione del voter SHA-256
		Sputnik::SHA256Voter voter(communicator);
		// creazione del memory manager
		Sputnik::MemoryManager manager(safeMemory, voter, votingMismatchHandler, NULL);
		logger.print(replicaPar->name, "creazione di safe memory, communicator, voter e manager completata");
		// Allocazione delle variabili da usare nella memoria protetta.
		// Per come sono allocati i vettori, ogni scrittura su Re_Res sporca il chunk dove risiede
		// anche Re_B, bloccando il task in attesa del voting. La stessa situazione si ripresenta
		// con Im_B e Im_Res.
		// N.B. È buona norma allocare i chunk su cui vengono effettuare operazioni di lettura i
		// chunk dove non ci siano dati da leggere.
		Sputnik::Pointer<Vector> Re_A = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> Re_B = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> Im_A = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> Im_B = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> Re_Res = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> Im_Res = manager.reserve<Vector>();

		logger.print(replicaPar->name, "allocazione oggetti in memoria completata");
		

		// il mutex viene usato per impedire che "sense" modifichi i dati mentre "compute" sta
		// eseguendo il calcolo.
		Sputnik::XenomaiMutex mutex;
		Sputnik::XenomaiConditionVariable cv;
		logger.print(replicaPar->name, "creazione mutex e c.v. per sense e compute completata");
		
		// inizializzazione dei parametri da passare ai task di sense & compute
		senseAndComputeTaskPar_t parameters;

		parameters.manager = &manager;
		parameters.Re_A = &Re_A;
		parameters.Re_B = &Re_B;
		parameters.Re_Res = &Re_Res;
		parameters.Im_A = &Im_A;
		parameters.Im_B = &Im_B;
		parameters.Im_Res = &Im_Res;
				
		parameters.name = replicaPar->name;
		parameters.sensed = false;
		parameters.mutex = &mutex;
		parameters.cv = &cv;

		logger.print(replicaPar->name, "impostazione parametri sense e compute completata");
		
		// creazione dei thread di sense and compute
		pthread_attr_t sense_attr, compute_attr;
		pthread_t senseThread, computeThread;
		
		// setting attributi e scheduling
		pthread_attr_init(&sense_attr);
		pthread_attr_setstacksize(&sense_attr, VOTER_THREAD_STACK_SIZE);
		pthread_attr_setinheritsched(&sense_attr, PTHREAD_EXPLICIT_SCHED);
		pthread_attr_setschedpolicy(&sense_attr, SCHED_FIFO);
		pthread_attr_setdetachstate(&sense_attr, PTHREAD_CREATE_JOINABLE);
		struct sched_param sense_schedpar = {.sched_priority = 80};
		pthread_attr_setschedparam(&sense_attr, &sense_schedpar);

		pthread_attr_init(&compute_attr);
		pthread_attr_setstacksize(&compute_attr, VOTER_THREAD_STACK_SIZE);
		pthread_attr_setinheritsched(&compute_attr, PTHREAD_EXPLICIT_SCHED);
		pthread_attr_setschedpolicy(&compute_attr, SCHED_FIFO);
		pthread_attr_setdetachstate(&compute_attr, PTHREAD_CREATE_JOINABLE);
		struct sched_param compute_schedpar = {.sched_priority = 80};
		pthread_attr_setschedparam(&compute_attr, &compute_schedpar);
		
		logger.print(replicaPar->name, "creazione attributi thread di sense and compute completata");
		
		pthread_create(&computeThread, &compute_attr, compute, (void*)&parameters);
		pthread_create(&senseThread, &sense_attr, sense, (void*)&parameters);
		pthread_setname_np(senseThread, "Sense Thread");
		pthread_setname_np(computeThread, "Compute Thread");
		
		logger.print(replicaPar->name, "creazione thread di sense e compute completata");
		
		pthread_join(senseThread, NULL);
		pthread_join(computeThread, NULL);

	}
	catch (Sputnik::CommError& e) {
		std::cout << replicaPar->name <<"\t" << e.what() << std::endl;
		assert(false && "Non deve verificarsi nessuna eccezione Sputnik::CommError");
	}
	catch (Sputnik::BadAlloc& e) {
		logger.print(replicaPar->name, "Bad Alloc");
		assert(false && "Non deve verificarsi nessuna eccezione Sputnik::BadAlloc");
	}

	return 0;
}

// *************************************************************************************************
// task di computing
// attende che avvenga il "sensing" da parte del thread sense
// effettua il prodotto tra due vettori di numeri complessi
// scrive il risultato
// resetta il flag sensed
void* compute(void* par) {
	// Configurazione delle modalita' con cui il task periodico verra' risvegliato
	// viene usato il segnale SIGALRM, spedito al thread a seconda del suo identificativo.
	struct sigevent sevent;
	sigset_t set;
	int signum = SIGALRM;
	sevent.sigev_notify = SIGEV_THREAD_ID;
	sevent.sigev_notify_thread_id = syscall(__NR_gettid);
	sevent.sigev_signo = signum;
	sigemptyset(&set);
	sigaddset(&set, signum);
	sigprocmask(SIG_BLOCK, &set, NULL);
	
	// Creazione del timer.
	// Viene creato un timer periodico, di periodo 1s e ritardo iniziale 1s per rendere periodico
	// il task.
	timer_t timer;	
	struct itimerspec new_value, old_value;
	// creazione del timer
	timer_create (CLOCK_MONOTONIC, &sevent, &timer);
	new_value.it_interval.tv_sec = 1;	// periodo (secondi)
	new_value.it_interval.tv_nsec = 0;	// periodo (nanosecondi)
	new_value.it_value.tv_sec = 1;		// ritardi di inizio conteggio (secondi)
	new_value.it_value.tv_nsec = 0;		// ritardo di inizio conteggio (nanosecondi)
	// set delle caratteristiche del timer
	timer_settime (timer, 0, &new_value, &old_value);
	
	
	Sputnik::XenomaiLogger& logger = Sputnik::XenomaiLogger::getLogger();
	
	senseAndComputeTaskPar_t* myPar = (senseAndComputeTaskPar_t*) par;
	std::string thread_name = std::string(myPar->name) + " compute";
	std::string ciclo = "ciclo ";
	for (int i = 0; i < CICLI_SENSING; i++) {
		logger.print(thread_name, ciclo + std::to_string(i));
		// aspetto il prossimo periodo
		// viene effettuata una lettura sul timer periodico utilizzandone il descrittore
		// la lettura causa il blocco del thread chiamante fin quando il timer non genererà
		// l'interruzione, ad ogni periodo, capace di risvegliare il thread.
		long unsigned ticks;
		
		if (sigwait (&set, &signum) == -1)
			perror ("sigwait");
		logger.print(thread_name, "compute risvegliato dal timer");
		ticks = timer_getoverrun (timer);
		if (ticks > 0)
			logger.print(thread_name, "deadline miss! Ticks=" + std::to_string(ticks));


		// attendo che avvenga il sensing
		myPar->mutex->lock();
		while (!myPar->sensed) {
			logger.print(thread_name, "sensed e' false");
			logger.print(thread_name, "attendo che avvenga il sensing");
			myPar->cv->wait(*(myPar->mutex));
		}
		logger.print(thread_name, "sensed e' true; procedo con computing");

		// computing
		// quando il sensing è avvenuto, ed il task compute risvegliato, la riacquisizione del mutex
		// già avvenuta e si può procedere al calcolo.
		// da qualche parte, il thread compute deve bloccarsi in attesa del voting!
		try {
			for (unsigned i = 0; i < N; i++) {
				myPar->Re_Res->write().element[i] 	= myPar->Re_A->read().element[i] * myPar->Re_B->read().element[i]
													- myPar->Im_A->read().element[i] * myPar->Im_B->read().element[i];

				myPar->Im_Res->write().element[i]	= myPar->Re_A->read().element[i] * myPar->Im_B->read().element[i]
													+ myPar->Im_A->read().element[i] * myPar->Im_B->read().element[i];
			}
		} catch (Sputnik::VoteMismatch& e) {
			logger.print(thread_name, "Vote Mismatch");
		}
		
		myPar->sensed = false;

		logger.print(thread_name, "termine computing, signaling");
		// signal su c.v. ed unlock del mutex
		myPar->cv->broadcast();
		myPar->mutex->unlock();
	}
	
	return 0;
}

// *************************************************************************************************
// task di sensing
// scrive sui dati di origine
// setta il flag sensed ed effettua una signal, in modo che il thread "compute" venga risvegliato ed
// esegua i calcoli
void* sense(void* par) {
	// Configurazione delle modalita' con cui il task periodico verra' risvegliato
	// viene usato il segnale SIGALRM, spedito al thread a seconda del suo identificativo.
	struct sigevent sevent;
	sigset_t set;
	int signum = SIGALRM;
	sevent.sigev_notify = SIGEV_THREAD_ID;
	sevent.sigev_notify_thread_id = syscall(__NR_gettid);
	sevent.sigev_signo = signum;
	sigemptyset(&set);
	sigaddset(&set, signum);
	sigprocmask(SIG_BLOCK, &set, NULL);
	
	// Creazione del timer.
	// Viene creato un timer periodico, di periodo 1s e ritardo iniziale 1s per rendere periodico
	// il task.
	timer_t timer;	
	struct itimerspec new_value, old_value;
	// creazione del timer
	timer_create (CLOCK_MONOTONIC, &sevent, &timer);
	new_value.it_interval.tv_sec = 1;	// periodo (secondi)
	new_value.it_interval.tv_nsec = 0;	// periodo (nanosecondi)
	new_value.it_value.tv_sec = 1;		// ritardi di inizio conteggio (secondi)
	new_value.it_value.tv_nsec = 0;		// ritardo di inizio conteggio (nanosecondi)
	// set delle caratteristiche del timer
	timer_settime (timer, 0, &new_value, &old_value);
	
	Sputnik::XenomaiLogger& logger = Sputnik::XenomaiLogger::getLogger();
	
	senseAndComputeTaskPar_t* myPar = (senseAndComputeTaskPar_t*) par;
	std::string thread_name = std::string(myPar->name) + " sense";
	std::string ciclo = "ciclo ";
	for (int i = 0; i < CICLI_SENSING; i++) {
		logger.print(thread_name, ciclo + std::to_string(i));
		// aspetto il prossimo periodo
		// viene effettuata una lettura sul timer periodico utilizzandone il descrittore
		// la lettura causa il blocco del thread chiamante fin quando il timer non genererà
		// l'interruzione, ad ogni periodo, capace di risvegliare il thread.
		long unsigned ticks;
		
		if (sigwait (&set, &signum) == -1)
			perror ("sigwait");
		logger.print(thread_name, "compute risvegliato dal timer");
		ticks = timer_getoverrun (timer);
		if (ticks > 0)
			logger.print(thread_name, "deadline miss! Ticks=" + std::to_string(ticks));

		// attendo che sia avvenuto il computing
		myPar->mutex->lock();
		while (myPar->sensed) {
			logger.print(thread_name, "sensed e' true; attendo che avvenga il computing");
			myPar->cv->wait(*(myPar->mutex));
		}
		logger.print(thread_name, "sensed e' false; procedo con il sensing");

		// sensing
		for (unsigned i = 0; i < N; i++) {
			myPar->Re_A->write().element[i] = i*3;
			myPar->Re_B->write().element[i] = i*7;
			myPar->Im_A->write().element[i] = i*11;
			myPar->Im_A->write().element[i] = i*13;
		}

		logger.print(thread_name, "sensing completato, signaling");
		
		myPar->sensed = true;
		// signal su c.v. ed unlock del mutex
		myPar->cv->broadcast();
		myPar->mutex->unlock();
	}
	
	return 0;
}

