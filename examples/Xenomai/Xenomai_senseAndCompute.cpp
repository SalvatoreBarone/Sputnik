/**
 * @file Xenomai_senseAndCompute.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 * 
 * Copyright 2018 Salvatore Barone <salvator.barone@gmail.com>
 *
 * This file is part of Sputnik.
 *
 * Sputnik is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * Sputnik is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Sputnik; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 * 
 * @example Xenomai_senseAndCompute.cpp
 * Semplice esempio d'uso dei communicator SPI per Xenomai.
 * 
 * Ogni replica esegue crea due thread, uno di sensing e l'altro di computing, periodici e
 * sincronizzati tra loro. Ciascuno dei task di computing attende che sia avvenuto il sensing da
 * parte dei thread di sensing appartenente alla sua stessa "replica", mentre il task di sensing
 * attende che siano stati "consumati" i valori sensiti, da parte del task di computing.
 * 
 * Entrambi i thread eseguono un numero di cicli di sensing e comptuting pari alla costante
 * CICLI_SENSING, poi terminano ed il controllo ritorna al main, che, nel frattempo, viene messo in
 * attesa del completamento delle operazioni di sensing e computing.
 */
#include "Sputnik.hpp"
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/syscall.h>
#include <sys/mman.h>
#include <inttypes.h>
#include <iostream>

/*
 ***************************************************************************************************
 * Insieme di indirizzi e funzioni che agiscono du led, switch e button on-board
 */
#define 	LED_ADDR	0x41200000U		// Indirizzo fisico device AXI-GPIO per led
#define 	BTN_ADDR	0x41210000U		// Indirizzo fisico device AXI-GPIO per push-button
#define 	SWH_ADDR	0x41220000U		// Indirizzo fisico device AXI-GPIO per switch
uint32_t 	*led_vaddr 	= NULL;			// Indirizzo virtuale device AXI-GPIO per led
uint32_t	*btn_vaddr 	= NULL;			// Indirizzo virtuale device AXI-GPIO per push-button
uint32_t 	*swh_vaddr 	= NULL;			// Indirizzo virtuale device AXI-GPIO per switch
#define 	DATA_REG1	0				// indice del registro DATA per device AXI-GPIO, ch1
#define 	MODE_REG1	1				// indice del registro MODE per device AXI-GPIO, ch1
#define 	DATA_REG2	2				// indice del registro DATA per device AXI-GPIO, ch2
#define 	MODE_REG2	3				// indice del registro MODE per device AXI-GPIO, ch2

void driveLed(uint32_t mask) {
	led_vaddr[MODE_REG1] = 0x0;
	led_vaddr[DATA_REG1] = mask;
}

uint32_t readBtn() {
	btn_vaddr[MODE_REG1] = 0xf;
	return btn_vaddr[DATA_REG1];
}

uint32_t readSwh() {
	swh_vaddr[MODE_REG1] = 0xf;
	return swh_vaddr[DATA_REG1];
}
/**************************************************************************************************/

// Dimensione dello stack dei thread
#define VOTER_THREAD_STACK_SIZE 4096
// Numero di cicli
#define CICLI_SENSING 100
// Dimensioni dei vettori
#define N 1280 // 1280 * 4 = 5120 BYTE
typedef struct {
	int element[N];
} Vector;

// Funzione eseguita dal thread che simula una delle due repliche
void* replica(void*);
// Ogni replica crea due thread periodici:
// - task di sensing: simula il "sensing", creando dati di origine, poi setta il flag sensed ed
//   effettua una signal, in modo che il thread "compute" venga risvegliato ed esegua i calcoli
// - task di computing, che attende che avvenga il "sensing" da parte del thread sense, effettua il
//   prodotto tra due vettori di numeri complessi, scrive il risultato e resetta il flag "sensed"
void* sense(void*);
void* compute(void*);

void votingMismatchHandler(void*) {
	Sputnik::XenomaiLogger& logger = Sputnik::XenomaiLogger::getLogger();
	logger.print("Vote mismatch");
	driveLed(0xf);
	abort();
}

// handler chiamato quando assert fallisce
void abortHandler(int) {
	driveLed(0xf);
}

// definizione della safe-memory usata dalla replica
Sputnik::SafeMemory safeMemory;

// Struttura dati usata per il passaggio dei parametri ai task di sense e computing
typedef struct {
	// puntatore al manager che gestisce la memoria della replica
	Sputnik::MemoryManager* manager;
	// puntatore ai dati allocati in memoria sicura
	Sputnik::Pointer<Vector>* Re_A;
	Sputnik::Pointer<Vector>* Re_B;
	Sputnik::Pointer<Vector>* Re_Res;
	Sputnik::Pointer<Vector>* Im_A;
	Sputnik::Pointer<Vector>* Im_B;
	Sputnik::Pointer<Vector>* Im_Res;
	
	// il mutex viene usato per impedire che "sense" modifichi i dati mentre "compute" sta eseguendo
	// il calcolo.
	Sputnik::XenomaiMutex* mutex;
	// se non è stato eseguito il sensing, il thread "compute" va bloccato
	bool sensed;
	// la c.v. viene usata per bloccare "compute" fino a quando non viene effettuato il sensing
	Sputnik::XenomaiConditionVariable* cv;
} senseAndComputeTaskPar_t;

// *************************************************************************************************
// Main
int main() {
	signal(SIGABRT, abortHandler);
	
/*
 ***************************************************************************************************
 * Mapping AXI-GPIO con indirizzi virtuali
 */
	int descr = 0;									// descrittore del file /dev/mem
	uint32_t page_size = sysconf(_SC_PAGESIZE);		// dimensione della pagina
	uint32_t page_mask = ~(page_size-1);			// maschera di conversione indirizzo -> indirizzo pagina
	uint32_t page_addr = 0;							// indirizzo della "pagina fisica" a cui è mappato il device
	uint32_t offset = 0;							// offset indirizzo pagina - indirizzo device
	descr = open ("/dev/mem", O_RDWR);
	if (descr < 1) {
		std::cout << "Impossibile aprire /dev/mem" << std::endl;
		return -1;
	}
	
	// mapping AXI-GPIO per led
	page_addr = LED_ADDR & page_mask;
	offset = LED_ADDR - page_addr;
	led_vaddr = (uint32_t*) mmap(NULL, page_size, PROT_READ | PROT_WRITE, MAP_SHARED, descr, page_addr);
	if (led_vaddr == MAP_FAILED) {
		std::cout << "Mapping indirizzo fisico - indirizzo virtuale FALLITO per led!" << std::endl;
		close(descr);
		return -1;
	}
	led_vaddr += (offset >> 2);
	
	// mapping AXI-GPIO per push-button
	page_addr = BTN_ADDR & page_mask;
	offset = BTN_ADDR - page_addr;
	btn_vaddr = (uint32_t*) mmap(NULL, page_size, PROT_READ | PROT_WRITE, MAP_SHARED, descr, page_addr);
	if (btn_vaddr == MAP_FAILED) {
		std::cout << "Mapping indirizzo fisico - indirizzo virtuale FALLITO per btn!" << std::endl;
		close(descr);
		return -1;
	}
	btn_vaddr += (offset >> 2);
	
	// mapping AXI-GPIO per switch
	page_addr = SWH_ADDR & page_mask;
	offset = SWH_ADDR - page_addr;
	swh_vaddr = (uint32_t*) mmap(NULL, page_size, PROT_READ | PROT_WRITE, MAP_SHARED, descr, page_addr);
	if (swh_vaddr == MAP_FAILED) {
		std::cout << "Mapping indirizzo fisico - indirizzo virtuale FALLITO!" << std::endl;
		close(descr);
		return -1;
	}
	swh_vaddr += (offset >> 2);
	driveLed(0x0);
/**************************************************************************************************/

	// creazione del thread "replica"
	pthread_attr_t replica_attr;
	pthread_t replicaThread;
	
	// setting attributi e scheduling
	pthread_attr_init(&replica_attr);
	pthread_attr_setstacksize(&replica_attr, 1<<24); // stack size  64K
	pthread_attr_setinheritsched(&replica_attr, PTHREAD_EXPLICIT_SCHED);
	pthread_attr_setschedpolicy(&replica_attr, SCHED_FIFO);
	pthread_attr_setdetachstate(&replica_attr, PTHREAD_CREATE_JOINABLE);
	struct sched_param replica_schedpar = {.sched_priority = 80};
	pthread_attr_setschedparam(&replica_attr, &replica_schedpar);
	
	pthread_create(&replicaThread, &replica_attr, replica, NULL);
	pthread_join(replicaThread, NULL);
	
	return 0;
}

// *************************************************************************************************
// Funzione eseguita dal thread che simula una delle due repliche
void* replica(void*) {
	Sputnik::XenomaiLogger& logger = Sputnik::XenomaiLogger::getLogger();
	
	try {
		
		// creazione del communicator real-time SPI
		Sputnik::DualSPI communicator(
					Sputnik::SPI::SPI0,
					Sputnik::SPI::SPI1,
					Sputnik::SPI::slave1,
					Sputnik::SPI::SCKLPolarityHigh,
					Sputnik::SPI::sampleOnFirstEdge,
					Sputnik::SPI::prescaler32,
					Sputnik::SPI::manualChipSelectEnable,
					Sputnik::SPI::manualStartEnable);
		
		// creazione del voter SHA-256
		Sputnik::SHA256Voter voter(communicator);
		// creazione del memory manager
		Sputnik::MemoryManager manager(safeMemory, voter, votingMismatchHandler, NULL);
		logger.print("replica", "creazione di safe memory, communicator, voter e manager completata");
		// Allocazione delle variabili da usare nella memoria protetta.
		// Per come sono allocati i vettori, ogni scrittura su Re_Res sporca il chunk dove risiede
		// anche Re_B, bloccando il task in attesa del voting. La stessa situazione si ripresenta
		// con Im_B e Im_Res.
		// N.B. È buona norma allocare i chunk su cui vengono effettuare operazioni di lettura i
		// chunk dove non ci siano dati da leggere.
		Sputnik::Pointer<Vector> Re_A = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> Re_B = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> Im_A = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> Im_B = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> Re_Res = manager.reserve<Vector>();
		Sputnik::Pointer<Vector> Im_Res = manager.reserve<Vector>();

		logger.print("replica", "allocazione oggetti in memoria completata");

		// il mutex viene usato per impedire che "sense" modifichi i dati mentre "compute" sta
		// eseguendo il calcolo.
		Sputnik::XenomaiMutex mutex;
		Sputnik::XenomaiConditionVariable cv;
		logger.print("replica", "creazione mutex e c.v. per sense e compute completata");
		
		// inizializzazione dei parametri da passare ai task di sense & compute
		senseAndComputeTaskPar_t parameters;

		parameters.manager = &manager;
		parameters.Re_A = &Re_A;
		parameters.Re_B = &Re_B;
		parameters.Re_Res = &Re_Res;
		parameters.Im_A = &Im_A;
		parameters.Im_B = &Im_B;
		parameters.Im_Res = &Im_Res;
		parameters.sensed = false;
		parameters.mutex = &mutex;
		parameters.cv = &cv;

		logger.print("replica", "impostazione parametri sense e compute completata");
		
		// creazione dei thread di sense and compute
		pthread_attr_t sense_attr, compute_attr;
		pthread_t senseThread, computeThread;
		
		// setting attributi e scheduling
		pthread_attr_init(&sense_attr);
		pthread_attr_setstacksize(&sense_attr, 1<<16); // stack size  64K
		pthread_attr_setinheritsched(&sense_attr, PTHREAD_EXPLICIT_SCHED);
		pthread_attr_setschedpolicy(&sense_attr, SCHED_FIFO);
		pthread_attr_setdetachstate(&sense_attr, PTHREAD_CREATE_JOINABLE);
		struct sched_param sense_schedpar = {.sched_priority = 80};
		pthread_attr_setschedparam(&sense_attr, &sense_schedpar);

		pthread_attr_init(&compute_attr);
		pthread_attr_setstacksize(&compute_attr, 1<<16); // stack size  64K
		pthread_attr_setinheritsched(&compute_attr, PTHREAD_EXPLICIT_SCHED);
		pthread_attr_setschedpolicy(&compute_attr, SCHED_FIFO);
		pthread_attr_setdetachstate(&compute_attr, PTHREAD_CREATE_JOINABLE);
		struct sched_param compute_schedpar = {.sched_priority = 80};
		pthread_attr_setschedparam(&compute_attr, &compute_schedpar);
		
		logger.print("replica", "creazione attributi thread di sense and compute completata");
		
		pthread_create(&computeThread, &compute_attr, compute, (void*)&parameters);
		pthread_create(&senseThread, &sense_attr, sense, (void*)&parameters);
		pthread_setname_np(senseThread, "Sense Thread");
		pthread_setname_np(computeThread, "Compute Thread");
		
		logger.print("replica", "creazione thread di sense e compute completata");
		
		pthread_join(senseThread, NULL);
		pthread_join(computeThread, NULL);

	}
	catch (Sputnik::CommError& e) {
		std::cout << "replica" <<"\t" << e.what() << std::endl;
		assert(false && "Non deve verificarsi nessuna eccezione Sputnik::CommError");
	}
	catch (Sputnik::BadAlloc& e) {
		std::cout << "replica: Bad Alloc" << std::endl;
		assert(false && "Non deve verificarsi nessuna eccezione Sputnik::BadAlloc");
	}
	
	return 0;
}


// *************************************************************************************************
// task di computing
// attende che avvenga il "sensing" da parte del thread sense
// effettua il prodotto tra due vettori di numeri complessi
// scrive il risultato
// resetta il flag sensed
void* compute(void* par) {
	// Configurazione delle modalita' con cui il task periodico verra' risvegliato
	// viene usato il segnale SIGALRM, spedito al thread a seconda del suo identificativo.
	struct sigevent sevent;
	sigset_t set;
	int signum = SIGALRM;
	sevent.sigev_notify = SIGEV_THREAD_ID;
	sevent.sigev_notify_thread_id = syscall(__NR_gettid);
	sevent.sigev_signo = signum;
	sigemptyset(&set);
	sigaddset(&set, signum);
	sigprocmask(SIG_BLOCK, &set, NULL);
	
	// Creazione del timer.
	// Viene creato un timer periodico, di periodo 1s e ritardo iniziale 1s per rendere periodico
	// il task.
	timer_t timer;	
	struct itimerspec new_value, old_value;
	// creazione del timer
	timer_create (CLOCK_MONOTONIC, &sevent, &timer);
	new_value.it_interval.tv_sec = 1;	// periodo (secondi)
	new_value.it_interval.tv_nsec = 0;	// periodo (nanosecondi)
	new_value.it_value.tv_sec = 1;		// ritardi di inizio conteggio (secondi)
	new_value.it_value.tv_nsec = 0;		// ritardo di inizio conteggio (nanosecondi)
	// set delle caratteristiche del timer
	timer_settime (timer, 0, &new_value, &old_value);
	
	
	Sputnik::XenomaiLogger& logger = Sputnik::XenomaiLogger::getLogger();
	
	senseAndComputeTaskPar_t* myPar = (senseAndComputeTaskPar_t*) par;
	std::string thread_name = "compute";
	std::string ciclo = "ciclo ";
	for (int i = 0; i < CICLI_SENSING; i++) {
		logger.print(thread_name, ciclo + std::to_string(i));
		// aspetto il prossimo periodo
		// viene effettuata una lettura sul timer periodico utilizzandone il descrittore
		// la lettura causa il blocco del thread chiamante fin quando il timer non genererà
		// l'interruzione, ad ogni periodo, capace di risvegliare il thread.
		long unsigned ticks;
		
		if (sigwait (&set, &signum) == -1)
			perror ("sigwait");
		ticks = timer_getoverrun (timer);
		logger.print(thread_name, "compute risvegliato dal timer");
		if (ticks > 0)
			logger.print(thread_name, "deadline miss! Ticks=" + std::to_string(ticks));

		// attendo che avvenga il sensing
		myPar->mutex->lock();
		while (!myPar->sensed) {
			logger.print(thread_name, "sensed e' false");
			logger.print(thread_name, "attendo che avvenga il sensing");
			myPar->cv->wait(*(myPar->mutex));
		}
		logger.print(thread_name, "sensed e' true; procedo con computing");

		// computing
		// quando il sensing è avvenuto, ed il task compute risvegliato, la riacquisizione del mutex
		// già avvenuta e si può procedere al calcolo.
		// da qualche parte, il thread compute deve bloccarsi in attesa del voting!
		Vector re, im; 	// variabili temporanee dove vengono provvisoriamente salvati i risultati
		try {
			// calcolo del risultato
			for (unsigned i = 0; i < N; i++) {
				re.element[i] 	= myPar->Re_A->read().element[i] * myPar->Re_B->read().element[i]
								- myPar->Im_A->read().element[i] * myPar->Im_B->read().element[i];

				im.element[i]	= myPar->Re_A->read().element[i] * myPar->Im_B->read().element[i]
								+ myPar->Im_A->read().element[i] * myPar->Im_B->read().element[i];
			}
			
			// copia del risultato
			for (unsigned i = 0; i < N; i++) {
				myPar->Re_Res->write().element[i] 	= re.element[i];
				myPar->Im_Res->write().element[i]	= im.element[i];
			}
		} catch (Sputnik::VoteMismatch& e) {
			logger.print(thread_name, "Vote Mismatch");
		}
		
		myPar->sensed = false;

		logger.print(thread_name, "termine computing, signaling");
		// signal su c.v. ed unlock del mutex
		myPar->cv->broadcast();
		myPar->mutex->unlock();

		struct itimerspec expiration;
		timer_gettime(timer, &expiration);
		logger.print(thread_name, "expiration:" + std::to_string(expiration.it_value.tv_sec) +  " sec, " + std::to_string(expiration.it_value.tv_nsec) +  "nsec");
	}
	
	return 0;
}

// *************************************************************************************************
// task di sensing
// scrive sui dati di origine
// setta il flag sensed ed effettua una signal, in modo che il thread "compute" venga risvegliato ed
// esegua i calcoli
void* sense(void* par) {
	// Configurazione delle modalita' con cui il task periodico verra' risvegliato
	// viene usato il segnale SIGALRM, spedito al thread a seconda del suo identificativo.
	struct sigevent sevent;
	sigset_t set;
	int signum = SIGALRM;
	sevent.sigev_notify = SIGEV_THREAD_ID;
	sevent.sigev_notify_thread_id = syscall(__NR_gettid);
	sevent.sigev_signo = signum;
	sigemptyset(&set);
	sigaddset(&set, signum);
	sigprocmask(SIG_BLOCK, &set, NULL);
	
	// Creazione del timer.
	// Viene creato un timer periodico, di periodo 1s e ritardo iniziale 1s per rendere periodico
	// il task.
	timer_t timer;	
	struct itimerspec new_value, old_value;
	// creazione del timer
	timer_create (CLOCK_MONOTONIC, &sevent, &timer);
	new_value.it_interval.tv_sec = 1;	// periodo (secondi)
	new_value.it_interval.tv_nsec = 0;	// periodo (nanosecondi)
	new_value.it_value.tv_sec = 1;		// ritardi di inizio conteggio (secondi)
	new_value.it_value.tv_nsec = 0;		// ritardo di inizio conteggio (nanosecondi)
	// set delle caratteristiche del timer
	timer_settime (timer, 0, &new_value, &old_value);
	
	Sputnik::XenomaiLogger& logger = Sputnik::XenomaiLogger::getLogger();
	
	senseAndComputeTaskPar_t* myPar = (senseAndComputeTaskPar_t*) par;
	std::string thread_name = "sense";
	std::string ciclo = "ciclo ";
	for (int i = 0; i < CICLI_SENSING; i++) {
		logger.print(thread_name, ciclo + std::to_string(i));
		// aspetto il prossimo periodo
		// viene effettuata una lettura sul timer periodico utilizzandone il descrittore
		// la lettura causa il blocco del thread chiamante fin quando il timer non genererà
		// l'interruzione, ad ogni periodo, capace di risvegliare il thread.
		long unsigned ticks;
		
		if (sigwait (&set, &signum) == -1)
			perror ("sigwait");
		logger.print(thread_name, "compute risvegliato dal timer");
		ticks = timer_getoverrun (timer);
		if (ticks > 0)
			logger.print(thread_name, "deadline miss! Ticks=" + std::to_string(ticks));

		// attendo che sia avvenuto il computing
		myPar->mutex->lock();
		while (myPar->sensed) {
			logger.print(thread_name, "sensed e' true; attendo che avvenga il computing");
			myPar->cv->wait(*(myPar->mutex));
		}
		logger.print(thread_name, "sensed e' false; procedo con il sensing");

		// sensing
		for (unsigned i = 0; i < N; i++) {
			myPar->Re_A->write().element[i] = i*3;
			myPar->Re_B->write().element[i] = i*7;
			myPar->Im_A->write().element[i] = i*11;
			myPar->Im_A->write().element[i] = i*13;
		}

		logger.print(thread_name, "sensing completato, signaling");
		
		myPar->sensed = true;
		// signal su c.v. ed unlock del mutex
		myPar->cv->broadcast();
		myPar->mutex->unlock();
		
		struct itimerspec expiration;
		timer_gettime(timer, &expiration);
		logger.print(thread_name, "expiration:" + std::to_string(expiration.it_value.tv_sec) +  " sec, " + std::to_string(expiration.it_value.tv_nsec) +  "nsec");
	}
	
	return 0;
}

