#!/bin/bash
# ATTENZIONE!
# Imposta correttamente il valore dei seguenti parametri prima di lanciare lo script!
VIVADO_PATH=$HOME/Vivado/Vivado
VIVADO_VERS=2017.4
SDK_PATH=$HOME/Vivado/SDK
SDK_VERS=2017.4
ROOT_FS=/media/$(whoami)/root
BOOT_FS=/media/$(whoami)/BOOT


WORKING_DIR=$PWD
LINARO_GZ=$WORKING_DIR/linaro.tar.gz
LINARO_BINARY=$WORKING_DIR/binary

# preparazione console
source $VIVADO_PATH/$VIVADO_VERS/settings64.sh
source $SDK_PATH/$SDK_VERS/settings64.sh
export ARCH=arm
export CROSS_COMPILE=arm-linux-gnueabihf-
export PATH=$WORKING_DIR/u-boot-xlnx/tools/:$PATH

# Download del software
git clone https://github.com/Xilinx/u-boot-xlnx.git
git clone https://github.com/Xilinx/linux-xlnx.git 
git clone https://github.com/Xilinx/device-tree-xlnx
wget -O $LINARO_GZ https://releases.linaro.org/debian/images/developer-armhf/latest/linaro-stretch-developer-20170706-43.tar.gz

mkdir -p SDCard

#export dei percorsi directory
export UBOOT=$PWD/u-boot-xlnx
export $LINUX_HOME=$PWD/linux-xlnx
export SDCARD=$PWD/SDCard

# compilazione di u-boot
cd $UBOOT
editor include/configs/zynq-common.h 
make zynq_zybo_config
make -j 2 u-boot.elf
cp u-boot.elf $SDCARD
cd $WORKING_DIR

#compilazione kernel
cd $LINUX_HOME
make xilinx_zynq_defconfig
make menuconfig
make
make UIMAGE_LOADADDR=0x8000 uImage
cp $LINUX_HOME/arch/arm/boot/uImage $SDCARD

# preparazione del filesystem
cd $WORKING_DIRECTORY
sudo tar -xzf $LINARO_GZ
sudo rsync -aAXv $LINARO_BINARY/* $ROOT_FS
cp $SDCARD/* $BOOT_FS
