#!/bin/bash
# ATTENZIONE!
# Imposta correttamente il valore dei seguenti parametri prima di lanciare lo script!
export VIVADO_PATH=$HOME/Vivado/Vivado
export VIVADO_VERS=2017.4
export SDK_PATH=$HOME/Vivado/SDK
export SDK_VERS=2017.4
export ROOT_FS=/media/$(whoami)/root
export BOOT_FS=/media/$(whoami)/BOOT


export WORKING_DIR=$PWD
export LINARO_GZ=$WORKING_DIR/linaro.tar.gz
export LINARO_BINARY=$WORKING_DIR/binary

# preparazione console
source $VIVADO_PATH/$VIVADO_VERS/settings64.sh
source $SDK_PATH/$SDK_VERS/settings64.sh
export ARCH=arm
export CROSS_COMPILE=arm-linux-gnueabihf-
export PATH=$WORKING_DIR/u-boot-xlnx/tools/:$PATH

# Download del software
git clone https://github.com/Xilinx/u-boot-xlnx.git
git clone https://github.com/Xilinx/linux-xlnx.git 
git clone https://github.com/Xilinx/device-tree-xlnx
git clone git://git.xenomai.org/xenomai-3.git
git clone git://git.xenomai.org/ipipe-arm.git
wget -O $LINARO_GZ https://releases.linaro.org/debian/images/developer-armhf/latest/linaro-stretch-developer-20170706-43.tar.gz

# creazione delle directory
mkdir -p xeno3_build
mkdir -p xeno3_zynq_stage
mkdir -p zynq_modules
mkdir -p SDCard

#export dei percorsi directory
export UBOOT=$PWD/u-boot-xlnx
export XENO_HOME=$PWD/xenomai-3
export LINUX_HOME=$PWD/ipipe-arm
export XIL_LINUX=$PWD/linux-xlnx
export XENO_BUILD=$PWD/xeno3_build
export XENO_ZYNQ_STAGE=$PWD/xeno3_zynq_stage
export ZYNQ_MODULES=$PWD/zynq_modules
export SDCARD=$PWD/SDCard

# compilazione di u-boot
cd $UBOOT
editor include/configs/zynq-common.h 
make zynq_zybo_config
make -j 2 u-boot.elf
cp u-boot.elf $SDCARD
cd $WORKING_DIR

# patching del kernel
cp $XIL_LINUX/arch/arm/configs/xilinx_zynq_defconfig $LINUX_HOME/arch/arm/configs/
$XENO_HOME/scripts/prepare-kernel.sh --linux=$LINUX_HOME --arch=arm --verbose
cd $LINUX_HOME
make xilinx_zynq_defconfig
make menuconfig
make
make UIMAGE_LOADADDR=0x8000 uImage modules
make modules_install INSTALL_MOD_PATH=$ZYNQ_MODULES
cp $LINUX_HOME/arch/arm/boot/uImage $SDCARD

# compilazione della libreria XENOMAI
cd $XENO_HOME
git checkout tags/v3.0.6 -b xenomai_3.0.6
./scripts/bootstrap
cd $XENO_BUILD
$XENO_HOME/configure CFLAGS="-march=armv7-a -mfpu=vfp3 -mfloat-abi=hard" LDFLAGS="-march=armv7-a" --build=x86_64-pc-linux-gnu --host=arm-none-linux-gnueabi --with-core=cobalt --enable-smp --enable-tls CC=arm-linux-gnueabihf-gcc LD=arm-linux-gnueabihf-ld
make DESTDIR=$XENO_ZYNQ_STAGE install

# preparazione del filesystem
cd $WORKING_DIR
sudo tar -xzf $LINARO_GZ
sudo rsync -aAXv $LINARO_BINARY/* $ROOT_FS
sudo rsync -aAXv $XENO_ZYNQ_STAGE/* $ROOT_FS
sudo rsync -aAXv $ZYNQ_MODULES/* $ROOT_FS
sudo cp $SDCARD/* $BOOT_FS

